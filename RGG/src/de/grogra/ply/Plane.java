/*
  * Copyright (C) 2021 GroIMP Developer Team
  *
  * This program is free software; you can redistribute it and/or
  * modify it under the terms of the GNU General Public License
  * as published by the Free Software Foundation; either version 3
  * of the License, or any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 
02111-1307, USA.
  */

package de.grogra.ply;

import java.io.IOException;

import javax.vecmath.Matrix4d;
import javax.vecmath.Point4d;

import de.grogra.imp3d.objects.SceneTree.InnerNode;
import de.grogra.imp3d.objects.SceneTree.Leaf;
import de.grogra.pf.registry.Item;
import de.grogra.pf.ui.Workbench;
import de.grogra.util.Utils;

/**
 * Wrapper class. Part of PLYExport
 * 
 * http://en.wikipedia.org/wiki/PLY_(file_format)
 * 
 * MH 2020-01-18
 */

public class Plane extends ObjectBase {

	@Override
	public void exportImpl (Leaf node, InnerNode transform, PLYExport export)
			throws IOException
	{

		float w = 10;
		float l = 10;

		double x0 = -w/2d;
		double y0 = -l/2d;
		double x1 = w/2d;
		double y1 = l/2d;

		Matrix4d m = export.matrixStack.peek();
		Matrix4d n = new Matrix4d ();
		if (transform != null) {
			transform.transform (m, n);
		} else {
			n.set (m);
		}
		m = n;

		Point4d[] p = new Point4d[] {
				new Point4d (x0, y0, 0, 1), new Point4d (x0, y1, 0, 1),
				new Point4d (x1, y0, 0, 1), new Point4d (x1, y1, 0, 1)};

		for (int i = 0; i < p.length; i++) {
			m.transform (p[i]);
		}

		//generate colour string
		getColorString(node);
		
		Item general = Item.resolveItem (Workbench.current (), "/export/ply");
		
		// write object
		writeVertices(p);
		if (Utils.getBoolean(general, "planetwofaces")) {
			writeFacets(new String[] {
				//to make it visible from both sides: double the facets with opposite order
				"1 0 2", "1 2 3", //one side
				"1 2 0", "1 3 2", //other side
			});
		} else {
			writeFacets(new String[] {
					//only one side
					"1 0 2", "1 2 3", //one side
				});
		}
		
	}

}

