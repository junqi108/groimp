/*
  * Copyright (C) 2021 GroIMP Developer Team
  *
  * This program is free software; you can redistribute it and/or
  * modify it under the terms of the GNU General Public License
  * as published by the Free Software Foundation; either version 3
  * of the License, or any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 
02111-1307, USA.
  */

package de.grogra.ply;

import java.io.IOException;

import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;

import de.grogra.graph.ContextDependent;
import de.grogra.graph.GraphState;
import de.grogra.imp3d.PolygonArray;
import de.grogra.imp3d.objects.Attributes;
import de.grogra.imp3d.objects.SceneTree.InnerNode;
import de.grogra.imp3d.objects.SceneTree.Leaf;

/**
 * Wrapper class. Part of PLYExport
 * 
 * http://en.wikipedia.org/wiki/PLY_(file_format)
 * 
 * MH 2020-01-18
 */

public class Polygonizable extends ObjectBase {

	@Override
	void exportImpl (Leaf node, InnerNode transform, PLYExport export)
			throws IOException
	{
		Point3d pos = new Point3d ();
		Matrix4d m = getTransformation ();
//		m.transform (pos);
		float flatness = export.getMetaData (PLYExport.FLATNESS, 1f);
		int flags = 0;

		PolygonArray p = new PolygonArray ();
		
		// TODO why this line ?
		export.getGraphState ().setObjectContext (node.object, node.asNode);
		
		GraphState gs = export.getGraphState ();
		de.grogra.imp3d.Polygonizable surface = (de.grogra.imp3d.Polygonizable) node.getObject (Attributes.SHAPE);
		ContextDependent source = surface.getPolygonizableSource (gs);
		surface.getPolygonization ().polygonize (source, gs, p, flags, flatness);

		// transform the mesh
		for (int i = 0; i < p.vertices.size(); i+=3) {
			pos.x = p.vertices.get(i+0);
			pos.y = p.vertices.get(i+1);
			pos.z = p.vertices.get(i+2);
			m.transform(pos);
			p.vertices.set(i+0, (float)pos.x);
			p.vertices.set(i+1, (float)pos.y);
			p.vertices.set(i+2, (float)pos.z);
		}

		// write object
		mesh2(p, node, false);
	}

}
