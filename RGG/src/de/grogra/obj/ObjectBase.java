/*
  * Copyright (C) 2021 GroIMP Developer Team
  *
  * This program is free software; you can redistribute it and/or
  * modify it under the terms of the GNU General Public License
  * as published by the Free Software Foundation; either version 3
  * of the License, or any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 
02111-1307, USA.
  */

package de.grogra.obj;

import java.io.IOException;

import javax.vecmath.Matrix4d;
import javax.vecmath.Point4d;

import de.grogra.imp3d.PolygonArray;
import de.grogra.imp3d.io.SceneGraphExport;
import de.grogra.imp3d.objects.SceneTree.InnerNode;
import de.grogra.imp3d.objects.SceneTree.Leaf;
import de.grogra.imp3d.objects.SceneTreeWithShader;
import de.grogra.imp3d.shading.Phong;
import de.grogra.imp3d.shading.RGBAShader;
import de.grogra.imp3d.shading.Shader;
import de.grogra.math.ChannelMap;
import de.grogra.math.RGBColor;

/**
 * Wrapper class. Part of OBJExport
 * 
 * http://en.wikipedia.org/wiki/Wavefront_.obj_file
 * 
 * MH 2021-05-30
 */

public abstract class ObjectBase implements SceneGraphExport.NodeExport {

	Matrix4d transformation;
	StringBuffer outV, outF;
	OBJExport export;
	
	int[] tmpIdx;

	float r = 0;
	float g = 0;
	float b = 0;
	float t = 0;

	protected void getColorString(Leaf node) {
		r = 0;
		g = 0;
		b = 0;
		t = 0;
		
		// RGBA
		Shader shader = ((SceneTreeWithShader.Leaf)node).shader;
		if (shader instanceof RGBAShader) {
			RGBAShader rgba = (RGBAShader) shader;
			int i = rgba.getAverageColor();
			r = ((i >> 16) & 255)/255f;
			g = ((i >> 8) & 255)/255f;
			b = (i & 255)/255f;
			t = (i >>> 24)/255f;
		}
		if (shader instanceof Phong) {
			Phong phong = (Phong) shader;
			ChannelMap diffuseMap = phong.getDiffuse();
			RGBColor rgbDiffuse = new RGBColor(1,1,1);
			if (diffuseMap instanceof RGBColor) {
				rgbDiffuse = (RGBColor) diffuseMap;
			}
			ChannelMap transparencyMap = phong.getTransparency();
			RGBColor rgbTransparency = new RGBColor(0f,0f,0f);
			if (transparencyMap instanceof RGBColor) {
				rgbTransparency = (RGBColor) transparencyMap;
			}
			int i = rgbDiffuse.getAverageColor();
			r = ((i >> 16) & 255)/255f;
			g = ((i >> 8) & 255)/255f;
			b = (i & 255)/255f;
			i = rgbTransparency.getAverageColor();
			t = ((((i >> 16) & 255)+((i >> 8) & 255)+(i & 255))/3f /255f);
		}

	}
	
	protected void writeVertices(Point4d[] p) {
		tmpIdx = new int[p.length];
		for (int i = 0; i < p.length; i++) {
			outV.append("v "+round(p[i].x)+" "+round(p[i].z)+" "+round(p[i].y)+"\n");
			//outV.append("vt "+round()+" "+round()+" "+round()+"\n");
			//outV.append("vn "+round()+" "+round()+" "+round()+"\n");
			tmpIdx[i] = export.vertices;
			export.vertices++;
		}
	}

	protected void writeFacets(String[] f) {
		String[] s;
		for (int i = 0; i < f.length; i++) {
			s = f[i].split(" ");
//			outF.append("f");
//			outF.append(" "+tmpIdx[Integer.parseInt(s[0])]+"/"+tmpIdx[Integer.parseInt(s[0])]+"/");
//			outF.append(" "+tmpIdx[Integer.parseInt(s[2])]+"/"+tmpIdx[Integer.parseInt(s[2])]+"/");
//			outF.append(" "+tmpIdx[Integer.parseInt(s[1])]+"/"+tmpIdx[Integer.parseInt(s[1])]+"/"+"\n");
			outF.append("f "+tmpIdx[Integer.parseInt(s[0])]+" "+tmpIdx[Integer.parseInt(s[2])]+" "+tmpIdx[Integer.parseInt(s[1])]+"\n");
		}
		export.facets += f.length;
	}

	@Override
	public void export (Leaf node, InnerNode transform, SceneGraphExport sge) throws IOException {
		// convert to OBJExport
		export = (OBJExport) sge;
		
		// obtain output buffer
		outV = export.outV;
		outF = export.outF;

		// obtain transformation matrix for this node
		Matrix4d m = export.matrixStack.peek ();
		Matrix4d n = new Matrix4d ();
		if (transform != null) {
			transform.transform (m, n);
		} else {
			n.set (m);
		}
		transformation = n;
		export.primitives++;

		// call implementation export function
		exportImpl (node, transform, export);
	}

	abstract void exportImpl (Leaf node, InnerNode transform, OBJExport export) throws IOException;

	Matrix4d getTransformation () {
		return transformation;
	}

	// round to 5 decimal digits
	static double round (double d) {
		return (double) Math.round (d * 100000) / 100000;
	}

	void mesh2 (PolygonArray p, Leaf node, boolean twoF) {

		//generate colour string
		getColorString(node);
		
		if (p.polygons.size == 0) return;
		int v1,v2,v3;
		if (p.edgeCount==3) {
			// output indices
			for (int i = 0; i < p.polygons.size (); i += p.edgeCount) {
				v1 = p.polygons.get (i + 0);
				v2 = p.polygons.get (i + 1);
				v3 = p.polygons.get (i + 2);
				
				//first facet of the rectangle
				Point4d[] pA = new Point4d[] {
					new Point4d(p.vertices.get(v1*p.dimension+0),p.vertices.get(v1*p.dimension+1),p.vertices.get(v1*p.dimension+2),1),
					new Point4d(p.vertices.get(v2*p.dimension+0),p.vertices.get(v2*p.dimension+1),p.vertices.get(v2*p.dimension+2),1),
					new Point4d(p.vertices.get(v3*p.dimension+0),p.vertices.get(v3*p.dimension+1),p.vertices.get(v3*p.dimension+2),1)
				};
				if (checkArea(pA)) {
					writeVertices(pA);
					writeFacets(new String[] {"0 1 2"});
				}
				
				if(twoF) {
					//second facet of the rectangle
					pA = new Point4d[] {
						new Point4d(p.vertices.get(v1*p.dimension+0),p.vertices.get(v1*p.dimension+1),p.vertices.get(v1*p.dimension+2),1),
						new Point4d(p.vertices.get(v3*p.dimension+0),p.vertices.get(v3*p.dimension+1),p.vertices.get(v3*p.dimension+2),1),
						new Point4d(p.vertices.get(v2*p.dimension+0),p.vertices.get(v2*p.dimension+1),p.vertices.get(v2*p.dimension+2),1)
					};
					if (checkArea(pA)) {
						writeVertices(pA);
						writeFacets(new String[] {"0 1 2"});
					}
				}
			}
		}

		if (p.edgeCount==4) {
			int v4;
			// output indices
			for (int i = 0; i < p.polygons.size (); i += p.edgeCount) {
				v1 = p.polygons.get (i + 0);
				v2 = p.polygons.get (i + 1);
				v3 = p.polygons.get (i + 2);
				v4 = p.polygons.get (i + 3);
				
				//first facet of the rectangle
				Point4d[] pA = new Point4d[] {
					new Point4d(p.vertices.get(v1*p.dimension+0),p.vertices.get(v1*p.dimension+1),p.vertices.get(v1*p.dimension+2),1),
					new Point4d(p.vertices.get(v2*p.dimension+0),p.vertices.get(v2*p.dimension+1),p.vertices.get(v2*p.dimension+2),1),
					new Point4d(p.vertices.get(v3*p.dimension+0),p.vertices.get(v3*p.dimension+1),p.vertices.get(v3*p.dimension+2),1)
				};
				if (checkArea(pA)) {
					writeVertices(pA);
					writeFacets(new String[] {"0 1 2"});
				}
				
				if(twoF) {
					//second facet of the rectangle
					pA = new Point4d[] {
						new Point4d(p.vertices.get(v1*p.dimension+0),p.vertices.get(v1*p.dimension+1),p.vertices.get(v1*p.dimension+2),1),
						new Point4d(p.vertices.get(v3*p.dimension+0),p.vertices.get(v3*p.dimension+1),p.vertices.get(v3*p.dimension+2),1),
						new Point4d(p.vertices.get(v4*p.dimension+0),p.vertices.get(v4*p.dimension+1),p.vertices.get(v4*p.dimension+2),1)
					};
					if (checkArea(pA)) {
						writeVertices(pA);
						writeFacets(new String[] {"0 1 2"});
					}
				}
			}
		}
	}
	

	private boolean checkArea(Point4d[] pA) {
		return calcTrArea(pA)>0.0000001;
	}
	
	private double calcTrArea(Point4d[] pA) {
		double x1 = pA[0].x;
		double y1 = pA[0].y;
		double z1 = pA[0].z;
		double x2 = pA[1].x;
		double y2 = pA[1].y;
		double z2 = pA[1].z;
		double x3 = pA[2].x;
		double y3 = pA[2].y;
		double z3 = pA[2].z;
		double a = Math.sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1)+(z2-z1)*(z2-z1));
		double b = Math.sqrt((x3-x1)*(x3-x1)+(y3-y1)*(y3-y1)+(z3-z1)*(z3-z1));
		double c = Math.sqrt((x3-x2)*(x3-x2)+(y3-y2)*(y3-y2)+(z3-z2)*(z3-z2));
		double p = 0.5f*(a+b+c);
		double d = p*(p-a)*(p-b)*(p-c);
		if (d<0) { // bad value
			d=0;
		}
		return Math.sqrt(d);
	}
}
