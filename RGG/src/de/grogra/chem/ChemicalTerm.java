package de.grogra.chem;

public class ChemicalTerm {
	
	int index;
	
	double factor;
	Molecule m;

	public ChemicalTerm(Molecule m) {
		this.factor = 1;
		this.m = m;
	}

	public ChemicalTerm(double factor, Molecule m) {
		this.factor = factor;
		this.m = m;
	}

	public String toString() {
		return factor + m.toString();
	}
}
