/*
  * Copyright (C) 2021 GroIMP Developer Team
  *
  * This program is free software; you can redistribute it and/or
  * modify it under the terms of the GNU General Public License
  * as published by the Free Software Foundation; either version 3
  * of the License, or any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 
02111-1307, USA.
  */

package de.grogra.stl;

import java.io.IOException;

import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;

import de.grogra.imp3d.PolygonArray;
import de.grogra.imp3d.objects.Attributes;
import de.grogra.imp3d.objects.SceneTree.InnerNode;
import de.grogra.imp3d.objects.SceneTree.Leaf;
import de.grogra.pf.registry.Item;
import de.grogra.pf.ui.Workbench;
import de.grogra.util.Utils;

/**
 * Wrapper class. Part of STLExport
 * 
 * http://en.wikipedia.org/wiki/STL_(file_format)
 * 
 * MH 2015-04-28
 */

public class Cone extends ObjectBase {

	@Override
	void exportImpl (Leaf node, InnerNode transform, STLExport export)
			throws IOException
	{
		Point3d pos = new Point3d ();
		Matrix4d m = getTransformation ();
		float r = node.getFloat (Attributes.RADIUS);
		double l = node.getDouble (Attributes.LENGTH);

		// prepare polygon array for indexed geometry
		// store 3d triangles in this array
		PolygonArray p = new PolygonArray ();
		p.dimension = 3;
		p.edgeCount = 3;

		Item general = Item.resolveItem (Workbench.current (), "/export/stl");
		final int uCount = Utils.getInt(general, "coneucount", 15);

		// generate geometry
		int index = 2;
		pos.set(0, 0, 0);
		m.transform (pos);
		p.vertices.push ((float)pos.x).push ((float)pos.y).push ((float)pos.z);
		pos.set(0, 0, l);
		m.transform (pos);
		p.vertices.push ((float)pos.x).push ((float)pos.y).push ((float)pos.z);
		for (int u = 0; u < uCount; u++) {
			float phi = (float) (Math.PI * 2 * u / uCount);
			float cosPhi = (float) Math.cos (phi);
			float sinPhi = (float) Math.sin (phi);
			pos.set (cosPhi, sinPhi, 0);
			pos.scale (r);
			m.transform (pos);
			p.vertices.push ((float) pos.x).push ((float) pos.y).push ((float) pos.z);
			if (Utils.getBoolean(general, "conebottom"))
				p.polygons.push (0).push (index + (uCount - u > 1 ? 1 : 1 - uCount)).push (index);
			p.polygons.push (index).push (index + (uCount - u > 1 ? 1 : 1 - uCount)).push (1);
			index++;
		}

		// write object
		out.println("solid Cone"+node.pathId);
		mesh1(p);
		out.println("endsolid Cone"+node.pathId);
	}

}
