/*
 *  Copyright (c) 2011 Dietger van Antwerpen (dietger@xs4all.nl)
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef _GRIDTRACE_H
#define _GRIDTRACE_H

typedef struct {
	Vec2 spacing;
	AABB grid_bbox;  // cell space grid bounding box
	AABB cell_bbox;  // cell space single cell bounding box
	float repeat; // repeat radius
} grid_t;

// clip ray against axis aligned bounding box
// returns false if ray is clipped away
// clipped line segment is stored as <p0,p1>
bool clip_ray(
	const Ray *r,
	__constant const AABB *bbox,
	const float t,
	Vec2 *p0,
	Vec2 *p1,
	Vec2 *dt)
{
	const float tx1 = r->d.x != 0 ? (bbox->x0 - r->o.x) / r->d.x : 0;
	const float tx2 = r->d.x != 0 ? (bbox->x1 - r->o.x) / r->d.x : FLT_MAX;

	const float ty1 = r->d.y != 0 ? (bbox->y0 - r->o.y) / r->d.y : 0;
	const float ty2 = r->d.y != 0 ? (bbox->y1 - r->o.y) / r->d.y : FLT_MAX;

	const float tz1 = r->d.z != 0 ? (bbox->z0 - r->o.z) / r->d.z : 0;
	const float tz2 = r->d.z != 0 ? (bbox->z1 - r->o.z) / r->d.z : FLT_MAX;

	dt->x = max(max(
		min(tx1, tx2), min(ty1, ty2)), max(0.f,min(tz1, tz2)));
		
	dt->y = min(min(
		max(tx1, tx2), max(ty1, ty2)), min(t,max(tz1, tz2)));
		
	if (dt->x > dt->y)
		return false;

	p0->x = r->o.x + dt->x * r->d.x;
	p0->y = r->o.y + dt->x * r->d.y;

	p1->x = r->o.x + dt->y * r->d.x;
	p1->y = r->o.y + dt->y * r->d.y;

	return true;
}

#define SWAP_FLOAT(x,y) { float tmp = x; x = y; y = tmp; }

void raster_octant(DEBUG_PAR,
	Intc *intc, 
	const Ray *r,
	int np ,
	const __global char *prims,
	const __global int *offsets,
	const __global char *bvh,
	int root,
	bool shadowRay ,
	__constant Vec2 *spacing, // cell matrix
	const float radius,
	float x1,const float y1,
	float x2,const float y2,
	const float t0, const float t1,
	const float width,  // horizontal overlap
	const float height, // vertical overlap
	const int m11, const int m12,
	const int m21, const int m22)
{
	// preconditions
	// ray in first octant
	// (y2 - y1 >= 0);
	// (x2 - x1 >= y2 - y1);
	// non-negative cell overlap
	// (width >= 0);
	// (height >= 0);
	const float dx = (x2 - x1);
	const float dt = (t1 - t0) / dx;
	const float dy = (dx != 0) ? (y2 - y1) / dx : 0;

	// account for horizontal cell overlap
	x1 -= width;

	const float fix = max((float)floor(x1),-radius);

	// minimum possible intersection distance
	float lt = (fix - x1 - width) * dt;

	// y intersection at previous cell boundary
	float ly = y1 - (height + width * dy) - (x1 - fix) * dy;
	// y intersection at next cell boundary
	float hy = y1 + (fix + 1 - x1) * dy;

	// clamped y range
	const int ily = (int)max(floor(y1 - height), -radius);
	const int ihy = (int)min(ceil(y2), radius + 1.f);

	// SIMD friendly raster loop

	// setup
	int ix = (int)fix;
	const int ixe = (int)min(floor(x2),radius);

	// setup first scanline
	int iy = max((int)floor(ly), ily) - 1;	
	int iye = min((int)ceil(hy), ihy); // first scanline

	while (true)
	{
		// next cell
		while (++iy >= iye)
		{
			// next scanline
			if (++ix > ixe)
				return;

			// move minimum hit distance
			lt += dt;

			// early out when no closer intersection can be found
			if (lt > intc->t)
				break;

			// move bounds by one cell
			hy += dy;
			ly += dy;

			// init next scanline
			iy = max((int)floor(ly), ily) - 1;
			iye = min((int)ceil(hy), ihy); // first scanline
		};

		// transform current cell back
		const int x = ix * m11 + iy * m12;
		const int y = ix * m21 + iy * m22;

		// process cell
		{
			// transform ray to cell
			Vec3 o;
			v3init( &o, spacing->x * x, spacing->y * y, 0 );
						
			Ray cr;
			v3sub( &cr.o, &r->o, &o );
			cr.d = r->d;
					
			RayAux aux;
			raux( &aux, &cr );

			// backup prim
			const __global Prim *prev_prim = intc->prim;
			intc->prim = 0;
			
			// trace ray against cell
#ifdef SPATIAL_STRUCTURE
	#ifdef BVH
			bvhTrace( DEBUG_ARG, intc, &cr, &aux, prims, offsets, (__global BVHNode*)bvh, root, false );
	#else
			bihTrace( DEBUG_ARG, intc, &cr, &aux, prims, offsets, (__global BIHNode*)bvh, root, false );
	#endif
#else
			computeIntersect( DEBUG_ARG , intc, 0, np, prims, offsets, &cr, &aux, false );
#endif

			if( intc->prim != 0 )
			{
				// update cell shift
				intc->shiftx = spacing->x * x;
				intc->shifty = spacing->y * y;
			} else {
				intc->prim = prev_prim; // restore
			}

		}
	};
}

// raster ray on regular grid of unit cells with possily overlapping bounding boxes
void raster(
	DEBUG_PAR,
	Intc *intc, 
	const Ray *r,
	int np ,
	const __global char *prims,
	const __global int *offsets,
	const __global char *bvh,
	int root,
	bool shadowRay,
	__constant Vec2 *spacing, // cell matrix
	const float radius,
	float rx1,float ry1,
	float rx2,float ry2,
	const float t0, const float t1,
	float bx1,float by1,
	float bx2,float by2)
{
	// preconditions
	// valid bounding box
	// bx1 <= bx2
	// by1 <= by2

	int m11 = 1,m12 = 0,m21 = 0,m22 = 1;

	// transform to first octant (dy > 0 && dx > dy)
	if( ry2 < ry1 ) {
		// mirror vertically
		m22 = -1;

		ry1 = -ry1; ry2 = -ry2;
		by1 = -by1; by2 = -by2;
		SWAP_FLOAT(by1, by2);
	}
	if( rx2 < rx1 ) {
		// mirror horizontally
		m11 = -1;

		rx1 = -rx1; rx2 = -rx2;
		bx1 = -bx1; bx2 = -bx2;
		SWAP_FLOAT(bx1, bx2);
	}
	if( rx2 - rx1 < ry2 - ry1 ) {
		// mirror diagonally
		m21 = m22;
		m12 = m11;
		m11 = 0; m22 = 0;

		SWAP_FLOAT(rx1,ry1);
		SWAP_FLOAT(rx2,ry2);

		SWAP_FLOAT(bx1,by1);
		SWAP_FLOAT(bx2,by2);
	}

	// normalize bounding volume
	rx1 -= bx1;	rx2 -= bx1;
	ry1 -= by1;	ry2 -= by1;

	const float width = max(bx2 - bx1 - 1.f,0.f);
	const float height = max(by2 - by1 - 1.f,0.f);

	// raster transformed line in first octant
	raster_octant(DEBUG_ARG,
		intc, r, 
		np,
		prims,
		offsets,
		bvh,
		root,
		shadowRay,
		spacing,	
		radius,
		rx1, ry1, 
		rx2, ry2, 
		t0, t1,
		width,height,
		m11,m12,m21,m22);
}

// trace against a grid
void gridtrace( DEBUG_PAR,
	__constant grid_t *grid,
	Intc *intc, 
	const Ray *r,
	int np ,
	const __global char *prims,
	const __global int *offsets,
	const __global char *bvh,
	int root,
	bool shadowRay )
{	
	Ray cell_ray = *r;

	// normalize cell
	cell_ray.o.x /= grid->spacing.x;
	cell_ray.o.y /= grid->spacing.y;
	
	cell_ray.d.x /= grid->spacing.x;
	cell_ray.d.y /= grid->spacing.y;
		
	Vec2 x0, x1, t;
	// clip ray against complete grid
	clip_ray(
		&cell_ray,
		&grid->grid_bbox,
		intc->t,
		&x0, &x1, &t);
	
	// raster through the grid
	raster(
		DEBUG_ARG,
		intc, 
		r,
		np,
		prims,
		offsets,
		bvh,
		root,
		shadowRay,
		&grid->spacing,
		grid->repeat,
		x0.x, x0.y,
		x1.x, x1.y,
		t.x, t.y,
		grid->cell_bbox.x0, grid->cell_bbox.y0,
		grid->cell_bbox.x1, grid->cell_bbox.y1);
}

#endif