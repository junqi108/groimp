package de.grogra.gpuflux.scene.experiment;

import java.io.Serializable;

/**
 * @author      Dietger van Antwerpen <dietger@xs4all.nl>
 * @version     1.0                                       
 * @since       2011.08.24                                 
 *
 * The Measurement class contains a vector of measurements applying to a single object or group of objects.
 * The dimension and units for the measurements are not defined and depend on the context.
 */

public class Measurement implements Serializable {
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 135792468L;

	/**
	 * Constructor
	 * 
	 * @param length the dimension of the measurement vector
	 * */
	public Measurement(int length) {
		data = new double[length];
	}

	/**
	 * Constructor
	 * */
	public Measurement() {
	}

	/**
	 * Constructor
	 * 
	 * @param data the measurement vector data
	 * */
	public Measurement(double[] data) {
		this.data = data;
	}

	/**
	 * Adds a measurement vector to this measurement. 
	 * The measurements are assumed to have the same dimensions and units. 
	 * 
	 * @param m measurement to add 
	 * */
	public void add(Measurement m) {
		if( (m == null) || (m.data == null) != (data == null) )
			throw new NullPointerException();
		if( data != null ) {
			if(m.data.length != data.length)
				throw new IllegalArgumentException( "Measurement dimensions must agree" );
			for( int i = 0 ; i < data.length ; i++ )
				data[i] += m.data[i];
		}
	}

	/**
	 * Scales a measurement vector by a single factor. 
	 * 
	 * @param scale the scaling factor 
	 * */
	public void mul(double scale) {
		if( data != null ) {	
			for( int i = 0 ; i < data.length ; i++ )
				data[i] *= scale;
		}
	}
	
	/**
	 * Scaled a measurement vector and adds it to this measurement. 
	 * The measurements are assumed to have the same dimensions and units. 
	 * 
	 * @param m measurement to scall and add 
	 * @param scale scale factor
	 * */
	public void mad(Measurement m, double scale) {
		if( (m == null) || (m.data == null) != (data == null) )
			throw new NullPointerException();
		if( data != null ) {
			if(m.data.length != data.length)
				throw new IllegalArgumentException( "Measurement dimensions must agree" );
			for( int i = 0 ; i < data.length ; i++ )
				data[i] += m.data[i] * scale;
		}
	}
	
	/**
	 * Returns the sum of the measurement vector elements.
	 * All dimensions are assumed to be of the same unit. 
	 * 
	 * @return sum of all vector elements
	 * */
	public double integrate()
	{	
		double ret = 0.0;
		if( data != null ) {
			for( int i = 0 ; i < data.length ; i++ )
				ret += data[i];
		}
		return ret;
	}
	
	@Override
	public String toString()
	{
		if(data==null || data.length==0) {
			String s = "Sorry, something went wrong.\n";
			if(data==null) {
				s += "Measurement.toString(): data = null";
				return s;
			}
			if(data.length==0) s += "Measurement.toString(): |data| = 0";
			return s;
		}

		String str = "[ ";
		for( int i = 0 ; i < data.length - 1 ; i++ )
			str += data[i] + " , ";
		str += data[data.length - 1] + " ]";
		return str;
	}
	
	/**
	 * @return true if this measurement contains no data 
	 * */
	public boolean isEmpty()
	{
		return data == null || data.length == 0;
	}
	
	@Override
	public Object clone()
	{
		if( data != null )
			return new Measurement( data.clone() );
		else
			return new Measurement();
	}
	
	/**
	 * Measurement vector data 
	 * */
	public double [] data;
}
