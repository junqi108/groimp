package de.grogra.imp3d.shading;

import de.grogra.imp3d.spectral.RGBSpectralCurve;
import de.grogra.imp3d.spectral.SpectralCurve;
import de.grogra.persistence.SCOType;

public class RGBSPD extends SPD
{

// enh:sco SCOType
	
	private float r;
	//enh:field getter setter
	private float g;
	//enh:field getter setter
	private float b;
	//enh:field getter setter

	// enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final Type $TYPE;

	public static final Type.Field r$FIELD;
	public static final Type.Field g$FIELD;
	public static final Type.Field b$FIELD;

	public static class Type extends SCOType
	{
		public Type (Class c, de.grogra.persistence.SCOType supertype)
		{
			super (c, supertype);
		}

		public Type (RGBSPD representative, de.grogra.persistence.SCOType supertype)
		{
			super (representative, supertype);
		}

		Type (Class c)
		{
			super (c, SCOType.$TYPE);
		}

		private static final int SUPER_FIELD_COUNT = SCOType.FIELD_COUNT;
		protected static final int FIELD_COUNT = SCOType.FIELD_COUNT + 3;

		static Field _addManagedField (Type t, String name, int modifiers, de.grogra.reflect.Type type, de.grogra.reflect.Type componentType, int id)
		{
			return t.addManagedField (name, modifiers, type, componentType, id);
		}

		@Override
		protected void setFloat (Object o, int id, float value)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 0:
					((RGBSPD) o).r = (float) value;
					return;
				case Type.SUPER_FIELD_COUNT + 1:
					((RGBSPD) o).g = (float) value;
					return;
				case Type.SUPER_FIELD_COUNT + 2:
					((RGBSPD) o).b = (float) value;
					return;
			}
			super.setFloat (o, id, value);
		}

		@Override
		protected float getFloat (Object o, int id)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 0:
					return ((RGBSPD) o).getR ();
				case Type.SUPER_FIELD_COUNT + 1:
					return ((RGBSPD) o).getG ();
				case Type.SUPER_FIELD_COUNT + 2:
					return ((RGBSPD) o).getB ();
			}
			return super.getFloat (o, id);
		}

		@Override
		public Object newInstance ()
		{
			return new RGBSPD ();
		}

	}

	public de.grogra.persistence.ManageableType getManageableType ()
	{
		return $TYPE;
	}


	static
	{
		$TYPE = new Type (RGBSPD.class);
		r$FIELD = Type._addManagedField ($TYPE, "r", Type.Field.PRIVATE  | Type.Field.SCO, de.grogra.reflect.Type.FLOAT, null, Type.SUPER_FIELD_COUNT + 0);
		g$FIELD = Type._addManagedField ($TYPE, "g", Type.Field.PRIVATE  | Type.Field.SCO, de.grogra.reflect.Type.FLOAT, null, Type.SUPER_FIELD_COUNT + 1);
		b$FIELD = Type._addManagedField ($TYPE, "b", Type.Field.PRIVATE  | Type.Field.SCO, de.grogra.reflect.Type.FLOAT, null, Type.SUPER_FIELD_COUNT + 2);
		$TYPE.validate ();
	}

	public float getR ()
	{
		return r;
	}

	public void setR (float value)
	{
		this.r = (float) value;
	}

	public float getG ()
	{
		return g;
	}

	public void setG (float value)
	{
		this.g = (float) value;
	}

	public float getB ()
	{
		return b;
	}

	public void setB (float value)
	{
		this.b = (float) value;
	}

//enh:end
	
	public RGBSPD()
	{
		super();
		g=1;
	}

	public RGBSPD( float r, float g, float b )
	{
		super();
		this.r = r;
		this.g = g;
		this.b = b;
	}
	
	@Override
	public SpectralCurve getSpectralDistribution() {
		return new RGBSpectralCurve(r,g,b);
	}

}
