package de.grogra.imp3d.shading;

import de.grogra.imp3d.objects.Attributes;
import de.grogra.imp3d.spectral.CIENormSpectralCurve;
import de.grogra.imp3d.spectral.SpectralCurve;
import de.grogra.persistence.SCOType;

public class CIENormSPD extends SPD {

// enh:sco SCOType
	
	protected int cieNorms = Attributes.CIE_NORM_D65;
	//enh:field type=Attributes.CIE_NORMS_TYPE getter setter

	// enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final Type $TYPE;

	public static final Type.Field cieNorms$FIELD;

	public static class Type extends SCOType
	{
		public Type (Class c, de.grogra.persistence.SCOType supertype)
		{
			super (c, supertype);
		}

		public Type (CIENormSPD representative, de.grogra.persistence.SCOType supertype)
		{
			super (representative, supertype);
		}

		Type (Class c)
		{
			super (c, SCOType.$TYPE);
		}

		private static final int SUPER_FIELD_COUNT = SCOType.FIELD_COUNT;
		protected static final int FIELD_COUNT = SCOType.FIELD_COUNT + 1;

		static Field _addManagedField (Type t, String name, int modifiers, de.grogra.reflect.Type type, de.grogra.reflect.Type componentType, int id)
		{
			return t.addManagedField (name, modifiers, type, componentType, id);
		}

		@Override
		protected void setInt (Object o, int id, int value)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 0:
					((CIENormSPD) o).cieNorms = (int) value;
					return;
			}
			super.setInt (o, id, value);
		}

		@Override
		protected int getInt (Object o, int id)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 0:
					return ((CIENormSPD) o).getCieNorms ();
			}
			return super.getInt (o, id);
		}

		@Override
		public Object newInstance ()
		{
			return new CIENormSPD ();
		}

	}

	public de.grogra.persistence.ManageableType getManageableType ()
	{
		return $TYPE;
	}


	static
	{
		$TYPE = new Type (CIENormSPD.class);
		cieNorms$FIELD = Type._addManagedField ($TYPE, "cieNorms", Type.Field.PROTECTED  | Type.Field.SCO, Attributes.CIE_NORMS_TYPE, null, Type.SUPER_FIELD_COUNT + 0);
		$TYPE.validate ();
	}

	public int getCieNorms ()
	{
		return cieNorms;
	}

	public void setCieNorms (int value)
	{
		this.cieNorms = (int) value;
	}

//enh:end
	
	public CIENormSPD()
	{
		super();
	}

	public CIENormSPD( int temp )
	{
		super();
		cieNorms = temp;
	}
	
	@Override
	public SpectralCurve getSpectralDistribution() {
		return new CIENormSpectralCurve(cieNorms);
	}

}
