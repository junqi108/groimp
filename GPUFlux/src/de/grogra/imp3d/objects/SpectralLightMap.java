package de.grogra.imp3d.objects;

import de.grogra.imp3d.spectral.SpectralCurve;

public interface SpectralLightMap {

	public abstract SpectralCurve getSpectralDistribution();
	
}
