
package de.grogra.ext.dxf;

import java.io.*;
import java.util.ArrayList;
import java.util.Stack;

import javax.vecmath.Matrix4d;

import de.grogra.imp3d.View3D;
import de.grogra.imp3d.io.SceneGraphExport;
import de.grogra.imp3d.objects.Attributes;
import de.grogra.imp3d.objects.SceneTree;
import de.grogra.imp3d.objects.SceneTreeWithShader;
import de.grogra.imp3d.objects.SceneTree.InnerNode;
import de.grogra.imp3d.objects.SceneTree.Leaf;
import de.grogra.pf.io.*;
import de.grogra.util.MimeType;

public class DXFExport extends SceneGraphExport implements FileWriterSource
{
	public static final MetaDataKey<Float> FLATNESS = new MetaDataKey<Float> ("flatness");

	//	public static final MimeType MIME_TYPE = new MimeType ("model/x-dxf", null);
	//	public static final IOFlavor FLAVOR = new IOFlavor (MIME_TYPE,
	//		IOFlavor.FILE_WRITER, null);

	PrintWriter out;

	final Stack matrixStack = new Stack();
	
	public DXFExport (FilterItem item, FilterSource source)
	{
		super (item, source);
		//		setFlavor (FLAVOR);
		setFlavor (item.getOutputFlavor ());
		
		// put an initial identity transform into the matrixStack
		Matrix4d m = new Matrix4d();
		m.setIdentity();
		matrixStack.push(m);
	}

	@Override
	protected void beginGroup (InnerNode group) throws IOException
	{
		// TODO Auto-generated method stub
//		System.err.println ("begingroup was called");
		// push new transformation matrix onto matrix stack
		Matrix4d m = (Matrix4d)matrixStack.peek();
		Matrix4d n = new Matrix4d();
		group.transform(m, n);
		matrixStack.push(n);
	}

	@Override
	protected SceneTree createSceneTree (View3D scene)
	{
		// TODO Auto-generated method stub
//		System.err.println ("createSceneTree was called");
		//		return null;
		SceneTree t = new SceneTreeWithShader (scene)
		{

			@Override
			protected boolean acceptLeaf (Object object, boolean asNode)
			{
				// TODO Auto-generated method stub
//				System.err.println ("acceptLeaf was called: " + asNode + ", "
//					+ object);
				return getExportFor(object, asNode) != null;
			}

			@Override
			protected Leaf createLeaf (Object object, boolean asNode, long id)
			{
				// TODO Auto-generated method stub
//				System.err.println ("createLeaf was called");
				Leaf l = new Leaf (object, asNode, id);
				init (l);
				return l;
			}

		};
		t.createTree (true);
		return t;
	}

	@Override
	protected void endGroup (InnerNode group) throws IOException
	{
		// TODO Auto-generated method stub
//		System.err.println ("endgroup was called");
		// remove transformation matrix from matrix stack
		matrixStack.pop();
	}

	public void write (File file) throws IOException
	{
		// TODO Auto-generated method stub
//		System.err.println ("write was called: " + file);
		FileWriter fw = new FileWriter (file);
		BufferedWriter br = new BufferedWriter (fw);
		out = new PrintWriter(br);
		
		// write header
		out.println("0");
		out.println("SECTION");
		out.println("2");
		out.println("ENTITIES");
		
		// write contents of the graph
		write ();
		
		// write footer
		out.println("0");
		out.println("ENDSEC");
		out.println("0");
		out.println("EOF");
		
		out.close ();
		br.close ();
		fw.close ();
	}

	public NodeExport getExportFor (Object object, boolean asNode)
	{
		Object s = getGraphState ().getObjectDefault (object, asNode,
			Attributes.SHAPE, null);
		if (s == null)
		{
			return null;
		}
		NodeExport ex = super.getExportFor (s, asNode);
		if (ex != null)
		{
			return ex;
		}
		return null;
	}
}
