package de.grogra.ext.obj;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;

import org.jagatoo.loaders.models.obj.OBJFace;
import org.jagatoo.loaders.models.obj.OBJGroup;
import org.jagatoo.loaders.models.obj.OBJModelPrototype;
import org.jagatoo.loaders.models.obj.OBJPrototypeLoader;

import de.grogra.graph.Graph;
import de.grogra.graph.impl.Node;
import de.grogra.imp3d.objects.MeshNode;
import de.grogra.imp3d.objects.PolygonMesh;
import de.grogra.imp3d.objects.ShadedNull;
import de.grogra.xl.util.FloatList;
import de.grogra.xl.util.IntList;
import de.grogra.xl.util.LongHashMap;

public class ObjImporter {

	InputStream in;
	URL path;

	public ObjImporter(InputStream in, URL filePath) {

		this.in = in;
		this.path = filePath;
	}

	public Node doImport() throws IOException {

		Node result = new ShadedNull( );

		//	((ReaderSource) getSource()).get
		//	InputStream is = ((InputStreamSource)getSource()).getInputStream();


		// load the model
		OBJModelPrototype model = OBJPrototypeLoader.load( in , path );

		// map (v, n, t) to index into vertex array
		LongHashMap<Integer> lhm = new LongHashMap<Integer>( );

		// provide space for converted model data
		// index counts the number of allocated vertices
		// va, na and ta store the data for those vertices
		int index;
		IntList ia = new IntList( );
		FloatList va = new FloatList( );
//		ByteList na = new ByteList( );
		FloatList na = new FloatList( );
		FloatList ta = new FloatList( );

		// process tree of objects
		OBJGroup topGroup = model.getTopGroup( );
		LinkedList<OBJGroup> objects = new LinkedList<OBJGroup>( );
		OBJGroup object = topGroup;
		while (object != null) {
			// prepare data space for converted model
			ia.clear( );
			va.clear( );
			na.clear( );
			ta.clear( );
			index = 0;

			// convert object into visible mesh
			// original model has independent indices for
			// position, normal and texture coordinate
			// resulting model can only index per vertex instead
			List<OBJFace> faces = object.getFaces( );
			List<float[]> vertexList = object.getVertexList( );
			List<float[]> normalList = object.getNormalList( );
			List<float[]> texList = object.getTexList( );
			lhm.clear( );

			// loop over faces
			for (OBJFace face : faces) {
				// store index of first and previous vertex
				// to triangulate polygons on the fly
				int v0 = 0;
				int vlast = 0;

				// loop over vertices of that face
				for (int i = 0; i < face.i; i++) {
					// map independent indices into vertex index
					int v = face.vData[i];
					int n = face.nData[i];
					int t = face.tData[i];
					long l = calculateHashIndex( v , n , t );
					Integer vertexIndex = lhm.get( l );

					// obtain that vertex index or create it on demand
					int vi;
					if (vertexIndex != null) {
						vi = vertexIndex;
					} else {
						vi = index++;
						va.push( 0 , 0 , 0 );
//						na.push( (byte) 0 , (byte) 0 , (byte) 0 );
//						na.push(0, 0, 0);
//						ta.push( 0 , 0 );
						lhm.put( l , vi );

						// convert that vertex
						va.set( 3 * vi + 0 , vertexList.get( v )[0] );
						va.set( 3 * vi + 1 , vertexList.get( v )[1] );
						va.set( 3 * vi + 2 , vertexList.get( v )[2] );
						if (n >= 0) {
//							na.set( 3 * vi + 0 , (byte) (127 * normalList.get( n )[0]) );
//							na.set( 3 * vi + 1 , (byte) (127 * normalList.get( n )[1]) );
//							na.set( 3 * vi + 2 , (byte) (127 * normalList.get( n )[2]) );
							na.set( 3 * vi + 0 , normalList.get( n )[0]) ;
							na.set( 3 * vi + 1 , normalList.get( n )[1]) ;
							na.set( 3 * vi + 2 , normalList.get( n )[2]) ;
						}
						if (t >= 0) {
							ta.set( 2 * vi + 0 , texList.get( t )[0] );
							ta.set( 2 * vi + 1 , texList.get( t )[1] );
						}
					}

					// check for first vertex
					if (i == 0) {
						v0 = vi;
					} else if (i >= 3) // triangulate polygons
					{
						// prepare new triangle
						ia.push( v0 ).push( vlast );
					}

					// put index into index list
					ia.push( vi );
					vlast = vi;
				}
			}

			// prepare a mesh node
			MeshNode mesh = new MeshNode( );
			PolygonMesh pm = new PolygonMesh( );
			pm.setIndexData( ia );
			pm.setVertexData( va );
			
//			float[] normals = new float[na.size];
//			for (int i = 0; i < na.size; i++) {
//				normals[i] = (na.get(i) + 128f) / 256f;
//				
//			}
			if (na.size > 0)
				pm.setNormalData( na.toArray( ) );
			if (ta.size > 0)
				pm.setTextureData( ta.toArray( ) );
			mesh.setPolygons( pm );

			// append the mesh node to the result node
			result.addEdgeBitsTo( mesh , Graph.BRANCH_EDGE , null );

			// fill up the queue with child objects
			objects.addAll( object.getChildren( ) );
			// get next object to process
			object = objects.poll( );
		}

		return result;
	}

	private static long calculateHashIndex( int v , int n , int t ) {

		long result = 0;
		result = (result << 21) | (v & 0x1FFFFF);
		result = (result << 21) | (n & 0x1FFFFF);
		result = (result << 21) | (t & 0x1FFFFF);
		return result;
	}
}
