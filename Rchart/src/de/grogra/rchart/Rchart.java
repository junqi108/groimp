/*
  * Copyright (C) 2020 GroIMP Developer Team
  *
  * This program is free software; you can redistribute it and/or
  * modify it under the terms of the GNU General Public License
  * as published by the Free Software Foundation; either version 3
  * of the License, or any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 
02111-1307, USA.
  */
  
package de.grogra.rchart;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Desktop;
import java.awt.Dialog;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.math.BigDecimal;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import javax.imageio.ImageIO;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SpinnerListModel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.jfree.data.general.DatasetChangeEvent;
import org.jfree.data.general.DatasetChangeListener;

import de.grogra.pf.data.Datacell;
import de.grogra.pf.data.Dataset;
import de.grogra.pf.data.DatasetRef;
import de.grogra.pf.ui.ChartPanel;
import de.grogra.pf.ui.swing.SwingPanel;
import de.grogra.rgg.Library;
import de.grogra.util.Disposable;

/**
* Chart plugin for GroIMP based on R/ggplot2
* @author Lukas Gürtler
* @version 1.0
*/
	public class Rchart implements 	DatasetChangeListener, 
									Serializable, 
									ComponentListener, 
									MouseListener, 
									MouseMotionListener, 
									SliderMouseReleasedListener,
									Disposable {
		
		private static final long serialVersionUID = 1L;
		
		//controls
		private transient ImagePanel PlotPanel;
		private transient RangeSlider VerticalRangeSlider, HorizontalRangeSlider;
		private transient JMenuBar menuBar;
		private transient JCheckBoxMenuItem JCheckBoxMenuItemShowHoverPoints;
		private transient JLabel XMinLabel,XMaxLabel, YMinLabel,YMaxLabel;
		private transient JTextField XMinTextField, XMaxTextField, YMinTextField, YMaxTextField;
		private transient JButton JButtonResetSelectionRange, JButtonApplySelectionRange;
		private transient JMenu JMenuData, JMenuPlot, JMenuExport, JMenuExportSelection;
		private transient JMenuItem JMenuItemPlotEdit, JMenuItemExportDataCSV, JMenuItemExportDataHTML, JMenuItemExportDataRDATAFRAME, JMenuItemInspectData, JMenuItemSaveImage, JMenuItemExportSelectionDataCSV, JMenuItemExportSelectionDataHTML, JMenuItemExportSelectionDataRDATAFRAME;
				
		//data
		private transient Dataset dataset;
		private transient String dataFrameString = "";
		private transient ArrayList<ArrayList<Double>> data = new ArrayList<ArrayList<Double>>();
					
		//properties
		private transient String title;
		private transient boolean showZoomMenu = false;
		private void SetShowZoomMenu(boolean value) {
			showZoomMenu = value;
		}
		private void ZoomMenuOnOff() {
				this.VerticalRangeSlider.setEnabled(showZoomMenu);
				this.VerticalRangeSlider.setVisible(showZoomMenu);
				this.HorizontalRangeSlider.setEnabled(showZoomMenu);
				this.HorizontalRangeSlider.setVisible(showZoomMenu);
				this.JCheckBoxMenuItemShowHoverPoints.setEnabled(showZoomMenu);
				this.JCheckBoxMenuItemShowHoverPoints.setVisible(showZoomMenu);
				this.XMinLabel.setEnabled(showZoomMenu);
				this.XMinLabel.setVisible(showZoomMenu);
				this.XMaxLabel.setEnabled(showZoomMenu);
				this.XMaxLabel.setVisible(showZoomMenu);
				this.YMinLabel.setEnabled(showZoomMenu);
				this.YMinLabel.setVisible(showZoomMenu);
				this.YMaxLabel.setEnabled(showZoomMenu);
				this.YMaxLabel.setVisible(showZoomMenu);
				this.XMinTextField.setEnabled(showZoomMenu);
				this.XMinTextField.setVisible(showZoomMenu);
				this.XMaxTextField.setEnabled(showZoomMenu);
				this.XMaxTextField.setVisible(showZoomMenu);
				this.YMinTextField.setEnabled(showZoomMenu);
				this.YMinTextField.setVisible(showZoomMenu);
				this.YMaxTextField.setEnabled(showZoomMenu);
				this.YMaxTextField.setVisible(showZoomMenu);
				this.JMenuExportSelection.setEnabled(showZoomMenu);
				this.JMenuExportSelection.setVisible(showZoomMenu);
				this.JButtonResetSelectionRange.setEnabled(showZoomMenu);
				this.JButtonResetSelectionRange.setVisible(showZoomMenu);
				this.JButtonApplySelectionRange.setEnabled(showZoomMenu);
				this.JButtonApplySelectionRange.setVisible(showZoomMenu);
				this.JMenuItemExportSelectionDataCSV.setEnabled(showZoomMenu);
				this.JMenuItemExportSelectionDataCSV.setVisible(showZoomMenu);
				this.JMenuItemExportSelectionDataHTML.setEnabled(showZoomMenu);
				this.JMenuItemExportSelectionDataHTML.setVisible(showZoomMenu);
				this.JMenuItemExportSelectionDataRDATAFRAME.setEnabled(showZoomMenu);
				this.JMenuItemExportSelectionDataRDATAFRAME.setVisible(showZoomMenu);
				
		}
		
		//working variables
		private transient String ScriptEngine = "Rscript";
		private transient boolean initialized = false;
		private transient String tempFile;
		private transient String tempDirectory;
		private transient PlotOptionList plotOptionList = new PlotOptionList();
		private transient String[] TablePlotColNames;
		private transient ArrayList<String> expressions = new ArrayList<String>();
		
		/**
		 * legend positions
		 */
		public enum LegendPosition {BOTTOM,TOP,LEFT,RIGHT,NONE};
		private transient int plotRectLeft, plotRectTop, plotRectRight, plotRectBottom;
		private transient double XMin, XMax, YMin, YMax;
		private transient int RangeSliderMaxValue = 10000;
		private transient Object PlotLock = new Object();
		private transient Object dsChangedLock = new Object();
		private transient boolean ThreadInDsChanged = false;
		private enum exportDataFileType {CSV,HTML,RDATAFRAME};
		
		private transient int minNumberOfColumns = 1;
		/**
		 * line types
		 */
		public enum LineType {BLANK, SOLID, DASHED, DOTTED, DOTDASH, LONGDASH, TWODASH, LT1F, LTF1, LT4C88C488, LT12345678};
		/**
		 * point shapes
		 */
		public enum PointShape {SQUARE,CIRCLE,TRIANGLE_POINT_UP,PLUS,CROSS,DIAMOND,TRIANGLE_POINT_DOWN,SQUARE_CROSS,STAR,DIAMOND_PLUS,CIRCLE_PLUS,TRIANGLES_UP_AND_DOWN,SQUARE_PLUS,CIRCLE_CROSS,SQUARE_AND_TRIANGLE_DOWN,FILLED_SQUARE,FILLED_CIRCLE,FILLED_TRIANGLE_POINT_UP,FILLED_DIAMOND,SOLID_CIRCLE,BULLET,FILLED_CIRCLE_BLUE,FILLED_SQUARE_BLUE,FILLED_DIAMOND_BLUE,FILLED_TRIANGLE_POINT_UP_BLUE,FILLED_TRIANGLE_POINT_DOWN_BLUE};
		
		/**
		 * ggplot2 commands
		 */
		public enum GGPLOT2COMMAND {annotate, annotation_custom, coord_cartesian, coord_fixed, coord_flip, coord_map, coord_polar, coord_quickmap, coord_trans, expand_limits, 
			facet_grid, facet_wrap, geom_abline, geom_area, geom_bar, geom_bin2d, geom_blank, geom_boxplot, geom_col, geom_contour, geom_count, geom_crossbar, 
			geom_curve, geom_density, geom_density2d, geom_dotplot, geom_errorbar, geom_errorbarh, geom_freqpoly, geom_hex, geom_histogram, geom_hline, 
			geom_jitter, geom_label, geom_label_repel, geom_line, geom_linerange, geom_map, geom_path, geom_point, geom_pointrange, geom_polygon, geom_qq, geom_quantile, 
			geom_raster, geom_rect, geom_ribbon, geom_rug, geom_segment, geom_smooth, geom_spoke, geom_step, geom_text, geom_tile, geom_violin, geom_vline, 
			ggtitle, guides, labs, scale_color_manual, scale_linetype_manual, scale_fill_brewer, scale_fill_continuous, scale_fill_discrete, scale_fill_distiller, scale_fill_gradient, scale_fill_gradient2, 
			scale_fill_gradientn, scale_fill_manual, scale_radius, scale_shape, scale_shape_manual, scale_size, scale_size_area, scale_x_continuous, 
			scale_x_date, scale_x_datetime, scale_x_discrete, scale_x_identity, scale_x_log10, scale_x_manual, scale_x_reverse, scale_x_sqrt, 
			scale_y_continuous, scale_y_date, scale_y_datetime, scale_y_discrete, scale_y_identity, scale_y_log10, scale_y_manual, scale_y_reverse, 
			scale_y_sqrt, theme, theme_bw, theme_classic, theme_dark, theme_grey, theme_light, theme_linedraw, theme_minimal, theme_void, xlab, xlim, 
			ylab, ylim, zlab, zlim};
		/**
		* class that represents a plotting command
		*/	
		public class PlotOption {
			
			private String command = "";
			private ArrayList<String> arguments;
			private boolean isGGPLOT2COMMAND = true;
			private Color lineColor = null;
			private double lineWidth = -1;
			private LineType lineType = null;
			private PointShape pointShape = null;
			
			/**
		     * constructor
		     */
			public PlotOption() {
				command = null;
				arguments = new ArrayList<String>();
			}
			/**
		     * constructor
		     * @param isGGPLOT2COMMAND true if command is from ggplot2
		     */
			public PlotOption(boolean isGGPLOT2COMMAND) {
				this();
				this.isGGPLOT2COMMAND = isGGPLOT2COMMAND;
			}
			/**
		     * set the command
		     * @param command 
		     */
			public void setCommand(GGPLOT2COMMAND command) {
				this.command = command.toString();
			}
			/**
		     * set the command
		     * @param command 
		     */
			public void setCommand(String command) {
				this.command = command;
			}
			/**
		     * add argument to the command/plot function
		     * @param argument
		     */
			public void addArgument(String argument) {
				arguments.add(argument);
			}
			/**
		     * add arguments to the command/plot function
		     * @param args argument array
		     */
			public void addAllArguments(String[] args) {
				for (String arg : args) {
					arguments.add(arg);
				}
			}
			/**
		     * get the string representation of the command
		     * @return command
		     */
			public String getCommand() {
				return command;
			}
			/**
		     * get the number of arguments
		     * @return count
		     */
			public int argumentsCount() {
				return arguments.size();
			}
			/**
		     * get argument at index i
		     * @param i index
		     * @return argument
		     */
			public String getArgument(int i) {
				return arguments.get(i);
			}
			/**
		     * set argument at index i
		     * @param i index
		     * @param value argument
		     */
			public void setArgument(int i, String value) {
				arguments.set(i, value);
			}
			/**
		     * remove argument
		     * @param arg argument
		     */
			public void removeArgument(String arg) {
				arguments.remove(arg);
			}
			/**
		     * clear argument list
		     */
			public void clearArgumentList() {
				arguments.clear();
			}
			/**
		     * set line color
		     * @param r red value
		     * @param g green value
		     * @param b blue value
		     * @return PlotOption instance
		     */			
			public PlotOption setLineColor(int r, int g, int b) {
				lineColor = new Color(r,g,b);
				return this;
			}
			/**
		     * set line color
		     * @param col color
		     * @return PlotOption instance
		     */	
			public PlotOption setLineColor(Color col) {
				return setLineColor(col.getRed(), col.getGreen(), col.getBlue());
			}
			/**
		     * set line width
		     * @param width line width
		     * @return PlotOption instance
		     */	
			public PlotOption setLineWidth(double width) {
				lineWidth = round(width,1);
				return this;
			}
			/**
		     * set point shape
		     * @param ps point shape
		     * @return PlotOption instance
		     */	
			public PlotOption setPointShape(PointShape ps) {
				pointShape = ps;
				return this;
			}
			/**
		     * set line type
		     * @param lt line type
		     * @return PlotOption instance
		     */	
			public PlotOption setLineType(LineType lt) {
				lineType = lt;
				return this;
			}
			/**
		     * add argument to ggplot2 aesthetics mapping
		     * @param addarg argument to add
		     * @return PlotOption instance
		     */	
			public PlotOption aes(String addarg) {
									
				if (getCommand().startsWith("geom")) {
					for (int i=0; i<argumentsCount();i++) {
						String arg = getArgument(i);
						if (arg.startsWith("aes")) {
							setArgument(i, arg.substring(0,arg.length()-1) + "," + addarg + ")" );
							return this;
						}	
					}
				}
				addArgument(String.format("aes(%s)",addarg));
				return this;
			}
			/**
		     * get the string representation of the plot option
		     * @return string representation
		     */	
			@Override
			public String toString() {
								
				if (command != null && command.length() > 0)  {
					StringBuilder sb = new StringBuilder();
					
					sb.append(command);
					sb.append("(");
					
					if (lineColor != null)
						sb.append("color=" + String.format("'#%02x%02x%02x',", lineColor.getRed(), lineColor.getGreen(), lineColor.getBlue()));
					
					if (lineWidth >= 0)
						sb.append("size="+round(lineWidth,1)+",");
					
					if (lineType != null) 
						sb.append(String.format("linetype='%s',",lineType.toString().replace("LT", "").toLowerCase()));
					
					if (pointShape != null)
						sb.append("shape="+pointShape.ordinal()+",");
					
					if (arguments.size() > 0) {
						String args = arguments.toString();
						sb.append(args.substring(1,args.length()-1));
					}
											
					sb.append(")");
					return sb.toString();
				}
				return "";
			}
		}

		private class PlotOptionList {
			
			private ArrayList<PlotOption> list;
			
			public PlotOptionList() {
				list = new ArrayList<PlotOption>();
			}
			
			public void add(PlotOption po) {
				list.add(po);
			}
			public void remove(PlotOption po) {
				list.remove(po);
			}
			public void remove(String po) {
				for (int i=list.size()-1; i>=0; i--) {
					if (list.get(i).toString().equals(po)) {
						list.remove(i);
					}
				}
			}
			public void removeCommand(String command) {
				for (int i=list.size()-1; i>=0; i--) {
					if (list.get(i).getCommand().equals(command)) {
						list.remove(i);
					}
				}
			}
			public void remove(GGPLOT2COMMAND command) {
				removeCommand(command.toString());
			}
			public int size() {
				return list.size();
			}
			public PlotOption get(int i) {
				return list.get(i);
			}
			public void clear() {
				list.clear();
			}
			
			@Override
			public String toString() {
				
				String others = "";
				
				if (list.size() > 0) {
					StringBuilder sb = new StringBuilder();
					for (PlotOption po : list) {
						if (po.isGGPLOT2COMMAND) {
							sb.append("+");
							sb.append(po);
						} else {
							others += ("\n" +po.toString());
						}
					}
					return sb.toString() + others;
				}
				return "";
			}
			
		}
		private static double round(double value, int places) {
		    if (places < 0) throw new IllegalArgumentException();

		    long factor = (long) Math.pow(10, places);
		    value = value * factor;
		    long tmp = Math.round(value);
		    return (double) tmp / factor;
		}
		
		private void listf(String directoryName, ArrayList<String> files) {
		    File directory = new File(directoryName);

		    // Get all files from a directory.
		    File[] fList = directory.listFiles();
		    if(fList != null) {
		        for (File file : fList) {      
		            if (file.isFile()) {
		                files.add(file.toString());
		            } else if (file.isDirectory()) {
		                listf(file.getAbsolutePath(), files);
		            }
		        }
		    }
		}
		private static boolean isOSWindows() {
			return System.getProperty("os.name").toLowerCase().contains("windows");
		}
		private static boolean isOSLinux() {
			return System.getProperty("os.name").toLowerCase().contains("nux");
		}
		private static boolean isOSMac() {
			return System.getProperty("os.name").toLowerCase().contains("mac");
		}
		
		private boolean checkRInstallation() {
			if (isOSWindows()) {
				try {
					File settingsDir = new File("plugins\\Rchart\\settings");
										
					if (!settingsDir.exists())
						settingsDir.mkdirs();
										
					File settingsFile = new File("plugins\\Rchart\\settings\\RPATH");
										
					if (settingsFile.exists()) {
						String rpath = new String(Files.readAllBytes(settingsFile.toPath()));
						
						if (new File(rpath).getAbsoluteFile().exists()) {
							ScriptEngine = rpath;
							checkRPackages();
							return true;
						}
					}
				
					JFileChooser folderPicker = new JFileChooser();
					folderPicker.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
					folderPicker.setDialogTitle("Please select the R installation folder!");
					
					if (folderPicker.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) { 
					    
						String dir = folderPicker.getSelectedFile().toString();
						ArrayList<String> files = new ArrayList<String>();
						listf(dir,files);
						String toSearch = "rscript.exe";
						boolean found = false;
					
						for (String file : files) {
							if (file.toLowerCase().endsWith(toSearch)) {
								ScriptEngine = file;
								BufferedWriter writer = new BufferedWriter(new FileWriter(settingsFile));
							    writer.write(file);
							    writer.close();
							    found = true;
							    break;
							}
						}
						
						if (!found) {
							JOptionPane.showMessageDialog(null,"R installation corrupted!","Error", JOptionPane.INFORMATION_MESSAGE);
						} else {
							checkRPackages();
							return true;
						}
					}
				
				} catch (Exception e) {
					System.out.println(e);
				}
				
			} else if (isOSLinux()) {
				try {
					new ProcessBuilder(ScriptEngine).start();
					checkRPackages();
				} catch (IOException e) {
					
					JDialog diag = new JDialog();
					diag.setTitle("R installation required");
			    	diag.setLayout(new BorderLayout());
			    	EmptyBorder eb = new EmptyBorder(5,10,5,10);
			    	
			    	JLabel l1 = new JLabel("R could not be found! Please perform the following steps:");
			    	JLabel l2 = new JLabel("     1. Open terminal");
			    	JLabel l3 = new JLabel("     2. Type in 'sudo apt-get install r-base' and press Enter");
			    	JLabel l4 = new JLabel("     3. Type in 'sudo apt-get install r-recommended' and press Enter");
			    	JLabel l5 = new JLabel("     4. Type in: 'sudo R' and press Enter");
			    	JLabel l6 = new JLabel("     5. Type in: 'install.packages(c('ggplot2','shiny','shinyWidgets','gridExtra',");
			    	JLabel l7 = new JLabel("          'DT','hexbin','quantreg','scatterplot3d','plot3D','ggrepel','akima'))' and press Enter");
			    	JLabel l8 = new JLabel("     6. Close terminal");
			    				    	
			    	l1.setBorder(eb);
			    	l2.setBorder(eb);
			    	l3.setBorder(eb);
			    	l4.setBorder(eb);
			    	l5.setBorder(eb);
			    	l6.setBorder(eb);
			    	l7.setBorder(eb);
			    	l8.setBorder(eb);
			    	
			    	JButton b1 = new JButton("copy to clipboard");
			    	b1.setName("sudo apt-get install r-base");
			    	JButton b2 = new JButton("copy to clipboard");
			    	b2.setName("sudo apt-get install r-recommended");
			    	JButton b3 = new JButton("copy to clipboard");
			    	b3.setName("sudo R");
			    	JButton b4 = new JButton("copy to clipboard");
			    	b4.setName("install.packages(c('ggplot2','shiny','shinyWidgets','gridExtra','DT','hexbin','quantreg','scatterplot3d','plot3D','ggrepel','akima'))");
			    
			    	ActionListener al = new ActionListener() { public void actionPerformed(ActionEvent e) {
			    		Component component = (Component) e.getSource();
			    		StringSelection stringSelection = new StringSelection(component.getName());
			    	    Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);
			        }};
			    	
			    	b1.addActionListener(al);
			    	b2.addActionListener(al);
			    	b3.addActionListener(al);
			    	b4.addActionListener(al);
			    				    	
			    	JPanel jp1 = new JPanel(new GridLayout(8,1));
			    	JPanel jp2 = new JPanel(new GridLayout(8,1));
			    				    	
			    	jp1.add(l1);
			    	jp1.add(l2);
			    	jp1.add(l3);
			    	jp1.add(l4);
			    	jp1.add(l5);
			    	jp1.add(l6);
			    	jp1.add(l7);
			    	jp1.add(l8);
			    	jp2.add(new JLabel(""));
			    	jp2.add(new JLabel(""));
			    	jp2.add(b1);
			    	jp2.add(b2);
			    	jp2.add(b3);
			    	jp2.add(b4);
			    	diag.add(jp1,BorderLayout.WEST);
			    	diag.add(jp2,BorderLayout.EAST);
			    	diag.pack();
			        diag.setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
			        diag.setLocationRelativeTo(null);
			        diag.setVisible(true);
			      			       
				}
			} else if (isOSMac()) {
				ScriptEngine = "/usr/bin/Rscript";
				try {
					new ProcessBuilder(ScriptEngine).start();
					checkRPackages();
				} catch (IOException e) {
					JOptionPane.showMessageDialog(null,"R installation required!","Error", JOptionPane.INFORMATION_MESSAGE);
				}
			} else {
				JOptionPane.showMessageDialog(null,"OS not supported!","", JOptionPane.INFORMATION_MESSAGE);
				return false;
			}
			return true;
		}
		private void checkRPackages() {
			try {
				
				String RScriptPath = tempFile + "PackageCheck";
				FileWriter FR = new FileWriter(RScriptPath);
								
				FR.write("Sys.setenv(LANGUAGE = 'en')\n");
				FR.write("packages <- c('ggplot2','shiny','shinyWidgets','gridExtra','DT','hexbin','quantreg','scatterplot3d','plot3D','ggrepel','akima')\n");
				FR.write("print(setdiff(packages, rownames(installed.packages())))");
				FR.close();		
				
				ProcessBuilder RScriptPB = new ProcessBuilder();
				
				RScriptPB.command(ScriptEngine, RScriptPath);
				RScriptPB.directory(new File(tempDirectory));
				RScriptPB.redirectErrorStream(true);
				Process RScript = RScriptPB.start();
				RScript.waitFor();
				BufferedReader in = new BufferedReader(new InputStreamReader(RScript.getInputStream()));
							
				String line="", tmp;
				while ((tmp = in.readLine()) != null)
					line += tmp;
				
				if (line.contains("character(0)")) {
					return;
				} else {
					String[] missingPackages = line.split(" ");

					String outMsg = "";
					
					for (String p : missingPackages) {
												
						if (p.startsWith("\""))
							outMsg += String.format("\n    > %s", p);
					}

					JOptionPane.showMessageDialog(null,"Please install the following R packages:"+outMsg,"", JOptionPane.INFORMATION_MESSAGE);
					return;
				}
				
			
			} catch (Exception e) {
				System.out.println(e);
			}
		}
		/**
	     * constructor
	     * @param dataset plot data
	     * @param interactive determines if the plot updates when the data changes
	     */	
		public Rchart(Dataset dataset, boolean interactive) {
			this.tempDirectory = System.getProperty("java.io.tmpdir");
			this.tempFile = Paths.get(tempDirectory, String.format("GroIMP-%s", System.currentTimeMillis())).toString();		
			if (checkRInstallation()) {
				setDataset(dataset,interactive);
				setTitle(dataset.getTitle());
			}
		}
		/**
	     * constructor
	     * @param dsr plot data
	     * @param interactive determines if the plot updates when the data changes
	     */			
		public Rchart(DatasetRef dsr, boolean interactive) {
			this(dsr.toDataset(),interactive);
		}
		/**
	     * get the dataset
	     * @return dataset
	     */	
		public Dataset getDataset() {
			return this.dataset;
		}
		/**
	     * set the dataset
	     * @param dataset
	     * @param interactive determines if the plot updates when the data changes
	     */
		private void setDataset(Dataset dataset, boolean interactive) {
			this.dataset = dataset;
			if (interactive)
				dataset.asDatasetAdapter().addChangeListener(this);
		}
		/**
	     * get the plot title
	     * @return title
	     */		
		public String getTitle() {
			return title;
		}
		/**
	     * set the plot title
	     * @param title
	     */	
		public void setTitle(String title) {
			this.title = title;
			this.ggtitle("'"+title+"'");
		}
	
		private void overrideFrom(Rchart rchart) {
			
			this.PlotPanel = rchart.PlotPanel;
			this.VerticalRangeSlider = rchart.VerticalRangeSlider;
			this.HorizontalRangeSlider = rchart.HorizontalRangeSlider;
			this.menuBar = rchart.menuBar;
			this.JCheckBoxMenuItemShowHoverPoints = rchart.JCheckBoxMenuItemShowHoverPoints;
			this.XMinLabel = rchart.XMinLabel;
			this.XMaxLabel = rchart.XMaxLabel;
			this.YMinLabel = rchart.YMinLabel;
			this.YMaxLabel = rchart.YMaxLabel;
			this.XMinTextField = rchart.XMinTextField;
			this.XMaxTextField = rchart.XMaxTextField;
			this.YMinTextField = rchart.YMinTextField;
			this.YMaxTextField = rchart.YMaxTextField;
			this.JButtonResetSelectionRange = rchart.JButtonResetSelectionRange;
			this.JButtonApplySelectionRange = rchart.JButtonApplySelectionRange;
			this.tempFile = rchart.tempFile;
			this.tempDirectory = rchart.tempDirectory;
			this.JMenuData = rchart.JMenuData;
			this.JMenuPlot = rchart.JMenuPlot;
			this.JMenuItemPlotEdit = rchart.JMenuItemPlotEdit;
			this.JMenuItemSaveImage = rchart.JMenuItemSaveImage;
			this.JMenuExport = rchart.JMenuExport;
			this.JMenuExportSelection = rchart.JMenuExportSelection;
			this.JMenuItemExportDataCSV = rchart.JMenuItemExportDataCSV;
			this.JMenuItemExportDataHTML = rchart.JMenuItemExportDataHTML;
			this.JMenuItemExportDataRDATAFRAME = rchart.JMenuItemExportDataRDATAFRAME;
			this.JMenuItemExportSelectionDataCSV = rchart.JMenuItemExportSelectionDataCSV;
			this.JMenuItemExportSelectionDataHTML = rchart.JMenuItemExportSelectionDataHTML;
			this.JMenuItemExportSelectionDataRDATAFRAME = rchart.JMenuItemExportSelectionDataRDATAFRAME;
			this.JMenuItemInspectData = rchart.JMenuItemInspectData;		
		}
						
		private void initWindow() {
			
			ChartPanel cp = Library.workbench().getChartPanel(title, null);
			cp.setChart(new Dataset(),0,null);
			Container c = ((SwingPanel) cp.getComponent()).getContentPane();
			SwingPanel sp = (SwingPanel) c.getParent().getParent();
						
			boolean controlsDeclared = false;
			
			if (sp.tag != null && sp.tag instanceof Rchart) {
				Rchart oldInstance = (Rchart) sp.tag;
				Dataset exDS = oldInstance.dataset;
				
				if (exDS == dataset) {
					oldInstance.RemoveListeners();
					overrideFrom(oldInstance);
					controlsDeclared = true;
				}
				
			}
			
			sp.tag = this;
			
			if (!controlsDeclared) {
				c.remove(0);
				
				//control definitions
				this.PlotPanel = new ImagePanel(null);
				this.menuBar = new JMenuBar();
				this.JMenuData = new JMenu("data");
				this.JMenuPlot = new JMenu("plot");
				this.JMenuItemPlotEdit = new JMenuItem("edit");
				this.JMenuItemSaveImage = new JMenuItem("save plot image");
				this.JMenuExport = new JMenu("export...");
				this.JMenuExportSelection = new JMenu("export selection...");
				this.JMenuItemExportDataCSV = new JMenuItem("as CSV");
				this.JMenuItemExportDataHTML = new JMenuItem("as HTML");
				this.JMenuItemExportDataRDATAFRAME = new JMenuItem("as RDATAFRAME");
				this.JMenuItemExportSelectionDataCSV = new JMenuItem("as CSV");
				this.JMenuItemExportSelectionDataHTML = new JMenuItem("as HTML");
				this.JMenuItemExportSelectionDataRDATAFRAME = new JMenuItem("as RDATAFRAME");
				this.JMenuItemInspectData = new JMenuItem("inspect");
				this.VerticalRangeSlider = new RangeSlider(0,RangeSliderMaxValue);
				this.HorizontalRangeSlider = new RangeSlider(0,RangeSliderMaxValue);
				this.JCheckBoxMenuItemShowHoverPoints = new JCheckBoxMenuItem("show coordinates");
				this.XMinLabel = new JLabel("Plot selection   X_min:");
				this.XMaxLabel = new JLabel("X_max:");
				this.YMinLabel = new JLabel("Y_min:");
				this.YMaxLabel = new JLabel("Y_max:");
				this.XMinTextField = new JTextField("");
				this.XMaxTextField = new JTextField("");
				this.YMinTextField = new JTextField("");
				this.YMaxTextField = new JTextField("");
				this.JButtonResetSelectionRange = new JButton("reset");
				this.JButtonApplySelectionRange = new JButton("apply");
				
				//control properties
				VerticalRangeSlider.setOrientation(SwingConstants.VERTICAL);
				VerticalRangeSlider.setValue(0);
				VerticalRangeSlider.setUpperValue(RangeSliderMaxValue);
				HorizontalRangeSlider.setOrientation(SwingConstants.HORIZONTAL);
				HorizontalRangeSlider.setValue(0);
				HorizontalRangeSlider.setUpperValue(RangeSliderMaxValue);
																				
				//arrange layout
				c.setLayout(new BorderLayout());
				c.add(menuBar, BorderLayout.NORTH);
				menuBar.add(JMenuData);
				menuBar.add(JMenuPlot);
				JMenuPlot.add(JMenuItemPlotEdit);
				JMenuPlot.add(JMenuItemSaveImage);
				JMenuData.add(JMenuExport);
				JMenuData.add(JMenuExportSelection);
				JMenuExport.add(JMenuItemExportDataCSV);
				JMenuExport.add(JMenuItemExportDataHTML);
				JMenuExport.add(JMenuItemExportDataRDATAFRAME);
				JMenuExportSelection.add(JMenuItemExportSelectionDataCSV);
				JMenuExportSelection.add(JMenuItemExportSelectionDataHTML);
				JMenuExportSelection.add(JMenuItemExportSelectionDataRDATAFRAME);
				
				JMenuData.add(JMenuItemInspectData);
				menuBar.add(JCheckBoxMenuItemShowHoverPoints);
				menuBar.add(XMinLabel);
		        menuBar.add(XMinTextField);
		        menuBar.add(XMaxLabel);
		        menuBar.add(XMaxTextField);
		        menuBar.add(YMinLabel);
		        menuBar.add(YMinTextField);
		        menuBar.add(YMaxLabel);
		        menuBar.add(YMaxTextField);
		        menuBar.add(JButtonResetSelectionRange);
		        menuBar.add(JButtonApplySelectionRange);
		        c.add(HorizontalRangeSlider,BorderLayout.SOUTH);
				c.add(VerticalRangeSlider,BorderLayout.WEST);
				c.add(this.PlotPanel,BorderLayout.CENTER);
			} 
			
			//add event handlers
			PlotPanel.addComponentListener(this);
			PlotPanel.addMouseListener(this);
			PlotPanel.addMouseMotionListener(this);
			JMenuItemExportDataCSV.addActionListener(ButtonExportDatasetToCSV_clicked);
			JMenuItemExportDataHTML.addActionListener(ButtonExportDatasetToHTML_clicked);
			JMenuItemExportDataRDATAFRAME.addActionListener(ButtonExportDatasetToRDATAFRAME_clicked);
			JMenuItemExportSelectionDataCSV.addActionListener(ButtonExportDatasetToCSV_clicked);
			JMenuItemExportSelectionDataHTML.addActionListener(ButtonExportDatasetToHTML_clicked);
			JMenuItemExportSelectionDataRDATAFRAME.addActionListener(ButtonExportDatasetToRDATAFRAME_clicked);
			JMenuItemInspectData.addActionListener(ButtonInspectData_clicked);
			JMenuItemSaveImage.addActionListener(ButtonSaveImage_clicked);
			VerticalRangeSlider.addSliderMouseReleasedListener(this);
			HorizontalRangeSlider.addSliderMouseReleasedListener(this);
			JCheckBoxMenuItemShowHoverPoints.addActionListener(JCheckBoxMenuItemShowHoverPoints_CheckedChanged);
			JButtonResetSelectionRange.addActionListener(ResetButton_Clicked);
			JButtonApplySelectionRange.addActionListener(ApplyButton_Clicked);
			JMenuItemPlotEdit.addActionListener(ButtonEdit_clicked);
			
			ZoomMenuOnOff();
			redraw();
			cp.show(true,null);
			
			initialized = true;		
		}
		
		private void RemoveListeners() {
			dataset.asDatasetAdapter().removeChangeListener(this);
			PlotPanel.removeComponentListener(this);
			PlotPanel.removeMouseListener(this);
			PlotPanel.removeMouseMotionListener(this);
			VerticalRangeSlider.RemoveSliderMouseReleasedListener();
			HorizontalRangeSlider.RemoveSliderMouseReleasedListener();
			JCheckBoxMenuItemShowHoverPoints.removeActionListener(JCheckBoxMenuItemShowHoverPoints_CheckedChanged);	
			JButtonResetSelectionRange.removeActionListener(ResetButton_Clicked);
			JButtonApplySelectionRange.removeActionListener(ApplyButton_Clicked);
			JMenuItemExportDataCSV.removeActionListener(ButtonExportDatasetToCSV_clicked);
			JMenuItemExportDataHTML.removeActionListener(ButtonExportDatasetToHTML_clicked);
			JMenuItemExportDataRDATAFRAME.removeActionListener(ButtonExportDatasetToRDATAFRAME_clicked);
			JMenuItemExportSelectionDataCSV.removeActionListener(ButtonExportDatasetToCSV_clicked);
			JMenuItemExportSelectionDataHTML.removeActionListener(ButtonExportDatasetToHTML_clicked);
			JMenuItemExportSelectionDataRDATAFRAME.removeActionListener(ButtonExportDatasetToRDATAFRAME_clicked);
			JMenuItemInspectData.removeActionListener(ButtonInspectData_clicked);
			JMenuItemSaveImage.removeActionListener(ButtonSaveImage_clicked);
			JMenuItemPlotEdit.removeActionListener(ButtonEdit_clicked);
		}
		
		private void redraw() {
			PlotPanel.revalidate();
			PlotPanel.repaint();
		}
				
		private ArrayList<ArrayList<Double>> DatasetToArrayList(Dataset ds) {
			
			ArrayList<ArrayList<Double>> res = new ArrayList<ArrayList<Double>>();
			int column_count = ds.getColumnCount();
			int row_count = ds.getRowCount();
			
			for (int i=0; i<column_count; i++)
				res.add(new ArrayList<Double>());			
									
			for (int i = 0; i<column_count; i++) {
				for(int j = 0; j < row_count; j++) {
					
					Datacell dc =  ds.getCell(j, i);
					
					if (!dc.isXNull()) {
						double value = ds.getCell(j, i).getX();
						res.get(i).add(value);	
					}
				}
			}
			return res;
		}
		
		private String ArrayListArrayListToRDATAFRAME(ArrayList<ArrayList<Double>> list) {
			
			if (list.size() == 0) {
				 return "";
			}
			
			StringBuilder sb = new StringBuilder();	
			sb.append("data.frame(");
			
			int c = -1;
			
			for (ArrayList<Double> column : list) {
				c++;
				String values = column.toString();
				sb.append(String.format("'x%s'=c(%s)",c+1,values.substring(1, values.length()-1)));
				sb.append(",");		
				
			}
			sb.deleteCharAt(sb.length()-1);
			sb.append(")");
							
			return sb.toString();	
		}
			
		private void exportData(exportDataFileType ft, String file, boolean selection) {
						
			ArrayList<Integer> cols = new ArrayList<Integer>();
			
			if (selection) {
				JDialog diag = new JDialog();
				diag.setLayout(new BorderLayout());
										
		    	JComboBox<String> jcbx = new JComboBox<String>(new String[] {});
		    	JComboBox<String> jcby = new JComboBox<String>(new String[] {});
		    	JButton jbOK = new JButton("OK");
		    		    	
		    	for (int i=0; i<minNumberOfColumns; i++) {
		    		if (TablePlotColNames != null && TablePlotColNames.length > i) {
		    			jcbx.addItem(TablePlotColNames[i]);
		    			jcby.addItem(TablePlotColNames[i]);
		    		} else {
		    			jcbx.addItem("column "+(i+1));
		    			jcby.addItem("column "+(i+1));
		    		}
		    	}
		    	
		    	jbOK.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent e) {
		    		Component component = (Component) e.getSource();
		            JDialog dialog = (JDialog) SwingUtilities.getRoot(component);
		            dialog.setName("ok");
		            dialog.setVisible(false);
		        }});
		    	
		    	JPanel jp1 = new JPanel(new GridLayout(3,1));
		    	JPanel jp2 = new JPanel(new GridLayout(3,1));
		    	jp1.add(new JLabel("          x:"));
		    	jp1.add(new JLabel("          y:"));
		    	jp2.add(jcbx);
		    	jp2.add(jcby);
		    	jp2.add(jbOK);
		    	JLabel msg = new JLabel("Please assign the data to the axis!");
		    	msg.setBorder(new EmptyBorder(10,10,10,10));
		    	diag.add(msg,BorderLayout.NORTH);
		    	diag.add(jp1,BorderLayout.WEST);
		    	diag.add(jp2,BorderLayout.EAST);
		    	diag.pack();
	    	   	diag.setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
		        diag.setLocationRelativeTo(null);
		        diag.setVisible(true);
		    	  
		        if (diag.getName().equals("ok")) {
		        	cols.add(jcbx.getSelectedIndex());
		        	cols.add(jcby.getSelectedIndex());
		        } else {
		        	return;
		        }
		        	
			} else {
				for (int i=0; i<data.size(); i++)
					cols.add(i);
			}
						
			int c_rows = data.get(cols.get(0)).size();
			for (int i=1; i<cols.size();i++)
				c_rows = Math.max(c_rows, data.get(cols.get(i)).size());
			
			if (c_rows > 0) {
				String[] lines = new String[c_rows];
				
				for (int i = 0; i<c_rows; i++) {
					lines[i] = "";
					
					if (selection) {
						double x = data.get(cols.get(0)).get(i);
						double y = data.get(cols.get(1)).get(i);
						
						if (x >= XMin && x<= XMax && y>=YMin && y<=YMax) {
							lines[i] += String.format(";%s;%s",x,y);
						}
						
						continue;
						
					}
					
					for (int j=0; j<cols.size(); j++) {
												
						if (i<data.get(cols.get(j)).size()) {
							lines[i] += ";" + data.get(cols.get(j)).get(i);
						} else {
							lines[i] += ";";
						}
						
					}
				}
				FileWriter FR;
				switch (ft) {
					case CSV :
							
						try {
							FR = new FileWriter(file);
							for(int i=0;i<lines.length-1;i++) {
								FR.write(lines[i].substring(1));
								FR.write("\n");
							}
							FR.write(lines[lines.length-1].substring(1));
							FR.close();
							
						} catch (IOException e) {
							System.out.println(e);
						}
						
						break;
					case HTML :
						
						try {
							
							FR = new FileWriter(file);						
							FR.write("<html><body><table border ='1'><tr>");
							
							for (int i=0; i<cols.size(); i++) {
								FR.write("<td>");
								
								if (TablePlotColNames != null && TablePlotColNames.length > cols.get(i)) {
									FR.write(TablePlotColNames[cols.get(i)]);
								} else {
									FR.write("x"+(cols.get(i)+1));
								}
								
								
							}
							FR.write("</tr>\n");     
							
							for(int i=0;i<lines.length;i++) {
								FR.write("</tr>");
								FR.write(lines[i].replace(";","<td>"));
								FR.write("</tr>\n");
							}
							FR.close();
							
							
						} catch (IOException e) {
							System.out.println(e);
						}
						
						
						break;
					case RDATAFRAME :
						
						
						StringBuilder sb = new StringBuilder();	
						sb.append("data.frame(");
						
						ArrayList<ArrayList<Double>> data2;
						
						if (selection) {
							data2 = new ArrayList<ArrayList<Double>>();
							data2.add((ArrayList<Double>)data.get(cols.get(0)).clone());
							data2.add((ArrayList<Double>)data.get(cols.get(1)).clone());
							
							for (int i=c_rows-1; i>=0;i--) {
								double x = data2.get(0).get(i);
								double y = data2.get(1).get(i);
								
								if (x<XMin || x>XMax || y<YMin || y>YMax) {
									data2.get(0).remove(i);
									data2.get(1).remove(i);
								}
									
							}
							
						} else {
							data2 = data;
						}
												
						for (int i=0; i<cols.size(); i++) {
							
							String values;
							if (selection)
								values = data2.get(i).toString();
							else
								values = data2.get(cols.get(i)).toString();
							
							if (TablePlotColNames != null && TablePlotColNames.length > cols.get(i)) {
								sb.append(String.format("'" + TablePlotColNames[cols.get(i)] + "'= c(%s)",values.substring(1, values.length()-1)));
							} else {
								sb.append(String.format("'x%s'=c(%s)",cols.get(i)+1,values.substring(1, values.length()-1)));
								
							}
							sb.append(",");		
							
						}
						sb.deleteCharAt(sb.length()-1);
						sb.append(")");
						try {
							FR = new FileWriter(file);
							FR.write(sb.toString());
							FR.close();
						} catch (IOException e) {
							System.out.println(e);
						}
						
						
						break;
						
				
					default:
						return;
				}
			}
		}
			
		private BufferedImage getPlotImage(int width, int height, boolean forExport) {
			try {
													
				String RScriptPath = tempFile;
				FileWriter FR = new FileWriter(RScriptPath);
				String ImageFilePath = (forExport? RScriptPath+"Export.png" : RScriptPath+".png");
				
				FR.write("Sys.setenv(LANGUAGE = 'en')\n" +
						 "library(ggplot2)\n" +
						 "library(gridExtra)\n" +
						 "library(scatterplot3d)\n" +
						 "library(plot3D)\n" +
						 "library(ggrepel)\n" +
						 "library(akima)\n" +
						 "my_signif = function(x) floor(x) + signif(x %% 1, 1)\n" +
						 "lm_eqn <- function(df,lx,ly){\n" + 
						 "  m <- lm(x2 ~ x1, df);\n" + 
						 "  eq <- substitute(italic(y) == b %.% italic(x) + a, \n" + 
						 "                   list(a = unname(my_signif(coef(m)[1])),\n" + 
						 "                        b = unname(my_signif(coef(m)[2])),\n" +
						 "						  x = lx,\n" +
						 "						  y = ly));\n" +
						 "  as.character(as.expression(eq));\n" + 
						"}\n" +
						 String.format("df <- %s\n", dataFrameString) +
						 String.format("%s\n",joinStr(expressions,"\n")) +
						 String.format("png('%s',width=%s,height=%s)\n", Paths.get(ImageFilePath).getFileName().toString(), width, height) +
						 "ggplot(df)+theme(panel.border=element_rect(colour='#333333',size=2,fill=NA))" +
						 plotOptionList.toString() +
						 "\ndev.off()\n" +
						 "suppressWarnings(suppressMessages(XRANGE <- ggplot_build(last_plot())$layout$panel_params[[1]]$x.range))\n" +
						 "suppressWarnings(suppressMessages(YRANGE <- ggplot_build(last_plot())$layout$panel_params[[1]]$y.range))\n" +
						 "'XYRANGE'\n" +
					 	 "XRANGE[1]\n" +
						 "XRANGE[2]\n" +
						 "YRANGE[1]\n" +
						 "YRANGE[2]\n");
				FR.close();				
					
				ProcessBuilder RScriptPB = new ProcessBuilder();
				RScriptPB.command(ScriptEngine, RScriptPath);
				RScriptPB.directory(new File(tempDirectory));
				RScriptPB.redirectErrorStream(true);
				Process RScript = RScriptPB.start();
				RScript.waitFor();
				
				BufferedReader in = new BufferedReader(new InputStreamReader(RScript.getInputStream()));
				
				ArrayList<String> RScripStreamOutput = new ArrayList<String>();
				
				String Rout = "";
				String line;
				while ((line = in.readLine()) != null) {
					RScripStreamOutput.add(line);
					Rout += ("\n" + line);
				}
								
				if (Rout.contains("error")) {
					System.out.println(Rout.substring(1));
					Library.println(Rout.substring(1));
					return null;
				} else {
					
					try {
						for (int i=0; i<RScripStreamOutput.size(); i++) {
							if(RScripStreamOutput.get(i).contains("XYRANGE")) {
								XMin = Double.parseDouble(RScripStreamOutput.get(i+1).replace(" ","").split("]")[1]);
								XMax = Double.parseDouble(RScripStreamOutput.get(i+2).replace(" ","").split("]")[1]);
								YMin = Double.parseDouble(RScripStreamOutput.get(i+3).replace(" ","").split("]")[1]);
								YMax = Double.parseDouble(RScripStreamOutput.get(i+4).replace(" ","").split("]")[1]);
								break;
							}
						}
					} catch (Exception ex) {}
					
				}
				
				File out = new File(ImageFilePath);
				
				if (out.exists())
					return ImageIO.read(out);
				else
					return null;
				
			} catch (Exception ex) {
				System.out.println(ex);
				return null;
			}
			
		}
		/**
		 * Opens the plot window (if not already open) and plots according to settings. Only needed when initialized with interactive=false.
		 */
		public void show() {
			datasetChanged(null);
		}
				
		private boolean Plot() {
			synchronized(PlotLock) {
				if (dataFrameString.length() > 0) {
					BufferedImage RPlotImage = 	getPlotImage(Math.max(100, PlotPanel.getWidth()),Math.max(100, PlotPanel.getHeight()),false);					
					PlotPanel.disposeImage();
					if (RPlotImage == null) {
						return false;
					} else {
						PlotPanel.setImage(RPlotImage);
						
						if (showZoomMenu) {
							
							int h = PlotPanel.image.getHeight();
							int w = PlotPanel.image.getWidth();
							
							int[] horizontals = new int[h];
							int[] verticals = new int[w];
													
							for (int i = 0; i<h; i++) {
								for (int j = 0; j<w; j++) {
									Color col = new Color(PlotPanel.image.getRGB(j, i));
									
									if (col.getRed() == 51 && col.getGreen() == 51 && col.getBlue() == 51) {
										horizontals[i]++;
										verticals[j]++;
									}
								}
							}
							
							int top=-1;
							int bottom = -1;
							for ( int i=0; i<h; i++) {
								if (top < 0 && horizontals[i] > (w/2)) {
									top = i;
								} else if (horizontals[i] > (w/2)) {
									bottom = i;
								}
							}
							
							int left = -1;
							int right = -1;
							for ( int i=0; i<w; i++) {
								if (left < 0 && verticals[i] > (h/2)) {
									left = i;
								} else if (verticals[i] > (h/2)) {
									right = i;
								}
							}
										
							if (top<0 || left<0 || right<0 || bottom<0 || left == right || top == bottom) {
								plotRectLeft = 0;
								plotRectTop = 0;
								plotRectRight = PlotPanel.getWidth();
								plotRectBottom = PlotPanel.getHeight();
								
								HorizontalRangeSlider.setBorder(new EmptyBorder(0,0,0,0));
								VerticalRangeSlider.setBorder(new EmptyBorder(0,0,0,0));
											
							} else {
								plotRectLeft = left;
								plotRectTop = top;
								plotRectRight = right;
								plotRectBottom = bottom;
								
								HorizontalRangeSlider.setBorder(new EmptyBorder(0, plotRectLeft+VerticalRangeSlider.getWidth(), 0,PlotPanel.getWidth()-plotRectRight));
								VerticalRangeSlider.setBorder(new EmptyBorder(plotRectTop,0,PlotPanel.getHeight()-plotRectBottom, 0));
							}
						}
						
						redraw();
						return true;
					}
				}
			}
			return false;
		}
		private void ReadDataset() {
			int column_count = dataset.getColumnCount();
			int row_count = dataset.getRowCount();
								
			if (column_count == 0 || row_count == 0) {
				ThreadInDsChanged = false;
				return;
			}
							
			ArrayList<ArrayList<Double>> data_new = DatasetToArrayList(dataset);
						
			boolean isdataValid = true;
			int c_rows;
			
			if (data_new.size() >= minNumberOfColumns && data_new.get(0).size()>0) {
				
				c_rows = data_new.get(0).size();
								
				for (ArrayList<Double> column: data_new) {
					if (c_rows != column.size()) {
						isdataValid = false;
						break;
					}
				}	
			} else {
				isdataValid=false;
			}
			
			if (isdataValid) {
				data = data_new;
				dataFrameString = ArrayListArrayListToRDATAFRAME(data);				
			}
		}
		/**
	     * opens a browser table application for the dataset
	     */	
		public void inspectData() {
  			 ReadDataset();
			 try {	
				String RScriptPath = tempFile + "InspectData";
			
				FileWriter FR = new FileWriter(RScriptPath);
				FR.write("Sys.setenv(LANGUAGE = 'en')\n"+			
						 "library(shiny)\n" +
						 "library(DT)\n" +
						 "library(shinyWidgets)\n" +
						 String.format("df <- %s\n", dataFrameString));
				
				if (TablePlotColNames != null) {
					for (int i=0; i<TablePlotColNames.length; i++)
						FR.write(String.format("try(colnames(df)[%s]<-'%s',silent=TRUE)\n", i+1, TablePlotColNames[i]));
				}
				
				FR.write("\n" +		
						 "callback <- c(\n" + 
						 "  \"table.on('dblclick.dt', 'thead th', function(e) {\",\n" + 
						 "  \"  var $th = $(this);\",\n" + 
						 "  \"  var index = $th.index();\",\n" + 
						 "  \"  var colname = $th.text(), newcolname = colname;\",\n" + 
						 "  \"  var $input = $('<input type=\\\"text\\\">')\",\n" + 
						 "  \"  $input.val(colname);\",\n" + 
						 "  \"  $th.empty().append($input);\",\n" + 
						 "  \"  $input.on('change', function(){\",\n" + 
						 "  \"    newcolname = $input.val();\",\n" + 
						 "  \"    if(newcolname != colname){\",\n" + 
						 "  \"      $(table.column(index).header()).text(newcolname);\",\n" + 
						 "  \"    }\",\r\n" + 
						 "  \"    $input.remove();\",\n" + 
						 "  \"  }).on('blur', function(){\",\n" + 
						 "  \"    $(table.column(index).header()).text(newcolname);\",\n" + 
						 "  \"    $input.remove();\",\n" + 
						 "  \"  });\",\n" + 
						 "  \"});\"\n" + 
						 ")\n" +
						 "\n" +
						 "ui <- basicPage(\n" + 
						 "  pickerInput(\n" + 
						 "    inputId = \"columnPicker\",\n" + 
						 "    label = \"Select columns\",\n" + 
						 "    choices = colnames(df),\n" + 
						 "    selected = colnames(df),\n" + 
						 "    options = list(\n" + 
						 "      `actions-box` = TRUE,\n" + 
						 "      size = 10\n" + 
						 "    ),\n" + 
						 "    multiple = TRUE\n" + 
						 "  ),\n" +
						 "   DT::dataTableOutput(\"mytable\")\n" + 
						 ")\n" + 
						 "\n" + 
						 "server <- function(input, output, session) {\n" + 
						 "  output$mytable = DT::renderDataTable({\n" + 
						 "    DT::datatable(df[input$columnPicker], editable = 'cell', callback = JS(callback),filter = list(position = 'top'))\n" + 
						 "  })\n" + 
						 "  session$onSessionEnded(function() {\n"+
					     "    stopApp()\n"+
					     "  })\n"+
						 "}\n" + 
					     "\n" +
						 "shinyApp(ui, server)");
				FR.close();		
			
				ProcessBuilder RScriptPB = new ProcessBuilder(ScriptEngine, RScriptPath);
				RScriptPB.directory(new File(tempDirectory));
				
				RScriptPB.redirectErrorStream(true);
				Process RScript = RScriptPB.start();
									
				BufferedReader in = new BufferedReader(new InputStreamReader(RScript.getInputStream()));
				
				String RScripStreamOutput = "";
				String line;
				String url = "";
				
				
				while ((line = in.readLine()) != null) {
					 line = line.toLowerCase();											
					 if (line.contains("http")) {
						url = line.substring(line.indexOf("http"));
						break;
					 } else if (line.contains("error")) {
						 System.out.println(RScripStreamOutput);
						 return;
					 }
				}
				
				OpenURL(url);
														
			 } catch (Exception ex) {
					System.out.println(ex);
			 }
		}
		/**
	     * event handler that is invoked when the dataset is changed
	     */				
		public void datasetChanged(DatasetChangeEvent event) {
			
			if (ThreadInDsChanged)				
				return;
			
			synchronized(dsChangedLock) {
				ThreadInDsChanged = true;
				if (!initialized)				
					initWindow();
 				
				int column_count = dataset.getColumnCount();
				int row_count = dataset.getRowCount();
									
				if (column_count == 0 || row_count == 0) {
					ThreadInDsChanged = false;
					return;
				}
								
				ArrayList<ArrayList<Double>> data_new = DatasetToArrayList(dataset);
							
				boolean isdataValid = true;
				int c_rows;
				
				if (data_new.size() >= minNumberOfColumns && data_new.get(0).size()>0) {
					
					c_rows = data_new.get(0).size();
									
					for (ArrayList<Double> column: data_new) {
						if (c_rows != column.size()) {
							isdataValid = false;
							break;
						}
					}	
				} else {
					isdataValid=false;
				}
				
				if (isdataValid) {
					StringBuilder sb = new StringBuilder();
					
					for (ArrayList<Double> column: data) {
						sb.append(column.toString());
					}
					
					String strOld = sb.toString();
					sb.setLength(0);
					for (ArrayList<Double> column: data_new) {
						sb.append(column.toString());
					}
					String strNew = sb.toString();
									
					if (!strOld.equals(strNew)) {
						
						ArrayList<ArrayList<Double>> data_old = data;
						
						data = data_new;
						dataFrameString = ArrayListArrayListToRDATAFRAME(data);
						
						if (!Plot()) {
							data = data_old;
							dataFrameString = ArrayListArrayListToRDATAFRAME(data);
						}						
					}
				}
																								
				ThreadInDsChanged = false;
			}
		}
		/**
	     * ggplot2 function
	     * @param args arguments
	     */			
		public void annotate(String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.annotate);
			po.addAllArguments(args);
			plotOptionList.add(po);
		}
		/**
	     * ggplot2 function
	     * @param args arguments
	     */
		public void annotation_custom(String...args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.annotation_custom);
			po.addAllArguments(args);
			plotOptionList.add(po);
		}
		/**
	     * ggplot2 function
	     * @param args arguments
	     */
		public void coord_cartesian(String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.coord_cartesian);
			po.addAllArguments(args);
			plotOptionList.remove(GGPLOT2COMMAND.coord_cartesian);
			plotOptionList.add(po);
		}
		/**
	     * ggplot2 function
	     * @param args arguments
	     */
		public void coord_fixed(String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.coord_fixed);
			po.addAllArguments(args);
			plotOptionList.remove(GGPLOT2COMMAND.coord_fixed);
			plotOptionList.add(po);
		}
		/**
	     * ggplot2 function
	     * @param args arguments
	     */
		public void coord_flip(String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.coord_flip);
			po.addAllArguments(args);
			plotOptionList.remove(GGPLOT2COMMAND.coord_flip);
			plotOptionList.add(po);
		}
		/**
	     * ggplot2 function
	     * @param args arguments
	     */
		public void coord_map(String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.coord_map);
			po.addAllArguments(args);
			plotOptionList.remove(GGPLOT2COMMAND.coord_map);
			plotOptionList.add(po);
		}
		/**
	     * ggplot2 function
	     * @param args arguments
	     */
		public void coord_polar(String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.coord_polar);
			po.addAllArguments(args);
			plotOptionList.remove(GGPLOT2COMMAND.coord_polar);
			plotOptionList.add(po);
		}
		/**
	     * ggplot2 function
	     * @param args arguments
	     */
		public void coord_quickmap(String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.coord_quickmap);
			po.addAllArguments(args);
			plotOptionList.remove(GGPLOT2COMMAND.coord_quickmap);
			plotOptionList.add(po);
		}
		/**
	     * ggplot2 function
	     * @param args arguments
	     */
		public void coord_trans(String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.coord_trans);
			po.addAllArguments(args);
			plotOptionList.remove(GGPLOT2COMMAND.coord_trans);
			plotOptionList.add(po);
		}
		/**
	     * ggplot2 function
	     * @param args arguments
	     */
		public void expand_limits(String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.expand_limits);
			po.addAllArguments(args);
			plotOptionList.remove(GGPLOT2COMMAND.expand_limits);
			plotOptionList.add(po);
		}
		/**
	     * ggplot2 function
	     * @param args arguments
	     */
		public void facet_grid(String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.facet_grid);
			po.addAllArguments(args);
			plotOptionList.remove(GGPLOT2COMMAND.facet_grid);
			plotOptionList.add(po);
		}
		/**
	     * ggplot2 function
	     * @param args arguments
	     */
		public void facet_wrap(String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.facet_wrap);
			po.addAllArguments(args);
			plotOptionList.remove(GGPLOT2COMMAND.facet_wrap);
			plotOptionList.add(po);
		}
		/**
	     * ggplot2 function
	     * @param args arguments
	     * @return PlotOption instance
	     */
		public PlotOption geom_abline(String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.geom_abline);
			po.addAllArguments(args);
			plotOptionList.add(po);
			SetShowZoomMenu(true);
			return po;
		}
		/**
	     * ggplot2 function
	     * @param Xcolumn column index from dataset to map column to x-values
	     * @param Ycolumn column index from dataset to map column to y-values
	     * @param args arguments
	     * @return PlotOption instance
	     */
		public PlotOption geom_area(int Xcolumn, int Ycolumn, String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.geom_area);
			po.addArgument(String.format("aes(x=x%s,y=x%s)",Xcolumn,Ycolumn));
			po.addAllArguments(args);
			plotOptionList.add(po);
			SetShowZoomMenu(true);
			minNumberOfColumns = Math.max(minNumberOfColumns, Math.max(Xcolumn,Ycolumn));
			return po;
		}
		/**
	     * ggplot2 function
	     * @param column column index from dataset to map column to values
	     * @param args arguments
	     * @return PlotOption instance
	     */
		public PlotOption geom_bar(int column, String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.geom_bar);
			po.addArgument(String.format("aes(x=x%s)",column));
			po.addAllArguments(args);
			plotOptionList.add(po);
			minNumberOfColumns = Math.max(minNumberOfColumns, column);
			return po;
		}
		/**
	     * ggplot2 function
	     * @param Xcolumn column index from dataset to map column to x-values
	     * @param Ycolumn column index from dataset to map column to y-values
	     * @param args arguments
	     * @return PlotOption instance
	     */
		public PlotOption geom_bar(int Xcolumn, int Ycolumn, String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.geom_bar);
			po.addArgument(String.format("aes(x=x%s,y=x%s)",Xcolumn,Ycolumn));
			po.addArgument("stat = 'identity'");
			po.addAllArguments(args);
			plotOptionList.add(po);
			SetShowZoomMenu(true);
			minNumberOfColumns = Math.max(minNumberOfColumns, Math.max(Xcolumn,Ycolumn));
			return po;
		}
		/**
	     * ggplot2 function
	     * @param Xcolumn column index from dataset to map column to x-values
	     * @param Ycolumn column index from dataset to map column to y-values
	     * @param args arguments
	     * @return PlotOption instance
	     */
		public PlotOption geom_bin2d(int Xcolumn, int Ycolumn, String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.geom_bin2d);
			po.addArgument(String.format("aes(x=x%s,y=x%s)",Xcolumn,Ycolumn));
			po.addAllArguments(args);
			plotOptionList.add(po);
			SetShowZoomMenu(true);
			minNumberOfColumns = Math.max(minNumberOfColumns, Math.max(Xcolumn,Ycolumn));
			return po;
		}
		/**
	     * ggplot2 function
	     * @param args arguments
	     * @return PlotOption instance
	     */
		public PlotOption geom_blank(String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.geom_blank);
			po.addAllArguments(args);
			plotOptionList.add(po);
			return po;
		}
		/**
	     * ggplot2 function
	     * @param column column index from dataset to map column to values
	     * @param args arguments
	     * @return PlotOption instance
	     */
		public PlotOption geom_boxplot(int column, String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.geom_boxplot);
			po.addArgument(String.format("aes(x=1,y=x%s)",column));
			po.addAllArguments(args);
			plotOptionList.add(po);
			minNumberOfColumns = Math.max(minNumberOfColumns, column);
			return po;
		}
		/**
	     * ggplot2 function
	     * @param Xcolumn column index from dataset to map column to x-values
	     * @param Ycolumn column index from dataset to map column to y-values
	     * @param args arguments
	     * @return PlotOption instance
	     */
		public PlotOption geom_col(int Xcolumn, int Ycolumn, String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.geom_col);
			po.addArgument(String.format("aes(x=x%s,y=x%s)",Xcolumn,Ycolumn));
			po.addAllArguments(args);
			plotOptionList.add(po);
			SetShowZoomMenu(true);
			minNumberOfColumns = Math.max(minNumberOfColumns, Math.max(Xcolumn,Ycolumn));
			return po;
		}
		/**
	     * ggplot2 function
	     * @param Xcolumn column index from dataset to map column to x-values
	     * @param Ycolumn column index from dataset to map column to y-values
	     * @param Zcolumn column index from dataset to map column to z-values
	     * @param args arguments
	     * @return PlotOption instance
	     */
		public PlotOption geom_contour(int Xcolumn, int Ycolumn, int Zcolumn, String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.geom_contour);
			po.addArgument(String.format("aes(x=x%s,y=x%s,z=x%s)",Xcolumn,Ycolumn,Zcolumn));
			po.addAllArguments(args);
			plotOptionList.add(po);
			minNumberOfColumns = Math.max(minNumberOfColumns, Math.max(Math.max(Xcolumn,Ycolumn),Zcolumn));
			return po;
		}
		/**
	     * ggplot2 function
	     * @param Xcolumn column index from dataset to map column to x-values
	     * @param Ycolumn column index from dataset to map column to y-values
	     * @param args arguments
	     * @return PlotOption instance
	     */
		public PlotOption geom_count(int Xcolumn, int Ycolumn, String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.geom_count);
			po.addArgument(String.format("aes(x=x%s,y=x%s)",Xcolumn,Ycolumn));
			po.addAllArguments(args);
			plotOptionList.add(po);
			SetShowZoomMenu(true);
			minNumberOfColumns = Math.max(minNumberOfColumns, Math.max(Xcolumn,Ycolumn));
			return po;
		}
		/**
	     * ggplot2 function
	     * @param Xcolumn column index from dataset to map column to x-values
	     * @param Ycolumn column index from dataset to map column to y-values
	     * @param Ymin column index from dataset to map column to range x-values
	     * @param Ymax column index from dataset to map column to range y-values
	     * @param args arguments
	     * @return PlotOption instance
	     */
		public PlotOption geom_crossbar(int Xcolumn, int Ycolumn, int Ymin, int Ymax, String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.geom_crossbar);
			po.addArgument(String.format("aes(x=x%s,y=x%s,ymin=x%s,ymax=x%s)",Xcolumn,Ycolumn,Ymin,Ymax));
			po.addAllArguments(args);
			plotOptionList.add(po);
			SetShowZoomMenu(true);
			minNumberOfColumns = Math.max(minNumberOfColumns, Math.max(Xcolumn,Math.max(Ycolumn,Math.max(Ymin,Ymax))));
			return po;
		}
		/**
	     * ggplot2 function
	     * @param Xstart column index from dataset to map column to start x-values
	     * @param Ystart column index from dataset to map column to start y-values
	     * @param Xend column index from dataset to map column to end x-values
	     * @param Yend column index from dataset to map column to end y-values
	     * @param args arguments
	     * @return PlotOption instance
	     */
		public PlotOption geom_curve(int Xstart, int Ystart, int Xend, int Yend, String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.geom_curve);
			po.addArgument(String.format("aes(x=x%s,y=x%s,xend=x%s,yend=x%s)",Xstart,Ystart,Xend,Yend));
			po.addAllArguments(args);
			plotOptionList.add(po);
			SetShowZoomMenu(true);
			minNumberOfColumns = Math.max(minNumberOfColumns, Math.max(Xstart,Math.max(Ystart,Math.max(Xend,Yend))));
			return po;
		}
		/**
	     * ggplot2 function
	     * @param column column index from dataset to map column to values
	     * @param args arguments
	     * @return PlotOption instance
	     */
		public PlotOption geom_density(int column, String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.geom_density);
			po.addArgument(String.format("aes(x=x%s)",column));
			po.addAllArguments(args);
			plotOptionList.add(po);
			minNumberOfColumns = Math.max(minNumberOfColumns, column);
			return po;
		}
		/**
	     * ggplot2 function
	     * @param Xcolumn column index from dataset to map column to x-values
	     * @param Ycolumn column index from dataset to map column to y-values
	     * @param args arguments
	     * @return PlotOption instance
	     */
		public PlotOption geom_density2d(int Xcolumn, int Ycolumn, String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.geom_density2d);
			po.addArgument(String.format("aes(x=x%s,y=x%s)",Xcolumn,Ycolumn));
			po.addAllArguments(args);
			plotOptionList.add(po);
			SetShowZoomMenu(true);
			minNumberOfColumns = Math.max(minNumberOfColumns, Math.max(Xcolumn,Ycolumn));
			return po;
		}
		/**
	     * ggplot2 function
	     * @param column column index from dataset to map column to values
	     * @param args arguments
	     * @return PlotOption instance
	     */
		public PlotOption geom_dotplot(int column, String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.geom_dotplot);
			po.addArgument(String.format("aes(x=x%s)",column));
			po.addAllArguments(args);
			plotOptionList.add(po);
			minNumberOfColumns = Math.max(minNumberOfColumns, column);
			return po;
		}
		/**
	     * ggplot2 function
	     * @param column column index from dataset to map column to values
	     * @param Ymin column index from dataset to map column to range x-values
	     * @param Ymax column index from dataset to map column to range y-values
	     * @param args arguments
	     * @return PlotOption instance
	     */
		public PlotOption geom_errorbar(int column, int Ymin, int Ymax, String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.geom_errorbar);
			po.addArgument(String.format("aes(x=x%s,y=x%s,ymin=x%s,ymax=x%s)",column,Ymin,Ymax));
			po.addAllArguments(args);
			plotOptionList.add(po);
			minNumberOfColumns = Math.max(minNumberOfColumns, Math.max(column,Math.max(Ymin,Ymax)));
			return po;
		}
		/**
	     * ggplot2 function
	     * @param column column index from dataset to map column to values
	     * @param Xmin column index from dataset to map column to range x-values
	     * @param Xmax column index from dataset to map column to range y-values
	     * @param args arguments
	     * @return PlotOption instance
	     */
		public PlotOption geom_errorbarh(int column, int Xmin, int Xmax, String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.geom_errorbarh);
			po.addArgument(String.format("aes(y=x%s,y=x%s,xmin=x%s,xmax=x%s)",column,Xmin,Xmax));
			po.addAllArguments(args);
			plotOptionList.add(po);
			minNumberOfColumns = Math.max(minNumberOfColumns, Math.max(column,Math.max(Xmin,Xmax)));
			return po;
		}
		/**
	     * ggplot2 function
	     * @param column column index from dataset to map column to values
	     * @param args arguments
	     * @return PlotOption instance
	     */
		public PlotOption geom_freqpoly(int column, String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.geom_freqpoly);
			po.addArgument(String.format("aes(x=x%s)",column));
			po.addAllArguments(args);
			plotOptionList.add(po);
			minNumberOfColumns = Math.max(minNumberOfColumns, column);
			return po;
		}
		/**
	     * ggplot2 function
	     * @param Xcolumn column index from dataset to map column to x-values
	     * @param Ycolumn column index from dataset to map column to y-values
	     * @param args arguments
	     * @return PlotOption instance
	     */
		public PlotOption geom_hex(int Xcolumn, int Ycolumn, String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.geom_hex);
			po.addArgument(String.format("aes(x=x%s,y=x%s)",Xcolumn,Ycolumn));
			po.addAllArguments(args);
			plotOptionList.add(po);
			SetShowZoomMenu(true);
			minNumberOfColumns = Math.max(minNumberOfColumns, Math.max(Xcolumn,Ycolumn));
			return po;
		}
		/**
	     * ggplot2 function
	     * @param column column index from dataset to map column to values
	     * @param args arguments
	     * @return PlotOption instance
	     */
		public PlotOption geom_histogram(int column, String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.geom_histogram);
			po.addArgument(String.format("aes(x=x%s)",column));
			po.addAllArguments(args);
			plotOptionList.add(po);
			minNumberOfColumns = Math.max(minNumberOfColumns, column);
			return po;
		}
		/**
	     * ggplot2 function
	     * @param yintercept height to intercept y-axis
	     * @param args arguments
	     * @return PlotOption instance
	     */
		public PlotOption geom_hline(double yintercept, String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.geom_hline);
			po.addArgument(String.format("aes(yintercept=%s)",yintercept));
			po.addAllArguments(args);
			plotOptionList.add(po);
			return po;
		}
		/**
	     * ggplot2 function
	     * @param Xcolumn column index from dataset to map column to x-values
	     * @param Ycolumn column index from dataset to map column to y-values
	     * @param args arguments
	     * @return PlotOption instance
	     */
		public PlotOption geom_jitter(int Xcolumn, int Ycolumn, String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.geom_jitter);
			po.addArgument(String.format("aes(x=x%s,y=x%s)",Xcolumn,Ycolumn));
			po.addAllArguments(args);
			plotOptionList.add(po);
			SetShowZoomMenu(true);
			minNumberOfColumns = Math.max(minNumberOfColumns, Math.max(Xcolumn,Ycolumn));
			return po;
		}
		/**
	     * ggplot2 function
	     * @param args arguments
	     * @return PlotOption instance
	     */
		public PlotOption geom_label(String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.geom_label);
			po.addAllArguments(args);
			plotOptionList.add(po);
			return po;
		}
		/**
	     * ggplot2 function
	     * @param Xcolumn column index from dataset to map column to x-values
	     * @param Ycolumn column index from dataset to map column to y-values
	     * @param labels label text array
	     * @param args arguments
	     * @return PlotOption instance
	     */
		public PlotOption geom_label(int Xcolumn, int Ycolumn, String[] labels, String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.geom_label);
			po.addArgument(String.format("aes(x=x%s,y=x%s,label=c(%s))",Xcolumn,Ycolumn,"'"+String.join("','", labels)+"'"));
			po.addAllArguments(args);
			plotOptionList.add(po);
			return po;
		}
		/**
	     * ggplot2 function
	     * @param x x-coordinate to place the label
	     * @param y y-coordinate to place the label
	     * @param label label text
	     * @param args arguments
	     * @return PlotOption instance
	     */
		public PlotOption geom_label(double x, double y, String label, String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.geom_label);
			po.addArgument(String.format("aes(x=%s,y=%s,label=%s)",x,y,label));
			po.addAllArguments(args);
			plotOptionList.add(po);
			return po;
		}
		/**
	     * ggplot2 function
	     * @param Xcolumn column index from dataset to map column to x-values
	     * @param Ycolumn column index from dataset to map column to y-values
	     * @param labels label text array
	     * @param args arguments
	     * @return PlotOption instance
	     */
		public PlotOption geom_label_repel(int Xcolumn, int Ycolumn, String[] labels, String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.geom_label_repel);
			po.addArgument(String.format("aes(x=x%s,y=x%s,label=c(%s))",Xcolumn,Ycolumn,"'"+String.join("','", labels)+"'"));
			po.addAllArguments(args);
			plotOptionList.add(po);
			return po;
		}
		/**
	     * ggplot2 function
	     * @param Xcolumn column index from dataset to map column to x-values
	     * @param Ycolumn column index from dataset to map column to y-values
	     * @param args arguments
	     * @return PlotOption instance
	     */
		public PlotOption geom_line(int Xcolumn, int Ycolumn, String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.geom_line);
			po.addArgument(String.format("aes(x=x%s,y=x%s)",Xcolumn,Ycolumn));
			po.addAllArguments(args);
			plotOptionList.add(po);
			SetShowZoomMenu(true);
			minNumberOfColumns = Math.max(minNumberOfColumns, Math.max(Xcolumn,Ycolumn));
			return po;
		}
		/**
	     * ggplot2 function
	     * @param column column index from dataset to map column to values
	     * @param Ymin column index from dataset to map column to line range x-values
	     * @param Ymax column index from dataset to map column to line range y-values
	     * @param args arguments
	     * @return PlotOption instance
	     */
		public PlotOption geom_linerange(int column, int Ymin, int Ymax, String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.geom_linerange);
			po.addArgument(String.format("aes(x=x%s,ymin=x%s,ymax=x%s)",column,Ymin,Ymax));
			po.addAllArguments(args);
			plotOptionList.add(po);
			minNumberOfColumns = Math.max(minNumberOfColumns, Math.max(column,Math.max(Ymin,Ymax)));
			return po;
		}
		/**
	     * ggplot2 function
	     * @param args arguments
	     * @return PlotOption instance
	     */
		public PlotOption geom_map(String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.geom_map);
			po.addAllArguments(args);
			plotOptionList.add(po);
			return po;
		}
		/**
	     * ggplot2 function
	     * @param Xcolumn column index from dataset to map column to x-values
	     * @param Ycolumn column index from dataset to map column to y-values
	     * @param args arguments
	     * @return PlotOption instance
	     */
		public PlotOption geom_path(int Xcolumn, int Ycolumn, String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.geom_path);
			po.addArgument(String.format("aes(x=x%s,y=x%s)",Xcolumn,Ycolumn));
			po.addAllArguments(args);
			plotOptionList.add(po);
			SetShowZoomMenu(true);
			minNumberOfColumns = Math.max(minNumberOfColumns, Math.max(Xcolumn,Ycolumn));
			return po;
		}
		/**
	     * ggplot2 function
	     * @param Xcolumn column index from dataset to map column to x-values
	     * @param Ycolumn column index from dataset to map column to y-values
	     * @param args arguments
	     * @return PlotOption instance
	     */
		public PlotOption geom_point(int Xcolumn, int Ycolumn, String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.geom_point);
			po.addArgument(String.format("aes(x=x%s,y=x%s)",Xcolumn,Ycolumn));
			po.addAllArguments(args);
			plotOptionList.add(po);
			SetShowZoomMenu(true);
			minNumberOfColumns = Math.max(minNumberOfColumns, Math.max(Xcolumn,Ycolumn));
			return po;
		}
		/**
	     * ggplot2 function
	     * @param Xcolumn column index from dataset to map column to x-values
	     * @param Ycolumn column index from dataset to map column to y-values
	     * @param Ymin column index from dataset to map column to range x-values
	     * @param Ymax column index from dataset to map column to range x-values
	     * @param args arguments
	     * @return PlotOption instance
	     */
		public PlotOption geom_pointrange(int Xcolumn, int Ycolumn, int Ymin, int Ymax, String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.geom_pointrange);
			po.addArgument(String.format("aes(x=x%s,y=x%s,ymin=x%s,ymax=x%s)",Xcolumn,Ycolumn,Ymin,Ymax));
			po.addAllArguments(args);
			plotOptionList.add(po);
			SetShowZoomMenu(true);
			minNumberOfColumns = Math.max(minNumberOfColumns, Math.max(Xcolumn,Math.max(Ycolumn,Math.max(Ymin,Ymax))));
			return po;
		}
		/**
	     * ggplot2 function
	     * @param Xcolumn column index from dataset to map column to x-values
	     * @param Ycolumn column index from dataset to map column to y-values
	     * @param args arguments
	     * @return PlotOption instance
	     */
		public PlotOption geom_polygon(int Xcolumn, int Ycolumn, String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.geom_polygon);
			po.addArgument(String.format("aes(x=x%s,y=x%s)",Xcolumn,Ycolumn));
			po.addAllArguments(args);
			plotOptionList.add(po);
			SetShowZoomMenu(true);
			minNumberOfColumns = Math.max(minNumberOfColumns, Math.max(Xcolumn,Ycolumn));
			return po;
		}
		/**
	     * ggplot2 function
	     * @param column column index from dataset to map column to values
	     * @param args arguments
	     * @return PlotOption instance
	     */
		public PlotOption geom_qq(int column, String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.geom_qq);
			po.addArgument(String.format("aes(sample=x%s)",column));
			po.addAllArguments(args);
			plotOptionList.add(po);
			minNumberOfColumns = Math.max(minNumberOfColumns, column);
			return po;
		}
		/**
	     * ggplot2 function
	     * @param Xcolumn column index from dataset to map column to x-values
	     * @param Ycolumn column index from dataset to map column to y-values
	     * @param args arguments
	     * @return PlotOption instance
	     */
		public PlotOption geom_quantile(int Xcolumn, int Ycolumn, String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.geom_quantile);
			po.addArgument(String.format("aes(x=x%s,y=x%s)",Xcolumn,Ycolumn));
			po.addAllArguments(args);
			plotOptionList.add(po);
			SetShowZoomMenu(true);
			minNumberOfColumns = Math.max(minNumberOfColumns, Math.max(Xcolumn,Ycolumn));
			return po;
		}
		/**
	     * ggplot2 function
	     * @param Xcolumn column index from dataset to map column to x-values
	     * @param Ycolumn column index from dataset to map column to y-values
	     * @param args arguments
	     * @return PlotOption instance
	     */
		public PlotOption geom_raster(int Xcolumn, int Ycolumn, String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.geom_raster);
			po.addArgument(String.format("aes(x=x%s,y=x%s)",Xcolumn,Ycolumn));
			po.addAllArguments(args);
			plotOptionList.add(po);
			SetShowZoomMenu(true);
			minNumberOfColumns = Math.max(minNumberOfColumns, Math.max(Xcolumn,Ycolumn));
			return po;
		}
		/**
	     * ggplot2 function
	     * @param xmin min x-value of rectangle
	     * @param xmax max x-value of rectangle
	     * @param ymin min y-value of rectangle
	     * @param ymax max y-value of rectangle
	     * @param args arguments
	     * @return PlotOption instance
	     */
		public PlotOption geom_rect(double xmin, double xmax, double ymin, double ymax, String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.geom_rect);
			po.addArgument(String.format("aes(ymin=%s,xmax=x%s,ymin=%s,ymax=%s)",xmin,xmax,ymin,ymax));
			po.addAllArguments(args);
			plotOptionList.add(po);
			return po;
		}
		/**
	     * ggplot2 function
	     * @param column column index from dataset to map column to values
	     * @param Ymin column index from dataset to map column to range x-values
	     * @param Ymax column index from dataset to map column to range x-values
	     * @param args arguments
	     * @return PlotOption instance
	     */
		public PlotOption geom_ribbon(int column, int Ymin, int Ymax, String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.geom_ribbon);
			po.addArgument(String.format("aes(x=x%s,ymin=%s,ymax=%s)",column,Ymin,Ymax));
			po.addAllArguments(args);
			plotOptionList.add(po);
			minNumberOfColumns = Math.max(minNumberOfColumns,Math.max(column,Math.max(Ymin,Ymax)));
			return po;
		}
		/**
	     * ggplot2 function
	     * @param args arguments
	     * @return PlotOption instance
	     */
		public PlotOption geom_rug(String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.geom_rug);
			po.addAllArguments(args);
			plotOptionList.add(po);
			return po;
		}
		/**
	     * ggplot2 function
	     * @param column column index from dataset to map column to values
	     * @param args arguments
	     * @return PlotOption instance
	     */
		public PlotOption geom_rug(int column, String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.geom_rug);
			po.addArgument(String.format("aes(x=x%s)",column));
			po.addAllArguments(args);
			plotOptionList.add(po);
			minNumberOfColumns = Math.max(minNumberOfColumns, column);
			return po;
		}
		/**
	     * ggplot2 function
	     * @param Xcolumn column index from dataset to map column to x-values
	     * @param Ycolumn column index from dataset to map column to y-values
	     * @param args arguments
	     * @return PlotOption instance
	     */
		public PlotOption geom_rug(int Xcolumn, int Ycolumn, String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.geom_rug);
			po.addArgument(String.format("aes(x=x%s,y=x%s)",Xcolumn,Ycolumn));
			po.addAllArguments(args);
			plotOptionList.add(po);
			SetShowZoomMenu(true);
			minNumberOfColumns = Math.max(minNumberOfColumns, Math.max(Xcolumn,Ycolumn));
			return po;
		}
		/**
	     * ggplot2 function
	     * @param Xstart column index from dataset to map column to start x-values
	     * @param Ystart column index from dataset to map column to start y-values
	     * @param Xend column index from dataset to map column to end x-values
	     * @param Yend column index from dataset to map column to end y-values
	     * @param args arguments
	     * @return PlotOption instance
	     */
		public PlotOption geom_segment(int Xstart, int Ystart, int Xend, int Yend, String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.geom_segment);
			po.addArgument(String.format("aes(x=x%s,y=x%s,xend=x%s,yend=x%s)",Xstart,Ystart,Xend,Yend));
			po.addAllArguments(args);
			plotOptionList.add(po);
			SetShowZoomMenu(true);
			minNumberOfColumns = Math.max(minNumberOfColumns, Math.max(Xstart,Math.max(Ystart,Math.max(Xend,Yend))));
			return po;
		}
		/**
	     * ggplot2 function
	     * @param Xcolumn column index from dataset to map column to x-values
	     * @param Ycolumn column index from dataset to map column to y-values
	     * @param args arguments
	     * @return PlotOption instance
	     */
		public PlotOption geom_smooth(int Xcolumn, int Ycolumn, String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.geom_smooth);
			po.addArgument(String.format("aes(x=x%s,y=x%s)",Xcolumn,Ycolumn));
			po.addAllArguments(args);
			plotOptionList.add(po);
			SetShowZoomMenu(true);
			minNumberOfColumns = Math.max(minNumberOfColumns, Math.max(Xcolumn,Ycolumn));
			return po;
		}
		/**
	     * ggplot2 function
	     * @param Xcolumn column index from dataset to map column to x-values
	     * @param Ycolumn column index from dataset to map column to y-values
	     * @param angle column index from dataset to map angle to data points
	     * @param radius column index from dataset to map radius to data points
	     * @param args arguments
	     * @return PlotOption instance
	     */
		public PlotOption geom_spoke(int Xcolumn, int Ycolumn, int angle, int radius, String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.geom_spoke);
			po.addArgument(String.format("aes(x=x%s,y=x%s,angle=x%s,radius=x%s)",Xcolumn,Ycolumn,angle,radius));
			po.addAllArguments(args);
			plotOptionList.add(po);
			SetShowZoomMenu(true);
			minNumberOfColumns = Math.max(minNumberOfColumns, Math.max(Xcolumn,Ycolumn));
			return po;
		}
		/**
	     * ggplot2 function
	     * @param Xcolumn column index from dataset to map column to x-values
	     * @param Ycolumn column index from dataset to map column to y-values
	     * @param args arguments
	     * @return PlotOption instance
	     */
		public PlotOption geom_step(int Xcolumn, int Ycolumn, String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.geom_step);
			po.addArgument(String.format("aes(x=x%s,y=x%s)",Xcolumn,Ycolumn));
			po.addAllArguments(args);
			plotOptionList.add(po);
			SetShowZoomMenu(true);
			minNumberOfColumns = Math.max(minNumberOfColumns, Math.max(Xcolumn,Ycolumn));
			return po;
		}
		/**
	     * ggplot2 function
	     * @param x x-coordinate to place the label
	     * @param y y-coordinate to place the label
	     * @param text text
	     * @param args arguments
	     * @return PlotOption instance
	     */
		public PlotOption geom_text(double x, double y, String text, String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.geom_text);
			po.addArgument(String.format("aes(x=x%s,y=x%s,label=%s)",x,y,text));
			po.addAllArguments(args);
			plotOptionList.add(po);
			return po;
		}
		/**
	     * ggplot2 function
	     * @param Xcolumn column index from dataset to map column to x-values
	     * @param Ycolumn column index from dataset to map column to y-values
	     * @param args arguments
	     * @return PlotOption instance
	     */
		public PlotOption geom_tile(int Xcolumn, int Ycolumn, String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.geom_tile);
			po.addArgument(String.format("aes(x=x%s,y=x%s)",Xcolumn,Ycolumn));
			po.addAllArguments(args);
			plotOptionList.add(po);
			SetShowZoomMenu(true);
			minNumberOfColumns = Math.max(minNumberOfColumns, Math.max(Xcolumn,Ycolumn));
			return po;
		}
		/**
	     * ggplot2 function
	     * @param Xcolumn column index from dataset to map column to x-values
	     * @param Ycolumn column index from dataset to map column to y-values
	     * @param args arguments
	     * @return PlotOption instance
	     */
		public PlotOption geom_violin(int Xcolumn, int Ycolumn, String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.geom_violin);
			po.addArgument(String.format("aes(x=x%s,y=x%s)",Xcolumn,Ycolumn));
			po.addAllArguments(args);
			plotOptionList.add(po);
			SetShowZoomMenu(true);
			minNumberOfColumns = Math.max(minNumberOfColumns, Math.max(Xcolumn,Ycolumn));
			return po;
		}
		/**
	     * ggplot2 function
	     * @param xintercept width to intercept x-axis
	     * @param args arguments
	     * @return PlotOption instance
	     */
		public PlotOption geom_vline(double xintercept, String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.geom_vline);
			po.addArgument(String.format("aes(xintercept=%s)",xintercept));
			po.addAllArguments(args);
			plotOptionList.add(po);
			return po;
		}
		/**
	     * ggplot2 function
	     * @param args arguments
	     */
		public void ggtitle(String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.ggtitle);
			po.addAllArguments(args);
			plotOptionList.remove(GGPLOT2COMMAND.ggtitle);
			plotOptionList.add(po);
		}
		/**
	     * ggplot2 function
	     * @param args arguments
	     */
		public void guides(String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.guides);
			po.addAllArguments(args);
			plotOptionList.remove(GGPLOT2COMMAND.guides);
			plotOptionList.add(po);
		}
		/**
	     * ggplot2 function
	     * @param args arguments
	     */
		public void labs(String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.labs);
			po.addAllArguments(args);
			plotOptionList.remove(GGPLOT2COMMAND.labs);
			plotOptionList.add(po);
		}
		/**
	     * ggplot2 function
	     * @param args arguments
	     */
		public void scale_color_manual(String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.scale_color_manual);
			po.addAllArguments(args);
			plotOptionList.remove(GGPLOT2COMMAND.scale_color_manual);
			plotOptionList.add(po);
		}
		/**
	     * ggplot2 function
	     * @param args arguments
	     */
		public void scale_linetype_manual(String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.scale_linetype_manual);
			po.addAllArguments(args);
			plotOptionList.remove(GGPLOT2COMMAND.scale_linetype_manual);
			plotOptionList.add(po);
		}
		/**
	     * ggplot2 function
	     * @param args arguments
	     */
		public void scale_fill_brewer(String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.scale_fill_brewer);
			po.addAllArguments(args);
			plotOptionList.remove(GGPLOT2COMMAND.scale_fill_brewer);
			plotOptionList.add(po);
		}
		/**
	     * ggplot2 function
	     * @param args arguments
	     */
		public void scale_fill_continuous(String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.scale_fill_continuous);
			po.addAllArguments(args);
			plotOptionList.remove(GGPLOT2COMMAND.scale_fill_continuous);
			plotOptionList.add(po);
		}
		/**
	     * ggplot2 function
	     * @param args arguments
	     */
		public void scale_fill_discrete(String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.scale_fill_discrete);
			po.addAllArguments(args);
			plotOptionList.remove(GGPLOT2COMMAND.scale_fill_discrete);
			plotOptionList.add(po);
		}
		/**
	     * ggplot2 function
	     * @param args arguments
	     */
		public void scale_fill_distiller(String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.scale_fill_distiller);
			po.addAllArguments(args);
			plotOptionList.remove(GGPLOT2COMMAND.scale_fill_distiller);
			plotOptionList.add(po);
		}
		/**
	     * ggplot2 function
	     * @param args arguments
	     */
		public void scale_fill_gradient(String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.scale_fill_brewer);
			po.addAllArguments(args);
			plotOptionList.remove(GGPLOT2COMMAND.scale_fill_brewer);
			plotOptionList.add(po);
		}
		/**
	     * ggplot2 function
	     * @param args arguments
	     */
		public void scale_fill_gradient2(String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.scale_fill_gradient2);
			po.addAllArguments(args);
			plotOptionList.remove(GGPLOT2COMMAND.scale_fill_gradient2);
			plotOptionList.add(po);
		}
		/**
	     * ggplot2 function
	     * @param args arguments
	     */
		public void scale_fill_gradientn(String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.scale_fill_gradientn);
			po.addAllArguments(args);
			plotOptionList.remove(GGPLOT2COMMAND.scale_fill_gradientn);
			plotOptionList.add(po);
		}
		/**
	     * ggplot2 function
	     * @param args arguments
	     */
		public void scale_fill_manual(String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.scale_fill_manual);
			po.addAllArguments(args);
			plotOptionList.remove(GGPLOT2COMMAND.scale_fill_manual);
			plotOptionList.add(po);
		}
		/**
	     * ggplot2 function
	     * @param args arguments
	     */
		public void scale_radius(String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.scale_radius);
			po.addAllArguments(args);
			plotOptionList.remove(GGPLOT2COMMAND.scale_radius);
			plotOptionList.add(po);
		}
		/**
	     * ggplot2 function
	     * @param args arguments
	     */
		public void scale_shape(String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.scale_shape);
			po.addAllArguments(args);
			plotOptionList.remove(GGPLOT2COMMAND.scale_shape);
			plotOptionList.add(po);
		}
		/**
	     * ggplot2 function
	     * @param args arguments
	     */
		public void scale_shape_manual(String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.scale_shape_manual);
			po.addAllArguments(args);
			plotOptionList.remove(GGPLOT2COMMAND.scale_shape_manual);
			plotOptionList.add(po);
		}
		/**
	     * ggplot2 function
	     * @param args arguments
	     */
		public void scale_size(String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.scale_size);
			po.addAllArguments(args);
			plotOptionList.remove(GGPLOT2COMMAND.scale_size);
			plotOptionList.add(po);
		}
		/**
	     * ggplot2 function
	     * @param args arguments
	     */
		public void scale_size_area(String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.scale_size_area);
			po.addAllArguments(args);
			plotOptionList.remove(GGPLOT2COMMAND.scale_size_area);
			plotOptionList.add(po);
		}
		/**
	     * ggplot2 function
	     * @param args arguments
	     */
		public void scale_x_continuous(String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.scale_x_continuous);
			po.addAllArguments(args);
			plotOptionList.remove(GGPLOT2COMMAND.scale_x_continuous);
			plotOptionList.add(po);
		}
		/**
	     * ggplot2 function
	     * @param args arguments
	     */
		public void scale_x_date(String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.scale_x_date);
			po.addAllArguments(args);
			plotOptionList.remove(GGPLOT2COMMAND.scale_x_date);
			plotOptionList.add(po);
		}
		/**
	     * ggplot2 function
	     * @param args arguments
	     */
		public void scale_x_datetime(String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.scale_x_datetime);
			po.addAllArguments(args);
			plotOptionList.remove(GGPLOT2COMMAND.scale_x_datetime);
			plotOptionList.add(po);
		}
		/**
	     * ggplot2 function
	     * @param args arguments
	     */
		public void scale_x_discrete(String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.scale_x_discrete);
			po.addAllArguments(args);
			plotOptionList.remove(GGPLOT2COMMAND.scale_x_discrete);
			plotOptionList.add(po);
		}
		/**
	     * ggplot2 function
	     * @param args arguments
	     */
		public void scale_x_identity(String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.scale_x_identity);
			po.addAllArguments(args);
			plotOptionList.remove(GGPLOT2COMMAND.scale_x_identity);
			plotOptionList.add(po);
		}
		/**
	     * ggplot2 function
	     * @param args arguments
	     */
		public void scale_x_log10(String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.scale_x_log10);
			po.addAllArguments(args);
			plotOptionList.remove(GGPLOT2COMMAND.scale_x_log10);
			plotOptionList.add(po);
		}
		/**
	     * ggplot2 function
	     * @param args arguments
	     */
		public void scale_x_manual(String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.scale_x_manual);
			po.addAllArguments(args);
			plotOptionList.remove(GGPLOT2COMMAND.scale_x_manual);
			plotOptionList.add(po);
		}
		/**
	     * ggplot2 function
	     * @param args arguments
	     */
		public void scale_x_reverse(String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.scale_x_reverse);
			po.addAllArguments(args);
			plotOptionList.remove(GGPLOT2COMMAND.scale_x_reverse);
			plotOptionList.add(po);
		}
		/**
	     * ggplot2 function
	     * @param args arguments
	     */
		public void scale_x_sqrt(String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.scale_x_sqrt);
			po.addAllArguments(args);
			plotOptionList.remove(GGPLOT2COMMAND.scale_x_sqrt);
			plotOptionList.add(po);
		}
		/**
	     * ggplot2 function
	     * @param args arguments
	     */
		public void scale_y_continuous(String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.scale_y_continuous);
			po.addAllArguments(args);
			plotOptionList.remove(GGPLOT2COMMAND.scale_y_continuous);
			plotOptionList.add(po);
		}
		/**
	     * ggplot2 function
	     * @param args arguments
	     */
		public void scale_y_date(String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.scale_y_date);
			po.addAllArguments(args);
			plotOptionList.remove(GGPLOT2COMMAND.scale_y_date);
			plotOptionList.add(po);
		}
		/**
	     * ggplot2 function
	     * @param args arguments
	     */
		public void scale_y_datetime(String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.scale_y_datetime);
			po.addAllArguments(args);
			plotOptionList.remove(GGPLOT2COMMAND.scale_y_datetime);
			plotOptionList.add(po);
		}
		/**
	     * ggplot2 function
	     * @param args arguments
	     */
		public void scale_y_discrete(String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.scale_y_discrete);
			po.addAllArguments(args);
			plotOptionList.remove(GGPLOT2COMMAND.scale_y_discrete);
			plotOptionList.add(po);
		}
		/**
	     * ggplot2 function
	     * @param args arguments
	     */
		public void scale_y_identity(String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.scale_y_identity);
			po.addAllArguments(args);
			plotOptionList.remove(GGPLOT2COMMAND.scale_y_identity);
			plotOptionList.add(po);
		}
		/**
	     * ggplot2 function
	     * @param args arguments
	     */
		public void scale_y_log10(String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.scale_y_log10);
			po.addAllArguments(args);
			plotOptionList.remove(GGPLOT2COMMAND.scale_y_log10);
			plotOptionList.add(po);
		}
		/**
	     * ggplot2 function
	     * @param args arguments
	     */
		public void scale_y_manual(String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.scale_y_manual);
			po.addAllArguments(args);
			plotOptionList.remove(GGPLOT2COMMAND.scale_y_manual);
			plotOptionList.add(po);
		}
		/**
	     * ggplot2 function
	     * @param args arguments
	     */
		public void scale_y_reverse(String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.scale_y_reverse);
			po.addAllArguments(args);
			plotOptionList.remove(GGPLOT2COMMAND.scale_y_reverse);
			plotOptionList.add(po);
		}
		/**
	     * ggplot2 function
	     * @param args arguments
	     */
		public void scale_y_sqrt(String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.scale_y_sqrt);
			po.addAllArguments(args);
			plotOptionList.remove(GGPLOT2COMMAND.scale_y_sqrt);
			plotOptionList.add(po);
		}
		/**
	     * ggplot2 function
	     * @param args arguments
	     */
		public void theme(String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.theme);
			po.addAllArguments(args);
			plotOptionList.remove(GGPLOT2COMMAND.theme);
			plotOptionList.add(po);
		}
		/**
	     * ggplot2 function
	     * @param args arguments
	     */
		public void theme_bw(String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.theme_bw);
			po.addAllArguments(args);
			plotOptionList.remove(GGPLOT2COMMAND.theme_bw);
			plotOptionList.add(po);
		}
		/**
	     * ggplot2 function
	     * @param args arguments
	     */
		public void theme_classic(String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.theme_classic);
			po.addAllArguments(args);
			plotOptionList.remove(GGPLOT2COMMAND.theme_classic);
			plotOptionList.add(po);
		}
		/**
	     * ggplot2 function
	     * @param args arguments
	     */
		public void theme_dark(String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.theme_dark);
			po.addAllArguments(args);
			plotOptionList.remove(GGPLOT2COMMAND.theme_dark);
			plotOptionList.add(po);
		}
		/**
	     * ggplot2 function
	     * @param args arguments
	     */
		public void theme_grey(String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.theme_grey);
			po.addAllArguments(args);
			plotOptionList.remove(GGPLOT2COMMAND.theme_grey);
			plotOptionList.add(po);
		}
		/**
	     * ggplot2 function
	     * @param args arguments
	     */
		public void theme_light(String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.theme_light);
			po.addAllArguments(args);
			plotOptionList.remove(GGPLOT2COMMAND.theme_light);
			plotOptionList.add(po);
		}
		/**
	     * ggplot2 function
	     * @param args arguments
	     */
		public void theme_linedraw(String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.theme_linedraw);
			po.addAllArguments(args);
			plotOptionList.remove(GGPLOT2COMMAND.theme_linedraw);
			plotOptionList.add(po);
		}
		/**
	     * ggplot2 function
	     * @param args arguments
	     */
		public void theme_minimal(String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.theme_minimal);
			po.addAllArguments(args);
			plotOptionList.remove(GGPLOT2COMMAND.theme_minimal);
			plotOptionList.add(po);
		}
		/**
	     * ggplot2 function
	     * @param args arguments
	     */
		public void theme_void(String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.theme_void);
			po.addAllArguments(args);
			plotOptionList.remove(GGPLOT2COMMAND.theme_void);
			plotOptionList.add(po);
		}
		/**
	     * ggplot2 function
	     * @param args arguments
	     */
		public void xlab(String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.xlab);
			po.addAllArguments(args);
			plotOptionList.remove(GGPLOT2COMMAND.xlab);
			plotOptionList.add(po);
		}
		/**
	     * ggplot2 function
	     * @param args arguments
	     */
		public void xlim(String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.xlim);
			po.addAllArguments(args);
			plotOptionList.remove(GGPLOT2COMMAND.xlim);
			plotOptionList.add(po);
		}
		/**
	     * ggplot2 function
	     * @param args arguments
	     */
		public void ylab(String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.ylab);
			po.addAllArguments(args);
			plotOptionList.remove(GGPLOT2COMMAND.ylab);
			plotOptionList.add(po);
		}
		/**
	     * ggplot2 function
	     * @param args arguments
	     */
		public void ylim(String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.ylim);
			po.addAllArguments(args);
			plotOptionList.remove(GGPLOT2COMMAND.ylim);
			plotOptionList.add(po);
		}
		/**
	     * ggplot2 function
	     * @param args arguments
	     */
		public void zlab(String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.zlab);
			po.addAllArguments(args);
			plotOptionList.remove(GGPLOT2COMMAND.zlab);
			plotOptionList.add(po);
		}
		/**
	     * ggplot2 function
	     * @param args arguments
	     */
		public void zlim(String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(GGPLOT2COMMAND.zlim);
			po.addAllArguments(args);
			plotOptionList.remove(GGPLOT2COMMAND.zlim);
			plotOptionList.add(po);
		}
		/**
	     * ggplot2 function
	     * @param function function name
	     * @param args arguments
	     */
		public void ggplot2_function(String function, String... args) {
			PlotOption po = new PlotOption();
			po.setCommand(function);
			po.addAllArguments(args);
			plotOptionList.add(po);
		}
		/**
	     * plot table
	     */
		public void Table() {
			plotOptionList.clear();
			PlotOption po = new PlotOption(false);
			po.setCommand("grid.table");
			po.addArgument("df");
			plotOptionList.add(po);
		}
		/**
	     * plot table
	     * @param colNames array of column names
	     */
		public void Table(String... colNames) {
			plotOptionList.clear();
			for (int i=0; i<colNames.length; i++) {
				PlotOption po = new PlotOption(false);
				po.setCommand("try");
				po.addArgument(String.format("colnames(df)[%s]<-'%s',silent=TRUE", i+1, colNames[i]));
				plotOptionList.add(po);
			}
			TablePlotColNames = colNames;
			PlotOption po = new PlotOption(false);
			po.setCommand("grid.table");
			po.addArgument("df");
			plotOptionList.add(po);
		}
		/**
	     * plot table
	     * @param colNames array of column names
	     * @param rowNames array of row names
	     */
		public void Table(String[] colNames, String[] rowNames) {
			plotOptionList.clear();
			for (int i=0; i<colNames.length; i++) {
				PlotOption po = new PlotOption(false);
				po.setCommand("try");
				po.addArgument(String.format("colnames(df)[%s]<-'%s',silent=TRUE", i+1, colNames[i]));
				plotOptionList.add(po);
			}
			for (int i=0; i<rowNames.length; i++) {
				PlotOption po = new PlotOption(false);
				po.setCommand("try");
				po.addArgument(String.format("rownames(df)[%s]<-'%s',silent=TRUE", i+1, rowNames[i]));
				plotOptionList.add(po);
			}
			TablePlotColNames = colNames;
			PlotOption po = new PlotOption(false);
			po.setCommand("grid.table");
			po.addArgument("df");
			plotOptionList.add(po);
		}
		/**
	     * 3D scatter plot
	     * @param Xcolumn column index from dataset to map column to x-values
	     * @param Ycolumn column index from dataset to map column to y-values
	     * @param Zcolumn column index from dataset to map column to z-values
	     */
		public void ScatterPlot3D(int Xcolumn, int Ycolumn, int Zcolumn) {
			plotOptionList.clear();
			SetShowZoomMenu(false);
			PlotOption po = new PlotOption(false);
			po.setCommand("scatterplot3d");
			po.addArgument(String.format("df[,%s],df[,%s],df[,%s]", Xcolumn, Ycolumn, Zcolumn));
			po.addArgument("xlab ='x'");
			po.addArgument("ylab ='y'");
			po.addArgument("zlab ='z'");
			plotOptionList.add(po);
			minNumberOfColumns = Math.max(minNumberOfColumns, Math.max(Math.max(Xcolumn,Ycolumn),Zcolumn));
		}
		/**
	     * 3D scatter plot
	     * @param Xcolumn column index from dataset to map column to x-values
	     * @param Ycolumn column index from dataset to map column to y-values
	     * @param Zcolumn column index from dataset to map column to z-values
	     * @param Xlabel x-axis label
	     * @param Ylabel y-axis label
	     * @param Zlabel z-axis label
	     */
		public void ScatterPlot3D(int Xcolumn, int Ycolumn, int Zcolumn, String Xlabel, String Ylabel, String Zlabel) {
			plotOptionList.clear();
			SetShowZoomMenu(false);
			PlotOption po = new PlotOption(false);
			po.setCommand("scatterplot3d");
			po.addArgument(String.format("df[,%s],df[,%s],df[,%s]", Xcolumn, Ycolumn, Zcolumn));
			po.addArgument(String.format("xlab ='%s'",Xlabel));
			po.addArgument(String.format("ylab ='%s'",Ylabel));
			po.addArgument(String.format("zlab ='%s'",Zlabel));
			plotOptionList.add(po);
			minNumberOfColumns = Math.max(minNumberOfColumns, Math.max(Math.max(Xcolumn,Ycolumn),Zcolumn));
		}
		/**
	     * 3D line plot
	     * @param Xcolumn column index from dataset to map column to x-values
	     * @param Ycolumn column index from dataset to map column to y-values
	     * @param Zcolumn column index from dataset to map column to z-values
	     */
		public void LinePlot3D(int Xcolumn, int Ycolumn, int Zcolumn) {
			plotOptionList.clear();
			SetShowZoomMenu(false);
			PlotOption po = new PlotOption(false);
			po.setCommand("scatter3D");
			po.addArgument(String.format("df[,%s],df[,%s],df[,%s]", Xcolumn, Ycolumn, Zcolumn));
			po.addArgument("type = 'l'");
			plotOptionList.add(po);
			minNumberOfColumns = Math.max(minNumberOfColumns, Math.max(Math.max(Xcolumn,Ycolumn),Zcolumn));
		}
		/**
	     * 3D line plot
	     * @param Xcolumn column index from dataset to map column to x-values
	     * @param Ycolumn column index from dataset to map column to y-values
	     * @param Zcolumn column index from dataset to map column to z-values
	     * @param Xlabel x-axis label
	     * @param Ylabel y-axis label
	     * @param Zlabel z-axis label
	     */
		public void LinePlot3D(int Xcolumn, int Ycolumn, int Zcolumn, String Xlabel, String Ylabel, String Zlabel) {
			plotOptionList.clear();
			SetShowZoomMenu(false);
			PlotOption po = new PlotOption(false);
			po.setCommand("scatter3D");
			po.addArgument(String.format("df[,%s],df[,%s],df[,%s]", Xcolumn, Ycolumn, Zcolumn));
			po.addArgument("type = 'l'");
			po.addArgument(String.format("xlab ='%s'",Xlabel));
			po.addArgument(String.format("ylab ='%s'",Ylabel));
			po.addArgument(String.format("zlab ='%s'",Zlabel));
			plotOptionList.add(po);
			minNumberOfColumns = Math.max(minNumberOfColumns, Math.max(Math.max(Xcolumn,Ycolumn),Zcolumn));
		}
		/**
	     * heat map plot
	     * @param Xcolumn column index from dataset to map column to x-values
	     * @param Ycolumn column index from dataset to map column to y-values
	     * @param HeatColumn column index from dataset to map column to heat values
	     * @param resolution interpolation resolution
	     */
		public void HeatMap(int Xcolumn, int Ycolumn, int HeatColumn, double resolution) {
			plotOptionList.clear();
			SetShowZoomMenu(false);
			PlotOption po = new PlotOption(false);
			po.setCommand("filled.contour");
			po.addArgument(String.format("interp(x=df$x%s, y=df$x%s, z=df$x%s, xo=seq(min(df$x%s),max(df$x%s),by=%s), yo=seq(min(df$x%s),max(df$x%s),by=%s), duplicate='mean')",Xcolumn, Ycolumn, HeatColumn,Xcolumn,Xcolumn,resolution,Ycolumn,Ycolumn,resolution));
			plotOptionList.add(po);
			minNumberOfColumns = Math.max(minNumberOfColumns, Math.max(Math.max(Xcolumn,Ycolumn),HeatColumn));
		}
		/**
	     * heat map plot
	     * @param Xcolumn column index from dataset to map column to x-values
	     * @param Ycolumn column index from dataset to map column to y-values
	     * @param HeatColumn column index from dataset to map column to heat values
	     * @param resolution interpolation resolution
	     * @param Xlabel x-axis label
	     * @param Ylabel y-axis label
	     * @param Heatlabel heat values label
	     */
		public void HeatMap(int Xcolumn, int Ycolumn, int HeatColumn, double resolution, String Xlabel, String Ylabel, String Heatlabel, String title) {
			plotOptionList.clear();
			SetShowZoomMenu(false);
			PlotOption po = new PlotOption(false);
			po.setCommand("filled.contour");
			po.addArgument(String.format("interp(x=df$x%s, y=df$x%s, z=df$x%s, xo=seq(min(df$x%s),max(df$x%s),by=%s), yo=seq(min(df$x%s),max(df$x%s),by=%s), duplicate='mean')",Xcolumn, Ycolumn, HeatColumn,Xcolumn,Xcolumn,resolution,Ycolumn,Ycolumn,resolution));
			po.addArgument(String.format("plot.title={par(cex.main=1);title(main='%s',xlab='%s',ylab='%s')}",title,Xlabel,Ylabel));
			po.addArgument(String.format("key.title=title(expression('%s'))",Heatlabel));
			plotOptionList.add(po);
			minNumberOfColumns = Math.max(minNumberOfColumns, Math.max(Math.max(Xcolumn,Ycolumn),HeatColumn));
		}
		/**
	     * heat map plot
	     * @param Xcolumn column index from dataset to map column to x-values
	     * @param Ycolumn column index from dataset to map column to y-values
	     * @param HeatColumn column index from dataset to map column to heat values
	     * @param resolution interpolation resolution
	     * @param Xlabel x-axis label
	     * @param Ylabel y-axis label
	     */
		public void HeatMap(int Xcolumn, int Ycolumn, int HeatColumn, double resolution, String Xlabel, String Ylabel, String Heatlabel) {
			HeatMap(Xcolumn,Ycolumn,HeatColumn,resolution,Xlabel,Ylabel,Heatlabel,getTitle());
		}
		/**
	     * wrapper for function geom_line(...)
	     * @param Xcolumn column index from dataset to map column to x-values
	     * @param Ycolumn column index from dataset to map column to y-values
	     * @return PlotOption instance
	     */
		public PlotOption LinePlot(int Xcolumn, int Ycolumn) {
			return geom_line(Xcolumn, Ycolumn);
		}
		/**
	     * wrapper for function geom_histogram(...)
	     * @param column column index from dataset to map column to values
	     * @return PlotOption instance
	     */
		public PlotOption Histogram(int column) {
			return geom_histogram(column);
		}
		/**
	     * wrapper for function geom_histogram(...)
	     * @param column column index from dataset to map column to values
	     * @param binwidth width of histogram bins
	     * @return PlotOption instance
	     */
		public PlotOption Histogram(int column, double binwidth) {
			return geom_histogram(column, "binwidth=" + binwidth);
		}
		/**
	     * wrapper for function geom_bar(...)
	     * @param Xcolumn column index from dataset to map column to x-values
	     * @param Ycolumn column index from dataset to map column to y-values
	     * @return PlotOption instance
	     */
		public PlotOption BarPlot(int Xcolumn, int Ycolumn) {
			return geom_bar(Xcolumn, Ycolumn);
		}
		/**
	     * wrapper for function geom_point(...)
	     * @param Xcolumn column index from dataset to map column to x-values
	     * @param Ycolumn column index from dataset to map column to y-values
	     * @return PlotOption instance
	     */
		public PlotOption DotPlot(int Xcolumn, int Ycolumn) {
			return geom_point(Xcolumn, Ycolumn);			
		}
		/**
	     * wrapper for function geom_step(...)
	     * @param Xcolumn column index from dataset to map column to x-values
	     * @param Ycolumn column index from dataset to map column to y-values
	     * @return PlotOption instance
	     */
		public PlotOption StepPlot(int Xcolumn, int Ycolumn) {
			return geom_step(Xcolumn, Ycolumn);
		}
		/**
	     * wrapper for function geom_boxplot(...)
	     * @param column column index from dataset to map column to values
	     * @return PlotOption instance
	     */
		public PlotOption BoxPlot(int column) {
			return geom_boxplot(column);
		}
		/**
	     * wrapper for function geom_area(...)
	     * @param Xcolumn column index from dataset to map column to x-values
	     * @param Ycolumn column index from dataset to map column to y-values
	     * @return PlotOption instance
	     */
		public PlotOption AreaPlot(int Xcolumn, int Ycolumn) {
			return geom_area(Xcolumn, Ycolumn);
		}
		/**
	     * wrapper for function geom_smooth(...,"method = 'loess'")
	     * @param Xcolumn column index from dataset to map column to x-values
	     * @param Ycolumn column index from dataset to map column to y-values
	     * @return PlotOption instance
	     */
		public PlotOption FittedLinePlot(int Xcolumn, int Ycolumn) {
			return geom_smooth(Xcolumn, Ycolumn, "method = 'loess'");
		}
		/**
		 * execute any R expression
		 * @param value expression
		 */
		public void expr(String value) {
			expressions.add(value);
		}
		/**
	     * set x-axis label
	     */
		public void setXAxisLabel(String label) {
			xlab("'"+label+"'");
		}
		/**
	     * set y-axis label
	     */
		public void setYAxisLabel(String label) {
			ylab("'"+label+"'");
		}
		/**
	     * set z-axis label
	     */
		public void setZAxisLabel(String label) {
			zlab("'"+label+"'");
		}
		/**
	     * set x-axis limits
	     * @param min minimum value
	     * @param max maximum value
	     */
		public void setXAxisLimits(double min, double max) {
			xlim(min+"", max+"");
		}
		/**
	     * set y-axis limits
	     * @param min minimum value
	     * @param max maximum value
	     */
		public void setYAxisLimits(double min, double max) {
			ylim(min+"", max+"");
		}
		/**
	     * set z-axis limits
	     * @param min minimum value
	     * @param max maximum value
	     */
		public void setZAxisLimits(double min, double max) {
			zlim(min+"", max+"");
		}
		/**
	     * set legend position
	     * @param pos legend position value
	     */		
		public void setLegendPosition(LegendPosition pos) {
			if (pos == LegendPosition.BOTTOM) {
				theme("legend.position='bottom'");
			} else if (pos == LegendPosition.TOP) {
				theme("legend.position='top'");
			} else if (pos == LegendPosition.LEFT) {
				theme("legend.position='left'");
			} else if (pos == LegendPosition.RIGHT) {
				theme("legend.position='right'");
			} else if (pos == LegendPosition.NONE) {
				theme("legend.position='none'");
			}
		}
		private String joinStr(ArrayList<String> arr, String delimiter) {
			if (arr.size() > 0) {
				String res = "";
				for (String str : arr)
					res += (delimiter + str);
				return res.substring(1);
			}
			return "";
		}
		private String RoundToSignificantDigits(Double dbl) {
			
			if (Double.isNaN(dbl) || Double.isInfinite(dbl))
				return "";
			
			String number = new BigDecimal(dbl).toPlainString();
			
			if (number.contains(".")) {
				
				int indDot = number.indexOf(".");
				int leadingZeros =0;
				for (int i=indDot+1; i<number.length(); i++) {
										
					if (number.charAt(i) == '0')
						leadingZeros++;
					else
						break;
				}
				return number.substring(0,Math.min(number.length(),leadingZeros+indDot+4));
			}
			return number;		
		}
		
		private void limitPlotAxis(double xmin, double xmax, double ymin, double ymax) {
			coord_cartesian(String.format("%s= c(%s, %s), %s = c(%s, %s)", 
					GGPLOT2COMMAND.xlim.toString(),
					xmin,
					xmax,
					GGPLOT2COMMAND.ylim.toString(),
					ymin,
					ymax),
					"expand = FALSE");
			
			Plot();
						
			XMinTextField.setText(RoundToSignificantDigits(XMin));
			XMaxTextField.setText(RoundToSignificantDigits(XMax));
			YMinTextField.setText(RoundToSignificantDigits(YMin));
			YMaxTextField.setText(RoundToSignificantDigits(YMax));
			
			PlotPanel.SelectionRectHeight = 0;
			PlotPanel.SelectionRectWidth = 0;
		}
		
		private void resetLimitSelection() {
			plotOptionList.remove(GGPLOT2COMMAND.coord_cartesian);
			ggtitle(String.format("'%s'",getTitle()));
			HorizontalRangeSlider.setValue(0);
			HorizontalRangeSlider.setUpperValue(RangeSliderMaxValue);
			VerticalRangeSlider.setValue(0);
			VerticalRangeSlider.setUpperValue(RangeSliderMaxValue);
			XMinTextField.setText("");
			XMaxTextField.setText("");
			YMinTextField.setText("");
			YMaxTextField.setText("");
			PlotPanel.SelectionRectHeight = 0;
			PlotPanel.SelectionRectWidth = 0;
			Plot();
		}
		
		public void mousePressed(MouseEvent e) {
			
			if (!showZoomMenu)
				return;
			
			if (e.getButton() == MouseEvent.BUTTON1) {
				PlotPanel.SelectionRectX = e.getX();
				PlotPanel.SelectionRectY = e.getY();
								
			} else if (e.getButton() == MouseEvent.BUTTON3) {
				resetLimitSelection();
			}	
		}
			
		public void mouseReleased(MouseEvent e) {
		
			if (!showZoomMenu)
				return;
			
			if (e.getButton() == MouseEvent.BUTTON1 && PlotPanel.SelectionRectHeight>0 && PlotPanel.SelectionRectWidth>0) {
				
				double dx = XMax - XMin;
				double dy = YMax - YMin;
				double leftX = XMin;
				double bottomY = YMin;				
				
				double pixels_x = dx / (plotRectRight-plotRectLeft);
				double pixels_y = dy / (plotRectBottom-plotRectTop);
				
				limitPlotAxis(	leftX + pixels_x * (-plotRectLeft+PlotPanel.SelectionRectX),
								leftX + pixels_x * (-plotRectLeft+PlotPanel.SelectionRectX+PlotPanel.SelectionRectWidth),
								bottomY + dy - pixels_y * (-plotRectTop+PlotPanel.SelectionRectY+PlotPanel.SelectionRectHeight),
								bottomY + dy - pixels_y * (-plotRectTop+PlotPanel.SelectionRectY));
				
				PlotPanel.DrawSelectionRect=false;
			}
		}
		
		public void mouseDragged(MouseEvent e) {
			
			if (!showZoomMenu)
				return;
			
			if (SwingUtilities.isLeftMouseButton(e)) {
				PlotPanel.SelectionRectHeight = e.getY() - PlotPanel.SelectionRectY;
				PlotPanel.SelectionRectWidth = e.getX() - PlotPanel.SelectionRectX;
				PlotPanel.DrawSelectionRect = true;
				
				if (PlotPanel.DrawHoverPoints)
					mouseMoved(e);
				else
					redraw();
			}
		}

		public void mouseMoved(MouseEvent e) {
			
			if (!showZoomMenu)
				return;
			
			if (PlotPanel.DrawHoverPoints) {
				PlotPanel.HoverPosX = e.getX();
				PlotPanel.HoverPosY = e.getY();
				
				double dx = XMax - XMin;
				double dy = YMax - YMin;
				double leftX = XMin;
				double bottomY = YMin;	
				
				double pixels_x = dx / (plotRectRight-plotRectLeft);
				double pixels_y = dy / (plotRectBottom-plotRectTop);
				PlotPanel.HoverValueX = leftX + pixels_x * (-plotRectLeft+PlotPanel.HoverPosX);
				PlotPanel.HoverValueY = bottomY + dy - pixels_y * (-plotRectTop+PlotPanel.HoverPosY);
								
				redraw();
			}
		   
		}

		public void SliderMouseReleasedEvent() {
			
			if (!showZoomMenu)
				return;
			
			double partXmin = ((double)HorizontalRangeSlider.getValue()) / RangeSliderMaxValue;
			double partXmax = ((double)HorizontalRangeSlider.getUpperValue()) / RangeSliderMaxValue;
			double partYmin = ((double)VerticalRangeSlider.getValue()) / RangeSliderMaxValue;
			double partYmax = ((double)VerticalRangeSlider.getUpperValue()) / RangeSliderMaxValue;
			
			if (partXmin == 0 && partXmax == 1 && partYmin == 0 && partYmax == 1) {
				resetLimitSelection();
				return;
			}
						
			double dx = XMax - XMin;
			double dy = YMax - YMin;
			
			limitPlotAxis(	XMin + partXmin * dx,
							XMin + partXmax * dx,
							YMin + partYmin * dy,
							YMin + partYmax * dy);
			
			HorizontalRangeSlider.setValue(0);
			HorizontalRangeSlider.setUpperValue(RangeSliderMaxValue);
			VerticalRangeSlider.setValue(0);
			VerticalRangeSlider.setUpperValue(RangeSliderMaxValue);
			
			
		}
		
		public void mouseExited(MouseEvent e) {
			
			PlotPanel.HoverPosX = -1;
			PlotPanel.HoverPosY = -1;
			redraw();
		}
		
		private void OpenURL(String url) {
			try {
				if (isOSWindows()) {
					Desktop.getDesktop().browse(new URL(url).toURI());
				} else if (isOSLinux()) {
					ProcessBuilder pbxdg = new ProcessBuilder("xdg-open",url);
					pbxdg.redirectErrorStream(true);
					Process xdg = pbxdg.start();
					BufferedReader inn = new BufferedReader(new InputStreamReader(xdg.getInputStream()));
					inn.readLine();
				} else if (isOSMac()) {
					Runtime.getRuntime().exec("open " + url);
				}
			} catch (Exception e) {
				System.out.println(e);
			}
		}
		
		public void componentResized(ComponentEvent e) {
						
			if (initialized) {
				try {
					Plot();
				} catch (Exception ex) {
				}
			}
		}
		
		private transient ActionListener JCheckBoxMenuItemShowHoverPoints_CheckedChanged = new ActionListener() { 
			public void actionPerformed(ActionEvent e) {
				
				PlotPanel.DrawHoverPoints = JCheckBoxMenuItemShowHoverPoints.isSelected();
			}};
			
		private transient ActionListener ApplyButton_Clicked = new ActionListener() { 
		    public void actionPerformed(ActionEvent e) {
		    	
		    	String t1 = XMinTextField.getText();
		    	String t2 = XMaxTextField.getText();
		    	String t3 = YMinTextField.getText();
		    	String t4 = YMaxTextField.getText();
		    	
		    	double d1 = Double.MIN_VALUE;
		    	double d2 = Double.MIN_VALUE;
		    	double d3 = Double.MIN_VALUE;
		    	double d4 = Double.MIN_VALUE;
		    	
		    	try {
		    		d1 = Double.parseDouble(t1);
		    	} catch (Exception ex){}
		       	try {
		       		d2 = Double.parseDouble(t2);
		    	} catch (Exception ex){}
		    	try {
		    		d3 = Double.parseDouble(t3);
		    	} catch (Exception ex){}
		    	try {
		    		d4 = Double.parseDouble(t4);
		    	} catch (Exception ex){}
		    			    			    	
		    	if (d1 != Double.MIN_VALUE && d2 !=Double.MIN_VALUE && d3 != Double.MIN_VALUE && d4 != Double.MIN_VALUE && t1.length() > 0 && t2.length() > 0 && t3.length() > 0 && t4.length() > 0) {
		    		limitPlotAxis(d1,d2,d3,d4);
		    	} else if (d1 != Double.MIN_VALUE && d2 != Double.MIN_VALUE && t3.length() == 0 && t4.length() == 0) {
		    		limitPlotAxis(d1,d2,YMin,YMax);
		    	} else if (d3 != Double.MIN_VALUE && d4 != Double.MIN_VALUE && t1.length() == 0 && t2.length() == 0) {
					limitPlotAxis(XMin,XMax,d3,d4);
		    	} else {
		    		JOptionPane.showMessageDialog(null,"Invalid value! Please check for typing errors!");
		    	}
		    }};
		
		private transient ActionListener ResetButton_Clicked = new ActionListener() { 
		    public void actionPerformed(ActionEvent e) {
		    	
		    	resetLimitSelection();
		    }};
		    
		private transient ActionListener ButtonExportDatasetToCSV_clicked = new ActionListener() { 
		    public void actionPerformed(ActionEvent e) {
		    	
		    	JFileChooser fileChooser = new JFileChooser();
		    	fileChooser.setAcceptAllFileFilterUsed(false);
		    	fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("*.csv", "CSV"));
		    	
		    	if (fileChooser.showSaveDialog(PlotPanel) == JFileChooser.APPROVE_OPTION) {
		    		String filepath = fileChooser.getSelectedFile().getPath();
		    		if (!filepath.toLowerCase().endsWith(".csv"))
		    			filepath += ".csv";
		    		
		    		if (e.getSource() == JMenuItemExportDataCSV) {
		    			exportData(exportDataFileType.CSV, filepath,false);
		    		} else if (e.getSource() == JMenuItemExportSelectionDataCSV) {
		    			exportData(exportDataFileType.CSV, filepath,true);
		    		}
		    		
		    		
				}
		    	
		    }};
		    
		private transient ActionListener ButtonExportDatasetToHTML_clicked = new ActionListener() { 
		    public void actionPerformed(ActionEvent e) {
		    	
		    	JFileChooser fileChooser = new JFileChooser();
		    	fileChooser.setAcceptAllFileFilterUsed(false);
		    	fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("*.html", "HTML"));
		    	
		    	if (fileChooser.showSaveDialog(PlotPanel) == JFileChooser.APPROVE_OPTION) {
		    		String filepath = fileChooser.getSelectedFile().getPath();
		    		if (!filepath.toLowerCase().endsWith(".html"))
		    			filepath += ".html";
		    		
		    		if (e.getSource() == JMenuItemExportDataHTML) {
		    			exportData(exportDataFileType.HTML, filepath,false);
		    		} else if (e.getSource() == JMenuItemExportSelectionDataHTML) {
		    			exportData(exportDataFileType.HTML, filepath,true);
		    		}
		    		
				}
		    }};
		    
		private transient ActionListener ButtonExportDatasetToRDATAFRAME_clicked = new ActionListener() { 
		    public void actionPerformed(ActionEvent e) {
		    	
		    	JFileChooser fileChooser = new JFileChooser();
		    			    	
		    	if (fileChooser.showSaveDialog(PlotPanel) == JFileChooser.APPROVE_OPTION) {
		    		
		    		if (e.getSource() == JMenuItemExportDataRDATAFRAME) {
		    			exportData(exportDataFileType.RDATAFRAME, fileChooser.getSelectedFile().getPath(),false);
		    		} else if (e.getSource() == JMenuItemExportSelectionDataRDATAFRAME) {
		    			exportData(exportDataFileType.RDATAFRAME, fileChooser.getSelectedFile().getPath(),true);
		    		}
				}
		    }};
		
		private transient ActionListener ButtonInspectData_clicked = new ActionListener() {
			 public void actionPerformed(ActionEvent e) {
				 try {	
					
					String RScriptPath = tempFile + "InspectData";
				
					FileWriter FR = new FileWriter(RScriptPath);
					FR.write("Sys.setenv(LANGUAGE = 'en')\n"+			
							 "library(shiny)\n" +
							 "library(DT)\n" +
							 "library(shinyWidgets)\n" +
							 String.format("df <- %s\n", dataFrameString));
					
					if (TablePlotColNames != null) {
						for (int i=0; i<TablePlotColNames.length; i++)
							FR.write(String.format("try(colnames(df)[%s]<-'%s',silent=TRUE)\n", i+1, TablePlotColNames[i]));
					}
					
					FR.write("\n" +		
							 "callback <- c(\n" + 
							 "  \"table.on('dblclick.dt', 'thead th', function(e) {\",\n" + 
							 "  \"  var $th = $(this);\",\n" + 
							 "  \"  var index = $th.index();\",\n" + 
							 "  \"  var colname = $th.text(), newcolname = colname;\",\n" + 
							 "  \"  var $input = $('<input type=\\\"text\\\">')\",\n" + 
							 "  \"  $input.val(colname);\",\n" + 
							 "  \"  $th.empty().append($input);\",\n" + 
							 "  \"  $input.on('change', function(){\",\n" + 
							 "  \"    newcolname = $input.val();\",\n" + 
							 "  \"    if(newcolname != colname){\",\n" + 
							 "  \"      $(table.column(index).header()).text(newcolname);\",\n" + 
							 "  \"    }\",\r\n" + 
							 "  \"    $input.remove();\",\n" + 
							 "  \"  }).on('blur', function(){\",\n" + 
							 "  \"    $(table.column(index).header()).text(newcolname);\",\n" + 
							 "  \"    $input.remove();\",\n" + 
							 "  \"  });\",\n" + 
							 "  \"});\"\n" + 
							 ")\n" +
							 "\n" +
							 "ui <- basicPage(\n" + 
							 "  pickerInput(\n" + 
							 "    inputId = \"columnPicker\",\n" + 
							 "    label = \"Select columns\",\n" + 
							 "    choices = colnames(df),\n" + 
							 "    selected = colnames(df),\n" + 
							 "    options = list(\n" + 
							 "      `actions-box` = TRUE,\n" + 
							 "      size = 10\n" + 
							 "    ),\n" + 
							 "    multiple = TRUE\n" + 
							 "  ),\n" +
							 "   DT::dataTableOutput(\"mytable\")\n" + 
							 ")\n" + 
							 "\n" + 
							 "server <- function(input, output, session) {\n" + 
							 "  output$mytable = DT::renderDataTable({\n" + 
							 "    DT::datatable(df[input$columnPicker], editable = 'cell', callback = JS(callback),filter = list(position = 'top'))\n" + 
							 "  })\n" + 
							 "  session$onSessionEnded(function() {\n"+
						     "    stopApp()\n"+
						     "  })\n"+
							 "}\n" + 
						     "\n" +
							 "shinyApp(ui, server)");
					FR.close();		
				
					ProcessBuilder RScriptPB = new ProcessBuilder(ScriptEngine, RScriptPath);
					RScriptPB.directory(new File(tempDirectory));
					
					RScriptPB.redirectErrorStream(true);
					Process RScript = RScriptPB.start();
										
					BufferedReader in = new BufferedReader(new InputStreamReader(RScript.getInputStream()));
					
					String RScripStreamOutput = "";
					String line;
					String url = "";
					
					
					while ((line = in.readLine()) != null) {
						 line = line.toLowerCase();											
						 if (line.contains("http")) {
							url = line.substring(line.indexOf("http"));
							break;
						 } else if (line.contains("error")) {
							 System.out.println(RScripStreamOutput);
							 return;
						 }
					}
					
					OpenURL(url);
															
				 } catch (Exception ex) {
						System.out.println(ex);
					}
			    }};
		
	    private transient ActionListener ButtonSaveImage_clicked = new ActionListener() { 
		    public void actionPerformed(ActionEvent e) {
		    	
		    	JLabel jlFormat = new JLabel("Format:");
		    	JComboBox<String> jcbFormat = new JComboBox<String>(new String[] {".jpg",".png",".gif"});
		    	JLabel jlWidth = new JLabel("Width (pixels):");
		    	JTextField jtfWidth = new JTextField();
		    	jtfWidth.setText(""+PlotPanel.getWidth());
		    	JLabel jlHeight = new JLabel("Height (pixels):");
		    	JTextField jtfHeight = new JTextField();
		    	jtfHeight.setText(""+PlotPanel.getHeight());
		    	
		    	KeyAdapter ka = new KeyAdapter() {
		    	      public void keyReleased(KeyEvent e) {
				    	JTextField jtf = (JTextField) e.getSource();
				    	
				        String txt = jtf.getText();
				        String allowed = "";
				        
				        
				    	for (int i = 0; i<txt.length(); i++) {
				    		char c = txt.charAt(i);
				    		if (c >= '0' && c <= '9') {
				    			allowed += c;
				    		}
				    	}
				    	if (!allowed.equals(txt))
				    		jtf.setText(allowed);
				    	
				    }};
				        		    	
		        jtfWidth.addKeyListener(ka);
		        jtfHeight.addKeyListener(ka);
		        
		    	JButton jbOK = new JButton("OK");
		    		    	
		    	jbOK.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent e) {
		    		Component component = (Component) e.getSource();
		            JDialog dialog = (JDialog) SwingUtilities.getRoot(component);
		            dialog.setName("ok");
		            dialog.setVisible(false);
		        }});
		    	
		    	JDialog diag = new JDialog();
		    	
		    	diag.setLayout(new GridLayout(4,2));
		    	
		    	diag.add(jlFormat);
		    	diag.add(jcbFormat);
		    	diag.add(jlWidth);
		    	diag.add(jtfWidth);
		    	diag.add(jlHeight);
		    	diag.add(jtfHeight);
		    	diag.add(new JLabel(""));
		        diag.add(jbOK);
		    	diag.setSize(300, 150);
		        diag.setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
		        diag.setLocationRelativeTo(null);
		        diag.setVisible(true);
		    	  
		        if (diag.getName().equals("ok")) {
		        	
		        	String format = jcbFormat.getSelectedItem().toString().substring(1);
		        	
		        	JFileChooser fileChooser = new JFileChooser();
			    	fileChooser.setAcceptAllFileFilterUsed(false);
			    	fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("*."+format, format.toUpperCase()));
			    	
			    	if (fileChooser.showSaveDialog(PlotPanel) == JFileChooser.APPROVE_OPTION) {
			    		String filepath = fileChooser.getSelectedFile().getPath();
			    		if (!filepath.toLowerCase().endsWith("."+format))
			    			filepath += ("."+format);
			    		
			    		int w;
			        	int h;
			        	
			        	if (jtfWidth.getText().length()==0 || jtfHeight.getText().length()==0) {
			        		w = 1000;
			        		h = 1000;
			        	} else {
			        		w = Integer.parseInt(jtfWidth.getText());
			        		h = Integer.parseInt(jtfHeight.getText());
			        	}
			        			        	
			        	BufferedImage img = getPlotImage(w,h,true);
			        	
			        	File outputfile = new File(filepath);
						try {
							ImageIO.write(img, format, outputfile);
						} catch (IOException e1) {
							System.out.println(e1);
						}
					}        	
		        }
		        diag.dispose();
		    }};
		    
	    private transient ActionListener ButtonEdit_clicked = new ActionListener() { 
		    public void actionPerformed(ActionEvent e) {
		    	
		    	
		    	JDialog diag = new JDialog();
		    	diag.setLayout(new GridLayout(1,2));
		    	
		    	final DefaultListModel<PlotOption> list = new DefaultListModel<PlotOption>();
		    	
		    	for (int i = 0; i<plotOptionList.size(); i++) {
		    		list.addElement(plotOptionList.get(i));
		    	}
		    	
		    	final JList ljoptions = new JList(list);
		    	ljoptions.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		    	
		    	ljoptions.addMouseListener(new MouseAdapter() {
		    		
		            public void mouseEntered(MouseEvent me) {
		    			ljoptions.grabFocus();
		    		}
		    	});
		    	
		    	JLabel jlCommand = new JLabel("Command: ");
		    	final JComboBox<String> jcbCommand = new JComboBox<String>();
		    	
		    	jcbCommand.addItem("//////new command//////");
		    	for (GGPLOT2COMMAND com : GGPLOT2COMMAND.values()) {
		    		jcbCommand.addItem(com.toString());
		    	}
		    	jcbCommand.addItem("scatterplot3d");
		    	jcbCommand.addItem("scatter3D");
		    	
		    	jcbCommand.addActionListener(new ActionListener() {public void actionPerformed(ActionEvent e) {
		    		
		    		PlotOption po = (PlotOption) ljoptions.getSelectedValue();
		    		
		    		if (jcbCommand.getSelectedIndex() > 0) {
		    			po.setCommand(jcbCommand.getSelectedItem().toString());
		    		} else {
		    			 String newcomm = JOptionPane.showInputDialog("command: ");
		    			 if (newcomm.trim().length() > 0) {
		    				 po.setCommand(newcomm);
		    				 jcbCommand.addItem(newcomm);
		    				 jcbCommand.setSelectedIndex(jcbCommand.getItemCount()-1);
		    			 } 
		    		}
		    	}});
		    			    	
		    	JLabel jlProperies = new JLabel("Properties (if applicable):");
		    	
		    	JLabel jlLineType = new JLabel("Line type: ");
		    	final JComboBox<String> jcbLineType = new JComboBox<String>();
		    	jcbLineType.addItem("");	
		    	for (LineType lt: LineType.values()) {
		    		jcbLineType.addItem(lt.toString());
		    	}
		    	
		    	jcbLineType.addActionListener(new ActionListener() {public void actionPerformed(ActionEvent e) {
		    		PlotOption po = (PlotOption) ljoptions.getSelectedValue();
		    		
		    		if (jcbLineType.getSelectedIndex() > 0) {
		    			po.setLineType(LineType.valueOf((String) jcbLineType.getSelectedItem()));
		    		} else {
		    			po.lineType = null;
		    		}
		    	    	  
		    	}});
		    	
		    	JLabel jlPointShape = new JLabel("Point shape: ");
		    	final JComboBox<String> jcbPointShape = new JComboBox<String>();
		    	jcbPointShape.addItem("");	
		    	for (PointShape ps: PointShape.values()) {
		    		jcbPointShape.addItem(ps.toString());
		    	}
		    	
		    	jcbPointShape.addActionListener(new ActionListener() {public void actionPerformed(ActionEvent e) {
		    		PlotOption po = (PlotOption) ljoptions.getSelectedValue();
		    		
		    		if (jcbPointShape.getSelectedIndex() > 0) {
		    			po.setPointShape(PointShape.valueOf((String) jcbPointShape.getSelectedItem()));
		    		} else {
		    			po.pointShape = null;
		    		}
		    	    	  
		    	}});
		    	
		    	
		    	JLabel jlLineColor = new JLabel("Line color: ");
		    	final JButton jbLineColor = new JButton();
		    	
		    	jbLineColor.addActionListener(new ActionListener() {public void actionPerformed(ActionEvent e) {
						
		    		Color col = JColorChooser.showDialog(null, "Choose color", null);
		    		PlotOption po = (PlotOption) ljoptions.getSelectedValue();
	    			
		    		if (col != null) {
	    	    		  jbLineColor.setBackground(col);
	    	    		  po.setLineColor(col);
    	    	    } else {
	    	    		  jbLineColor.setBackground(new JButton().getBackground());
	    	    		  po.lineColor = null;
    	    	    }
		    	}});
		    	
		    	JLabel jlLineWidth = new JLabel("Line width: ");
		    	
		    	String[] lwids = new String[10001];
		       
		    	lwids[0] = "";
		    	int c =0;
		    	for (double i=0; i<=1000; i+=0.1) {
		    		c++;
		    		lwids[c] = String.format("%s",round(i,1));
		    	}
		    			    	
		    	SpinnerListModel model = new SpinnerListModel(lwids);
		    			    			    			    	
		    	final JSpinner jsLineWidth = new JSpinner(model);
		    		        
		    	jsLineWidth.addChangeListener(new ChangeListener() {public void stateChanged(ChangeEvent e) {
		    		PlotOption po = (PlotOption) ljoptions.getSelectedValue();
		    		
		    		if (jsLineWidth.getValue().toString().length() > 0) {
		    			double value = Double.parseDouble(jsLineWidth.getValue().toString());
		    			po.setLineWidth(value);
		    		} else {
		    			po.setLineWidth(-1.0);
		    		}
		    	}});
		    	
		    	JLabel jlarguments = new JLabel("Arguments:");
		    	final JTextField jtfarguments = new JTextField();
		    	
		    	jtfarguments.getDocument().addDocumentListener(new DocumentListener() {

					public void insertUpdate(DocumentEvent e) {
						action();						
					}
					public void removeUpdate(DocumentEvent e) {
						action();						
					}
					public void changedUpdate(DocumentEvent e) {
						action();					
					}
		    		void action()  {
		    			PlotOption po = (PlotOption) ljoptions.getSelectedValue();
			    		
			    		String values[] = jtfarguments.getText().split(",");
			    		
			    		po.clearArgumentList();
			    		
			    		for (String str : values) {
			    			po.addArgument(str.trim());
			    		}	   
		    		}
		    	});
		    	
		    	JLabel jlisGGPLOT = new JLabel("ggplot2");
		    	final JCheckBox jckbisGGPLOT = new JCheckBox();
		    	
		    	jckbisGGPLOT.addActionListener(new ActionListener() {public void actionPerformed(ActionEvent e) {
		    		 PlotOption po = (PlotOption) ljoptions.getSelectedValue();
		    		 
		    		 po.isGGPLOT2COMMAND = jckbisGGPLOT.isSelected();
		    	}});
		    	
		    	
		    	JButton jbadd = new JButton("add");
		    	final JButton jbdelete = new JButton("delete");
		    	
		    	JButton jbapply = new JButton("apply");
		    	
		    	jbapply.addActionListener(new ActionListener() {public void actionPerformed(ActionEvent e) {
		    		
		    		Plot();
		    		
		    	}});
		    			    	
		    	jcbCommand.setEnabled(false);
				jbLineColor.setEnabled(false);
				jcbLineType.setEnabled(false);
				jsLineWidth.setEnabled(false);
				jtfarguments.setEnabled(false);
				jbdelete.setEnabled(false);
				jckbisGGPLOT.setEnabled(false);
				jcbPointShape.setEnabled(false);
				
		    	ljoptions.addListSelectionListener(new ListSelectionListener() {
		    	      public void valueChanged(ListSelectionEvent listSelectionEvent) {
		    	    	  		    	    	  
		    	    	  if (ljoptions.getSelectedIndex() >= 0) {
		    	    		  jcbCommand.setEnabled(true);
			    	    	  jbLineColor.setEnabled(true);
			    	    	  jcbLineType.setEnabled(true);
			    	    	  jsLineWidth.setEnabled(true);
			    	    	  jtfarguments.setEnabled(true);
			    	    	  jbdelete.setEnabled(true);
			    	    	  jckbisGGPLOT.setEnabled(true);
			    	    	  jcbPointShape.setEnabled(true);
			    	    	  
		    	    		  PlotOption po = (PlotOption) ljoptions.getSelectedValue();
	  		    	    	  
			    	    	  jcbCommand.setSelectedItem(po.getCommand());
			    	    	  
			    	    	  if (!jcbCommand.getSelectedItem().toString().equals(po.getCommand())) {
			    	    		  jcbCommand.addItem(po.getCommand());
			    	    		  jcbCommand.setSelectedItem(po.getCommand());
			    	    	  }
			    	    	  
			    	    	  jckbisGGPLOT.setSelected(po.isGGPLOT2COMMAND);
			    	    	  
			    	    	  if (po.lineColor != null) {
			    	    		  jbLineColor.setBackground(po.lineColor);
			    	    	  } else {
			    	    		  jbLineColor.setBackground(new JButton().getBackground());
			    	    	  }
			    	    	  
			    	    	  if (po.lineType != null) {
			    	    		  jcbLineType.setSelectedItem(po.lineType.toString());
			    	    	  } else  {
			    	    		  jcbLineType.setSelectedIndex(0);
			    	    	  }
			    	    	  
			    	    	  if (po.pointShape != null) {
			    	    		  jcbPointShape.setSelectedItem(po.pointShape.toString());
			    	    	  } else  {
			    	    		  jcbPointShape.setSelectedIndex(0);
			    	    	  }
			    	    	  
			    	    	  if (po.lineWidth >= 0) {
			    	    		  jsLineWidth.setValue(String.format("%s",po.lineWidth));
			    	    	  } else  {
			    	    		  jsLineWidth.setValue("");
			    	    	  }
			    	    	  
			    	    	  String s = po.arguments.toString();
			    	    	  jtfarguments.setText(s.substring(1,s.length()-1));
		    	    	  } else {
		    	    		    jcbCommand.setEnabled(false);
			  					jbLineColor.setEnabled(false);
			  					jcbLineType.setEnabled(false);
			  					jsLineWidth.setEnabled(false);
			  					jtfarguments.setEnabled(false);
			  					jbdelete.setEnabled(false); 
			  					jckbisGGPLOT.setEnabled(false);
			  					jcbPointShape.setEnabled(false);
		    	    	  }
		    	      }});
		    	
		    
		    	jbdelete.addActionListener(new ActionListener() {public void actionPerformed(ActionEvent e) {
		    		
		    		PlotOption po = (PlotOption) ljoptions.getSelectedValue();
		    		plotOptionList.remove(po);
		    		list.removeElement(po);
		    		
		    	}});
		    	
		    	jbadd.addActionListener(new ActionListener() {public void actionPerformed(ActionEvent e) {
		    				    		
		    		PlotOption po = new PlotOption();
		    		po.setCommand(GGPLOT2COMMAND.annotate);
		    		list.addElement(po);
		    		plotOptionList.add(po);
		    		ljoptions.setSelectedValue(po, true);
		    		jcbCommand.showPopup();
		    		
		    	}});
		    	
		    	JPanel jp1 = new JPanel();
		    	
		    	jp1.setLayout(new BorderLayout());
		    	JPanel jp12 = new JPanel();
		    	jp12.setLayout(new GridLayout(1,2));
		    	jp12.add(jbadd);
		    	jp12.add(jbdelete);
		    	jp1.add(jp12,BorderLayout.NORTH);
		    	jp1.add(ljoptions,BorderLayout.CENTER);
		    	
		    			    			    	
		    	JPanel jp2 = new JPanel();
		    	jp2.setLayout(new GridLayout(9,2));
		    	jp2.add(jlCommand);
		    	jp2.add(jcbCommand);
		    	jp2.add(jlisGGPLOT);
		    	jp2.add(jckbisGGPLOT);
		    	jp2.add(jlProperies);
		    	jp2.add(new JLabel());
		    	jp2.add(jlLineType);
		    	jp2.add(jcbLineType);
		    	jp2.add(jlPointShape);
		    	jp2.add(jcbPointShape);
		    	jp2.add(jlLineColor);
		    	jp2.add(jbLineColor);
		    	jp2.add(jlLineWidth);
		    	jp2.add(jsLineWidth);
		    	jp2.add(jlarguments);
		    	jp2.add(jtfarguments);
		    	jp2.add(jbapply);
		    	
		    	
		    	diag.add(jp1);
		    	diag.add(jp2);
		    	diag.pack();
		    	
		    	diag.setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
		        diag.setLocationRelativeTo(null);
		        diag.setVisible(true);
		    	
		        Plot();
		    }};
	    /**
	     * dispose instance
	     */
		@Override
		public void dispose() {
			RemoveListeners();
			PlotPanel.disposeImage();
		}
		
		//unused event handlers
		public void mouseClicked(MouseEvent e) {}
		public void mouseEntered(MouseEvent e) {}
		public void componentHidden(ComponentEvent e) {}
	 	public void componentShown(ComponentEvent e) {}
		public void componentMoved(ComponentEvent e) {}
		
		//ImagePanel class
		private class ImagePanel extends JComponent {
			
		 	private static final long serialVersionUID = 1L;
			private transient BufferedImage image;
		    public boolean DrawSelectionRect = false;
		    public int SelectionRectX;
		    public int SelectionRectY;
		    public int SelectionRectWidth;
		    public int SelectionRectHeight;
		    public boolean DrawHoverPoints = false;
		    public int  HoverPosX=0;
		    public int  HoverPosY=0;
		    public double HoverValueX;
		    public double HoverValueY;
		    	    
		    
		    public void setImage(BufferedImage image) {
		    	this.image = image;
		    }
		    
		    public ImagePanel(BufferedImage image) {
		    	setImage(image);
		    }
		    
		    public void disposeImage() {
				if (image != null)
					image.flush();
		    }
		    
		    protected void paintComponent(Graphics g) {
		        super.paintComponent(g);
		         
		        if (image != null) {
		        	int w = getWidth();
			        int h = getHeight();
			       
			        g.drawImage(image, 0, 0, w, h, null);
			        
			        if (DrawSelectionRect) {
			        	g.setColor(Color.BLACK);
			        	g.drawRect(SelectionRectX, SelectionRectY, SelectionRectWidth, SelectionRectHeight);
			        }
			        
			        if (DrawHoverPoints) {
			        	
			        	if (HoverPosX >= 0 && HoverPosX <= w && HoverPosY >=0 && HoverPosY <= h ) {
			        		String RX = RoundToSignificantDigits(HoverValueX);
				        	String RY = RoundToSignificantDigits(HoverValueY);
				        	
				        	String promt = RX + " | " + RY;
				        	
				        	int strW = g.getFontMetrics().stringWidth(promt);
				        	int strH = g.getFontMetrics().getHeight();
				        	
				        	if (HoverPosX+strW > w)
				        		HoverPosX = w-strW;
				        	
				        	if (HoverPosY-strH < 0)
				        		HoverPosY = strH;
				        	
				        	g.drawString(RX + " | " + RY,HoverPosX,HoverPosY);
			        	}
			        }
			        
		        }
		    }
		}

		
}
	
