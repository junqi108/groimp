/*
 * Copyright (C) 2012 GroIMP Developer Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.greenlab;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import de.grogra.greenlab.conf.GreenLabAtributes;
import de.grogra.greenlab.conf.GreenLabPropertyFileReader;
import de.grogra.greenlab.ui.elements.GreenLabBooleanCheckBox;
import de.grogra.greenlab.ui.elements.GreenLabJLabel;
import de.grogra.greenlab.ui.elements.GreenLabNumberField;

/**
 * The Topology Panel
 * 
 * @author Cong Ding
 * 
 */
public class GreenLabTopologyPanel extends JPanel {

	private static final long serialVersionUID = 16594658201938586L;

	private JPanel panel1, mainPanel, paPanel;
	private GreenLabJLabel label_Flag_Nu_Ma_Version;
	private GreenLabNumberField field_Flag_Nu_Ma_Version;
	private JTabbedPane jtp;
	private JPanel pap[];
	private int currentMeta[] = new int[GreenLabAtributes.TOPO_META_NUM];
	private JPanel paLeft[] = new JPanel[GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE];
	private JPanel paRight1[] = new JPanel[GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE];
	private JPanel paRight2[] = new JPanel[GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE];
	private JPanel paRight3[] = new JPanel[GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE];
	private JPanel paRL[] = new JPanel[GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE];
	private JPanel paRLMain[] = new JPanel[GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE];
	private GreenLabJLabel paRLLabel[] = new GreenLabJLabel[GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE];
	private JPanel paMeta[][] = new JPanel[GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE][GreenLabAtributes.TOPO_META_NUM];
	private GreenLabJLabel paMetaLabel[][] = new GreenLabJLabel[GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE][GreenLabAtributes.TOPO_META_NUM];
	private JButton paMetaButton[][] = new JButton[GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE][GreenLabAtributes.TOPO_META_NUM];
	private final GreenLabNumberField paMetaText[][] = new GreenLabNumberField[GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE][GreenLabAtributes.TOPO_META_NUM];
	private JPanel paPara[][] = new JPanel[GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE][GreenLabAtributes.TOPO_PARA_NUM];
	private GreenLabJLabel paParaLabel[] = new GreenLabJLabel[GreenLabAtributes.TOPO_PARA_NUM];
	private final JTextField paParaText[][] = new JTextField[GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE][GreenLabAtributes.TOPO_PARA_NUM];
	private JTextField rlText[][] = new JTextField[GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE][4];
	private GreenLabBooleanCheckBox controls[][] = new GreenLabBooleanCheckBox[GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE][2];

	GreenLabTopologyPanel() {
		label_Flag_Nu_Ma_Version = new GreenLabJLabel(("Flag_Nu_Ma_version"));
		field_Flag_Nu_Ma_Version = new GreenLabNumberField("Flag_Nu_Ma_version");

		field_Flag_Nu_Ma_Version.getDocument().addDocumentListener(
				new DocumentListener() {
					public void actionPerformed() {
						GreenLabPropertyFileReader.get().setProperty(
								"Flag_Nu_Ma_version",
								field_Flag_Nu_Ma_Version.getText());
					}

					@Override
					public void changedUpdate(DocumentEvent e) {
						actionPerformed();
					}

					@Override
					public void insertUpdate(DocumentEvent e) {
						actionPerformed();
					}

					@Override
					public void removeUpdate(DocumentEvent e) {
						actionPerformed();
					}
				});

		mainPanel = new JPanel(new GridLayout(1, 2));

		mainPanel.add(label_Flag_Nu_Ma_Version);
		mainPanel.add(field_Flag_Nu_Ma_Version);

		mainPanel.setBorder(new TitledBorder(""));

		panel1 = new JPanel(new BorderLayout());
		panel1.add(mainPanel, BorderLayout.WEST);

		this.setLayout(new BorderLayout());
		this.add(panel1, BorderLayout.NORTH);

		pap = new JPanel[GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE];
		for (int i = 0; i < GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE; i++) {
			pap[i] = new JPanel();
		}

		paPanel = new JPanel(new BorderLayout());
		jtp = new JTabbedPane();

		for (int j = 0; j < GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE; j++) {
			JPanel paRLMainM = new JPanel(new BorderLayout());
			paRL[j] = new JPanel(new GridLayout(5, 1));
			paRLMain[j] = new JPanel(new BorderLayout());
			paLeft[j] = new JPanel(new GridLayout(
					GreenLabAtributes.TOPO_META_NUM + 1, 1));
			paRight1[j] = new JPanel(new GridLayout(2, 1));
			paRight2[j] = new JPanel(new GridLayout(6, 1));
			paRight3[j] = new JPanel(new GridLayout(4, 1));
			JPanel metaTitle = new JPanel();
			int fileNameCntJJ = j + 1;
			
			metaTitle.add(new GreenLabJLabel(GreenLabAtributes.getImageIcon(this.getClass(), "pa" + fileNameCntJJ + "_trans")));
			metaTitle.add(new GreenLabJLabel(("Phy_ages_types_of_metamers")));
			paLeft[j].add(metaTitle);
			paRight1[j].setBorder(new TitledBorder(GreenLabGUI.getString("metation")));
			paRight2[j].setBorder(new TitledBorder(GreenLabGUI.getString("control")));
			paRight3[j].setBorder(new TitledBorder(GreenLabGUI.getString("probability")));
			JPanel tmp = new JPanel(new BorderLayout());
			tmp.add(paLeft[j], BorderLayout.WEST);
			paRLMainM.add(paRLMain[j], BorderLayout.SOUTH);
			tmp.add(paRLMainM, BorderLayout.EAST);
			Box box = Box.createVerticalBox();
			box.add(paRight1[j]);
			box.add(paRight2[j]);
			box.add(paRight3[j]);
			Box boxW = Box.createHorizontalBox();
			boxW.add(tmp);
			boxW.add(box);
			pap[j].add(boxW);
			paRLLabel[j] = new GreenLabJLabel("");
			paRLMain[j].add(paRLLabel[j], BorderLayout.WEST);
			paRLMain[j].add(new GreenLabJLabel(GreenLabAtributes.getImageIcon(this.getClass(), 
					"metamer_trans")), BorderLayout.EAST);
			paRLMain[j].add(paRL[j], BorderLayout.SOUTH);
			for (int i = 0; i < GreenLabAtributes.TOPO_META_NUM - j; i++) {
				paMeta[j][i] = new JPanel();
				paMetaButton[j][i] = new JButton(
						GreenLabGUI.getString("detail"));
				final int ii = i, jj = j;
				paMetaButton[j][i].addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						currentMeta[jj] = ii;
						updateDetails(jj, ii);
					}
				});
				int fileNameCntI = i + 1;
				int fileNameCntJ = j + 1;
				paMetaLabel[j][i] = new GreenLabJLabel(GreenLabAtributes.getImageIcon(this.getClass(), "met"
										+ fileNameCntJ + fileNameCntI + "_trans"));
				paMetaText[j][i] = new GreenLabNumberField("Micro_Num_Nu_B_");
				paMetaText[j][i].getDocument().addDocumentListener(
						new DocumentListener() {

							private void actionPerformed() {
								GreenLabPropertyFileReader.get().setProperty(
										topoMetaName(0),
										paMetaText[jj][ii].getText(), jj + 1,
										ii + 1);
							}

							@Override
							public void changedUpdate(DocumentEvent arg0) {
								actionPerformed();
							}

							@Override
							public void insertUpdate(DocumentEvent arg0) {
								actionPerformed();
							}

							@Override
							public void removeUpdate(DocumentEvent arg0) {
								actionPerformed();
							}
						});
				paMeta[j][i].add(paMetaLabel[j][i]);
				paMeta[j][i].add(paMetaButton[j][i]);
				paMeta[j][i].add(new GreenLabJLabel("Number"));
				paMeta[j][i].add(paMetaText[j][i]);
				paLeft[j].add(paMeta[j][i]);
			}
		}
		for (int j = 0; j < GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE; j++) {
			for (int i = 1; i < GreenLabAtributes.TOPO_PARA_NUM; i++) {
				paPara[j][i] = new JPanel(new BorderLayout());
				paParaLabel[i] = new GreenLabJLabel(
						(topoParaName(i + 1).replaceAll("[\\(\\) ]", "_")));
				if (i < 4)
					paParaText[j][i] = new GreenLabNumberField(topoParaName(
							i + 1).replaceAll("[\\(\\) ]", "_"), true);
				else if (i < 6) {
					// paParaText[j][i] = new GreenLabNumberField(true, 0, 1);
				} else if (i < 8)
					paParaText[j][i] = new GreenLabNumberField(topoParaName(
							i + 1).replaceAll("[\\(\\) ]", "_"), true);
				else if (i < 9)
					paParaText[j][i] = new GreenLabNumberField(topoParaName(
							i + 1).replaceAll("[\\(\\) ]", "_"));
				else
					paParaText[j][i] = new GreenLabNumberField(topoParaName(
							i + 1).replaceAll("[\\(\\) ]", "_"));
				final int ii = i, jj = j;
				if (i == 4 || i == 5) {
					controls[j][i - 4] = new GreenLabBooleanCheckBox(
							topoParaName(i + 1).replaceAll("[\\(\\) ]", "_"));
					JPanel tmp = new JPanel(new BorderLayout());
					tmp.add(controls[j][i - 4], BorderLayout.WEST);
					tmp.add(paParaLabel[i], BorderLayout.CENTER);

					paPara[j][i].add(tmp, BorderLayout.WEST);
					paPara[j][i].add(new GreenLabJLabel(""), BorderLayout.EAST);
					controls[j][i - 4].addActionListener(new ActionListener() {
						@Override
						public void actionPerformed(ActionEvent arg0) {
							GreenLabPropertyFileReader
									.get()
									.setProperty(
											topoParaName(ii + 1),
											controls[jj][ii - 4].getTextValue(),
											jj + 1);
						}
					});
				} else {
					paParaText[j][i].getDocument().addDocumentListener(
							new DocumentListener() {

								private void actionPerformed() {
									GreenLabPropertyFileReader.get()
											.setProperty(
													topoParaName(ii + 1),
													paParaText[jj][ii]
															.getText(), jj + 1);
								}

								@Override
								public void changedUpdate(DocumentEvent arg0) {
									actionPerformed();
								}

								@Override
								public void insertUpdate(DocumentEvent arg0) {
									actionPerformed();
								}

								@Override
								public void removeUpdate(DocumentEvent arg0) {
									actionPerformed();
								}
							});
					paPara[j][i].add(paParaLabel[i], BorderLayout.WEST);
					paPara[j][i].add(paParaText[j][i], BorderLayout.EAST);
				}
				if (i < 3)
					paRight1[j].add(paPara[j][i]);
				else if (i < 9)
					paRight2[j].add(paPara[j][i]);
				else
					paRight3[j].add(paPara[j][i]);
			}

			JPanel paList = new JPanel(new BorderLayout());
			paList.add(new GreenLabJLabel(""), BorderLayout.WEST);
			paList.add(new GreenLabJLabel(("Number"), SwingConstants.CENTER),
					BorderLayout.EAST);
			paRL[j].add(paList);
			for (int i = 0; i < 4; i++) {
				paList = new JPanel(new BorderLayout());
				rlText[j][i] = new GreenLabNumberField(metaName(i).replaceAll(
						"[\\(\\)]", "_"));
				final int jj = j, ii = i;
				rlText[j][i].getDocument().addDocumentListener(
						new DocumentListener() {

							private void actionPerformed() {
								GreenLabPropertyFileReader.get().setProperty(
										metaName(ii), rlText[jj][ii].getText(),
										jj + 1, currentMeta[jj] + 1);
							}

							@Override
							public void changedUpdate(DocumentEvent arg0) {
								actionPerformed();
							}

							@Override
							public void insertUpdate(DocumentEvent arg0) {
								actionPerformed();
							}

							@Override
							public void removeUpdate(DocumentEvent arg0) {
								actionPerformed();
							}
						});
				paList.add(
						new GreenLabJLabel((metaName(i).replaceAll("[\\(\\)]",
								"_"))), BorderLayout.WEST);
				paList.add(rlText[j][i], BorderLayout.EAST);
				paRL[j].add(paList);
			}
		}

		for (int i = 0; i < GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE; i++) {
			jtp.add(GreenLabGUI.getString("PA") + (i + 1), pap[i]);
		}
		JPanel paMainPanel = new JPanel(new BorderLayout());
		this.add(paMainPanel, BorderLayout.NORTH);
		paMainPanel.add(paPanel, BorderLayout.WEST);

		paPanel.add(jtp, BorderLayout.NORTH);
		updateValue();
	}

	public void activeTab(int n) {
		for (int i = 0; i < GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE; i++) {
			if (i <= n) {
				jtp.setEnabledAt(i, true);
			} else {
				jtp.setEnabledAt(i, false);
			}
		}
		// jtp.setSelectedIndex(n);
	}

	public void updateValue() {
		field_Flag_Nu_Ma_Version.setValue(GreenLabPropertyFileReader.get()
				.getProperty("Flag_Nu_Ma_version"));
		for (int j = 0; j < GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE; j++) {
			for (int i = 0; i < GreenLabAtributes.TOPO_META_NUM - j; i++) {
				paMetaText[j][i].setValue(GreenLabPropertyFileReader.get()
						.getProperty(topoMetaName(0), j + 1, i + 1));
			}
		}
		for (int j = 0; j < GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE; j++) {
			for (int i = 1; i < GreenLabAtributes.TOPO_PARA_NUM; i++) {
				if (i != 5 && i != 4)
					paParaText[j][i].setText(GreenLabPropertyFileReader.get()
							.getProperty(topoParaName(i + 1), j + 1));
				else
					controls[j][i - 4].setTextValue(GreenLabPropertyFileReader
							.get().getProperty(topoParaName(i + 1), j + 1));
			}
		}
		for (int i = GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE - 1; i >= 0; i--) {
			updateDetails(i, 0);
		}
	}

	private void updateDetails(int age, int n) {
		for (int k = 0; k < 4; k++) {
			rlText[age][k].setText(GreenLabPropertyFileReader.get()
					.getProperty(metaName(k), age + 1, n + 1));
		}
		paRLMain[age].setBorder(new TitledBorder(
		// GreenLabGUI.getString("Meta")
		// + (age + n + 1) + " -- "
		// +
				GreenLabGUI.getString("metamer_parameters")));
		paRLLabel[age].setIcon(GreenLabAtributes.getImageIcon(this.getClass(), "met" + (age + 1) + (n + 1)
				+ "_trans"));
	}

	private String metaName(int n) {
		String str;
		switch (n) {
		case 0:
			str = "Leaf_Num(Nu_B)";
			break;
		case 1:
			str = "Female_Num(Nu_Ff)";
			break;
		case 2:
			str = "Male_Num(Nu_Fm)";
			break;
		case 3:
			str = "Axillary_Num(Nu_A)";
			break;
		default:
			str = "";
			break;
		}
		return str;
	}

	private String topoMetaName(int n) {
		String str;
		switch (n) {
		case 0:
			str = "Micro_Num(Nu_I)";
			break;
		case 1:
			str = "Leaf_Num(Nu_B)";
			break;
		case 2:
			str = "Female_Num(Nu_Ff)";
			break;
		case 3:
			str = "Male_Num(Nu_Fm)";
			break;
		case 4:
			str = "Axillary_Num(Nu_A)";
			break;
		default:
			str = "";
			break;
		}
		return str;
	}

	private String topoParaName(int n) {
		String str;
		switch (n) {
		case 0:
			str = "topological parameter";
			break;
		case 1:
			str = "Sample_Size(Tr)";
			break;
		case 2:
			str = "Macro_Num(Nu_Ma)";
			break;
		case 3:
			str = "Struc_Jump(st_j)";
			break;
		case 4:
			str = "Reiter_Order(b_o)";
			break;
		case 5:
			str = "Bran_Control(br_a)";
			break;
		case 6:
			str = "Reiter_Control(re_a)";
			break;
		case 7:
			str = "Rest_Func(rs_A)";
			break;
		case 8:
			str = "Rest_Func(rs_B)";
			break;
		case 9:
			str = "rythm_Ratio(rt_a)";
			break;
		case 10:
			str = "branching_proba(a)";
			break;
		case 11:
			str = "Growth_macro(b)";
			break;
		case 12:
			str = "Growth_micro(bu)";
			break;
		case 13:
			str = "Survive(c)";
			break;
		default:
			str = "";
			break;
		}
		return str;
	}

}
