/*
 * Copyright (C) 2012 GroIMP Developer Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.greenlab;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import de.grogra.greenlab.conf.GreenLabAtributes;
import de.grogra.greenlab.conf.GreenLabPropertyFileReader;
import de.grogra.greenlab.ui.elements.GreenLabConstComboBox;
import de.grogra.greenlab.ui.elements.GreenLabJLabel;
import de.grogra.greenlab.ui.elements.GreenLabNumberField;

/**
 * The Source Panel of Growth Panel
 * 
 * @author Cong Ding
 * 
 */
public class GreenLabGrowthSourcePanel extends JPanel {

	private static final long serialVersionUID = 1199462343518132612L;
	private JTabbedPane jtp;
	private ButtonGroup bg[];
	private JPanel pa[];
	private GreenLabNumberField constText[][][];
	private GreenLabNumberField varText[][][];
	private JTextField paraText[];
	private GreenLabConstComboBox constCombo, photoModelCombo;

	GreenLabGrowthSourcePanel() {
		bg = new ButtonGroup[6];
		pa = new JPanel[GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE];
		jtp = new JTabbedPane();
		constText = new GreenLabNumberField[GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE][5][8];
		varText = new GreenLabNumberField[GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE][4][6];
		for (int i = 0; i < GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE; i++) {
			bg[i] = new ButtonGroup();
			pa[i] = new JPanel(new BorderLayout());

			JPanel picPanel = new JPanel(new GridLayout(1, 9));
			picPanel.add(new GreenLabJLabel(""));
			for (int j = 0; j < 8; j++) {
				picPanel.add(new GreenLabJLabel(GreenLabAtributes.getImageIcon(this.getClass(), ""
						+ getOrganType(j + 1).toLowerCase() + "_trans")));
			}

			JPanel mainPA = new JPanel(new GridLayout(13, 9));

			for (int j = 0; j < 9; j++) {
				if (j == 0)
					mainPA.add(new GreenLabJLabel(""));
				else
					mainPA.add(new GreenLabJLabel((getOrganType(j).replaceAll(
							"[\\(\\)]", "_")), SwingConstants.CENTER));
			}

			mainPA.add(new GreenLabJLabel(("source_constant"),
					SwingConstants.RIGHT));
			for (int j = 0; j < 8; j++) {
				mainPA.add(new GreenLabJLabel(""));
			}

			for (int j = 0; j < 5; j++) {
				for (int k = 0; k < 8; k++) {
					if (j == 0)
						constText[i][j][k] = new GreenLabNumberField(
								"source_constant", true);
					else if (j == 1)
						constText[i][j][k] = new GreenLabNumberField(
								"Time_appearance_2_", true);
					else if (j == 2)
						constText[i][j][k] = new GreenLabNumberField(
								"Time_disappearance_3_", true);
					else if (j == 3)
						constText[i][j][k] = new GreenLabNumberField(
								"Time_busy_4_", true);
					else if (j == 4)
						constText[i][j][k] = new GreenLabNumberField(
								"Time_idle_5_", true);
				}
			}

			int n = 0;
			mainPA.add(new GreenLabJLabel((getConstName(n).replaceAll(
					"[\\(\\)]", "_"))));
			for (int j = 0; j < 8; j++) {
				mainPA.add(constText[i][n][j]);
			}

			mainPA.add(new GreenLabJLabel(("source_variable"),
					SwingConstants.RIGHT));
			for (int j = 0; j < 8; j++) {
				mainPA.add(new GreenLabJLabel(""));
			}

			for (int j = 0; j < 4; j++) {
				for (int k = 0; k < 6; k++) {
					varText[i][j][k] = new GreenLabNumberField(
							"source_variable", true);
				}
			}

			for (int k = 0; k < 4; k++) {
				mainPA.add(new GreenLabJLabel((getVarName(k).replaceAll(
						"[\\(\\)]", "_"))));
				for (int j = 0; j < 8; j++) {
					if (j == 5 || j == 7) {
						mainPA.add(new GreenLabJLabel(""));
					} else {
						mainPA.add(varText[i][k][j == 6 ? 5 : j]);
					}
				}
			}

			mainPA.add(new GreenLabJLabel(("source_others"),
					SwingConstants.RIGHT));
			for (int j = 0; j < 8; j++) {
				mainPA.add(new GreenLabJLabel(""));
			}

			for (int k = 1; k < 5; k++) {
				mainPA.add(new GreenLabJLabel((getConstName(k).replaceAll(
						"[\\(\\)]", "_"))));
				for (int j = 0; j < 8; j++) {
					mainPA.add(constText[i][k][j]);
				}
			}

			pa[i].add(picPanel, BorderLayout.NORTH);
			pa[i].add(mainPA, BorderLayout.CENTER);
			jtp.add(pa[i], GreenLabGUI.getString("PA") + (i + 1));
		}

		String str[] = { GreenLabGUI.getString("source_constant"),
				GreenLabGUI.getString("source_variable"),
				GreenLabGUI.getString("from_file") };
		constCombo = new GreenLabConstComboBox(getFieldName(0).replaceAll(
				"[\\(\\)]", "_"), str);
		paraText = new JTextField[9];
		for (int i = 0; i < 10; i++) {
			if (i == 0) {
			} else {
				if (i == 1) {
					String[] strs = { GreenLabGUI.getString("Hydraulic"),
							GreenLabGUI.getString("Beer1"),
							GreenLabGUI.getString("Beer2") };
					photoModelCombo = new GreenLabConstComboBox(getFieldName(1)
							.replaceAll("[\\(\\)]", "_"), strs);
				} else if (i == 3)
					paraText[i - 1] = new GreenLabNumberField(getFieldName(i)
							.replaceAll("[\\(\\)]", "_"));
				else
					paraText[i - 1] = new GreenLabNumberField(getFieldName(i)
							.replaceAll("[\\(\\)]", "_"), true);
			}
		}

		// JPanel paraPanelTop = new JPanel(new GridLayout(1, 4));
		JPanel paraPanelMain = new JPanel(new GridLayout(2, 2));

		JPanel paraPanelMainList[] = new JPanel[4];
		JPanel paraPanelMainBottomList[] = new JPanel[4];
		for (int i = 0; i < 4; i++) {
			paraPanelMainList[i] = new JPanel(new GridLayout(2, 1));
			paraPanelMainBottomList[i] = new JPanel(new GridLayout(3, 1));
			if (i == 0) {
				paraPanelMainList[i].add(new GreenLabJLabel(""));
				paraPanelMainList[i].setBorder(new TitledBorder(GreenLabGUI
						.getString("GrowthSourceTab")));
			} else if (i == 1) {
				paraPanelMainList[i]
						.add(new GreenLabJLabel(GreenLabAtributes.getImageIcon(this.getClass(), "sp_trans")));
				paraPanelMainList[i].setBorder(new TitledBorder(GreenLabGUI
						.getString("Beer")));
			} else if (i == 2) {
				paraPanelMainList[i].add(new GreenLabJLabel(GreenLabAtributes.getImageIcon(this.getClass(), "seed_trans")));
				paraPanelMainList[i].setBorder(new TitledBorder(GreenLabGUI
						.getString("Seed")));
			} else {
				paraPanelMainList[i]
						.add(new GreenLabJLabel(GreenLabAtributes.getImageIcon(this.getClass(), "rp_trans")));
				paraPanelMainList[i].setBorder(new TitledBorder(GreenLabGUI
						.getString("Hydraulic")));
			}

			paraPanelMainList[i].add(paraPanelMainBottomList[i]);
			paraPanelMain.add(paraPanelMainList[i]);
		}
		int n = 0;
		paraPanelMainBottomList[0].add(new GreenLabJLabel((getFieldName(n)
				.replaceAll("[\\(\\)]", "_"))));
		paraPanelMainBottomList[0].add(constCombo);
		n = 1;
		paraPanelMainBottomList[0].add(new GreenLabJLabel((getFieldName(n)
				.replaceAll("[\\(\\)]", "_"))));
		paraPanelMainBottomList[0].add(photoModelCombo);
		paraPanelMainBottomList[0].add(new GreenLabJLabel(""));
		paraPanelMainBottomList[0].add(new GreenLabJLabel(""));
		n = 5;
		paraPanelMainBottomList[1].add(new GreenLabJLabel((getFieldName(n)
				.replaceAll("[\\(\\)]", "_"))));
		paraPanelMainBottomList[1].add(paraText[n - 1]);
		n = 6;
		paraPanelMainBottomList[1].add(new GreenLabJLabel((getFieldName(n)
				.replaceAll("[\\(\\)]", "_"))));
		paraPanelMainBottomList[1].add(paraText[n - 1]);
		n = 7;
		paraPanelMainBottomList[1].add(new GreenLabJLabel((getFieldName(n)
				.replaceAll("[\\(\\)]", "_"))));
		paraPanelMainBottomList[1].add(paraText[n - 1]);
		n = 8;
		paraPanelMainBottomList[2].add(new GreenLabJLabel((getFieldName(n)
				.replaceAll("[\\(\\)]", "_"))));
		paraPanelMainBottomList[2].add(paraText[n - 1]);
		n = 9;
		paraPanelMainBottomList[2].add(new GreenLabJLabel((getFieldName(n)
				.replaceAll("[\\(\\)]", "_"))));
		paraPanelMainBottomList[2].add(paraText[n - 1]);
		paraPanelMainBottomList[2].add(new GreenLabJLabel(""));
		paraPanelMainBottomList[2].add(new GreenLabJLabel(""));
		n = 2;
		paraPanelMainBottomList[3].add(new GreenLabJLabel((getFieldName(n)
				.replaceAll("[\\(\\)]", "_"))));
		paraPanelMainBottomList[3].add(paraText[n - 1]);
		n = 3;
		paraPanelMainBottomList[3].add(new GreenLabJLabel((getFieldName(n)
				.replaceAll("[\\(\\)]", "_"))));
		paraPanelMainBottomList[3].add(paraText[n - 1]);
		n = 4;
		paraPanelMainBottomList[3].add(new GreenLabJLabel((getFieldName(n)
				.replaceAll("[\\(\\)]", "_"))));
		paraPanelMainBottomList[3].add(paraText[n - 1]);

		Box box = Box.createVerticalBox();
		box.add(paraPanelMain);
		box.add(jtp);
		this.setLayout(new BorderLayout());
		this.add(box, BorderLayout.NORTH);

		addAction();
		updateValue();
		constVisiable();
	}

	public void activeTab(int n) {
		for (int i = 0; i < GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE; i++) {
			if (i <= n) {
				jtp.setEnabledAt(i, true);
			} else {
				jtp.setEnabledAt(i, false);
			}
		}
		// jtp.setSelectedIndex(n);
	}

	public void updateValue() {
		for (int i = 0; i < GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE; i++) {
			for (int j = 0; j < 8; j++) {
				for (int k = 0; k < 5; k++) {
					constText[i][k][j].setValue(GreenLabPropertyFileReader
							.get().getProperty(
									getConstName(k) + "__" + (i + 1), (j + 1)));
				}
			}
			for (int j = 0; j < 6; j++) {
				for (int k = 0; k < 4; k++) {
					varText[i][k][j].setValue(GreenLabPropertyFileReader.get()
							.getProperty(
									getOrganType(j == 5 ? 7 : j + 1) + "__"
											+ (i + 1), (k + 1)));
				}
			}
		}
		for (int i = 1; i < 9; i++) {
			paraText[i].setText(GreenLabPropertyFileReader.get().getProperty(
					getFieldName(i + 1)));
		}
		constCombo.setText(Integer.parseInt(GreenLabPropertyFileReader.get()
				.getProperty(getFieldName(0))));
		photoModelCombo.setText(Integer.parseInt(GreenLabPropertyFileReader
				.get().getProperty(getFieldName(1))));
	}

	private void addAction() {
		for (int i = 0; i < GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE; i++) {
			for (int j = 0; j < 8; j++) {
				for (int k = 0; k < 5; k++) {
					final int fi = i, fj = j, fk = k;
					constText[i][k][j].getDocument().addDocumentListener(
							new DocumentListener() {

								public void actionPerformed() {

									GreenLabPropertyFileReader.get()
											.setProperty(
													getConstName(fk) + "__"
															+ (fi + 1),
													constText[fi][fk][fj]
															.getText(),
													(fj + 1));
								}

								@Override
								public void changedUpdate(DocumentEvent e) {
									actionPerformed();
								}

								@Override
								public void insertUpdate(DocumentEvent e) {
									actionPerformed();
								}

								@Override
								public void removeUpdate(DocumentEvent e) {
									actionPerformed();
								}
							});
				}
			}
			for (int j = 0; j < 6; j++) {
				for (int k = 0; k < 4; k++) {
					final int fi = i, fj = j, fk = k;
					varText[i][k][j].getDocument().addDocumentListener(
							new DocumentListener() {

								public void actionPerformed() {

									GreenLabPropertyFileReader.get()
											.setProperty(
													getOrganType(fj == 5 ? 7
															: fj + 1)
															+ "__"
															+ (fi + 1),
													varText[fi][fk][fj]
															.getText(),
													(fk + 1));
								}

								@Override
								public void changedUpdate(DocumentEvent e) {
									actionPerformed();
								}

								@Override
								public void insertUpdate(DocumentEvent e) {
									actionPerformed();
								}

								@Override
								public void removeUpdate(DocumentEvent e) {
									actionPerformed();
								}
							});

				}
			}
		}
		for (int i = 1; i < 9; i++) {
			final int fi = i;
			paraText[i].getDocument().addDocumentListener(
					new DocumentListener() {

						public void actionPerformed() {

							GreenLabPropertyFileReader.get().setProperty(
									getFieldName(fi + 1),
									paraText[fi].getText());
						}

						@Override
						public void changedUpdate(DocumentEvent e) {
							actionPerformed();
						}

						@Override
						public void insertUpdate(DocumentEvent e) {
							actionPerformed();
						}

						@Override
						public void removeUpdate(DocumentEvent e) {
							actionPerformed();
						}
					});
		}
		constCombo.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				constVisiable();
				GreenLabPropertyFileReader.get().setProperty(
						getFieldName(0 + 1), constCombo.getText());
			}
		});

		photoModelCombo.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				photoModelVisiable();
				GreenLabPropertyFileReader.get().setProperty(getFieldName(1),
						photoModelCombo.getPos() + "");
			}
		});
	}

	private void photoModelVisiable() {
		int n = photoModelCombo.getPos();
		boolean h = false;
		if (n == 0) {
			h = true;
		}
		for (int i = 2; i < 5; i++) {
			paraText[i - 1].setEditable(h);
		}
		for (int i = 5; i < 8; i++) {
			paraText[i - 1].setEditable(!h);
		}
	}

	private void constVisiable() {
		boolean v1 = constCombo.getPos() == 0;
		boolean v2 = constCombo.getPos() == 1;
		for (int k = 0; k < GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE; k++) {
			for (int i = 0; i < 1; i++) {
				for (int j = 0; j < 8; j++) {
					constText[k][i][j].setEditable(v1);
				}
			}
			for (int i = 0; i < 4; i++) {
				for (int j = 0; j < 6; j++) {
					varText[k][i][j].setEditable(v2);
				}
			}
		}
	}

	private String getConstName(int i) {
		switch (i) {
		case 0:
			return "Time_Function(Tu_O(1))";
		case 1:
			return "Time_appearance(2)";
		case 2:
			return "Time_disappearance(3)";
		case 3:
			return "Time_busy(4)";
		case 4:
			return "Time_idle(5)";
		default:
			return null;
		}
	}

	private String getVarName(int i) {
		switch (i) {
		case 0:
			return "GC";
		case 1:
			return "min";
		case 2:
			return "GC";
		case 3:
			return "max";
		default:
			return null;
		}
	}

	private String getOrganType(int i) {
		switch (i) {
		case 0:
			return "Organ";
		case 1:
			return "Blade";
		case 2:
			return "Petiole";
		case 3:
			return "Internode";
		case 4:
			return "Femalefruit";
		case 5:
			return "Malefruit";
		case 6:
			return "Layer";
		case 7:
			return "Root";
		case 8:
			return "Branch";
		default:
			return null;
		}
	}

	private String getFieldName(int i) {
		switch (i) {
		case 0:
			return "Flag_Time_function_version";
		case 1:
			return "Flag_field";
		case 2:
			return "Resist_Blade(r_B)";
		case 3:
			return "Resist_Petiole(r_P)";
		case 4:
			return "Resist_Root(r_R)";
		case 5:
			return "Projected_area(Sp)";
		case 6:
			return "Beer_law(rp)";
		case 7:
			return "Beer_law(kp)";
		case 8:
			return "Seed_Biomass(Q0)";
		case 9:
			return "Flow_Biomass(dQ0)";
		default:
			return null;
		}
	}

}
