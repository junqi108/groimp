/*
 * Copyright (C) 2012 GroIMP Developer Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.greenlab.amapsymbol;

import java.io.IOException;

import de.grogra.pf.io.FileSource;
import de.grogra.pf.io.FilterBase;
import de.grogra.pf.io.FilterItem;
import de.grogra.pf.io.FilterSource;
import de.grogra.pf.io.IOFlavor;
import de.grogra.pf.io.ObjectSource;
import de.grogra.pf.io.VirtualFileReaderSource;

/**
 * The SMBFilter describes an object of class of Mesh stored in the SMB file
 * format of the Amap software.
 * 
 * @author yongzhi ong, mhenke
 * 
 */
public class SMBFilter extends FilterBase implements ObjectSource {

	public SMBFilter(FilterItem item, FilterSource source) {
		super(item, source);
		setFlavor(IOFlavor.NODE);
	}

	public SMBFilter(FileSource source) {
		super(null, source);
		setFlavor(IOFlavor.NODE);
	}

	public Object getObject() throws IOException {
		VirtualFileReaderSource fileSource = (VirtualFileReaderSource) getSource();
		SMBReader reader = new SMBReader(fileSource.getFileSystem()
				.getInputStream(fileSource.getFile()), fileSource
				.getFileSystem().getSize(fileSource.getFile()));
		return reader.getObeject();
	}

}