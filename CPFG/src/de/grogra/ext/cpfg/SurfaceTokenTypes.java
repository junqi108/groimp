// $ANTLR 2.7.6 (2005-12-22): "Surface.g" -> "SurfaceParser.java"$


/*
 * Copyright (C) 2002 - 2007 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.ext.cpfg;

import javax.vecmath.*;
import de.grogra.grammar.*;
import de.grogra.graph.*;
import de.grogra.math.*;
import de.grogra.imp3d.objects.*;
import de.grogra.imp3d.objects.Attributes;

public interface SurfaceTokenTypes {
	int EOF = 1;
	int NULL_TREE_LOOKAHEAD = 3;
	int BOOLEAN_LITERAL = 4;
	int INT_LITERAL = 5;
	int LONG_LITERAL = 6;
	int FLOAT_LITERAL = 7;
	int DOUBLE_LITERAL = 8;
	int CHAR_LITERAL = 9;
	int STRING_LITERAL = 10;
	int IDENT = 11;
	int COLON = 12;
	int LITERAL_PRECISION = 13;
	int LITERAL_S = 14;
	int LITERAL_T = 15;
	int LITERAL_CONTACT = 16;
	int LITERAL_POINT = 17;
	int LITERAL_END = 18;
	int LITERAL_HEADING = 19;
	int LITERAL_UP = 20;
	int LITERAL_SIZE = 21;
	int LITERAL_TOP = 22;
	int LITERAL_BOTTOM = 23;
	int LITERAL_AL = 24;
	int LITERAL_A = 25;
	int LITERAL_AR = 26;
	int LITERAL_L = 27;
	int LITERAL_R = 28;
	int LITERAL_BL = 29;
	int LITERAL_B = 30;
	int LITERAL_BR = 31;
	int LITERAL_X = 32;
	int LITERAL_Y = 33;
	int LITERAL_Z = 34;
	int LITERAL_COLOR = 35;
	int LITERAL_DIFFUSE = 36;
	// "~" = 37
}
