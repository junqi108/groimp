package de.grogra.billboard.vrml;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import de.grogra.billboard.BBResources;
import de.grogra.billboard.GridBillboarder;
import de.grogra.pf.ui.Workbench;
import de.grogra.util.MimeType;

public class VRMLGridBuilder extends GridBillboarder implements VRMLBuilder {

	protected String vrmlFileName	= "bb";
	
	private String vrmlTempl 		= "";
	
	protected File vrmlDest;
	
	public VRMLGridBuilder(File file, MimeType mt)
	{
		super (file, mt);		
		
		vrmlDest = new File(file.getPath() + 
								File.separator + 
								vrmlFileName + ".wrl"
							);
		
		imageFolder = "";
		

		vrmlTempl				=	BBResources.getVRMLTempl(this.getClass(), "vrmlgridtempl.txt");
	}
	
	public VRMLGridBuilder(File file, MimeType mt, String subDirectionary) throws IOException
	{
		super (file, mt);		
		
		vrmlDest				= new File(file.getPath() + 
										File.separator + 
										vrmlFileName + ".wrl");

		file					= new File(file.getPath() + 
										File.separator + 
										subDirectionary);
		if(!file.isDirectory())
		{
			file.mkdir();
		}
		
		imageFolder 			= subDirectionary;
		
		vrmlTempl				=	BBResources.getVRMLTempl(this.getClass(), "vrmlgridtempl.txt");
	}
	
	public void writeVRML(int width, int height)
	{
		if(currentSide == 0)
			makeAllNames();
		
		String vrmlOutput		= vrmlTempl;
		String vrmlProtos 		= "";	
		String vrmlProtosDefine = "";
				
		String lineBreak		= System.getProperty("line.separator");
		double rel				= ((double) height)/((double) width);
		
		String defineProto = "\t\t\tShape { " + lineBreak +
			"\t\t\t\tappearance Appearance {" + lineBreak +
				"\t\t\t\t\ttexture ImageTexture { url IS url }" + lineBreak +
			"\t\t\t\t}" + lineBreak +
			"\t\t\t\tgeometry IndexedFaceSet {" + lineBreak +
				"\t\t\t\t\tcoord Coordinate { point [ ::START_X:: ::REL_HEIGHT:: 0, ::END_X:: ::REL_HEIGHT:: 0, ::END_X:: 0 0, ::START_X:: 0 0 ] }" + lineBreak +
				"\t\t\t\t\tcoordIndex [ 0 1 2 3 -1 ]" + lineBreak +
				"\t\t\t\t\ttexCoord TextureCoordinate { point [ ::TEXT_START_X:: 1, ::TEXT_END_X:: 1 , ::TEXT_END_X:: 0, ::TEXT_START_X:: 0 ] }" + lineBreak +
			"\t\t\t\t}"	+ lineBreak +
		"\t\t\t}";

		double startX 		= 0.0;
		double endX 		= 0.0;
		double textStartX 	= 0.0;
		double texEndX 		= 0.0;
		double tempZ		= 0.0;
		double rotation		= 0;
		
		int i 				= 0;
		int tempStep		= steps%2 == 0 ? steps : steps - 1 ;
		
		while(i<(steps*sides))
		{			
			tempZ			= (1/((double) tempStep))*(i%steps) - 0.5;
			tempZ			= (Math.round(tempZ*100D)/100D);
			
			if(i < tempStep)
			{				
				startX		= tempZ;
				endX 		= startX + (1/((double) tempStep));
				textStartX 	= (1/((double) tempStep)) * i;
				texEndX 	= (1/((double) tempStep)) * (i+1);

				vrmlProtosDefine += defineProto.replaceAll("::START_X::", (Double.toString(startX))) + lineBreak;
				vrmlProtosDefine = vrmlProtosDefine.replaceAll("::END_X::", Double.toString(endX));
				vrmlProtosDefine = vrmlProtosDefine.replaceAll("::TEXT_START_X::", Double.toString(textStartX));
				vrmlProtosDefine = vrmlProtosDefine.replaceAll("::TEXT_END_X::", Double.toString(texEndX));
				vrmlProtosDefine =	vrmlProtosDefine.replaceAll("::REL_HEIGHT::", Double.toString(rel));								
			}
			if(i%steps == 0)
			{
				vrmlProtos += "Transform { " + lineBreak + "\trotation 0 1 0 " + rotation + lineBreak + "\tchildren [" + lineBreak;
				rotation -= (Math.PI/2);
			}
			vrmlProtos += "\t\tPngFrame { url \"" + getBBName(i) + "\" trans 0 0 " + (-(tempZ/2)) + "}" + lineBreak;	
			
			if((i+1)%steps == 0)
				vrmlProtos += "\t]" + lineBreak + "}" + lineBreak;
			
			i++;
		}
		
		vrmlOutput	=	vrmlOutput.replaceAll("::PROTO_DEFINE::", vrmlProtosDefine);
		vrmlOutput	=	vrmlOutput.replaceAll("::PNG_PROTO_CALL::", vrmlProtos);
		vrmlOutput	=	vrmlOutput.replaceAll("::BILL_NUM::", Integer.toString(sides));		

		try 
		{
			FileWriter outputStream 	= new FileWriter(vrmlDest);
			BufferedWriter  outputObj	= new BufferedWriter(outputStream);
			outputObj.write(vrmlOutput);
			
			outputObj.flush();
			outputStream.close();
		} catch (IOException ex)
		{
			Workbench.current().logGUIInfo ("Error: " + ex.getMessage());
		}
	
		
	}
	
	@Override
	protected void finalAction()
	{
		// Write the VRML-file
		writeVRML(imgWidth, imgHeight);
		
		//super.finalAction();
	}

}
