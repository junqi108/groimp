package de.grogra.billboard;

import java.io.File;
import java.io.IOException;

import javax.vecmath.Point3d;

import de.grogra.graph.Attributes;
import de.grogra.graph.Graph;
import de.grogra.graph.impl.Edge;
import de.grogra.graph.impl.Node;
import de.grogra.imp3d.objects.Null;
import de.grogra.imp3d.objects.Parallelogram;
import de.grogra.imp3d.objects.ShadedNull;
import de.grogra.imp3d.shading.Light;
import de.grogra.math.TVector3d;
import de.grogra.util.MimeType;
import de.grogra.vecmath.geom.HalfSpace;
import de.grogra.vecmath.geom.Line;
import de.grogra.xl.util.ObjectList;

/**
 * The object which has to be rendered will separate into cake-pieces.
 * 
 * @author adgen
 *
 */

public class CakeBillboarder extends Billboarder {
		
		protected int visibleLayer 		= 0;
		protected int invisibleLayer  	= 15;
		
		protected float centerWidth		= 0.01F;
		protected float centerHeight	= 30F;	
	
		public CakeBillboarder(File file, MimeType mt)
		{		
			super.camera 				= new RotationCamera();	
			super.view3d				= camera.getView3D();
			
			try 
			{				
				super.setDestination(file, mt);
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}			
		}
		
		/**
		 * The center of the cake is a line which shows in positive z direction. The line
		 * starts at the origin of the global coordinates-system. The length of a line is defined
		 * by <code>centerHeight</code>. All objects  with a distance less then <code>centerWidth</code>
		 * are visible too. So the center is a quasi cylinder.
		 * 
		 * @param centerWidth
		 * @param centerHeight
		 */
		public void setCenterDim(float centerWidth, float centerHeight)
		{
			this.centerWidth 	= centerWidth;
			this.centerHeight 	= centerHeight;
		}
		
		/**
		 * Defined which layer is for the visible and which for the invisible
		 * objects. The default-value for visible is 0 and invisible 15. 
		 * 
		 * @param visible The Number of the visible layer in the 3D-view
		 * @param invisible The Number of the invisible layer in the 3D-view
		 */
		public void setLayer(int visible, int invisible)
		{
			if (visible < Attributes.LAYER_COUNT && visible >= 0)
				visibleLayer = visible;
			
			if (invisible < Attributes.LAYER_COUNT && invisible >= 0)
				invisibleLayer = invisible;
		}
		
		@Override
		public void billboarding()
		{
			cake();
			super.billboarding();			
		}
		
		/***
		 * Which pieces of the cake has to be chosen, depends on the actual side of the camera 
		 * position.
		 */
		public void cake()
		{
			cake(currentSide);
		}
		
		
		/**
		 * Rotates two HalfSpaces around z-axis. Both HS are twisted. The angle between HS is
		 * the same like one rotation-step angle. After the intersection of these two HS, exist
		 * a new space. All objects inside in this space will set to a visible Layer. All objects
		 * outside will set to an invisible Layer. Which Layer is visible or not can defined with
		 * {@link #setLayer()}. From the top of view, it looks like a piece of a cake.
		 * 
		 * @param side Which two pieces will be visible.
		 */
		public void cake(int side)
		{		
			final HalfSpace hs1 = new HalfSpace();
			final HalfSpace hs2 = new HalfSpace();
			
			// For the center of the cake
			final Line line		= new Line(new Point3d(), new TVector3d(0,0,1), 0, centerHeight);
			
			// The angle between both HalfSpaces is RotationAngle.
			double radient1 	= Math.toRadians((float) ((RotationAngle * side) - (RotationAngle/2)));
			double radient2 	= Math.toRadians((float) ((RotationAngle * side) + (RotationAngle/2)));
			
			// Origin of the cake
			Point3d origin 		= new Point3d(0.0,0,0.0);
			
			// Rotation for the two HS
			TVector3d axis1 	= new TVector3d(Math.sin(radient1),Math.cos(radient1),0);
			TVector3d axis2		= new TVector3d(Math.sin(radient2),Math.cos(radient2),0);
			
			hs1.setTransformation(origin, axis1);
			hs2.setTransformation(origin, axis2);
			
			// To traverse through the graph
			ObjectList<Node> stack = new ObjectList<Node> (100);
			stack.push (graph().getRoot());
			
			Point3d location = new Point3d();
			while (!stack.isEmpty ())
			{			
				// get next node
				Node n = (Node) stack.pop ();
				if((n instanceof ShadedNull) && (!(n instanceof Light) || (n instanceof Parallelogram)))
				{
					location = location(((Null) n));
					// Exclusive OR: To be visible a node is only allowed to be in one of the Half-Space or
					// in the Bounding-Box-Area. Otherwise the node has to be invisible.
					if( (hs1.contains(location, true) ^ hs2.contains(location, true)) || 
							( line.distance(location) < centerWidth ) )
					{
						// Object is visible
						((Null) n).setLayer(visibleLayer);
					} else {
						// Object is invisible
						((Null) n).setLayer(invisibleLayer);
					}
				}
				// visit all edges e starting with n
				for (Edge e = n.getFirstEdge (); e != null; e = e.getNext (n))
				{
					// Get target node of e
					Node t = e.getTarget ();

					if ((t != n) && e.testEdgeBits (Graph.BRANCH_EDGE | Graph.SUCCESSOR_EDGE))
					{
						stack.push (t);
					}
				}
			}
		}
		
		@Override
		protected void setRunLater()
		{
			// Rotate the camera to the next viewpoint
			nextSide();
											
			// Make only object in the active cake-piece visible
			cake();
						
			// Invoke next render-thread
			billboarding();			
		}
	}