
/*
 * Copyright (C) 2002 - 2007 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.texgen;

import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.util.Random;

import de.grogra.imp.objects.ImageAdapter;

public class AutoregressiveTexture extends SyntheticTexture
{
	// enh:sco

	// image width
	int width = 256;
	// enh:field getter setter

	// image height
	int height = 256;
	// enh:field getter setter

	// contains calculated image data
	float[] image = new float[0];

	private int ensureRange (int value, int min, int max)
	{
		int range = max - min;
		while (value < min)
			value += range;
		while (value >= max)
			value -= range;
		return value;
	}

	protected float getPixel (int x, int y)
	{
		x = ensureRange (x, 0, width);
		y = ensureRange (y, 0, height);
		return image[y * width + x];
	}

	protected void setPixel (int x, int y, float value)
	{
		x = ensureRange (x, 0, width);
		y = ensureRange (y, 0, height);
		image[y * width + x] = value;
	}

	float a0 = -0.1f;
	// enh:field getter setter
	float a1 = 0.5f;
	// enh:field getter setter
	float a2 = -0.1f;
	// enh:field getter setter
	float a3 = 0.5f;
	// enh:field getter setter

	float noise = 0.3f;
	// enh:field getter setter

	private BufferedImage img;

	private static final Random rnd = new Random ();

	private transient int imageStamp = -1;


	public AutoregressiveTexture ()
	{
	}

	@Override
	public BufferedImage getBufferedImage ()
	{
		int s = getStamp ();
		// check if image parameters were modified
		synchronized (this)
		{
			if (s != imageStamp)
			{
				// recreate image
				createImage ();
				// remember stamp
				imageStamp = s;
			}
		}

		return img;
	}

	int randomRGB ()
	{
		return rnd.nextInt ();
	}

	@Override
	public Dimension getPreferredIconSize (boolean small)
	{
		return small ? new Dimension (width * 32 / height, 32) : new Dimension (width, height);
	}

	protected void createImage ()
	{
		// create image buffer if necessary
		if (image == null || image.length < width * height)
		{
			image = new float[width * height];
		}

		// check if buffer fits required size
		assert image.length >= (width * height);

		// generate content for the image
		calculateImageData ();

		// convert to BufferedImage
		img = new BufferedImage (width, height, BufferedImage.TYPE_INT_RGB);
		for (int y = 0; y < height; y++)
		{
			for (int x = 0; x < width; x++)
			{
				// img.getRaster ().setSample (x, y, 0, getPixel (x, y) * 255);
				int v = (int) (getPixel (x, y) * 255);
				// v = x + y;
				v = Math.min (255, Math.max (0, v));
				img.setRGB (x, y, 0x010101 * v);
			}
		}
	}

	protected void calculateImageData ()
	{
		// initialise bottom line
		for (int x = 0; x < width; x++)
		{
			setPixel (x, height - 1, rnd.nextFloat ());
		}

		// initialise right column
		for (int y = 0; y < height; y++)
		{
			setPixel (width - 1, y, rnd.nextFloat ());
		}

		// generate texture
		for (int y = 0; y < height; y++)
		{
			for (int x = 0; x < width; x++)
			{
				// calculate new color value for pixel
				float v = 0;
				v += a0 * getPixel (x - 1, y - 1);
				v += a1 * getPixel (x, y - 1);
				v += a2 * getPixel (x + 1, y - 1);
				v += a3 * getPixel (x - 1, y);
				v += noise * rnd.nextFloat ();

				// set color of calculated pixel
				setPixel (x, y, v);
			}
		}

	}

	// enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final Type $TYPE;

	public static final Type.Field width$FIELD;
	public static final Type.Field height$FIELD;
	public static final Type.Field a0$FIELD;
	public static final Type.Field a1$FIELD;
	public static final Type.Field a2$FIELD;
	public static final Type.Field a3$FIELD;
	public static final Type.Field noise$FIELD;

	public static class Type extends SyntheticTexture.Type
	{
		public Type (Class c, de.grogra.persistence.SCOType supertype)
		{
			super (c, supertype);
		}

		public Type (AutoregressiveTexture representative, de.grogra.persistence.SCOType supertype)
		{
			super (representative, supertype);
		}

		Type (Class c)
		{
			super (c, SyntheticTexture.$TYPE);
		}

		private static final int SUPER_FIELD_COUNT = SyntheticTexture.Type.FIELD_COUNT;
		protected static final int FIELD_COUNT = SyntheticTexture.Type.FIELD_COUNT + 7;

		static Field _addManagedField (Type t, String name, int modifiers, de.grogra.reflect.Type type, de.grogra.reflect.Type componentType, int id)
		{
			return t.addManagedField (name, modifiers, type, componentType, id);
		}

		@Override
		protected void setInt (Object o, int id, int value)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 0:
					((AutoregressiveTexture) o).width = (int) value;
					return;
				case Type.SUPER_FIELD_COUNT + 1:
					((AutoregressiveTexture) o).height = (int) value;
					return;
			}
			super.setInt (o, id, value);
		}

		@Override
		protected int getInt (Object o, int id)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 0:
					return ((AutoregressiveTexture) o).getWidth ();
				case Type.SUPER_FIELD_COUNT + 1:
					return ((AutoregressiveTexture) o).getHeight ();
			}
			return super.getInt (o, id);
		}

		@Override
		protected void setFloat (Object o, int id, float value)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 2:
					((AutoregressiveTexture) o).a0 = (float) value;
					return;
				case Type.SUPER_FIELD_COUNT + 3:
					((AutoregressiveTexture) o).a1 = (float) value;
					return;
				case Type.SUPER_FIELD_COUNT + 4:
					((AutoregressiveTexture) o).a2 = (float) value;
					return;
				case Type.SUPER_FIELD_COUNT + 5:
					((AutoregressiveTexture) o).a3 = (float) value;
					return;
				case Type.SUPER_FIELD_COUNT + 6:
					((AutoregressiveTexture) o).noise = (float) value;
					return;
			}
			super.setFloat (o, id, value);
		}

		@Override
		protected float getFloat (Object o, int id)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 2:
					return ((AutoregressiveTexture) o).getA0 ();
				case Type.SUPER_FIELD_COUNT + 3:
					return ((AutoregressiveTexture) o).getA1 ();
				case Type.SUPER_FIELD_COUNT + 4:
					return ((AutoregressiveTexture) o).getA2 ();
				case Type.SUPER_FIELD_COUNT + 5:
					return ((AutoregressiveTexture) o).getA3 ();
				case Type.SUPER_FIELD_COUNT + 6:
					return ((AutoregressiveTexture) o).getNoise ();
			}
			return super.getFloat (o, id);
		}

		@Override
		public Object newInstance ()
		{
			return new AutoregressiveTexture ();
		}

	}

	public de.grogra.persistence.ManageableType getManageableType ()
	{
		return $TYPE;
	}


	static
	{
		$TYPE = new Type (AutoregressiveTexture.class);
		width$FIELD = Type._addManagedField ($TYPE, "width", 0 | Type.Field.SCO, de.grogra.reflect.Type.INT, null, Type.SUPER_FIELD_COUNT + 0);
		height$FIELD = Type._addManagedField ($TYPE, "height", 0 | Type.Field.SCO, de.grogra.reflect.Type.INT, null, Type.SUPER_FIELD_COUNT + 1);
		a0$FIELD = Type._addManagedField ($TYPE, "a0", 0 | Type.Field.SCO, de.grogra.reflect.Type.FLOAT, null, Type.SUPER_FIELD_COUNT + 2);
		a1$FIELD = Type._addManagedField ($TYPE, "a1", 0 | Type.Field.SCO, de.grogra.reflect.Type.FLOAT, null, Type.SUPER_FIELD_COUNT + 3);
		a2$FIELD = Type._addManagedField ($TYPE, "a2", 0 | Type.Field.SCO, de.grogra.reflect.Type.FLOAT, null, Type.SUPER_FIELD_COUNT + 4);
		a3$FIELD = Type._addManagedField ($TYPE, "a3", 0 | Type.Field.SCO, de.grogra.reflect.Type.FLOAT, null, Type.SUPER_FIELD_COUNT + 5);
		noise$FIELD = Type._addManagedField ($TYPE, "noise", 0 | Type.Field.SCO, de.grogra.reflect.Type.FLOAT, null, Type.SUPER_FIELD_COUNT + 6);
		$TYPE.validate ();
	}

	public int getWidth ()
	{
		return width;
	}

	public void setWidth (int value)
	{
		this.width = (int) value;
	}

	public int getHeight ()
	{
		return height;
	}

	public void setHeight (int value)
	{
		this.height = (int) value;
	}

	public float getA0 ()
	{
		return a0;
	}

	public void setA0 (float value)
	{
		this.a0 = (float) value;
	}

	public float getA1 ()
	{
		return a1;
	}

	public void setA1 (float value)
	{
		this.a1 = (float) value;
	}

	public float getA2 ()
	{
		return a2;
	}

	public void setA2 (float value)
	{
		this.a2 = (float) value;
	}

	public float getA3 ()
	{
		return a3;
	}

	public void setA3 (float value)
	{
		this.a3 = (float) value;
	}

	public float getNoise ()
	{
		return noise;
	}

	public void setNoise (float value)
	{
		this.noise = (float) value;
	}

//enh:end

}
