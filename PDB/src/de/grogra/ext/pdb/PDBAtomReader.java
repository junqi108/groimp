/*
 * Created on 04.06.2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */

package de.grogra.ext.pdb;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

/**
 * @author fdill
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class PDBAtomReader
{

	BufferedReader reader;

	public final int ATOM_NAME_START = 12;
	public final int ATOM_NAME_END = 16;
	public final int AMINO_NAME_START = 17;
	public final int AMINO_NAME_END = 20;
	public final int CHAINID_POS = 21;
	public final int RESIDUE_NR_START = 22;
	public final int RESIDUE_NR_END = 26;
	public final int X_START = 30;
	public final int X_END = 38;
	public final int Y_START = 38;
	public final int Y_END = 46;
	public final int Z_START = 46;
	public final int Z_END = 54;
	public final int SYMBOL_START = 76;
	public final int SYMBOL_END = 78;

	public PDBAtomReader (InputStream stream)
	{
		reader = new BufferedReader (new InputStreamReader (
				new BufferedInputStream (stream)));
	}

	public PDBAtomReader (Reader reader)
	{
		this.reader = new BufferedReader (reader);
	}

	public void read (ProteinFactory factory)
	{
		try
		{
			while (reader.ready ())
			{
				String currentLine = reader.readLine ();
				if (currentLine.startsWith ("ATOM  ")
						&& ((currentLine.charAt (16) == 'A') || (currentLine
								.charAt (16) == ' ')))
				{
					String atomName = currentLine.substring (ATOM_NAME_START,
							ATOM_NAME_END).trim ();
					char chainId = currentLine.charAt (CHAINID_POS);
					String aminoName = currentLine.substring (AMINO_NAME_START,
							AMINO_NAME_END).trim ();
					int resSeqNr = Integer.valueOf (
							currentLine.substring (RESIDUE_NR_START,
									RESIDUE_NR_END).trim ()).intValue ();
					float xCoord = Float.valueOf (
							currentLine.substring (X_START, X_END).trim ())
							.floatValue ();
					float yCoord = Float.valueOf (
							currentLine.substring (Y_START, Y_END).trim ())
							.floatValue ();
					float zCoord = Float.valueOf (
							currentLine.substring (Z_START, Z_END).trim ())
							.floatValue ();
					factory.processAtomEntry (atomName, aminoName, resSeqNr,
							xCoord, yCoord, zCoord);
				}
				//				 RH20060629
				else if (currentLine.startsWith ("TER"))
				{
				}
			}
			factory.closeProtein ();
		}
		catch (IOException e)
		{
			e.printStackTrace ();
		}
	}
}
