/*
 * Created on 04.06.2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package de.grogra.ext.pdb.model;

import java.util.HashMap;
import java.util.Vector;

/**
 * @author fdill
 * Respresenting amino acids. The amino acid holds its atoms. 
 */
public class AminoAcid {

	/**
	 * 
	 * @uml.property name="name"
	 */
	String name;

	/**
	 * 
	 * @uml.property name="resSeqNr"
	 */
	int resSeqNr;

	/**
	 * 
	 * @uml.property name="atoms"
	 */
	Vector atoms;

	public static int numberOfAcids = 0;
	private static HashMap atomNames = null;
	Vector reference;
	/**
	 * By every creation a counter is incremented. 
	 * @param chainId - a field of the pdb file
	 * @param name - the name of the atom
	 * @param resSeqNr - a unique identifier for the amino acid
	 */
	
	public AminoAcid(String name, int resSeqNr) {
		numberOfAcids++;
		this.resSeqNr = resSeqNr;
		this.name = name;
		atoms = new Vector();
		if(atomNames == null){
			atomNames = new HashMap();
			AminoAcid.initAtomNames();
		}
		reference = (Vector)AminoAcid.atomNames.get(name);
	}

	/**
	 * 
	 */
	private static void initAtomNames() {
		Vector names = new Vector();
		names.add("N");names.add("CA");names.add("C");names.add("O");names.add("CB");
		atomNames.put("ALA", names);
		names = new Vector();
		names.add("N");names.add("CA");names.add("C");names.add("O");names.add("CB");names.add("CG");names.add("CD");names.add("NE");names.add("CZ");names.add("NH1");names.add("NH2");
		atomNames.put("ARG", names);
		names = new Vector();
		names.add("N");names.add("CA");names.add("C");names.add("O");names.add("CB");names.add("CG");names.add("OD1");names.add("ND2");
		atomNames.put("ASN", names);
		names = new Vector();
		names.add("N");names.add("CA");names.add("C");names.add("O");names.add("CB");names.add("CG");names.add("OD1");names.add("OD2");
		atomNames.put("ASP", names);
		names = new Vector();
		names.add("N");names.add("CA");names.add("C");names.add("O");names.add("CB");names.add("SG");
		atomNames.put("CYS", names);
		names = new Vector();
		names.add("N");names.add("CA");names.add("C");names.add("O");names.add("CB");names.add("CG");names.add("CD");names.add("OE1");names.add("NE2");
		atomNames.put("GLN", names);
		names = new Vector();
		names.add("N");names.add("CA");names.add("C");names.add("O");names.add("CB");names.add("CG");names.add("CD");names.add("OE1");names.add("OE2");
		atomNames.put("GLU", names);
		names = new Vector();
		names.add("N");names.add("CA");names.add("C");names.add("O");
		atomNames.put("GLY", names);
		names = new Vector();
		names.add("N");names.add("CA");names.add("C");names.add("O");names.add("CB");names.add("CG");names.add("ND1");names.add("CD2");names.add("CE1");names.add("NE2");
		atomNames.put("HIS", names);
		names = new Vector();
		names.add("N");names.add("CA");names.add("C");names.add("O");names.add("CB");names.add("CG1");names.add("CG2");names.add("CD1");
		atomNames.put("ILE", names);
		names = new Vector();
		names.add("N");names.add("CA");names.add("C");names.add("O");names.add("CB");names.add("CG");names.add("CD1");names.add("CD2");
		atomNames.put("LEU", names);
		names = new Vector();
		names.add("N");names.add("CA");names.add("C");names.add("O");names.add("CB");names.add("CG");names.add("CD");names.add("CE");names.add("NZ");
		atomNames.put("LYS", names);
		names = new Vector();
		names.add("N");names.add("CA");names.add("C");names.add("O");names.add("CB");names.add("CG");names.add("SD");names.add("CE");
		atomNames.put("MET", names);
		names = new Vector();
		names.add("N");names.add("CA");names.add("C");names.add("O");names.add("CB");names.add("CG");names.add("CD1");names.add("CD2");names.add("CE1");names.add("CE2");names.add("CZ");
		atomNames.put("PHE", names);
		names = new Vector();
		names.add("N");names.add("CA");names.add("C");names.add("O");names.add("CB");names.add("CG");names.add("CD");
		atomNames.put("PRO", names);
		names = new Vector();
		names.add("N");names.add("CA");names.add("C");names.add("O");names.add("CB");names.add("OG");
		atomNames.put("SER", names);
		names = new Vector();
		names.add("N");names.add("CA");names.add("C");names.add("O");names.add("CB");names.add("OG1");names.add("CG2");
		atomNames.put("THR", names);
		names = new Vector();
		names.add("N");names.add("CA");names.add("C");names.add("O");names.add("CB");names.add("CG");names.add("CD1");names.add("CD2");names.add("NE1");names.add("CE2");names.add("CE3");names.add("CZ2");names.add("CZ3");names.add("CH2");
		atomNames.put("TRP", names);
		names = new Vector();
		names.add("N");names.add("CA");names.add("C");names.add("O");names.add("CB");names.add("CG");names.add("CD1");names.add("CD2");names.add("CE1");names.add("CE2");names.add("CZ");names.add("OH");
		atomNames.put("TYR", names);
		names = new Vector();
		names.add("N");names.add("CA");names.add("C");names.add("O");names.add("CB");names.add("CG1");names.add("CG2");
		atomNames.put("VAL", names);
		names = new Vector();
		names.add("N");names.add("CA");names.add("C");names.add("O");
		atomNames.put("UNK", names);		
	}

	/**
	 * @return
	 * 
	 * @uml.property name="atoms"
	 */
	public Vector getAtoms() {
		return atoms;
	}

	/**
	 * @return
	 * 
	 * @uml.property name="name"
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param vector
	 * 
	 * @uml.property name="atoms"
	 */
	public void setAtoms(Vector vector) {
		atoms = vector;
	}

	/**
	 * @param string
	 * 
	 * @uml.property name="name"
	 */
	public void setName(String string) {
		name = string;
	}

	/**
	 * Adds an atom to this amino acid
	 * @param atom
	 */
	public void addAtom(Atom atom) {
			atoms.addElement(atom); 
	}
	/**
	 * Compares the actual vector of atoms with a reference vector 
	 * from the HashMap which is initialized in the constructor.
	 * If they are not equal a text output is generated.
	 */
	public void validate() {
		boolean isValid = true;
		if(atoms.size() == reference.size()){
			for(int i = 0; i < atoms.size(); i++){
				if(!((Atom)atoms.get(i)).getAtomName().equals(reference.get(i)))isValid = false;
			}
		}else{
			isValid = false;
		}
		if(!isValid && !(((Atom)atoms.lastElement()).getAtomName().equals("OXT")))System.out.println("found nonstandard amino acid: " + resSeqNr + " " + name);		
	}

	/**
	 * @return
	 * 
	 * @uml.property name="resSeqNr"
	 */
	public int getResSeqNr() {
		return resSeqNr;
	}

	/**
	 * @param i
	 * 
	 * @uml.property name="resSeqNr"
	 */
	public void setResSeqNr(int i) {
		resSeqNr = i;
	}

}
