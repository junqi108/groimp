
package de.grogra.ext.pdb.model;

/**
 * @author fdill
 * This is the interface for the data model. Implementing these methods 
 * should enable every data model to be visualized, because the visualizers
 * work on this interface.
 */
public interface IProtein {
	
	public String getAcidName(int sequence, int acid);
	
	public int getResSeqNr(int sequence, int acid);
	
	public float getXCoord(int sequence, int acid, int atom);
	
	public float getYCoord(int sequence, int acid, int atom);
	
	public float getZCoord(int sequence, int acid, int atom);
	
	public String getElement(int sequence, int acid, int atom);
	
	public String getAtomName(int sequence, int acid, int atom);
	
	public int getNumberOfSequences();
	
	public int getNumberOfAcids(int sequence);
	
	public int getNumberOfAtoms(int sequence, int acid);
	
}
