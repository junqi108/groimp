
package de.grogra.ext.pdb;

import java.io.IOException;
import java.io.Reader;

import de.grogra.ext.pdb.model.IProtein;
import de.grogra.ext.pdb.view.BackboneVisualizer;
import de.grogra.ext.pdb.view.BallStickVisualizer;
import de.grogra.ext.pdb.view.SpaceFilledVisualizer;
import de.grogra.graph.Graph;
import de.grogra.pf.io.FilterBase;
import de.grogra.pf.io.FilterItem;
import de.grogra.pf.io.FilterSource;
import de.grogra.pf.io.IOFlavor;
import de.grogra.pf.io.ObjectSource;
import de.grogra.pf.io.ReaderSource;
import de.grogra.pf.registry.Option;
import de.grogra.reflect.Type;
import de.grogra.util.Configurable;
import de.grogra.util.Configuration;
import de.grogra.util.ConfigurationSet;
import de.grogra.util.EnumerationType;
import de.grogra.util.I18NBundle;

public class PDBImport extends FilterBase implements ObjectSource, Configurable
{
	public static final String OPTION_NAME_VISUALISATION_STYLE = "visualisation_style";
	//	public static final int OPTION_VALUE_VISUALISATION_STYLE_

	public static final I18NBundle I18N = I18NBundle
			.getInstance (PDBImport.class);

	public static final Type VISUALISATION_STYLE = new EnumerationType (
			"pdb.visualisation_style", PDBImport.I18N, 3);

	// edges of the visibility graph dont allow cycles
	// (i.e. edge types BRANCH and SUCCESSOR)
	// so structural edges are of type BOND, while edges for node visibility
	// determination are of type BRANCH and bound to the root node
	public static final int BOND_EDGE = Graph.STD_EDGE_5;

	Configuration conf;

	public PDBImport (FilterItem item, FilterSource source)
	{
		super (item, source);
		setFlavor (IOFlavor.NODE);
	}

	public void addConfigurations (ConfigurationSet set)
	{
		// retrieve all options of the subtree of item as list
		Option[] options = Option.getEditableOptions (super.item, true);

		// default configuration from plugin.xml
		//		Configuration 
		conf = new Configuration (options, item);

		// add configuration to configuration set
		set.add (conf);
	}

	//	public Result read (InputStream stream, de.grogra.util.StringMap metaData)
	//			throws IOException, RecognitionException
	public Object getObject () throws IOException
	{
		/*
		 HierarchicalStructureFactory factory = new HierarchicalStructureFactory (
		 "SomeProteinName");
		 PDBAtomReader reader = new PDBAtomReader (stream);
		 reader.read (factory);
		 IProtein protein = factory.getProtein ();
		 //protein.textOutput();
		 PDBStartNode startNode = new PDBStartNode (protein);
		 return new Result (startNode, metaData);
		 */

		// ??
		HierarchicalStructureFactory factory = new HierarchicalStructureFactory (
				"SomeProteinName");

		// obtain reader for input data
		Reader reader = ((ReaderSource) source).getReader ();

		// create atom reader from reader
		PDBAtomReader atomReader = new PDBAtomReader (reader);

		// read first atom into factory
		atomReader.read (factory);

		// create a protein from atom
		IProtein protein = factory.getProtein ();

		// ??
		PDBStartNode startNode = new PDBStartNode (protein);

		// obtain configuration option for visualisation style
		Object obj = conf
				.get (OPTION_NAME_VISUALISATION_STYLE, new Integer (0));
		int style = ((Integer) obj).intValue ();

		// select style depending on configuration#
		Object result = null;
		switch (style)
		{
			default:
			// fall through
			case 0:
				result = new SpaceFilledVisualizer (protein).createGraph ();
				break;
			case 1:
				result = new BackboneVisualizer (protein).createGraph ();
				break;
			case 2:
				result = new BallStickVisualizer (protein).createGraph ();
				break;
		}

		return result;
	}
}
