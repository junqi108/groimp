
package de.grogra.ext.pdb.view;

import java.util.HashMap;
import java.util.Vector;

import de.grogra.ext.pdb.PDBImport;
import de.grogra.ext.pdb.model.IProtein;
import de.grogra.graph.Graph;
import de.grogra.graph.impl.Edge;
import de.grogra.graph.impl.Node;
import de.grogra.imp3d.objects.Cylinder;
import de.grogra.imp3d.objects.Sphere;
import de.grogra.imp3d.shading.RGBAShader;
import de.grogra.imp3d.shading.Shader;
import de.grogra.pf.ui.Command;
import de.grogra.pf.ui.Context;

/**
 * @author fdill, mdube
 * This class is responsible for visualizing the data model as a ball and stick model.
 * The ball and stick model shows the atoms and their bonds. 
 * 
 * RH20060629 changed AColor to RGBAShader
 * RH20060629 changed Appereance to Shader
 * RH20060629 changed EdgeSet to Edge
 * RH20060629 changed EdgeSet.BRANCH to Graph.BRANCH_EDGE
 * RH20060629 changed EdgeSet.GEOMETRY to Graph.BRANCH_EDGE
 * RH20060629 changed EdgeSet.SUCCESSOR to Graph.SUCCESSOR_EDGE
 * RH20060629 changed sphere.basis.set to sphere.setTransform
 * RH20060629 changed stick.basis.set to stick.setTransform
 * RH20060629 changed "stick.radius = ..." to "stick.setRadius(...)"
 * RH20060629 changed "stick.appearance = ..." to "stick.setShader(...)"
 * RH20060629 changed "stick.axis" to "stick.getAxis()"
 */
public class BallStickVisualizer implements Visualizer, Command
{

	private final int C_BOND = 2;

	private final int N_BOND = 0;

	private IProtein protein;

	private static HashMap atomColorMapping = new HashMap ();

	private static HashMap aminoBondsIndices = new HashMap ();

	protected final float BALL_RADIUS = 0.4f;
	protected final float STICK_RADIUS = 0.1f;

	Node seqNode = new Node ();
	Node acidNode = new Node ();

	/**
	 * Creation of HashMaps for coloring the atoms and for bonding of them.
	 * The latter one is realized with the @link StickTupel which holds the indices of 
	 * the two atoms to be bonded.
	 * @param protein - the data model representing the read *.pdb file.
	 */
	public BallStickVisualizer (IProtein protein)
	{
		this.protein = protein;
		atomColorMapping.put ("O", RGBAShader.RED);
		atomColorMapping.put ("N", RGBAShader.BLUE);
		atomColorMapping.put ("C", RGBAShader.DARK_GRAY);
		atomColorMapping.put ("S", RGBAShader.YELLOW);

		Vector indices = new Vector ();
		indices.add (new StickTupel (0, 1));
		indices.add (new StickTupel (1, 4));
		indices.add (new StickTupel (1, 2));
		indices.add (new StickTupel (2, 3));
		aminoBondsIndices.put ("ALA", indices);
		indices = new Vector ();
		indices.add (new StickTupel (0, 1));
		indices.add (new StickTupel (1, 4));
		indices.add (new StickTupel (4, 5));
		indices.add (new StickTupel (5, 6));
		indices.add (new StickTupel (6, 7));
		indices.add (new StickTupel (7, 8));
		indices.add (new StickTupel (8, 9));
		indices.add (new StickTupel (8, 10));
		indices.add (new StickTupel (1, 2));
		indices.add (new StickTupel (2, 3));
		aminoBondsIndices.put ("ARG", indices);
		indices = new Vector ();
		indices.add (new StickTupel (0, 1));
		indices.add (new StickTupel (1, 4));
		indices.add (new StickTupel (4, 5));
		indices.add (new StickTupel (5, 6));
		indices.add (new StickTupel (5, 7));
		indices.add (new StickTupel (1, 2));
		indices.add (new StickTupel (2, 3));
		//they have the same structure
		aminoBondsIndices.put ("ASN", indices);
		aminoBondsIndices.put ("ASP", indices);
		aminoBondsIndices.put ("LEU", indices);
		indices = new Vector ();
		indices.add (new StickTupel (0, 1));
		indices.add (new StickTupel (1, 4));
		indices.add (new StickTupel (4, 5));
		indices.add (new StickTupel (1, 2));
		indices.add (new StickTupel (2, 3));
		aminoBondsIndices.put ("CYS", indices);
		aminoBondsIndices.put ("SER", indices);
		indices = new Vector ();
		indices.add (new StickTupel (0, 1));
		indices.add (new StickTupel (1, 4));
		indices.add (new StickTupel (4, 5));
		indices.add (new StickTupel (5, 6));
		indices.add (new StickTupel (6, 7));
		indices.add (new StickTupel (6, 8));
		indices.add (new StickTupel (1, 2));
		indices.add (new StickTupel (2, 3));
		aminoBondsIndices.put ("GLU", indices);
		aminoBondsIndices.put ("GLN", indices);
		indices = new Vector ();
		indices.add (new StickTupel (0, 1));
		indices.add (new StickTupel (1, 2));
		indices.add (new StickTupel (2, 3));
		aminoBondsIndices.put ("GLY", indices);
		aminoBondsIndices.put ("UNK", indices);
		indices = new Vector ();
		indices.add (new StickTupel (0, 1));
		indices.add (new StickTupel (1, 4));
		indices.add (new StickTupel (4, 5));
		indices.add (new StickTupel (5, 6));
		indices.add (new StickTupel (5, 7));
		indices.add (new StickTupel (6, 8));
		indices.add (new StickTupel (7, 9));
		indices.add (new StickTupel (8, 9));
		indices.add (new StickTupel (1, 2));
		indices.add (new StickTupel (2, 3));
		aminoBondsIndices.put ("HIS", indices);
		indices = new Vector ();
		indices.add (new StickTupel (0, 1));
		indices.add (new StickTupel (1, 4));
		indices.add (new StickTupel (4, 5));
		indices.add (new StickTupel (4, 6));
		indices.add (new StickTupel (5, 7));
		indices.add (new StickTupel (1, 2));
		indices.add (new StickTupel (2, 3));
		aminoBondsIndices.put ("ILE", indices);
		indices = new Vector ();
		indices.add (new StickTupel (0, 1));
		indices.add (new StickTupel (1, 4));
		indices.add (new StickTupel (4, 5));
		indices.add (new StickTupel (5, 6));
		indices.add (new StickTupel (6, 7));
		indices.add (new StickTupel (7, 8));
		indices.add (new StickTupel (1, 2));
		indices.add (new StickTupel (2, 3));
		aminoBondsIndices.put ("LYS", indices);
		indices = new Vector ();
		indices.add (new StickTupel (0, 1));
		indices.add (new StickTupel (1, 4));
		indices.add (new StickTupel (4, 5));
		indices.add (new StickTupel (5, 6));
		indices.add (new StickTupel (6, 7));
		indices.add (new StickTupel (1, 2));
		indices.add (new StickTupel (2, 3));
		aminoBondsIndices.put ("MET", indices);
		indices = new Vector ();
		indices.add (new StickTupel (0, 1));
		indices.add (new StickTupel (1, 4));
		indices.add (new StickTupel (4, 5));
		indices.add (new StickTupel (5, 6));
		indices.add (new StickTupel (5, 7));
		indices.add (new StickTupel (6, 8));
		indices.add (new StickTupel (7, 9));
		indices.add (new StickTupel (8, 10));
		indices.add (new StickTupel (9, 10));
		indices.add (new StickTupel (1, 2));
		indices.add (new StickTupel (2, 3));
		aminoBondsIndices.put ("PHE", indices);
		indices = new Vector ();
		indices.add (new StickTupel (0, 1));
		indices.add (new StickTupel (0, 6));
		indices.add (new StickTupel (1, 4));
		indices.add (new StickTupel (4, 5));
		indices.add (new StickTupel (5, 6));
		indices.add (new StickTupel (1, 2));
		indices.add (new StickTupel (2, 3));
		aminoBondsIndices.put ("PRO", indices);
		indices = new Vector ();
		indices.add (new StickTupel (0, 1));
		indices.add (new StickTupel (1, 4));
		indices.add (new StickTupel (4, 5));
		indices.add (new StickTupel (4, 6));
		indices.add (new StickTupel (1, 2));
		indices.add (new StickTupel (2, 3));
		aminoBondsIndices.put ("THR", indices);
		aminoBondsIndices.put ("VAL", indices);
		indices = new Vector ();
		indices.add (new StickTupel (0, 1));
		indices.add (new StickTupel (1, 4));
		indices.add (new StickTupel (4, 5));
		indices.add (new StickTupel (5, 6));
		indices.add (new StickTupel (5, 7));
		indices.add (new StickTupel (6, 8));
		indices.add (new StickTupel (7, 9));
		indices.add (new StickTupel (7, 10));
		indices.add (new StickTupel (8, 9));
		indices.add (new StickTupel (9, 11));
		indices.add (new StickTupel (10, 12));
		indices.add (new StickTupel (11, 13));
		indices.add (new StickTupel (12, 13));
		indices.add (new StickTupel (1, 2));
		indices.add (new StickTupel (2, 3));
		aminoBondsIndices.put ("TRP", indices);
		indices = new Vector ();
		indices.add (new StickTupel (0, 1));
		indices.add (new StickTupel (1, 4));
		indices.add (new StickTupel (4, 5));
		indices.add (new StickTupel (5, 6));
		indices.add (new StickTupel (5, 7));
		indices.add (new StickTupel (6, 8));
		indices.add (new StickTupel (7, 9));
		indices.add (new StickTupel (8, 10));
		indices.add (new StickTupel (9, 10));
		indices.add (new StickTupel (10, 11));
		indices.add (new StickTupel (1, 2));
		indices.add (new StickTupel (2, 3));
		aminoBondsIndices.put ("TYR", indices);
	}

	// helper function to allow default values
	private Object getAtomColorMapping (Object key)
	{
		Object value = atomColorMapping.get (key);
		if (value == null)
		{
			value = RGBAShader.PINK;
		}
		return value;
	}

	// helper function to allow default values
	private Object getAminoBondsIndices (Object key)
	{
		Object value = aminoBondsIndices.get (key);
		if (value == null)
		{
			value = new Vector ();
		}
		return value;
	}

	/**
	 * @see de.grogra.ext.pdb.Visualizer#createGraph()
	 **/
	public Node createGraph ()
	{
		Node root = new Node ();
		Sphere sphere;
		float size;
		//sequences
		for (int seq = 0; seq < protein.getNumberOfSequences (); seq++)
		{
			seqNode = new Node ();
			//acids
			for (int acid = 0; acid < protein.getNumberOfAcids (seq); acid++)
			{
				acidNode = new Node ();
				Node previousNode = null;
				if (acid > 0)
				{
					createAcidConnection (seq, acid);
				}
				//atoms
				for (int atom = 0; atom < protein.getNumberOfAtoms (seq, acid); atom++)
				{
					sphere = new Sphere (BALL_RADIUS);
					sphere.setTransform (protein.getXCoord (seq, acid, atom),
							protein.getYCoord (seq, acid, atom), protein
									.getZCoord (seq, acid, atom));
					Shader color = (Shader) getAtomColorMapping (protein
							.getElement (seq, acid, atom));
					if (color == null)
					{
						color = RGBAShader.BLACK;
					}
					sphere.setShader (color);
					if (previousNode == null)
					{
						previousNode = acidNode;
					}

//					previousNode.addEdgeBitsTo (sphere, Graph.SUCCESSOR_EDGE,
//							null);
					previousNode.addEdgeBitsTo (sphere, PDBImport.BOND_EDGE,
					null);
					root.addEdgeBitsTo (sphere, Graph.BRANCH_EDGE, null);

					previousNode = sphere;
					//This is a special treatment for the sequence closing OXT entry.
					//It is attached to the backbone C of the C-terminal residue
					if (protein.getAtomName (seq, acid, atom).equals ("OXT"))
					{
						createConnection (seq, acid, C_BOND, atom);
					}
				}
				Vector indices = (Vector) getAminoBondsIndices (protein
						.getAcidName (seq, acid));
				StickTupel tupel = null;
				for (int i = 0; i < indices.size (); i++)
				{
					tupel = (StickTupel) indices.get (i);
					createConnection (seq, acid, tupel.getStartAtomIndex (),
							tupel.getEndAtomIndex ());
				}
				//add the complete acid node to the current sequence node
//				seqNode.addEdgeBitsTo (acidNode, Graph.BRANCH_EDGE, null);
				seqNode.addEdgeBitsTo (acidNode, PDBImport.BOND_EDGE, null);
				root.addEdgeBitsTo (acidNode, Graph.BRANCH_EDGE, null);
			}
//			root.addEdgeBitsTo (seqNode, Graph.BRANCH_EDGE, null);
		}
		return root;
	}

	private void createConnection (int sequence, int acid, int start, int end)
	{
		// RH20060630 
		// problem for file 1A36.pdb
		// LYS should consist of 9 atoms, but only 5 atoms are specified
		// (atoms 283 to 287)
		// workaround: catch out of bounds exception and don't create stick
		try
		{
			Cylinder stick = new Cylinder ();
			stick.setRadius (STICK_RADIUS);
			stick.setEndPoints
				(protein.getXCoord (sequence, acid, start),
				 protein.getYCoord (sequence, acid, start),
				 protein.getZCoord (sequence, acid, start),
				 protein.getXCoord (sequence, acid, end),
				 protein.getYCoord (sequence, acid, end),
				 protein.getZCoord (sequence, acid, end));
			stick.setShader (RGBAShader.WHITE);
			Edge edge = acidNode.getFirstEdge ();
			Node source = null;
			Node target = null;
			for (int i = 0; i <= end; i++)
			{
				target = edge.getTarget ();
				if (i == start)
				{
					source = target;
				}
				edge = edge.getNext (target);
			}
			//System.out.println("source: " + ((Sphere)source).basis.x + " " + end + " target: " + ((Sphere)target).basis.x);
//			stick.addEdgeBitsTo (target, Graph.BRANCH_EDGE, null);
//			source.addEdgeBitsTo (stick, Graph.BRANCH_EDGE, null);

			stick.addEdgeBitsTo (target, PDBImport.BOND_EDGE, null);
			source.addEdgeBitsTo (stick, PDBImport.BOND_EDGE, null);
			source.getAxisParent().addEdgeBitsTo (stick, Graph.BRANCH_EDGE, null);
		}
		// RH20060630
		catch (Exception ex)
		{
			System.out.println (ex);
		}
	}

	private void createAcidConnection (int sequence, int acid)
	{
		Cylinder stick = new Cylinder ();
		stick.setTransform (protein.getXCoord (sequence, acid, N_BOND), protein
				.getYCoord (sequence, acid, N_BOND), protein.getZCoord (
				sequence, acid, N_BOND));
		stick.setRadius (STICK_RADIUS);
		stick.setEndPoints
		(protein.getXCoord (sequence, acid, N_BOND),
		 protein.getYCoord (sequence, acid, N_BOND),
		 protein.getZCoord (sequence, acid, N_BOND),
		 protein.getXCoord (sequence, acid - 1, C_BOND),
		 protein.getYCoord (sequence, acid - 1, C_BOND),
		 protein.getZCoord (sequence, acid - 1, C_BOND));
		stick.setShader (RGBAShader.WHITE);
		Edge edge = seqNode.getFirstEdge ();
		Node source = null;
		Node target = null;
		for (int i = 0; i <= acid; i++)
		{
			target = edge.getTarget ();
			if (i == acid - 1)
			{
				source = target;
			}
			edge = edge.getNext (target);
		}
//		stick.addEdgeBitsTo (target, Graph.BRANCH_EDGE, null);
//		source.addEdgeBitsTo (stick, Graph.BRANCH_EDGE, null);

		stick.addEdgeBitsTo (target, PDBImport.BOND_EDGE, null);
		source.addEdgeBitsTo (stick, PDBImport.BOND_EDGE, null);
		source.getAxisParent().addEdgeBitsTo (stick, Graph.BRANCH_EDGE, null);
}

	public String getCommandName ()
	{
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * This method is executed by choosing the corresponding menu entry "Ball&Stick Model" 
	 */
	public void run (Object info, Context ctx)
	{
		System.out.println ("running command Backbone Modell...");
		//		Editor3D editor = (Editor3D) ctx.getEditor ();
		//		editor.openEditor (createGraph ());
		//		System.out.println ("done");
	}
}
