
package de.grogra.ext.pdb.view;

import de.grogra.graph.impl.Node;

/**
 * @author fdill, mdube
 * Interface for the different visualizations of the data model.
 */
public interface Visualizer {
	/**
	 * Creates a graph by iterating through the 3 hierarchy layers of the protein model.
	 * @return Node - representing the graph of the according model of the given protein
	 */	
	public Node createGraph();

}
