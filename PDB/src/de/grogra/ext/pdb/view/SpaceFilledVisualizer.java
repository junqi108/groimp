
package de.grogra.ext.pdb.view;

import java.util.HashMap;

import de.grogra.ext.pdb.model.IProtein;
import de.grogra.graph.Graph;
import de.grogra.graph.impl.*;
import de.grogra.imp3d.objects.*;
import de.grogra.imp3d.shading.*;
import de.grogra.pf.ui.*;

/**
 * @author fdill, mdube
 * This class is responsible for visualizing the data model as a space filled model.
 * The space filled model shows the atoms in a molecular way.  
 * 
 * RH20060629 changed EdgeSet.BRANCH to Graph.BRANCH_EDGE
 * RH20060629 changed sphere.basis.set to sphere.setTransform
 */
public class SpaceFilledVisualizer implements Visualizer, Command
{

	private static HashMap atomColorMapping = new HashMap ();
	private static HashMap atomSizeMapping = new HashMap ();

	private IProtein protein;

	/**
	 * Creation of  HashMaps for coloring and sizing of the atoms.
	 * @param protein - the data model representing the read *.pdb file.
	 */
	public SpaceFilledVisualizer (IProtein protein)
	{
		super ();
		this.protein = protein;
		atomColorMapping.put ("O", RGBAShader.RED);
		atomColorMapping.put ("N", RGBAShader.BLUE);
		atomColorMapping.put ("C", RGBAShader.DARK_GRAY);
		atomColorMapping.put ("S", RGBAShader.YELLOW);

		atomSizeMapping.put ("O", new Float (1.52));
		atomSizeMapping.put ("N", new Float (1.55));
		atomSizeMapping.put ("C", new Float (1.7));
		atomSizeMapping.put ("S", new Float (1.8));
	}

	// helper function to allow default values
	private Object getAtomSizeMapping(Object key){
		Object value = atomSizeMapping.get( key);
		if (value == null)
		{
			value = new Float(1);
		}
		return value;
	}
	
	// helper function to allow default values
	private Object getAtomColorMapping(Object key){
		Object value = atomColorMapping.get( key);
		if (value == null)
		{
			value = RGBAShader.PINK;
		}
		return value;
	}
	
	/**
	 * @see de.grogra.ext.pdb.Visualizer#createGraph()
	 **/
	public Node createGraph ()
	{
		Node root = new Node ();
		Sphere sphere;
		float size;
		//sequences
		for (int seq = 0; seq < protein.getNumberOfSequences (); seq++)
		{
			//acid
			Node seqNode = new Node ();
			for (int acid = 0; acid < protein.getNumberOfAcids (seq); acid++)
			{
				//atoms
				Node acidNode = new Node ();
				for (int atom = 0; atom < protein.getNumberOfAtoms (seq, acid); atom++)
				{
					size = ((Float) getAtomSizeMapping (protein.getElement (
							seq, acid, atom))).floatValue ();
					sphere = new Sphere (size);
					sphere.setTransform (protein.getXCoord (seq, acid, atom),
							protein.getYCoord (seq, acid, atom), protein
									.getZCoord (seq, acid, atom));
					Shader color = (Shader) getAtomColorMapping (protein
							.getElement (seq, acid, atom));
					sphere.setShader (color);
					if (color == null)
					{
						color = RGBAShader.BLACK;
					}
					acidNode.addEdgeBitsTo (sphere, GraphManager.BRANCH_EDGE,
							null);
				}
				seqNode.addEdgeBitsTo (acidNode, Graph.BRANCH_EDGE, null);
			}
			root.addEdgeBitsTo (seqNode, Graph.BRANCH_EDGE, null);
		}
		return root;
	}

	public String getCommandName ()
	{
		return null;
	}

	/**
	 * This method is executed by choosing the corresponding menu entry "Space Filled Model" 
	 */
	public void run (Object info, Context ctx)
	{
		System.out.println ("running command space filled model...");
//		Editor3D editor = (Editor3D) ctx.getEditor ();
//		editor.openEditor (createGraph ());
//		System.out.println ("done");
	}
}
