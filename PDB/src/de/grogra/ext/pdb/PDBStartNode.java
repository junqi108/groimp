package de.grogra.ext.pdb;

import de.grogra.ext.pdb.model.IProtein;
import de.grogra.ext.pdb.view.BackboneVisualizer;
import de.grogra.ext.pdb.view.BallStickVisualizer;
import de.grogra.ext.pdb.view.SpaceFilledVisualizer;
import de.grogra.graph.impl.Node;
import de.grogra.pf.registry.Item;
import de.grogra.pf.registry.Registry;
import de.grogra.pf.ui.registry.CommandItem;

/**
 * @author fdill
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 * 
 * RH20060629 added null paramter to Registry.getDirectory() as second parameter
 * RH20060629 removed interface RegistryInitializing (obsolote)
 */
public class PDBStartNode extends Node {
	
	private IProtein protein;

	public PDBStartNode(IProtein protein){
		this.protein = protein;
	}
	/**
	 * @see de.grogra.imp.objects.RegistryInitializing#initializeRegistry(de.grogra.imp.registry.Registry)
	 */
	public void initializeRegistry(Registry r) {
		Item d = r.getDirectory ("/menu/PDB Visualization", null);
		CommandItem	spaceFilledModel = new CommandItem ("Space filled Model", new SpaceFilledVisualizer(protein));
		CommandItem backboneModel = new CommandItem ("Backbone Model", new BackboneVisualizer(protein));
		CommandItem ballStickModel = new CommandItem ("Ball&Stick Model", new BallStickVisualizer(protein));
		d.add(spaceFilledModel);
		d.add(backboneModel);
		d.add(ballStickModel);
	}

}
