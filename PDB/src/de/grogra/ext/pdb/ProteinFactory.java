/*
 * Created on 23.06.2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package de.grogra.ext.pdb;

import de.grogra.ext.pdb.model.IProtein;

/**
 * @author fdill, mdube
 * This interface is the connection between some reader and a ProteinFactory.
 * Which can build some data model.
 */
public interface ProteinFactory {
	
	/**
	 * 
	 * @param atomName - the name of the atom in the pdb file known as 'name'
	 * @param aminoName - the name of the amino acid in the pdb file known as 'resName'
	 * @param chainId - 
	 * @param resSeqNr - a unique identifier for the amino acids 
	 * @param x - the x coordinate of the atom
	 * @param y - the y coordinate of the atom
	 * @param z - the z coordinate of the atom
	 */
	public void processAtomEntry(String atomName, String aminoName, int resSeqNr, float x, float y, float z);
	/**
	 * 
	 * @return 
	 */
	public IProtein getProtein();
	
	public void closeProtein();
}
