/*
* Copyright (C) 2002 - 2006 Lehrstuhl Grafische Systeme, BTU Cottbus
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 3
* of the License, or any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

package de.grogra.blocks;

import java.io.File;

import de.grogra.blocks.xFrogFileParser.parser;

public class Test_xFrogFileParser {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("FunktionParser test\n");

		final String green0 = "C:\\Dokumente und Einstellungen\\Besitzer\\Eigene Dateien\\Diplomarbeit\\src\\green\\";
		final String green1 = "C:\\Dokumente und Einstellungen\\Besitzer\\Eigene Dateien\\Diplomarbeit\\src\\green\\meine\\";		
		final String n1 = "Wind_Animation.xfr";
		final String n2 = "Basic_Tree.xfr";
		final String n3 = "Flower_blossom.xfr";
		final String n4 = "Flower_blossom1.xfr";
		final String n5 = "horn_color+texture.xfr";
		
		// np FL06_1.xfr
		
		parser p1 = new parser();
		p1.parseFile(green1+n5, new File(green1), null);

//		System.out.println("Text\n");
//		 System.out.println(p1.toStringBuffer());

//		System.out.println("XL\n");
//		System.out.println(p1.toXL());

//		System.out.println("Xfr -> File OK\n");
//		p1.saveXfr("../test123.xfr");

//		System.out.println("XL -> File OK\n");
//		p1.saveXL("../test123.rgg");
		
		
		System.out.println("\n\nfertsch");
		System.exit(0);
	}

}
