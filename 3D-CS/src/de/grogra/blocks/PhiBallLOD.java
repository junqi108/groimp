
/*
 * Copyright (C) 2002 - 2006 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.blocks;

import javax.vecmath.Vector3d;

import de.grogra.imp.View;
import de.grogra.imp3d.View3D;
import de.grogra.math.Id;
import de.grogra.persistence.SCOType;
import de.grogra.persistence.ShareableBase;
import de.grogra.xl.lang.FloatToFloat;

public class PhiBallLOD extends ShareableBase 
{

	//enh:sco SCOType
	
	boolean useLOD = true;
	// enh:field getter setter		

	int number = 2*View.LOD_MAX;
	// enh:field getter setter min=View.LOD_MIN max=2*View.LOD_MAX

	FloatToFloat numberMode = new Id();
	// enh:field getter setter		

	int minNumber = 0;
	// enh:field getter setter min=0 max=50	
	
	int scale = View.LOD_MIN;
	// enh:field getter setter min=View.LOD_MIN max=2*View.LOD_MAX
	
	FloatToFloat scaleMode = new Id();
	// enh:field getter setter		
	
	
	private final float MAX_PIXEL = 15;
	private float localLod_P = 1.0f;
	private float globalLod_P = 1.0f;

	
	public void set (View view, Vector3d v, float size)
	{
		View3D view3d = (view instanceof View3D) ? (View3D) view : null;		
		// global gesetzte lod wert
		int globalLod = (view3d != null) ? view3d.getViewComponent().getGlobalLOD() : View.LOD_MAX;
		// anzahl pixel, die aktuell benoetigt werden, um eine strecke der laenge 1 zu zeichnen
		float lengthOfOne = size * ((view3d != null) ? view3d.getCanvasCamera().getScaleAt(v.x, v.y, v.z) : MAX_PIXEL);
		// akt global in prozent von lod_max 
		globalLod_P = (globalLod+1)/(float)(View.LOD_MAX+1);
		// akt local length von 1 (in pixel) in prozent von max_pixel 
		localLod_P  = (lengthOfOne/MAX_PIXEL<1) ? lengthOfOne/MAX_PIXEL : 1.0f;
		// akt lod wert: local prozent vom globalen
//		aktLod_P = localLod_P * globalLod_P;
//System.out.println (" lod (" + lengthOfOne+ " = " + localLod_P + ") =!= ("+globalLod+ ":"+globalLod_P+")  \t\t  15 = "+numberToLod(15)+"\t size= "+size);		
	}
	
	private float toAktLod(float value)
	{		
		return numberMode.evaluateFloat(localLod_P) * globalLod_P * value;
	}

	protected int numberToLod(float number)
	{
		if (useLOD) {		
			int n = Math.round(toAktLod((this.number+1)/(float)(2*View.LOD_MAX+1) * number));
			if (n<minNumber) {
				return minNumber;
			}
			return n;
		}
		return Math.round(number);			
	}
	
	protected float scaleToLod(float number)
	{		
		if (useLOD) {		
			return number + (1-(scaleMode.evaluateFloat(localLod_P) * globalLod_P))*((scale+1)/(float)(2*View.LOD_MAX+1));
		}
		return number;			
	}
	
	
	//enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final Type $TYPE;

	public static final Type.Field useLOD$FIELD;
	public static final Type.Field number$FIELD;
	public static final Type.Field numberMode$FIELD;
	public static final Type.Field minNumber$FIELD;
	public static final Type.Field scale$FIELD;
	public static final Type.Field scaleMode$FIELD;

	public static class Type extends SCOType
	{
		public Type (Class c, de.grogra.persistence.SCOType supertype)
		{
			super (c, supertype);
		}

		public Type (PhiBallLOD representative, de.grogra.persistence.SCOType supertype)
		{
			super (representative, supertype);
		}

		Type (Class c)
		{
			super (c, SCOType.$TYPE);
		}

		private static final int SUPER_FIELD_COUNT = SCOType.FIELD_COUNT;
		protected static final int FIELD_COUNT = SCOType.FIELD_COUNT + 6;

		static Field _addManagedField (Type t, String name, int modifiers, de.grogra.reflect.Type type, de.grogra.reflect.Type componentType, int id)
		{
			return t.addManagedField (name, modifiers, type, componentType, id);
		}

		@Override
		protected void setBoolean (Object o, int id, boolean value)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 0:
					((PhiBallLOD) o).useLOD = (boolean) value;
					return;
			}
			super.setBoolean (o, id, value);
		}

		@Override
		protected boolean getBoolean (Object o, int id)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 0:
					return ((PhiBallLOD) o).isUseLOD ();
			}
			return super.getBoolean (o, id);
		}

		@Override
		protected void setInt (Object o, int id, int value)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 1:
					((PhiBallLOD) o).number = (int) value;
					return;
				case Type.SUPER_FIELD_COUNT + 3:
					((PhiBallLOD) o).minNumber = (int) value;
					return;
				case Type.SUPER_FIELD_COUNT + 4:
					((PhiBallLOD) o).scale = (int) value;
					return;
			}
			super.setInt (o, id, value);
		}

		@Override
		protected int getInt (Object o, int id)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 1:
					return ((PhiBallLOD) o).getNumber ();
				case Type.SUPER_FIELD_COUNT + 3:
					return ((PhiBallLOD) o).getMinNumber ();
				case Type.SUPER_FIELD_COUNT + 4:
					return ((PhiBallLOD) o).getScale ();
			}
			return super.getInt (o, id);
		}

		@Override
		protected void setObject (Object o, int id, Object value)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 2:
					((PhiBallLOD) o).numberMode = (FloatToFloat) value;
					return;
				case Type.SUPER_FIELD_COUNT + 5:
					((PhiBallLOD) o).scaleMode = (FloatToFloat) value;
					return;
			}
			super.setObject (o, id, value);
		}

		@Override
		protected Object getObject (Object o, int id)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 2:
					return ((PhiBallLOD) o).getNumberMode ();
				case Type.SUPER_FIELD_COUNT + 5:
					return ((PhiBallLOD) o).getScaleMode ();
			}
			return super.getObject (o, id);
		}

		@Override
		public Object newInstance ()
		{
			return new PhiBallLOD ();
		}

	}

	public de.grogra.persistence.ManageableType getManageableType ()
	{
		return $TYPE;
	}


	static
	{
		$TYPE = new Type (PhiBallLOD.class);
		useLOD$FIELD = Type._addManagedField ($TYPE, "useLOD", 0 | Type.Field.SCO, de.grogra.reflect.Type.BOOLEAN, null, Type.SUPER_FIELD_COUNT + 0);
		number$FIELD = Type._addManagedField ($TYPE, "number", 0 | Type.Field.SCO, de.grogra.reflect.Type.INT, null, Type.SUPER_FIELD_COUNT + 1);
		numberMode$FIELD = Type._addManagedField ($TYPE, "numberMode", 0 | Type.Field.SCO, de.grogra.reflect.ClassAdapter.wrap (FloatToFloat.class), null, Type.SUPER_FIELD_COUNT + 2);
		minNumber$FIELD = Type._addManagedField ($TYPE, "minNumber", 0 | Type.Field.SCO, de.grogra.reflect.Type.INT, null, Type.SUPER_FIELD_COUNT + 3);
		scale$FIELD = Type._addManagedField ($TYPE, "scale", 0 | Type.Field.SCO, de.grogra.reflect.Type.INT, null, Type.SUPER_FIELD_COUNT + 4);
		scaleMode$FIELD = Type._addManagedField ($TYPE, "scaleMode", 0 | Type.Field.SCO, de.grogra.reflect.ClassAdapter.wrap (FloatToFloat.class), null, Type.SUPER_FIELD_COUNT + 5);
		number$FIELD.setMinValue (new Integer (View.LOD_MIN));
		number$FIELD.setMaxValue (new Integer (2*View.LOD_MAX));
		minNumber$FIELD.setMinValue (new Integer (0));
		minNumber$FIELD.setMaxValue (new Integer (50));
		scale$FIELD.setMinValue (new Integer (View.LOD_MIN));
		scale$FIELD.setMaxValue (new Integer (2*View.LOD_MAX));
		$TYPE.validate ();
	}

	public boolean isUseLOD ()
	{
		return useLOD;
	}

	public void setUseLOD (boolean value)
	{
		this.useLOD = (boolean) value;
	}

	public int getNumber ()
	{
		return number;
	}

	public void setNumber (int value)
	{
		this.number = (int) value;
	}

	public int getMinNumber ()
	{
		return minNumber;
	}

	public void setMinNumber (int value)
	{
		this.minNumber = (int) value;
	}

	public int getScale ()
	{
		return scale;
	}

	public void setScale (int value)
	{
		this.scale = (int) value;
	}

	public FloatToFloat getNumberMode ()
	{
		return numberMode;
	}

	public void setNumberMode (FloatToFloat value)
	{
		numberMode$FIELD.setObject (this, value);
	}

	public FloatToFloat getScaleMode ()
	{
		return scaleMode;
	}

	public void setScaleMode (FloatToFloat value)
	{
		scaleMode$FIELD.setObject (this, value);
	}

//enh:end
	
}
