/*
* Copyright (C) 2002 - 2006 Lehrstuhl Grafische Systeme, BTU Cottbus
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 3
* of the License, or any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

/**
 * Log.java
 *
 *
 * @author:	Michael Henke
 * @date:	25.10.2002
 */

package de.grogra.blocks.functionParser;

/*
 * eine Klasse zum Berechnen des Logarithmus
 */
public class Log extends UnaryExpr {

	public Log(Expr operand) {
		super(operand);
	}

	// eval wertet den Operanden
	public double eval() {
		return Math.log(operand.eval());
	}

	public String toString() {
		return "log(" + operand.toString() + ")";
	}
}
