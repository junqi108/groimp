/*
* Copyright (C) 2002 - 2006 Lehrstuhl Grafische Systeme, BTU Cottbus
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 3
* of the License, or any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

/**
 * BinaryMinus.java
 *
 *
 * @author:	Michael Henke
 * @date:	25.10.2002
 */

package de.grogra.blocks.functionParser;

/*
 * eine Klasse für das binäre Minus
 */

public class BinaryMinus extends BinaryExpr {

	public BinaryMinus(Expr left, Expr right) {
		super(left, right);
	}

	public double eval() {
		return left.eval() - right.eval();
	}

	public String toString() {
		return "(" + left.toString() + " - " + right.toString() + ")";
	}
}
