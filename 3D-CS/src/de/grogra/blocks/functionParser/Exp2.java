/*
* Copyright (C) 2002 - 2006 Lehrstuhl Grafische Systeme, BTU Cottbus
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 3
* of the License, or any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

/**
 * Exp2.java
 *
 *
 * @author:	Michael Henke
 * @date:	09.08.2006
 */

package de.grogra.blocks.functionParser;

public class Exp2 extends UnaryExpr {

	public Exp2(Expr operand) {
		super(operand);
	}

	// eval wertet den Operanden aus
	public double eval() {
		return Math.pow(2, operand.eval());
	}

	public String toString() {
		return "exp2(" + operand.toString() + ")";
	}
}
