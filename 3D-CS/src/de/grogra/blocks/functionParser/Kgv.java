/*
* Copyright (C) 2002 - 2006 Lehrstuhl Grafische Systeme, BTU Cottbus
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 3
* of the License, or any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

/**
 * Kgv.java
 *
 *
 * @author:	Michael Henke
 * @date:	10.09.2006
 */
package de.grogra.blocks.functionParser;

import java.math.BigInteger;

/*
 * eine Klasse für die Berechnung kgv(left,right)
 */
public class Kgv extends BinaryExpr {

	public Kgv(Expr left, Expr right) {
		super(left, right);
	}

	private Double berechnen(BigInteger n1, BigInteger n2) {
		BigInteger kgv = new BigInteger("1");
		BigInteger faktor = new BigInteger("2");
		BigInteger Null = new BigInteger("0");
		BigInteger eins = new BigInteger("1");
		BigInteger zwei = new BigInteger("2");
		while (faktor.compareTo(n1) == -1 || faktor.compareTo(n1) == 0
				|| faktor.compareTo(n2) == -1 || faktor.compareTo(n2) == 0) {
			while (n1.remainder(faktor).equals(Null)
					|| n2.remainder(faktor).equals(Null)) {
				if (n1.remainder(faktor).equals(Null)
						&& n2.remainder(faktor).equals(Null)) {
					n1 = n1.divide(faktor);
					n2 = n2.divide(faktor);
					kgv = kgv.multiply(faktor);
				}
				if (n1.remainder(faktor).equals(Null)
						&& !n2.remainder(faktor).equals(Null)) {
					n1 = n1.divide(faktor);
					kgv = kgv.multiply(faktor);
				}
				if (!n1.remainder(faktor).equals(Null)
						&& n2.remainder(faktor).equals(Null)) {
					n2 = n2.divide(faktor);
					kgv = kgv.multiply(faktor);
				}
			}
			do
				if (faktor.compareTo(zwei) == 0)
					faktor = faktor.add(eins);
				else
					faktor = faktor.add(zwei); while (!faktor
					.isProbablePrime(10));
		}
		return new Double(kgv.doubleValue());
	}

	public double eval() {
		return berechnen(new BigInteger("" + left.eval()), 
			new BigInteger("" + right.eval()));
	}

	public String toString() {
		return "kgv(" + left.toString() + ", " + right.toString() + ")";
	}
}
