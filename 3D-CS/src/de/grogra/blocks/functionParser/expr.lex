package de.grogra.blocks.functionParser;

import java_cup.runtime.*;

%%

%cup

%class scanner2
%function next_token
%type Symbol

X =	[xX]
I =	[iI]
P =	[pP]
H =	[hH]
D =	[dD]

digit=[0-9]
digits={digit}+
digits0={digit}*

%%

";"				{ return new Symbol(sym.SEMI); }
","				{ return new Symbol(sym.KOMMA); }
"+"				{ return new Symbol(sym.PLUS); }
"-"				{ return new Symbol(sym.MINUS); }
"*"				{ return new Symbol(sym.MUL); }
"%"				{ return new Symbol(sym.MOD); }
"^"				{ return new Symbol(sym.DACH); }
"/"				{ return new Symbol(sym.DIV); }
"("				{ return new Symbol(sym.KAUF); }
")"				{ return new Symbol(sym.KZU); }
"id"			{ return new Symbol(sym.ID); }
"sin"			{ return new Symbol(sym.SIN); }
"cos"			{ return new Symbol(sym.COS); }
"tan"			{ return new Symbol(sym.TAN); }
"asin"			{ return new Symbol(sym.ASIN); }
"acos"			{ return new Symbol(sym.ACOS); }
"atan"			{ return new Symbol(sym.ATAN); }
"sinh"			{ return new Symbol(sym.SINH); }
"cosh"			{ return new Symbol(sym.COSH); }
"tanh"			{ return new Symbol(sym.TANH); }
"sqrt"			{ return new Symbol(sym.SQRT); }
"sqr"			{ return new Symbol(sym.SQR); }
"pow"			{ return new Symbol(sym.POW); }
"exp"			{ return new Symbol(sym.EXP); }
"exp2"			{ return new Symbol(sym.EXP2); }
"expm1"			{ return new Symbol(sym.EXPM1); }
"log"			{ return new Symbol(sym.LOG); }
"log10"			{ return new Symbol(sym.LOG10); }
"log1p"			{ return new Symbol(sym.LOG1P); }
"abs"			{ return new Symbol(sym.ABS); }
"frac"			{ return new Symbol(sym.FRAC); }
"fac"			{ return new Symbol(sym.FAC); }
"trunc"			{ return new Symbol(sym.TRUNC); }
"phi"			{ return new Symbol(sym.PHI); }
"rad"			{ return new Symbol(sym.RAD); }
"deg"			{ return new Symbol(sym.DEG); }
"round"			{ return new Symbol(sym.ROUND); }
"ceil"			{ return new Symbol(sym.CEIL); }
"floor"			{ return new Symbol(sym.FLOOR); }
"pi"			{ return new Symbol(sym.PI); }
"e"				{ return new Symbol(sym.EULER); }
"rnd"			{ return new Symbol(sym.RND); }
"rndabs"		{ return new Symbol(sym.RNDABS); }
"sign"			{ return new Symbol(sym.SIGN); }
"kgv"			{ return new Symbol(sym.KGV); }
"gcd"			{ return new Symbol(sym.GCD); }
"nsqrt"			{ return new Symbol(sym.NSQRT); }
"random"		{ return new Symbol(sym.RANDOM); }

{X}				{ return new Symbol(sym.X); }
{I}				{ return new Symbol(sym.I); }
"i0"|"I0"		{ return new Symbol(sym.PREI); }
{P}				{ return new Symbol(sym.P); }
{H}				{ return new Symbol(sym.H); }
{D}				{ return new Symbol(sym.D); }
"hl"|"HL"		{ return new Symbol(sym.HL); }
"n1"|"N1"		{ return new Symbol(sym.N1); }
"n2"|"N2"		{ return new Symbol(sym.N2); }
"n3"|"N3"		{ return new Symbol(sym.N3); }

"&&"			{ return new Symbol(sym.AND); }
"||"			{ return new Symbol(sym.OR); }

"<"				{ return new Symbol(sym.KL); }
"<="			{ return new Symbol(sym.KLG); }
">"				{ return new Symbol(sym.GR); }
">="			{ return new Symbol(sym.GRG); }
"=="			{ return new Symbol(sym.GG); }
"!="			{ return new Symbol(sym.NG); }

"!"				{ return new Symbol(sym.NOT); }
"true"			{ return new Symbol(sym.TRUE); }
"false"			{ return new Symbol(sym.FALSE); }

"?"				{ return new Symbol(sym.FZ); }
":"				{ return new Symbol(sym.DP); }


{digits}	{ return new Symbol(sym.NUMBER, new String(yytext())); }
({digits}"."{digits0})|({digits0}"."{digits})	{ return new Symbol(sym.NUMBER, new String(yytext())); }

[ \t\r\n\f]	{ /* white spaces ingnorieren */ }
.   { System.err.println("unexpected function input: "  + yytext()); }
