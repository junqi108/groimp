/*
 * Copyright (C) 2002 - 2006 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.blocks;

import javax.vecmath.Color3f;
import javax.vecmath.Point2f;
import javax.vecmath.Point3f;
import javax.vecmath.Tuple2f;
import javax.vecmath.Tuple3f;
import javax.vecmath.Vector3d;

import de.grogra.blocks.arrangeBlock.GeometricArrange;
import de.grogra.blocks.arrangeBlock.ShareableArrangeBase;
import de.grogra.graph.GraphState;
import de.grogra.graph.Instantiator;
import de.grogra.graph.impl.Edge;
import de.grogra.graph.impl.Node;
import de.grogra.imp.View;
import de.grogra.imp3d.objects.GlobalTransformation;
import de.grogra.imp3d.objects.HeightField;
import de.grogra.imp3d.objects.NetworkHeightField;
import de.grogra.imp3d.objects.Null;
import de.grogra.imp3d.objects.Patch;
import de.grogra.imp3d.objects.RectangularHeightFieldMapping;
import de.grogra.turtle.Rotate;
import de.grogra.turtle.Scale;
import de.grogra.math.Channel;
import de.grogra.math.ChannelData;
import de.grogra.math.ChannelMap;
import de.grogra.math.Graytone;
import de.grogra.math.Id;
import de.grogra.math.SplineFunction;
import de.grogra.persistence.PersistenceField;
import de.grogra.persistence.Transaction;
import de.grogra.rgg.Library;
import de.grogra.rgg.model.Instantiation;
import de.grogra.vecmath.Matrix34d;
import de.grogra.xl.lang.FloatToFloat;

public class Arrange extends Patch implements de.grogra.xl.modules.Instantiator<Instantiation> 
{
	
	/** 
	 * 
	 */
	private static final long serialVersionUID = 8L;

	ShareableArrangeBase arrangeMethod = new GeometricArrange();
	//enh:field getter 

	String number = "";
	// enh:field getter setter
	
	RandomBase random = new RandomBase();
	// enh:field

	boolean fitToRaster = false;
	// enh:field getter setter
	
	FloatToFloat scaleFunction = (FloatToFloat) BlockTools.getObject("/objects/math/functions/ArrangeFunctions/scale", new Id());
	// enh:field getter setter
	
	FloatToFloat spinFunction = (FloatToFloat) BlockTools.getObject("/objects/math/functions/ArrangeFunctions/spin", new Id());
	// enh:field getter setter	
	
	FloatToFloat slopeFunction = (FloatToFloat) BlockTools.getObject("/objects/math/functions/ArrangeFunctions/slope", new Id());
	// enh:field getter setter	

	float densityMin = 0;
	// enh:field getter setter min=0 max=100

	float densityMax = 100;
	// enh:field getter setter min=0 max=100

	boolean useDensityI = true;
	// enh:field getter setter
	
	float densityI = 0;
	// enh:field getter setter min=0 max=1

	ChannelMap density = new Graytone();
	// enh:field setter
	
	Color3f locationParameterMin = new Color3f(0,0,0);
	// enh:field getter setter
	
	Color3f locationParameterMax = new Color3f(256,256,256);
	// enh:field getter setter
	
	FloatToFloat locationParameter1Function = new Id();
	// enh:field getter setter	

	FloatToFloat locationParameter2Function = new Id();
	// enh:field getter setter	

	FloatToFloat locationParameter3Function = new Id();
	// enh:field getter setter	
	
	ChannelMap locationParameter = new Graytone();
	// enh:field getter setter

	ArrangeLOD lod = new ArrangeLOD();
	// enh:field getter setter	
	
	boolean initAll = false;
	// enh:field getter setter
	
	// interne variablen, die von vater uebergeben werden
	private Point2f ids = new Point2f(0, 0);
	private Float densityValue = new Float(0);
	private Tuple2f height = new Point2f(0, 0);
	private Tuple3f nutrientsValues = new Point3f(0, 0, 0);
	private int childId = 0;
	
	
	public static final int MAX_RASTER = 128; // FEST (nur wegen HilbertDither)
	
	private float[][] densityField = null;
	private float[][] densityFieldLimited = null;
	
	private float[][] locationParameter1Field = null;
	private float[][] locationParameter2Field = null;
	private float[][] locationParameter3Field = null;
	private float[][] locationParameter1bField = null;
	private float[][] locationParameter2bField = null;
	private float[][] locationParameter3bField = null;
	
	private RectangularHeightFieldMapping recHfM = null;

	private final float xWidth = 10;
	private final float yWidth = 10;

	private int arrangeStamp = -1;
	private int densityStamp = -1;
	private ChannelMap oldDensity = null;
	private int locationParameterStamp = -1;
	private ChannelMap oldLocationParameter = null;
	
	private float[][] zOffset; // heightField
	
	private final static float MAX_SCALE = 2;
	private final static float MAX_SIZE = 15;
	
	// aktuelle Position auf der grundflaeche, 
	// an der eine instanz erzeugt wird 
	private Tuple3f pos = new Point3f(0, 0, 0);
	
	public Arrange() {
		super(new NetworkHeightField());

		initHeightField();
		initFunctionsToHermit();
	}

	public Arrange(int number) {
		super(new NetworkHeightField());		
		arrangeMethod.setNumber(number);
	
		initHeightField();
		initFunctionsToHermit();
	}

	private void initAttributes() {
		arrangeMethod = new GeometricArrange();
		number = "";
		random = new RandomBase();
		fitToRaster = false;
		scaleFunction = (FloatToFloat) BlockTools.getObject("/objects/math/functions/ArrangeFunctions/scale", new Id());
		spinFunction = (FloatToFloat) BlockTools.getObject("/objects/math/functions/ArrangeFunctions/spin", new Id());
		slopeFunction = (FloatToFloat) BlockTools.getObject("/objects/math/functions/ArrangeFunctions/slope", new Id());
		densityMin = 0;
		densityMax = 100;
		useDensityI = false;
		densityI = 0;
		density = new Graytone();
		locationParameterMin = new Color3f(0,0,0);
		locationParameterMax = new Color3f(256,256,256);
		lod = new ArrangeLOD();
		locationParameter = new Graytone();
		
		initFunctionsToHermit();
	}
		private void initFunctionsToHermit() {	
		((SplineFunction)scaleFunction).setType(SplineFunction.HERMITE);
		((SplineFunction)spinFunction).setType(SplineFunction.HERMITE);
		((SplineFunction)slopeFunction).setType(SplineFunction.HERMITE);
	}
	
	private void initHeightField() {
		recHfM = (RectangularHeightFieldMapping) getHeightField().getMapping();
		recHfM.setXWidth(xWidth);
		recHfM.setYWidth(yWidth);
	}	
	
	public Instantiator getInstantiator() {
		return de.grogra.rgg.model.Instantiation.INSTANTIATOR;
	}

	public void instantiate(Instantiation state) {
		Instantiation inst = (Instantiation) state;
		Library.setSeed(hashCode() + random.initial); // zufallsgenerator
		
		initFields();
		final float maxX = recHfM.getXWidth()+1;
		final float maxY = recHfM.getYWidth()+1;

		float[] xx = null, yy = null;
		if (arrangeMethod != null) {
			arrangeMethod.setAll(maxX, maxY, densityFieldLimited, hashCode() + random.initial);
			xx = arrangeMethod.getXx();
			yy = arrangeMethod.getYy();
			//problem: wird erst bei jedem zweiten mal aktualisiert
			setNumber("" + xx.length);
		}
		
		float x, y, xii;
		final HeightField hf = getHeightField();
		final GraphState gs = GraphState.current(getGraph());
		final float swX = (maxX) / (hf.getUSize(gs));
		final float swY = (maxY) / (hf.getVSize(gs));
		
		// uebergenenen werte holen
		ids = (Point2f)inst.getGraphState().getObjectDefault(this, true, Attributes.ID, null);
//		Float densityValue = (Float)inst.getGraphState().getObjectDefault(this, true, Attributes.DENSITY, 1);
		
		//aktuelle hoehe des obj ueber null bestimmen
		Matrix34d m = GlobalTransformation.get(this, true, inst.getGraphState(),false);
		Vector3d v = new Vector3d();
		m.get(v);		
		height = new Point2f( (float)v.z,
				(Float)inst.getGraphState().getObjectDefault(this, true, Attributes.HEIGHT, 1f)); 
		
		// lod daten setzen
		lod.set(View.get(inst.getGraphState()), v, ((maxX<maxY)?maxY:maxX)/MAX_SIZE);

		for (int i = 0; i < xx.length; i++) {
			childId = i;
			xii = (float)i / (float)(xx.length-1);
if (xx[i]>maxX-1 || yy[i]>maxY-1) {
	System.out.println("BINGO groesser max!!!  "+xx[i]+":"+yy[i]);
	if (xx[i]>maxX-1) xx[i] = maxX-1;
	if (yy[i]>maxY-1) yy[i] = maxY-1;	
}
if (xx[i]<0 || yy[i]<0) {
	System.out.println("BINGO kleiner 0!!!  "+xx[i]+":"+yy[i]);
}
		
			// die zufallswerte hinzufuegen
			x = xx[i] + Library.random(-random.randomX,  random.randomX)
					  + Library.random(-random.randomXY, random.randomXY);			
			while (x > maxX-1 || x < 0) {
				x = xx[i] + Library.random(-random.randomX,  random.randomX)
					      + Library.random(-random.randomXY, random.randomXY);
			}
			y= yy[i] + Library.random(-random.randomY,  random.randomY)
			  	     + Library.random(-random.randomXY, random.randomXY);
			while (y > maxY-1 || y < 0) {
				y = yy[i] + Library.random(-random.randomY,  random.randomY)
					      + Library.random(-random.randomXY, random.randomXY);
			}

			if (fitToRaster) {
				if (Math.abs(x - (((int)(x / swX)) * swX)) < 
					Math.abs(y - (((int)(y / swY)) * swY))) {
					x = ((int)(x / swX)) * swX;
				} else {
					y = ((int)(y / swY)) * swY;
				}
			}			

			float heightObj = interpolZOffset(x, y) * getHeightFieldMapping().getScale();
			height.y = heightObj; 
			pos.x = x;
			pos.y = y;
			pos.z = heightObj;
			inst.producer$push();
			inst.instantiate(new Null(x, y, heightObj));
			inst.instantiate(new Rotate(0, 
					(float)(slopeFunction.evaluateFloat(xii) *2*Math.PI) * (float)(180/Math.PI), 
					(float)(spinFunction.evaluateFloat(xii)  *2*Math.PI) * (float)(180/Math.PI)));
			Scale scaleN = new Scale(lod.scaleToLod(1 + (MAX_SCALE * scaleFunction.evaluateFloat(xii) - MAX_SCALE/2.0f)));
			inst.instantiate(scaleN);		

			int xi = (int)(x*(MAX_RASTER-1)/(maxX-1));
			int yi = (int)(y*(MAX_RASTER-1)/(maxY-1));
			nutrientsValues = new Point3f(locationParameter1bField[xi][yi],
					locationParameter2bField[xi][yi],
					locationParameter3bField[xi][yi]); 
			inst.getGraphState().setInstanceAttribute (Attributes.NUTRIENTS_TUPLE3F, nutrientsValues);
			// anzahl der angehaengten obj weitergeben
			inst.getGraphState().setInstanceAttribute (Attributes.NUMBER_INT, new Integer(xx.length));
			// hoehe des objs weitergeben
			inst.getGraphState().setInstanceAttribute (Attributes.HEIGHT_FLOAT, new Float(heightObj));
			// �latzierungswahrscheinlichkeit weitergeben
			densityValue = densityFieldLimited[xi][yi];
			inst.getGraphState().setInstanceAttribute (Attributes.DENSITY_FLOAT, new Float(densityValue));

//System.out.println("n at ("+x+","+y+", "+heightObj+")"); 413462699
			for (Edge e = getFirstEdge(); e != null; e = e.getNext(this)) {
				Node n = e.getTarget();
				if ((n != this) && e.testEdgeBits(BlockConst.MULTIPLY)) {
					// id des aktuellen Obj weitergeben
					setHeightLocal(inst, scaleN, height.x);					
					inst.getGraphState().setInstanceAttribute(Attributes.ID_TUPLE2F, new Point2f(i, ids.x));					
					inst.instantiate(n);		
				}
			}
			inst.producer$pop(null);
		}
		for (Edge e = getFirstEdge(); e != null; e = e.getNext(this)) {
			Node n = e.getTarget();
			if ((n != this) && e.testEdgeBits(BlockConst.CHILD)) {
				inst.producer$push();
				inst.instantiate(n);
				inst.producer$pop(null);
			}
		}
	}

	private void setHeightLocal(Instantiation inst, Node n, float height0) {
		Matrix34d m = GlobalTransformation.get(n, true, inst.getGraphState(),false);
		Vector3d heighti = new Vector3d();
		m.get(heighti);
//		height.y = (float)Math.abs(heighti.z-height0);
		inst.getGraphState().setInstanceAttribute(Attributes.HEIGHT_FLOAT, new Float(Math.abs(heighti.z-height0)));
	}
	
/*	private float interpolZOffset(float x, float y) {	
		final float xWidth = recHfM.getXWidth();
		final float yWidth = recHfM.getYWidth();
		
		HeightField hf = getHeightField();
		GraphState gs = GraphState.current(getGraph());

		final float maxX = hf.getUSize(gs);		
		final float maxY = hf.getVSize(gs);		
System.out.println("xWidth="+xWidth+"  yWidth="+yWidth+"   maxX="+maxX+"  maxY="+maxY);
		
		final double swX = xWidth / (maxX);
		final double swY = yWidth / (maxY);
		
		final int fx = (int) Math.floor(x / swX);
		final int fy = (int) Math.floor(y / swY);
System.out.println(x+"=x  swX="+swX+"  fx="+fx);
System.out.println(y+"=y  swY="+swY+"  fy="+fy);
		
		final double u = (x - fx * swX);
		final double v = (y - fy * swY);

System.out.println("00= ("+fx+", "+fy+")\t  01= ("+fx+", "+(fy+1)+ ")\t  "+
				   "10= ("+(fx+1)+", "+fy+")\t  11= ("+(fx+1)+", "+(fy+1)+")");
System.out.println("00= "+zOffset[fx][fy]+"\t  01= "+zOffset[fx][fy+1]+ "\t  "+
				   "10= "+zOffset[fx+1][fy]+"\t  11= "+zOffset[fx+1][fy+1]);

double z = zOffset[fx][fy] * (1 - u) * (1 - v) + zOffset[fx][fy + 1] * (1 - u) * v + zOffset[fx + 1][fy] * u * (1 - v) + zOffset[fx + 1][fy + 1] * u * v;

System.out.println("u="+u+"  v="+v+"   z== "+z);
return (float)z;
	}
*/


	private float interpolZOffset(float x, float y) {
		final float xWidth = recHfM.getXWidth();
		final float yWidth = recHfM.getYWidth();
		
		HeightField hf = getHeightField();
		GraphState gs = GraphState.current(getGraph());

		final float maxX = hf.getUSize(gs);		
		final float maxY = hf.getVSize(gs);		
		
		final double swX = xWidth / (maxX);
		final double swY = yWidth / (maxY);
		
		final int fx = (int) Math.floor(x / swX);
		final int fy = (int) Math.floor(y / swY);
		
		final double u = (x - fx * swX);
		final double v = (y - fy * swY);

		return (float)(zOffset[fx][fy] * (1 - u) * (1 - v) + zOffset[fx][((fy+1>=maxY)?(int)maxY-1:fy+1)]
				* (1 - u) * v + zOffset[((fx+1>=maxX)?(int)maxX-1:fx+1)][fy] * u * (1 - v)
				+ zOffset[((fx+1>=maxX)?(int)maxX-1:fx+1)][((fy+1>=maxY)?(int)maxY-1:fy+1)] * u * v);
	}

	
	public HeightField getHeightField() {
		return (HeightField) getGrid();
	}

	public RectangularHeightFieldMapping getHeightFieldMapping() {
		return (RectangularHeightFieldMapping) getHeightField().getMapping();
	}

	public void fieldModified (PersistenceField field, int[] indices, Transaction t)
	{
		super.fieldModified (field, indices, t);
		if (!Transaction.isApplying(t))
		{
			if (field.overlaps (indices, grid$FIELD, null))
			{		
				scannHeightField();
			}
			if (field.overlaps (indices, locationParameterMin$FIELD, null) ||
				field.overlaps (indices, locationParameterMax$FIELD, null) ||
				field.overlaps (indices, locationParameter3Function$FIELD, null) ||
				field.overlaps (indices, locationParameter2Function$FIELD, null) ||
				field.overlaps (indices, locationParameter1Function$FIELD, null))
			{
				scannLocationParameterField();
			}
			if (field.overlaps (indices, densityMin$FIELD, null) || 
				field.overlaps (indices, densityMax$FIELD, null) ||
				field.overlaps (indices, useDensityI$FIELD, null) ||
				field.overlaps (indices, density$FIELD, null) ||
				field.overlaps (indices, densityI$FIELD, null))
			{
				scannDansityField();
			}
			if (field.overlaps (indices, initAll$FIELD, null))
			{
				if (isInitAll()) 
				{
					initAttributes();
					setInitAll(false);
				}
			}			
		}
	}	
	
	public void heightFieldOut(int max) {
		HeightField hf = getHeightField();
		GraphState gs = GraphState.current(getGraph());
		for (int xx = 0; xx < max; xx++) {
			System.out.println("");
			for (int yy = 0; yy < max; yy++) {
				System.out.print(" "+hf.getHeight(xx, yy, gs));
			}
		}
	}	
	
	private void scannHeightField() {
		// Scanning of heightField
		HeightField hf = getHeightField();
		GraphState gs = GraphState.current(getGraph());
		int maxX = hf.getUSize(gs);
		int maxY = hf.getVSize(gs);
		zOffset = new float[maxX][maxY];
		// 0 <= x < maxX
		for (int xx = 0; xx < maxX; xx++) {
			for (int yy = 0; yy < maxY; yy++) {
				zOffset[xx][yy] = hf.getHeight(xx, yy, gs);
			}
		}
	}
	
	private void scannLocationParameterField() {
		locationParameter1bField = new float[MAX_RASTER][MAX_RASTER];
		locationParameter2bField = new float[MAX_RASTER][MAX_RASTER];
		locationParameter3bField = new float[MAX_RASTER][MAX_RASTER];		
			
		for (int xx = 0; xx < MAX_RASTER; xx++) {
			for (int yy = 0; yy < MAX_RASTER; yy++) {
				// werte ober und unterhalb der schranken ignorieren
				if (locationParameter1Field[xx][yy] < locationParameterMin.x || locationParameter1Field[xx][yy] > locationParameterMax.x) {
					locationParameter1bField[xx][yy] = 0;
				} else {
					locationParameter1bField[xx][yy] = locationParameter1Function.evaluateFloat(locationParameter1Field[xx][yy]);
				}
				// werte ober und unterhalb der schranken ignorieren
				if (locationParameter2Field[xx][yy] < locationParameterMin.y || locationParameter2Field[xx][yy] > locationParameterMax.y) {
					locationParameter2bField[xx][yy] = 0;
				} else {
					locationParameter2bField[xx][yy] = locationParameter2Function.evaluateFloat(locationParameter2Field[xx][yy]);
				}
				// werte ober und unterhalb der schranken ignorieren
				if (locationParameter3Field[xx][yy] < locationParameterMin.z || locationParameter3Field[xx][yy] > locationParameterMax.z) {
					locationParameter3bField[xx][yy] = 0;
				} else {
					locationParameter3bField[xx][yy] = locationParameter3Function.evaluateFloat(locationParameter3Field[xx][yy]);
				}
			}
		}
	}
	
	private void scannDansityField() {
		// min/max mapping
		final float SIGMA = 10;		
		final float densityOpt = (float) (densityMin + Math.abs(densityMax - densityMin) / 2.0);
		final double MAX1 = 1.0 / (SIGMA * Math.sqrt(2 * Math.PI));
		final float SIGMA2 = 2 * SIGMA * SIGMA;
		final double SIGMA3 = SIGMA * Math.sqrt(2 * Math.PI);
		densityFieldLimited = new float[MAX_RASTER][MAX_RASTER];
		for (int xx = 0; xx < MAX_RASTER; xx++) {
			for (int yy = 0; yy < MAX_RASTER; yy++) {
				// werte ober und unterhalb der schranken ignorieren
				if (densityField[xx][yy] < densityMin || densityField[xx][yy] > densityMax) {
					densityFieldLimited[xx][yy] = 0;
				} else {
					if (useDensityI) {
						if (densityI==0) {
							densityFieldLimited[xx][yy] = 1;
						} else {
							// zwischen min- und max normal verteilen 
							// (mittelpunktbetont, unter beachtung der densityI einstellung)
							double yi = ((Math.exp(-(densityField[xx][yy] - densityOpt)
									* (densityField[xx][yy] - densityOpt) / SIGMA2) / SIGMA3) / MAX1);
							densityFieldLimited[xx][yy] = (float) (1-((1-yi)*densityI));
						}								
					} else {
						densityFieldLimited[xx][yy] = densityField[xx][yy]/100f;
					}
				}
			}
		}
	}
	
	private void initFields() {
		recHfM = (RectangularHeightFieldMapping) getHeightField().getMapping();
		if (arrangeStamp != getStamp()) {
			arrangeStamp = getStamp();
			if (zOffset == null) 
			{
				scannHeightField();
			}
			ChannelData channelSrc = new ChannelData();
			// Preparation
			if ((oldDensity != density)||(densityStamp != density.getStamp()))
			{				
				oldDensity=density;
				densityStamp=density.getStamp();
				ChannelData densitySink = channelSrc.createSink(density);
				densityField = new float[MAX_RASTER][MAX_RASTER];
				for (int xx = 0; xx < MAX_RASTER; xx++) {
					for (int yy = 0; yy < MAX_RASTER; yy++) {
						channelSrc.setFloat(Channel.U, xx / (float)MAX_RASTER);
						channelSrc.setFloat(Channel.V, yy / (float)MAX_RASTER);
						channelSrc.setFloat(Channel.X, xx / (float)MAX_RASTER);
						channelSrc.setFloat(Channel.Y, yy / (float)MAX_RASTER);
						channelSrc.setFloat(Channel.Z, 0);
						densityField[yy][xx] = 100 * densitySink.getFloatValue(null, Channel.Z);
					}
				}
				scannDansityField();
			}
			if ((oldLocationParameter != locationParameter)||(locationParameterStamp != locationParameter.getStamp()))
			{
				oldLocationParameter = locationParameter;
				locationParameterStamp = locationParameter.getStamp();
				ChannelData nutrientsSink = channelSrc.createSink(locationParameter);
				locationParameter1Field = new float[MAX_RASTER][MAX_RASTER];
				locationParameter2Field = new float[MAX_RASTER][MAX_RASTER];
				locationParameter3Field = new float[MAX_RASTER][MAX_RASTER];
				for (int xx = 0; xx < MAX_RASTER; xx++) {
					for (int yy = 0; yy < MAX_RASTER; yy++) {
						channelSrc.setFloat(Channel.U, xx / (float)MAX_RASTER);
						channelSrc.setFloat(Channel.V, yy / (float)MAX_RASTER);
						channelSrc.setFloat(Channel.X, xx / (float)MAX_RASTER);
						channelSrc.setFloat(Channel.Y, yy / (float)MAX_RASTER);
						channelSrc.setFloat(Channel.Z, 0);
						locationParameter1Field[xx][yy] = nutrientsSink.getFloatValue(null, Channel.X);
						locationParameter2Field[xx][yy] = nutrientsSink.getFloatValue(null, Channel.Y);
						locationParameter3Field[xx][yy] = nutrientsSink.getFloatValue(null, Channel.Z);						
					}
				}
				scannLocationParameterField();
			}
		}
	}

	public void setXWidth(float value) {
		recHfM = getHeightFieldMapping();
		recHfM.setXWidth(value);
	}

	public void setYWidth(float value) {
		recHfM = getHeightFieldMapping();
		recHfM.setYWidth(value);
	}

	public void setDimension(float valueX, float valueY) {
		recHfM = getHeightFieldMapping();
		recHfM.setXWidth(valueX);
		recHfM.setYWidth(valueY);
	}
	
	public void setDimension(float valueX, float valueY, float scale) {
		recHfM = getHeightFieldMapping();
		recHfM.setXWidth(valueX);
		recHfM.setYWidth(valueY);
		recHfM.setScale(scale);
	}	

	public void setNumber(int number) {
		arrangeMethod.setNumber(number);
	}

	public void setArrangeMethod (ShareableArrangeBase arrangeMethod)
	{
		this.arrangeMethod = arrangeMethod;
	}
	

	public void useLod (boolean value)
	{
		lod.setUseLOD(value);
	}		

	public int getChildId() {
		return childId;
	}
	
	public int getParentId() {
		return (int)ids.x;
	}
	
	public int getThisId() {
		return (int)ids.y;
	}	

	public float getDensity() {
		return densityValue;
	}
	
	public ChannelMap getDensityField ()
	{
		return density;
	}
	
	public float getAbsoluteHeight() {
		return height.x;
	}
	
	public float getLocalHeight() {
		return height.y;
	}
	
	public float getN1() {
		return nutrientsValues.x;
	}	

	public float getN2() {
		return nutrientsValues.y;
	}
	
	public float getN3() {
		return nutrientsValues.z;
	}		
	
	public void setHeightFieldScale(float value) {
		getHeightFieldMapping().setScale(value);
	}

	public Tuple3f getPos() {
		return pos;
	}

	
	
	// enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final NType $TYPE;

	public static final NType.Field arrangeMethod$FIELD;
	public static final NType.Field number$FIELD;
	public static final NType.Field random$FIELD;
	public static final NType.Field fitToRaster$FIELD;
	public static final NType.Field scaleFunction$FIELD;
	public static final NType.Field spinFunction$FIELD;
	public static final NType.Field slopeFunction$FIELD;
	public static final NType.Field densityMin$FIELD;
	public static final NType.Field densityMax$FIELD;
	public static final NType.Field useDensityI$FIELD;
	public static final NType.Field densityI$FIELD;
	public static final NType.Field density$FIELD;
	public static final NType.Field locationParameterMin$FIELD;
	public static final NType.Field locationParameterMax$FIELD;
	public static final NType.Field locationParameter1Function$FIELD;
	public static final NType.Field locationParameter2Function$FIELD;
	public static final NType.Field locationParameter3Function$FIELD;
	public static final NType.Field locationParameter$FIELD;
	public static final NType.Field lod$FIELD;
	public static final NType.Field initAll$FIELD;

	private static final class _Field extends NType.Field
	{
		private final int id;

		_Field (String name, int modifiers, de.grogra.reflect.Type type, de.grogra.reflect.Type componentType, int id)
		{
			super (Arrange.$TYPE, name, modifiers, type, componentType);
			this.id = id;
		}

		@Override
		public void setBoolean (Object o, boolean value)
		{
			switch (id)
			{
				case 3:
					((Arrange) o).fitToRaster = (boolean) value;
					return;
				case 9:
					((Arrange) o).useDensityI = (boolean) value;
					return;
				case 19:
					((Arrange) o).initAll = (boolean) value;
					return;
			}
			super.setBoolean (o, value);
		}

		@Override
		public boolean getBoolean (Object o)
		{
			switch (id)
			{
				case 3:
					return ((Arrange) o).isFitToRaster ();
				case 9:
					return ((Arrange) o).isUseDensityI ();
				case 19:
					return ((Arrange) o).isInitAll ();
			}
			return super.getBoolean (o);
		}

		@Override
		public void setFloat (Object o, float value)
		{
			switch (id)
			{
				case 7:
					((Arrange) o).densityMin = (float) value;
					return;
				case 8:
					((Arrange) o).densityMax = (float) value;
					return;
				case 10:
					((Arrange) o).densityI = (float) value;
					return;
			}
			super.setFloat (o, value);
		}

		@Override
		public float getFloat (Object o)
		{
			switch (id)
			{
				case 7:
					return ((Arrange) o).getDensityMin ();
				case 8:
					return ((Arrange) o).getDensityMax ();
				case 10:
					return ((Arrange) o).getDensityI ();
			}
			return super.getFloat (o);
		}

		@Override
		protected void setObjectImpl (Object o, Object value)
		{
			switch (id)
			{
				case 0:
					((Arrange) o).arrangeMethod = (ShareableArrangeBase) value;
					return;
				case 1:
					((Arrange) o).number = (String) value;
					return;
				case 2:
					((Arrange) o).random = (RandomBase) value;
					return;
				case 4:
					((Arrange) o).scaleFunction = (FloatToFloat) value;
					return;
				case 5:
					((Arrange) o).spinFunction = (FloatToFloat) value;
					return;
				case 6:
					((Arrange) o).slopeFunction = (FloatToFloat) value;
					return;
				case 11:
					((Arrange) o).density = (ChannelMap) value;
					return;
				case 12:
					((Arrange) o).locationParameterMin = (Color3f) value;
					return;
				case 13:
					((Arrange) o).locationParameterMax = (Color3f) value;
					return;
				case 14:
					((Arrange) o).locationParameter1Function = (FloatToFloat) value;
					return;
				case 15:
					((Arrange) o).locationParameter2Function = (FloatToFloat) value;
					return;
				case 16:
					((Arrange) o).locationParameter3Function = (FloatToFloat) value;
					return;
				case 17:
					((Arrange) o).locationParameter = (ChannelMap) value;
					return;
				case 18:
					((Arrange) o).lod = (ArrangeLOD) value;
					return;
			}
			super.setObjectImpl (o, value);
		}

		@Override
		public Object getObject (Object o)
		{
			switch (id)
			{
				case 0:
					return ((Arrange) o).getArrangeMethod ();
				case 1:
					return ((Arrange) o).getNumber ();
				case 2:
					return ((Arrange) o).random;
				case 4:
					return ((Arrange) o).getScaleFunction ();
				case 5:
					return ((Arrange) o).getSpinFunction ();
				case 6:
					return ((Arrange) o).getSlopeFunction ();
				case 11:
					return ((Arrange) o).density;
				case 12:
					return ((Arrange) o).getLocationParameterMin ();
				case 13:
					return ((Arrange) o).getLocationParameterMax ();
				case 14:
					return ((Arrange) o).getLocationParameter1Function ();
				case 15:
					return ((Arrange) o).getLocationParameter2Function ();
				case 16:
					return ((Arrange) o).getLocationParameter3Function ();
				case 17:
					return ((Arrange) o).getLocationParameter ();
				case 18:
					return ((Arrange) o).getLod ();
			}
			return super.getObject (o);
		}
	}

	static
	{
		$TYPE = new NType (new Arrange ());
		$TYPE.addManagedField (arrangeMethod$FIELD = new _Field ("arrangeMethod", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (ShareableArrangeBase.class), null, 0));
		$TYPE.addManagedField (number$FIELD = new _Field ("number", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (String.class), null, 1));
		$TYPE.addManagedField (random$FIELD = new _Field ("random", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (RandomBase.class), null, 2));
		$TYPE.addManagedField (fitToRaster$FIELD = new _Field ("fitToRaster", 0 | _Field.SCO, de.grogra.reflect.Type.BOOLEAN, null, 3));
		$TYPE.addManagedField (scaleFunction$FIELD = new _Field ("scaleFunction", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (FloatToFloat.class), null, 4));
		$TYPE.addManagedField (spinFunction$FIELD = new _Field ("spinFunction", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (FloatToFloat.class), null, 5));
		$TYPE.addManagedField (slopeFunction$FIELD = new _Field ("slopeFunction", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (FloatToFloat.class), null, 6));
		$TYPE.addManagedField (densityMin$FIELD = new _Field ("densityMin", 0 | _Field.SCO, de.grogra.reflect.Type.FLOAT, null, 7));
		$TYPE.addManagedField (densityMax$FIELD = new _Field ("densityMax", 0 | _Field.SCO, de.grogra.reflect.Type.FLOAT, null, 8));
		$TYPE.addManagedField (useDensityI$FIELD = new _Field ("useDensityI", 0 | _Field.SCO, de.grogra.reflect.Type.BOOLEAN, null, 9));
		$TYPE.addManagedField (densityI$FIELD = new _Field ("densityI", 0 | _Field.SCO, de.grogra.reflect.Type.FLOAT, null, 10));
		$TYPE.addManagedField (density$FIELD = new _Field ("density", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (ChannelMap.class), null, 11));
		$TYPE.addManagedField (locationParameterMin$FIELD = new _Field ("locationParameterMin", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (Color3f.class), null, 12));
		$TYPE.addManagedField (locationParameterMax$FIELD = new _Field ("locationParameterMax", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (Color3f.class), null, 13));
		$TYPE.addManagedField (locationParameter1Function$FIELD = new _Field ("locationParameter1Function", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (FloatToFloat.class), null, 14));
		$TYPE.addManagedField (locationParameter2Function$FIELD = new _Field ("locationParameter2Function", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (FloatToFloat.class), null, 15));
		$TYPE.addManagedField (locationParameter3Function$FIELD = new _Field ("locationParameter3Function", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (FloatToFloat.class), null, 16));
		$TYPE.addManagedField (locationParameter$FIELD = new _Field ("locationParameter", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (ChannelMap.class), null, 17));
		$TYPE.addManagedField (lod$FIELD = new _Field ("lod", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (ArrangeLOD.class), null, 18));
		$TYPE.addManagedField (initAll$FIELD = new _Field ("initAll", 0 | _Field.SCO, de.grogra.reflect.Type.BOOLEAN, null, 19));
		densityMin$FIELD.setMinValue (new Float (0));
		densityMin$FIELD.setMaxValue (new Float (100));
		densityMax$FIELD.setMinValue (new Float (0));
		densityMax$FIELD.setMaxValue (new Float (100));
		densityI$FIELD.setMinValue (new Float (0));
		densityI$FIELD.setMaxValue (new Float (1));
		$TYPE.validate ();
	}

	@Override
	protected NType getNTypeImpl ()
	{
		return $TYPE;
	}

	@Override
	protected de.grogra.graph.impl.Node newInstance ()
	{
		return new Arrange ();
	}

	public boolean isFitToRaster ()
	{
		return fitToRaster;
	}

	public void setFitToRaster (boolean value)
	{
		this.fitToRaster = (boolean) value;
	}

	public boolean isUseDensityI ()
	{
		return useDensityI;
	}

	public void setUseDensityI (boolean value)
	{
		this.useDensityI = (boolean) value;
	}

	public boolean isInitAll ()
	{
		return initAll;
	}

	public void setInitAll (boolean value)
	{
		this.initAll = (boolean) value;
	}

	public float getDensityMin ()
	{
		return densityMin;
	}

	public void setDensityMin (float value)
	{
		this.densityMin = (float) value;
	}

	public float getDensityMax ()
	{
		return densityMax;
	}

	public void setDensityMax (float value)
	{
		this.densityMax = (float) value;
	}

	public float getDensityI ()
	{
		return densityI;
	}

	public void setDensityI (float value)
	{
		this.densityI = (float) value;
	}

	public ShareableArrangeBase getArrangeMethod ()
	{
		return arrangeMethod;
	}

	public String getNumber ()
	{
		return number;
	}

	public void setNumber (String value)
	{
		number$FIELD.setObject (this, value);
	}

	public FloatToFloat getScaleFunction ()
	{
		return scaleFunction;
	}

	public void setScaleFunction (FloatToFloat value)
	{
		scaleFunction$FIELD.setObject (this, value);
	}

	public FloatToFloat getSpinFunction ()
	{
		return spinFunction;
	}

	public void setSpinFunction (FloatToFloat value)
	{
		spinFunction$FIELD.setObject (this, value);
	}

	public FloatToFloat getSlopeFunction ()
	{
		return slopeFunction;
	}

	public void setSlopeFunction (FloatToFloat value)
	{
		slopeFunction$FIELD.setObject (this, value);
	}

	public void setDensity (ChannelMap value)
	{
		density$FIELD.setObject (this, value);
	}

	public Color3f getLocationParameterMin ()
	{
		return locationParameterMin;
	}

	public void setLocationParameterMin (Color3f value)
	{
		locationParameterMin$FIELD.setObject (this, value);
	}

	public Color3f getLocationParameterMax ()
	{
		return locationParameterMax;
	}

	public void setLocationParameterMax (Color3f value)
	{
		locationParameterMax$FIELD.setObject (this, value);
	}

	public FloatToFloat getLocationParameter1Function ()
	{
		return locationParameter1Function;
	}

	public void setLocationParameter1Function (FloatToFloat value)
	{
		locationParameter1Function$FIELD.setObject (this, value);
	}

	public FloatToFloat getLocationParameter2Function ()
	{
		return locationParameter2Function;
	}

	public void setLocationParameter2Function (FloatToFloat value)
	{
		locationParameter2Function$FIELD.setObject (this, value);
	}

	public FloatToFloat getLocationParameter3Function ()
	{
		return locationParameter3Function;
	}

	public void setLocationParameter3Function (FloatToFloat value)
	{
		locationParameter3Function$FIELD.setObject (this, value);
	}

	public ChannelMap getLocationParameter ()
	{
		return locationParameter;
	}

	public void setLocationParameter (ChannelMap value)
	{
		locationParameter$FIELD.setObject (this, value);
	}

	public ArrangeLOD getLod ()
	{
		return lod;
	}

	public void setLod (ArrangeLOD value)
	{
		lod$FIELD.setObject (this, value);
	}

//enh:end

}
