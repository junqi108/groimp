
/*
 * Copyright (C) 2002 - 2006 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.blocks;

import javax.vecmath.Point3f;
import javax.vecmath.Tuple3f;

import de.grogra.persistence.PersistenceField;
import de.grogra.persistence.SCOType;
import de.grogra.persistence.ShareableBase;
import de.grogra.persistence.Transaction;

public class LocationParameterBase extends ShareableBase 
{

	//enh:sco SCOType
	
	String aktLocationParameter = "";
	// enh:field getter setter

	String setLocationParameter1 = "0";
	// enh:field getter setter
	
	String setLocationParameter2 = "0";
	// enh:field getter setter

	String setLocationParameter3 = "0";
	// enh:field getter setter

	float deltaLocationParameter1 = 0;
	// enh:field getter setter min=-1 max=1

	float deltaLocationParameter2 = 0;
	// enh:field getter setter min=-1 max=1

	float deltaLocationParameter3 = 0;
	// enh:field getter setter min=-1 max=1
		
	private Tuple3f aktLocationParameterValues;
	private Tuple3f newLocationParameterValues;
	private Tuple3f locationParameterPoint = null;	
	
	private void calculatNewLocationParameter() {
		newLocationParameterValues = new Point3f(0,0,0);
		newLocationParameterValues.x = aktLocationParameterValues.x + aktLocationParameterValues.x * getDeltaLocationParameter1();
		newLocationParameterValues.y = aktLocationParameterValues.y + aktLocationParameterValues.y * getDeltaLocationParameter2();
		newLocationParameterValues.z = aktLocationParameterValues.z + aktLocationParameterValues.z * getDeltaLocationParameter3();
	}

	public Tuple3f setLocationParameter(Tuple3f oldLocationParameterValues) {
		this.aktLocationParameterValues = oldLocationParameterValues;
		setAktLocationParameter("(" + aktLocationParameterValues.x+", "+aktLocationParameterValues.y+", "+aktLocationParameterValues.z+")");		
		calculatNewLocationParameter();
		if (locationParameterPoint==null) {
			return new Point3f(newLocationParameterValues);			
		}
		return new Point3f(locationParameterPoint);
	}
	
	public void fieldModified (PersistenceField field, int[] indices, Transaction t)
	{
		super.fieldModified (field, indices, t);
		if (!Transaction.isApplying(t))
		{
			if (field.overlaps (indices, setLocationParameter1$FIELD, null) || 
				field.overlaps (indices, setLocationParameter2$FIELD, null) ||
				field.overlaps (indices, setLocationParameter3$FIELD, null))
			{		
				locationParameterPoint = new Point3f(
					Float.parseFloat(setLocationParameter1),
					Float.parseFloat(setLocationParameter2),
					Float.parseFloat(setLocationParameter3));
			}
			if (field.overlaps (indices, deltaLocationParameter1$FIELD, null) || 
				field.overlaps (indices, deltaLocationParameter2$FIELD, null) ||
				field.overlaps (indices, deltaLocationParameter3$FIELD, null))
			{
				locationParameterPoint = null;
			}
		}
	}
	
	
	//enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final Type $TYPE;

	public static final Type.Field aktLocationParameter$FIELD;
	public static final Type.Field setLocationParameter1$FIELD;
	public static final Type.Field setLocationParameter2$FIELD;
	public static final Type.Field setLocationParameter3$FIELD;
	public static final Type.Field deltaLocationParameter1$FIELD;
	public static final Type.Field deltaLocationParameter2$FIELD;
	public static final Type.Field deltaLocationParameter3$FIELD;

	public static class Type extends SCOType
	{
		public Type (Class c, de.grogra.persistence.SCOType supertype)
		{
			super (c, supertype);
		}

		public Type (LocationParameterBase representative, de.grogra.persistence.SCOType supertype)
		{
			super (representative, supertype);
		}

		Type (Class c)
		{
			super (c, SCOType.$TYPE);
		}

		private static final int SUPER_FIELD_COUNT = SCOType.FIELD_COUNT;
		protected static final int FIELD_COUNT = SCOType.FIELD_COUNT + 7;

		static Field _addManagedField (Type t, String name, int modifiers, de.grogra.reflect.Type type, de.grogra.reflect.Type componentType, int id)
		{
			return t.addManagedField (name, modifiers, type, componentType, id);
		}

		@Override
		protected void setFloat (Object o, int id, float value)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 4:
					((LocationParameterBase) o).deltaLocationParameter1 = (float) value;
					return;
				case Type.SUPER_FIELD_COUNT + 5:
					((LocationParameterBase) o).deltaLocationParameter2 = (float) value;
					return;
				case Type.SUPER_FIELD_COUNT + 6:
					((LocationParameterBase) o).deltaLocationParameter3 = (float) value;
					return;
			}
			super.setFloat (o, id, value);
		}

		@Override
		protected float getFloat (Object o, int id)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 4:
					return ((LocationParameterBase) o).getDeltaLocationParameter1 ();
				case Type.SUPER_FIELD_COUNT + 5:
					return ((LocationParameterBase) o).getDeltaLocationParameter2 ();
				case Type.SUPER_FIELD_COUNT + 6:
					return ((LocationParameterBase) o).getDeltaLocationParameter3 ();
			}
			return super.getFloat (o, id);
		}

		@Override
		protected void setObject (Object o, int id, Object value)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 0:
					((LocationParameterBase) o).aktLocationParameter = (String) value;
					return;
				case Type.SUPER_FIELD_COUNT + 1:
					((LocationParameterBase) o).setLocationParameter1 = (String) value;
					return;
				case Type.SUPER_FIELD_COUNT + 2:
					((LocationParameterBase) o).setLocationParameter2 = (String) value;
					return;
				case Type.SUPER_FIELD_COUNT + 3:
					((LocationParameterBase) o).setLocationParameter3 = (String) value;
					return;
			}
			super.setObject (o, id, value);
		}

		@Override
		protected Object getObject (Object o, int id)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 0:
					return ((LocationParameterBase) o).getAktLocationParameter ();
				case Type.SUPER_FIELD_COUNT + 1:
					return ((LocationParameterBase) o).getSetLocationParameter1 ();
				case Type.SUPER_FIELD_COUNT + 2:
					return ((LocationParameterBase) o).getSetLocationParameter2 ();
				case Type.SUPER_FIELD_COUNT + 3:
					return ((LocationParameterBase) o).getSetLocationParameter3 ();
			}
			return super.getObject (o, id);
		}

		@Override
		public Object newInstance ()
		{
			return new LocationParameterBase ();
		}

	}

	public de.grogra.persistence.ManageableType getManageableType ()
	{
		return $TYPE;
	}


	static
	{
		$TYPE = new Type (LocationParameterBase.class);
		aktLocationParameter$FIELD = Type._addManagedField ($TYPE, "aktLocationParameter", 0 | Type.Field.SCO, de.grogra.reflect.ClassAdapter.wrap (String.class), null, Type.SUPER_FIELD_COUNT + 0);
		setLocationParameter1$FIELD = Type._addManagedField ($TYPE, "setLocationParameter1", 0 | Type.Field.SCO, de.grogra.reflect.ClassAdapter.wrap (String.class), null, Type.SUPER_FIELD_COUNT + 1);
		setLocationParameter2$FIELD = Type._addManagedField ($TYPE, "setLocationParameter2", 0 | Type.Field.SCO, de.grogra.reflect.ClassAdapter.wrap (String.class), null, Type.SUPER_FIELD_COUNT + 2);
		setLocationParameter3$FIELD = Type._addManagedField ($TYPE, "setLocationParameter3", 0 | Type.Field.SCO, de.grogra.reflect.ClassAdapter.wrap (String.class), null, Type.SUPER_FIELD_COUNT + 3);
		deltaLocationParameter1$FIELD = Type._addManagedField ($TYPE, "deltaLocationParameter1", 0 | Type.Field.SCO, de.grogra.reflect.Type.FLOAT, null, Type.SUPER_FIELD_COUNT + 4);
		deltaLocationParameter2$FIELD = Type._addManagedField ($TYPE, "deltaLocationParameter2", 0 | Type.Field.SCO, de.grogra.reflect.Type.FLOAT, null, Type.SUPER_FIELD_COUNT + 5);
		deltaLocationParameter3$FIELD = Type._addManagedField ($TYPE, "deltaLocationParameter3", 0 | Type.Field.SCO, de.grogra.reflect.Type.FLOAT, null, Type.SUPER_FIELD_COUNT + 6);
		deltaLocationParameter1$FIELD.setMinValue (new Float (-1));
		deltaLocationParameter1$FIELD.setMaxValue (new Float (1));
		deltaLocationParameter2$FIELD.setMinValue (new Float (-1));
		deltaLocationParameter2$FIELD.setMaxValue (new Float (1));
		deltaLocationParameter3$FIELD.setMinValue (new Float (-1));
		deltaLocationParameter3$FIELD.setMaxValue (new Float (1));
		$TYPE.validate ();
	}

	public float getDeltaLocationParameter1 ()
	{
		return deltaLocationParameter1;
	}

	public void setDeltaLocationParameter1 (float value)
	{
		this.deltaLocationParameter1 = (float) value;
	}

	public float getDeltaLocationParameter2 ()
	{
		return deltaLocationParameter2;
	}

	public void setDeltaLocationParameter2 (float value)
	{
		this.deltaLocationParameter2 = (float) value;
	}

	public float getDeltaLocationParameter3 ()
	{
		return deltaLocationParameter3;
	}

	public void setDeltaLocationParameter3 (float value)
	{
		this.deltaLocationParameter3 = (float) value;
	}

	public String getAktLocationParameter ()
	{
		return aktLocationParameter;
	}

	public void setAktLocationParameter (String value)
	{
		aktLocationParameter$FIELD.setObject (this, value);
	}

	public String getSetLocationParameter1 ()
	{
		return setLocationParameter1;
	}

	public void setSetLocationParameter1 (String value)
	{
		setLocationParameter1$FIELD.setObject (this, value);
	}

	public String getSetLocationParameter2 ()
	{
		return setLocationParameter2;
	}

	public void setSetLocationParameter2 (String value)
	{
		setLocationParameter2$FIELD.setObject (this, value);
	}

	public String getSetLocationParameter3 ()
	{
		return setLocationParameter3;
	}

	public void setSetLocationParameter3 (String value)
	{
		setLocationParameter3$FIELD.setObject (this, value);
	}

//enh:end
	
}
