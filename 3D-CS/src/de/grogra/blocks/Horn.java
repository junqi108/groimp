 
/*
 * Copyright (C) 2002 - 2006 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.blocks;

import javax.vecmath.Point2f;
import javax.vecmath.Point3f;
import javax.vecmath.Tuple2f;
import javax.vecmath.Tuple3f;
import javax.vecmath.Vector3d;

import de.grogra.graph.Instantiator;
import de.grogra.graph.impl.Edge;
import de.grogra.graph.impl.Node;
import de.grogra.imp.View;
import de.grogra.imp3d.objects.Cone;
import de.grogra.imp3d.objects.GlobalTransformation;
import de.grogra.imp3d.objects.Mark;
import de.grogra.imp3d.objects.NURBSSurface;
import de.grogra.imp3d.objects.Null;
import de.grogra.imp3d.objects.Vertex;
import de.grogra.imp3d.objects.VertexSequence;
import de.grogra.turtle.D;
import de.grogra.turtle.F;
import de.grogra.turtle.M;
import de.grogra.turtle.RH;
import de.grogra.turtle.RL;
import de.grogra.turtle.Rotate;
import de.grogra.turtle.Scale;
import de.grogra.math.BSplineCurve;
import de.grogra.math.Cos;
import de.grogra.math.Id;
import de.grogra.math.ProfileSweep;
import de.grogra.math.RegularPolygon;
import de.grogra.math.SkinnedSurface;
import de.grogra.math.SplineFunction;
import de.grogra.persistence.PersistenceField;
import de.grogra.persistence.Transaction;
import de.grogra.rgg.Library;
import de.grogra.rgg.model.Instantiation;
import de.grogra.vecmath.Matrix34d;
import de.grogra.xl.lang.FloatToFloat;

public class Horn extends NullWithShaderNode implements de.grogra.xl.modules.Instantiator<Instantiation> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;

	CustomFunction segments = new CustomFunction(20, 1, 100);
	// enh:field setter

	CustomFunction numberPerSeg = new CustomFunction(1, 1, 10);
	// enh:field setter	
	
	CustomFunction length = new CustomFunction(1, 0.001, 10);
	// enh:field setter

	CustomFunction rotEndAngle = new CustomFunction(11.6, 0, 6*Math.PI);
	// enh:field setter

	CustomFunction rotX2 = new CustomFunction(0, -1, 1);
	// enh:field setter

	CustomFunction rotX1 = new CustomFunction(0, -1, 1);
	// enh:field setter

	FloatToFloat rotXMode = new Id();
	// enh:field getter setter

	CustomFunction rotY2 = new CustomFunction(0, -1, 1);
	// enh:field setter

	CustomFunction rotY1 = new CustomFunction(0, -1, 1);
	// enh:field setter

	FloatToFloat rotYMode = new Id();
	// enh:field getter setter

	CustomFunction rotZ2 = new CustomFunction(0, -1, 1);
	// enh:field setter

	CustomFunction rotZ1 = new CustomFunction(0, -1, 1);
	// enh:field setter

	FloatToFloat rotZMode = new Id();
	// enh:field getter setter

	CustomFunction transX2 = new CustomFunction(0, -20, 20);
	// enh:field setter

	CustomFunction transX1 = new CustomFunction(0, -20, 20);
	// enh:field setter

	FloatToFloat transXMode = new Id();
	// enh:field getter setter

	CustomFunction transY2 = new CustomFunction(0, -20, 20);
	// enh:field setter

	CustomFunction transY1 = new CustomFunction(0, -20, 20);
	// enh:field setter

	FloatToFloat transYMode = new Id();
	// enh:field getter setter

	CustomFunction transZ2 = new CustomFunction(0, -Math.PI, Math.PI);
	// enh:field setter

	CustomFunction transZ1 = new CustomFunction(0, -Math.PI, Math.PI);
	// enh:field setter

	FloatToFloat transZMode = new Id();
	// enh:field getter setter

	boolean useShape = false;
	// enh:field getter setter
	
	FloatToFloat shape = (FloatToFloat) BlockTools.getObject("/objects/math/functions/HornFunctions/shape", new Id());
	// enh:field getter setter	
	
	CustomFunction range2 = new CustomFunction(0, -Math.PI, Math.PI);
	// enh:field setter

	CustomFunction range1 = new CustomFunction("pi/2", -Math.PI, Math.PI);	
	// enh:field setter

	FloatToFloat rangeMode = new Cos();
	// enh:field getter setter
	
	boolean top = true;
	// enh:field getter setter

	CustomFunction scale2 = new CustomFunction(1, 0, 5);
	// enh:field setter

	CustomFunction scale1 = new CustomFunction(1, 0, 5);
	// enh:field setter

	FloatToFloat scaleMode = new Id();
	// enh:field getter setter

	CustomFunction steps2 = new CustomFunction(1, 0, 200);
	// enh:field setter

	CustomFunction steps1 = new CustomFunction(1, 0, 200);
	// enh:field setter
	
	FloatToFloat stepsMode = new Id();
	// enh:field getter setter

	CustomFunction screw2 = new CustomFunction(0, -50, 50);
	// enh:field setter

	CustomFunction screw1 = new CustomFunction(0, -50, 50);
	// enh:field setter

	FloatToFloat screwMode = new Id();
	// enh:field getter setter

	CustomFunction flap2 = new CustomFunction(0, -7, 7);
	// enh:field setter

	CustomFunction flap1 = new CustomFunction(0, -7, 7);
	// enh:field setter
	
	FloatToFloat flapMode = new Id();
	// enh:field getter setter

	BSplineCurve profile = new RegularPolygon();
	// enh:field getter setter
	
	boolean geometry = true;
	// enh:field getter setter	

	HornLOD lod = new HornLOD();
	// enh:field getter setter
	
	LocationParameterBase locationParameter = new LocationParameterBase();
	// enh:field getter setter
	
	boolean initAll = false;
	// enh:field getter setter
	
	// interne variablen, die von vater uebergeben werden
	private Point2f ids = new Point2f(0, 0);;
	private Float densityValue = new Float(0);
	private Tuple2f height = new Point2f(0, 0);
	private Tuple3f nutrientsValues = new Point3f(0, 0, 0);
	private int childId = 0;
	
	
	private static final RL rotRL90 = new RL(90);
	
	public Horn() {
		super();
		super.setLayer(1);
		
		initFunctionsToHermit();
	}

	public Horn(float n) {
		super();
		super.setLayer(1);
		segments.setFunction(n);
		
		initFunctionsToHermit();
	}
	
	private void initAttributes() {
		segments = new CustomFunction(20, 1, 100);
		numberPerSeg = new CustomFunction(1, 1, 10);
		length = new CustomFunction(1, 0.001f, 10);
		rotEndAngle = new CustomFunction(11.6, 0, 6*Math.PI);
		rotX2 = new CustomFunction(0, -1, 1);
		rotX1 = new CustomFunction(0, -1, 1);
		rotXMode = new Id();
		rotY2 = new CustomFunction(0, -1, 1);
		rotY1 = new CustomFunction(0, -1, 1);
		rotYMode = new Id();
		rotZ2 = new CustomFunction(0, -1, 1);
		rotZ1 = new CustomFunction(0, -1, 1);
		rotZMode = new Id();
		transX2 = new CustomFunction(0, -20, 20);
		transX1 = new CustomFunction(0, -20, 20);
		transXMode = new Id();
		transY2 = new CustomFunction(0, -20, 20);
		transY1 = new CustomFunction(0, -20, 20);
		transYMode = new Id();
		transZ2 = new CustomFunction(0, -Math.PI, Math.PI);
		transZ1 = new CustomFunction(0, -Math.PI, Math.PI);
		transZMode = new Id();
		useShape = false;	
		shape = (FloatToFloat) BlockTools.getObject("/objects/math/functions/HornFunctions/shape", new Id());
		range2 = new CustomFunction(0, -Math.PI, Math.PI);
		range1 = new CustomFunction("pi/2", -Math.PI, Math.PI);	
		rangeMode = new Cos();
		top = true;
		scale2 = new CustomFunction(1, 0, 5);
		scale1 = new CustomFunction(1, 0, 5);
		scaleMode = new Id();
		steps2 = new CustomFunction(1, 0, 200);
		steps1 = new CustomFunction(1, 0, 200);
		stepsMode = new Id();
		screw2 = new CustomFunction(0, -50, 50);
		screw1 = new CustomFunction(0, -50, 50);
		screwMode = new Id();
		flap2 = new CustomFunction(0, -7, 7);
		flap1 = new CustomFunction(0, -7, 7);
		flapMode = new Id();
		profile = new RegularPolygon();
		geometry = true;
		lod = new HornLOD();
		locationParameter = new LocationParameterBase();
		
		initFunctionsToHermit();
	}

	private void initFunctionsToHermit() {	
		((SplineFunction)shape).setType(SplineFunction.HERMITE);
		
		//gehoert hier zwar nicht hin, mach sich aber gut hier ;-)
		if (profile instanceof RegularPolygon) {
			((RegularPolygon)profile).setSideCount(9);
		}
	}	
	
	public Instantiator getInstantiator() {
		return de.grogra.rgg.model.Instantiation.INSTANTIATOR;
	}

	public void instantiate(Instantiation state) {
		Instantiation inst = (Instantiation) state;
		Library.setSeed(hashCode());
				
		// uebergenenen werte holen			
		ids = (Point2f)inst.getGraphState().getObjectDefault(this, true, Attributes.ID, null);
		densityValue = (Float)inst.getGraphState().getObjectDefault(this, true, Attributes.DENSITY, 1f);
		
		//aktuelle hoehe des obj ueber null bestimmen
		Matrix34d m = GlobalTransformation.get(this, true, inst.getGraphState(),false);
		Vector3d v = new Vector3d();
		m.get(v);		
		height = new Point2f( (float)v.z,
				(Float)inst.getGraphState().getObjectDefault(this, true, Attributes.HEIGHT, null));

		// locationParameter-werte holen und neu berechnen
		nutrientsValues = (Tuple3f)inst.getGraphState().getObjectDefault(this, true, Attributes.LOCATIONPARAMETER, null);
		Tuple3f nutrientsSetValues = locationParameter.setLocationParameter(nutrientsValues);		

		// lod daten setzen
		lod.set(View.get(inst.getGraphState()), v, length.evaluateFloat(ids, nutrientsValues, height, densityValue));
		
		final int n2 = lod.segmentsToLod(segments.evaluateFloat(ids, nutrientsValues, height, densityValue));
		
		final float sumN = (float) ((n2 * (n2 + 1)) / 2.0);
		final float alpha = rotEndAngle.evaluateFloat(ids, nutrientsValues, height, densityValue) / sumN;
		float f_rot_x1 = alpha * rotX1.evaluateFloat(ids, nutrientsValues, height, densityValue);
		float f_rot_x2 = alpha * rotX2.evaluateFloat(ids, nutrientsValues, height, densityValue);
		float f_rot_y1 = alpha * rotY1.evaluateFloat(ids, nutrientsValues, height, densityValue);
		float f_rot_y2 = alpha * rotY2.evaluateFloat(ids, nutrientsValues, height, densityValue);
		float f_rot_z1 = alpha * rotZ1.evaluateFloat(ids, nutrientsValues, height, densityValue);
		float f_rot_z2 = alpha * rotZ2.evaluateFloat(ids, nutrientsValues, height, densityValue);
		float f_trans_x1 = transX1.evaluateFloat(ids, nutrientsValues, height, densityValue) / (n2 + 1);
		float f_trans_x2 = transX2.evaluateFloat(ids, nutrientsValues, height, densityValue) / (n2 + 1);
		float f_trans_y1 = transY1.evaluateFloat(ids, nutrientsValues, height, densityValue) / (n2 + 1);
		float f_trans_y2 = transY2.evaluateFloat(ids, nutrientsValues, height, densityValue) / (n2 + 1);
		float f_trans_z1 = transZ1.evaluateFloat(ids, nutrientsValues, height, densityValue) / (n2 + 1);
		float f_trans_z2 = transZ2.evaluateFloat(ids, nutrientsValues, height, densityValue) / (n2 + 1);
		float f_range1 = range1.evaluateFloat(ids, nutrientsValues, height, densityValue) / n2;
		float f_range2 = range2.evaluateFloat(ids, nutrientsValues, height, densityValue) / n2;
		float f_scale1 = scale1.evaluateFloat(ids, nutrientsValues, height, densityValue) / n2;
		float f_scale2 = scale2.evaluateFloat(ids, nutrientsValues, height, densityValue) / n2;
		float f_steps1 = steps1.evaluateFloat(ids, nutrientsValues, height, densityValue) / n2;
		float f_steps2 = steps2.evaluateFloat(ids, nutrientsValues, height, densityValue) / n2;
		float f_screw1 = screw1.evaluateFloat(ids, nutrientsValues, height, densityValue) / n2;
		float f_screw2 = screw2.evaluateFloat(ids, nutrientsValues, height, densityValue) / n2;
		float f_flap1 = flap1.evaluateFloat(ids, nutrientsValues, height, densityValue) / n2;
		float f_flap2 = flap2.evaluateFloat(ids, nutrientsValues, height, densityValue) / n2;
		
		int a = 1;
		int j = 0;
		boolean steps_b = true;
		f_steps1 = (steps1.evaluateFloat(ids, nutrientsValues, height, densityValue) > 1) ? n2 / 
				(steps1.evaluateFloat(ids, nutrientsValues, height, densityValue) - 1) : 0;
		f_steps2 = (steps2.evaluateFloat(ids, nutrientsValues, height, densityValue) > 1) ? n2 / 
				(steps2.evaluateFloat(ids, nutrientsValues, height, densityValue) - 1) : 0;
		float ii = f_steps1 + (n2 - 1) * f_steps2;
		float flap = 0;
		float scale = 1;
		float screw = 0;
		
		if (lod.isProfile() && geometry) {
			inst.instantiate(new Mark());
			if (useShape) {
				inst.instantiate(new Vertex(shape.evaluateFloat(0)));
			} else {
				inst.instantiate(new Vertex(ModeList.function(0, n2, f_range1, f_range2, rangeMode,
						new Point2f(0,0), nutrientsValues, height, densityValue)));			
			}
		}
		
		float laengei = 20 * length.evaluateFloat(ids, nutrientsValues, height, densityValue) / n2;
		float range = 0;
		for (int i = 0; i < n2; i++) {
			childId = i;
			// neue nutrientswerte setzen
			inst.getGraphState().setInstanceAttribute (Attributes.NUTRIENTS_TUPLE3F, nutrientsSetValues);			
			// anzahl der angehaengten obj weitergeben
			inst.getGraphState().setInstanceAttribute(Attributes.NUMBER_INT, new Integer(n2));			
			
			float rot_x = ModeList.function(i, n2, f_rot_x1, f_rot_x2,	rotXMode, 
					ids, nutrientsValues, height, densityValue);
			float rot_y = ModeList.function(i, n2, f_rot_y1, f_rot_y2,	rotYMode, 
					ids, nutrientsValues, height, densityValue);
			float rot_z = ModeList.function(i, n2, f_rot_z1, f_rot_z2,	rotZMode, 
					ids, nutrientsValues, height, densityValue);
			float trans_x = ModeList.function(i, n2, f_trans_x1, f_trans_x2, transXMode, 
					ids, nutrientsValues, height, densityValue);
			float trans_y = ModeList.function(i, n2, f_trans_y1, f_trans_y2, transYMode, 
					ids, nutrientsValues, height, densityValue);
			float trans_z = ModeList.function(i, n2, f_trans_z1, f_trans_z2, transZMode, 
					ids, nutrientsValues, height, densityValue);			
			if (useShape) {
				range = shape.evaluateFloat((float)i / (float)(n2));
			} else {
				range = ModeList.function(i, n2, f_range1, f_range2, rangeMode, 
					ids, nutrientsValues, height, densityValue);
			}
			scale = lod.scaleToLod(ModeList.function(i, n2, f_scale1, f_scale2, scaleMode,						
					ids, nutrientsValues, height, densityValue));
			float steps = ModeList.function(i, n2, f_steps1, f_steps2, stepsMode, 
					ids, nutrientsValues, height, densityValue);
			screw = ModeList.function(i, n2, f_screw1, f_screw2, screwMode, 
					ids, nutrientsValues, height, densityValue);
			flap = ModeList.function(i, n2, f_flap1, f_flap2, flapMode, 
					ids, nutrientsValues, height, densityValue);

			if (steps1.evaluateFloat(ids, nutrientsValues, height, densityValue) > 1) {
				if (i + 1 > ii * a)
					a += 1;
				steps_b = false;
				j = j + 1;
				if (j == a + 1) {
					steps_b = true;
					j = 0;
				}
			}

			// trans_x, _y und _z
			inst.instantiate(new Null(trans_x, trans_y, trans_z));
			inst.instantiate(new Rotate(rot_x * (float)(180/Math.PI), rot_y * (float)(180/Math.PI), rot_z * (float)(180/Math.PI)));
			if (geometry) {
				if (lod.isProfile()) {
					if (top && i==n2-1) {
						inst.instantiate(new M(laengei));
						inst.instantiate(new Vertex(0));
					} else {
						inst.instantiate(new M(laengei));
						inst.instantiate(new Vertex(range));						
					}
				} else {
					if (top && i==n2-1) {
						Cone co = new Cone();
						co.setLength(laengei);
						co.setRadius(range);
						inst.instantiate(co);
					} else {
						inst.instantiate(new D(2*range));
						inst.instantiate(new F(laengei));
					}
				}
			} else {
				//keine Geometrie				
				inst.instantiate(new M(laengei));
			}

			// numberPerSeg-viele "seitenarme" an jedem segment
			int anz = (int)Math.round(numberPerSeg.evaluateFloat(ids, nutrientsValues, height, densityValue));
			float wi = 360.0f / (float)anz;
			for (int k = 0; k<anz; k++) {				
				if (i < n2 - 1 & steps_b) {
					inst.producer$push();
					inst.instantiate(new RH(k*wi));
					inst.instantiate(rotRL90);
					inst.instantiate(new Rotate(0, screw * (float)(180/Math.PI), flap * (float)(180/Math.PI)));
					Scale scaleN = new Scale(scale);
					inst.instantiate(scaleN);
					setHeightLocal(inst, scaleN, height.x);
					
					for (Edge e = getFirstEdge(); e != null; e = e.getNext(this)) {
						Node n = e.getTarget();
						if ((n != this) && e.testEdgeBits(BlockConst.MULTIPLY)) {						
							// id des aktuellen Obj weitergeben						
							inst.getGraphState().setInstanceAttribute(Attributes.ID_TUPLE2F, new Point2f(i, ids.x));					
							inst.instantiate(n);
						}
					}
					inst.producer$pop(null);
				}
			}
		}
		if (lod.isProfile() && geometry) {
			inst.instantiate(new NURBSSurface(new SkinnedSurface(new ProfileSweep(
				profile, new VertexSequence(null)))));
		}
		
		// "seitenarm" nur am letzten segment, oben an die spitze
		inst.producer$push();
		inst.instantiate(new Rotate(0, screw * (float)(180/Math.PI), flap * (float)(180/Math.PI)));		
		Scale scaleN = new Scale(scale);
		inst.instantiate(scaleN);
		setHeightLocal(inst, scaleN, height.x);
		
		for (Edge e = getFirstEdge(); e != null; e = e.getNext(this)) {
			Node n = e.getTarget();
			if ((n != this) && e.testEdgeBits(BlockConst.CHILD)) {
				inst.producer$push();
				inst.getGraphState().setInstanceAttribute(Attributes.ID_TUPLE2F, new Point2f(0, ids.x));
				inst.instantiate(n);
				inst.producer$pop(null);
			}
		}
		inst.producer$pop(null);
	}

	private void setHeightLocal(Instantiation inst, Node n, float height0) {
		Matrix34d m = GlobalTransformation.get(n, true, inst.getGraphState(),false);
		Vector3d heighti = new Vector3d();
		m.get(heighti);
		height.y = (float)Math.abs(heighti.z-height0);
		inst.getGraphState().setInstanceAttribute(Attributes.HEIGHT_FLOAT, new Float(height.y));
	}	
	
	public void fieldModified (PersistenceField field, int[] indices, Transaction t)
	{
		super.fieldModified (field, indices, t);
		if (!Transaction.isApplying(t))
		{
			if (field.overlaps (indices, initAll$FIELD, null))
			{
				if (isInitAll()) 
				{
					initAttributes();
					setInitAll(false);
				}
			}
		}
	}

	public float getSegments ()
	{
		return segments.evaluateZerro();
	}

	public void setSegments (double value)
	{
		segments.setFunction(value);
	}

	public float getNumberPerSeg ()
	{
		return numberPerSeg.evaluateZerro();
	}

	public void setNumberPerSeg (double value)
	{
		numberPerSeg.setFunction(value);
	}
	
	
	public float getLength ()
	{
		return length.evaluateZerro();
	}

	public void setLength (double value)
	{
		length.setFunction(value);
	}
	
	public float getRotEndAngle ()
	{
		return rotEndAngle.evaluateZerro();
	}

	public void setRotEndAngle (double value)
	{
		rotEndAngle.setFunction(value);
	}

	public float getRotX2 ()
	{
		return rotX2.evaluateZerro();
	}

	public void setRotX2 (double value)
	{
		rotX2.setFunction(value);
	}

	public float getRotX1 ()
	{
		return rotX1.evaluateZerro();
	}

	public void setRotX1 (double value)
	{
		rotX1.setFunction(value);
	}

	public void setRotX (double value1, double value2)
	{
		rotX1.setFunction(value1);
		rotX2.setFunction(value2);
	}
	
	public float getRotY2 ()
	{
		return rotY2.evaluateZerro();
	}

	public void setRotY2 (double value)
	{
		rotY2.setFunction(value);
	}

	public float getRotY1 ()
	{
		return rotY1.evaluateZerro();
	}

	public void setRotY1 (double value)
	{
		rotY1.setFunction(value);
	}

	public void setRotY (double value1, double value2)
	{
		rotY1.setFunction(value1);
		rotY2.setFunction(value2);
	}	
	
	public float getRotZ2 ()
	{
		return rotZ2.evaluateZerro();
	}

	public void setRotZ2 (double value)
	{
		rotZ2.setFunction(value);
	}

	public float getRotZ1 ()
	{
		return rotZ1.evaluateZerro();
	}

	public void setRotZ1 (double value)
	{
		rotZ1.setFunction(value);
	}

	public void setRotZ (double value1, double value2)
	{
		rotZ1.setFunction(value1);
		rotZ2.setFunction(value2);
	}	
	
	public float getTransX2 ()
	{
		return transX2.evaluateZerro();
	}

	public void setTransX2 (double value)
	{
		transX2.setFunction(value);
	}

	public float getTransX1 ()
	{
		return transX1.evaluateZerro();
	}

	public void setTransX1 (double value)
	{
		transX1.setFunction(value);
	}

	public void setTransX (double value1, double value2)
	{
		transX1.setFunction(value1);
		transX2.setFunction(value2);
	}	
	
	public float getTransY2 ()
	{
		return transY2.evaluateZerro();
	}

	public void setTransY2 (double value)
	{
		transY2.setFunction(value);
	}

	public float getTransY1 ()
	{
		return transY1.evaluateZerro();
	}

	public void setTransY1 (double value)
	{
		transY1.setFunction(value);
	}

	public void setTransY (double value1, double value2)
	{
		transY1.setFunction(value1);
		transY2.setFunction(value2);
	}	
	
	public float getTransZ2 ()
	{
		return transZ2.evaluateZerro();
	}

	public void setTransZ2 (double value)
	{
		transZ2.setFunction(value);
	}

	public float getTransZ1 ()
	{
		return transZ1.evaluateZerro();
	}

	public void setTransZ1 (double value)
	{
		transZ1.setFunction(value);
	}

	public void setTransZ (double value1, double value2)
	{
		transZ1.setFunction(value1);
		transZ2.setFunction(value2);
	}	
	
	public float getRange2 ()
	{
		return range2.evaluateZerro();
	}

	public void setRange2 (double value)
	{
		range2.setFunction(value);
	}

	public float getRange1 ()
	{
		return range1.evaluateZerro();
	}

	public void setRange1 (double value)
	{
		range1.setFunction(value);
	}

	public void setRange (double value1, double value2)
	{
		range1.setFunction(value1);
		range2.setFunction(value2);
	}	
	
	public float getScale2 ()
	{
		return scale2.evaluateZerro();
	}

	public void setScale2 (double value)
	{
		scale2.setFunction(value);
	}

	public float getScale1 ()
	{
		return scale1.evaluateZerro();
	}

	public void setScale1 (double value)
	{
		scale1.setFunction(value);
	}

	public void setScale (double value1, double value2)
	{
		scale1.setFunction(value1);
		scale2.setFunction(value2);
	}
	
	public float getSteps2 ()
	{
		return steps2.evaluateZerro();
	}

	public void setSteps2 (double value)
	{
		steps2.setFunction(value);
	}

	public float getSteps1 ()
	{
		return steps1.evaluateZerro();
	}

	public void setSteps1 (double value)
	{
		steps1.setFunction(value);
	}

	public void setSteps (double value1, double value2)
	{
		steps1.setFunction(value1);
		steps2.setFunction(value2);
	}
	
	public float getScrew2 ()
	{
		return screw2.evaluateZerro();
	}

	public void setScrew2 (double value)
	{
		screw2.setFunction(value);
	}

	public float getScrew1 ()
	{
		return screw1.evaluateZerro();
	}

	public void setScrew1 (double value)
	{
		screw1.setFunction(value);
	}

	public void setScrew (double value1, double value2)
	{
		screw1.setFunction(value1);
		screw2.setFunction(value2);
	}	
	public float getFlap2 ()
	{
		return flap2.evaluateZerro();
	}

	public void setFlap2 (double value)
	{
		flap2.setFunction(value);
	}

	public float getFlap1 ()
	{
		return flap1.evaluateZerro();
	}

	public void setFlap1 (double value)
	{
		flap1.setFunction(value);
	}
	
	public void setFlap (double value1, double value2)
	{
		flap1.setFunction(value1);
		flap2.setFunction(value2);
	}
	
	public void setSegments (String value)
	{
		segments.setFunction(value);
	}

	public void setNumberPerSeg (String value)
	{
		numberPerSeg.setFunction(value);
	}
	
	public void setLength (String value)
	{
		length.setFunction(value);
	}
	
	public void setRotEndAngle (String value)
	{
		rotEndAngle.setFunction(value);
	}

	public void setRotX2 (String value)
	{
		rotX2.setFunction(value);
	}

	public void setRotX1 (String value)
	{
		rotX1.setFunction(value);
	}

	public void setRotX (String value1, String value2)
	{
		rotX1.setFunction(value1);
		rotX2.setFunction(value2);
	}
	
	public void setRotY2 (String value)
	{
		rotY2.setFunction(value);
	}

	public void setRotY1 (String value)
	{
		rotY1.setFunction(value);
	}

	public void setRotY (String value1, String value2)
	{
		rotY1.setFunction(value1);
		rotY2.setFunction(value2);
	}	
	
	public void setRotZ2 (String value)
	{
		rotZ2.setFunction(value);
	}

	public void setRotZ1 (String value)
	{
		rotZ1.setFunction(value);
	}

	public void setRotZ (String value1, String value2)
	{
		rotZ1.setFunction(value1);
		rotZ2.setFunction(value2);
	}	

	public void setTransX2 (String value)
	{
		transX2.setFunction(value);
	}

	public void setTransX1 (String value)
	{
		transX1.setFunction(value);
	}

	public void setTransX (String value1, String value2)
	{
		transX1.setFunction(value1);
		transX2.setFunction(value2);
	}	
	
	public void setTransY2 (String value)
	{
		transY2.setFunction(value);
	}

	public void setTransY1 (String value)
	{
		transY1.setFunction(value);
	}

	public void setTransY (String value1, String value2)
	{
		transY1.setFunction(value1);
		transY2.setFunction(value2);
	}	

	public void setTransZ2 (String value)
	{
		transZ2.setFunction(value);
	}

	public void setTransZ1 (String value)
	{
		transZ1.setFunction(value);
	}

	public void setTransZ (String value1, String value2)
	{
		transZ1.setFunction(value1);
		transZ2.setFunction(value2);
	}	
	
	public void setRange2 (String value)
	{
		range2.setFunction(value);
	}

	public void setRange1 (String value)
	{
		range1.setFunction(value);
	}

	public void setRange (String value1, String value2)
	{
		range1.setFunction(value1);
		range2.setFunction(value2);
	}

	public void setScale2 (String value)
	{
		scale2.setFunction(value);
	}

	public void setScale1 (String value)
	{
		scale1.setFunction(value);
	}

	public void setScale (String value1, String value2)
	{
		scale1.setFunction(value1);
		scale2.setFunction(value2);
	}

	public void setSteps2 (String value)
	{
		steps2.setFunction(value);
	}

	public void setSteps1 (String value)
	{
		steps1.setFunction(value);
	}

	public void setSteps (String value1, String value2)
	{
		steps1.setFunction(value1);
		steps2.setFunction(value2);
	}
	
	public void setScrew2 (String value)
	{
		screw2.setFunction(value);
	}

	public void setScrew1 (String value)
	{
		screw1.setFunction(value);
	}

	public void setScrew (String value1, String value2)
	{
		screw1.setFunction(value1);
		screw2.setFunction(value2);
	}	

	public void setFlap2 (String value)
	{
		flap2.setFunction(value);
	}

	public void setFlap1 (String value)
	{
		flap1.setFunction(value);
	}
	
	public void setFlap (String value1, String value2)
	{
		flap1.setFunction(value1);
		flap2.setFunction(value2);
	}	
	
	public void useLod (boolean value)
	{
		lod.setUseLOD(value);
	}		
	
	public int getChildId() {
		return childId;
	}
	
	public int getParentId() {
		return (int)ids.x;
	}

	public int getThisId() {
		return (int)ids.y;
	}	
	
	public float getDensity() {
		return densityValue;
	}
	
	public float getAbsoluteHeight() {
		return height.x;
	}
	
	public float getLocalHeight() {
		return height.y;
	}
	
	public float getN1() {
		return nutrientsValues.x;
	}	

	public float getN2() {
		return nutrientsValues.y;
	}
	
	public float getN3() {
		return nutrientsValues.z;
	}	
	
	
	// enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final NType $TYPE;

	public static final NType.Field segments$FIELD;
	public static final NType.Field numberPerSeg$FIELD;
	public static final NType.Field length$FIELD;
	public static final NType.Field rotEndAngle$FIELD;
	public static final NType.Field rotX2$FIELD;
	public static final NType.Field rotX1$FIELD;
	public static final NType.Field rotXMode$FIELD;
	public static final NType.Field rotY2$FIELD;
	public static final NType.Field rotY1$FIELD;
	public static final NType.Field rotYMode$FIELD;
	public static final NType.Field rotZ2$FIELD;
	public static final NType.Field rotZ1$FIELD;
	public static final NType.Field rotZMode$FIELD;
	public static final NType.Field transX2$FIELD;
	public static final NType.Field transX1$FIELD;
	public static final NType.Field transXMode$FIELD;
	public static final NType.Field transY2$FIELD;
	public static final NType.Field transY1$FIELD;
	public static final NType.Field transYMode$FIELD;
	public static final NType.Field transZ2$FIELD;
	public static final NType.Field transZ1$FIELD;
	public static final NType.Field transZMode$FIELD;
	public static final NType.Field useShape$FIELD;
	public static final NType.Field shape$FIELD;
	public static final NType.Field range2$FIELD;
	public static final NType.Field range1$FIELD;
	public static final NType.Field rangeMode$FIELD;
	public static final NType.Field top$FIELD;
	public static final NType.Field scale2$FIELD;
	public static final NType.Field scale1$FIELD;
	public static final NType.Field scaleMode$FIELD;
	public static final NType.Field steps2$FIELD;
	public static final NType.Field steps1$FIELD;
	public static final NType.Field stepsMode$FIELD;
	public static final NType.Field screw2$FIELD;
	public static final NType.Field screw1$FIELD;
	public static final NType.Field screwMode$FIELD;
	public static final NType.Field flap2$FIELD;
	public static final NType.Field flap1$FIELD;
	public static final NType.Field flapMode$FIELD;
	public static final NType.Field profile$FIELD;
	public static final NType.Field geometry$FIELD;
	public static final NType.Field lod$FIELD;
	public static final NType.Field locationParameter$FIELD;
	public static final NType.Field initAll$FIELD;

	private static final class _Field extends NType.Field
	{
		private final int id;

		_Field (String name, int modifiers, de.grogra.reflect.Type type, de.grogra.reflect.Type componentType, int id)
		{
			super (Horn.$TYPE, name, modifiers, type, componentType);
			this.id = id;
		}

		@Override
		public void setBoolean (Object o, boolean value)
		{
			switch (id)
			{
				case 22:
					((Horn) o).useShape = (boolean) value;
					return;
				case 27:
					((Horn) o).top = (boolean) value;
					return;
				case 41:
					((Horn) o).geometry = (boolean) value;
					return;
				case 44:
					((Horn) o).initAll = (boolean) value;
					return;
			}
			super.setBoolean (o, value);
		}

		@Override
		public boolean getBoolean (Object o)
		{
			switch (id)
			{
				case 22:
					return ((Horn) o).isUseShape ();
				case 27:
					return ((Horn) o).isTop ();
				case 41:
					return ((Horn) o).isGeometry ();
				case 44:
					return ((Horn) o).isInitAll ();
			}
			return super.getBoolean (o);
		}

		@Override
		protected void setObjectImpl (Object o, Object value)
		{
			switch (id)
			{
				case 0:
					((Horn) o).segments = (CustomFunction) value;
					return;
				case 1:
					((Horn) o).numberPerSeg = (CustomFunction) value;
					return;
				case 2:
					((Horn) o).length = (CustomFunction) value;
					return;
				case 3:
					((Horn) o).rotEndAngle = (CustomFunction) value;
					return;
				case 4:
					((Horn) o).rotX2 = (CustomFunction) value;
					return;
				case 5:
					((Horn) o).rotX1 = (CustomFunction) value;
					return;
				case 6:
					((Horn) o).rotXMode = (FloatToFloat) value;
					return;
				case 7:
					((Horn) o).rotY2 = (CustomFunction) value;
					return;
				case 8:
					((Horn) o).rotY1 = (CustomFunction) value;
					return;
				case 9:
					((Horn) o).rotYMode = (FloatToFloat) value;
					return;
				case 10:
					((Horn) o).rotZ2 = (CustomFunction) value;
					return;
				case 11:
					((Horn) o).rotZ1 = (CustomFunction) value;
					return;
				case 12:
					((Horn) o).rotZMode = (FloatToFloat) value;
					return;
				case 13:
					((Horn) o).transX2 = (CustomFunction) value;
					return;
				case 14:
					((Horn) o).transX1 = (CustomFunction) value;
					return;
				case 15:
					((Horn) o).transXMode = (FloatToFloat) value;
					return;
				case 16:
					((Horn) o).transY2 = (CustomFunction) value;
					return;
				case 17:
					((Horn) o).transY1 = (CustomFunction) value;
					return;
				case 18:
					((Horn) o).transYMode = (FloatToFloat) value;
					return;
				case 19:
					((Horn) o).transZ2 = (CustomFunction) value;
					return;
				case 20:
					((Horn) o).transZ1 = (CustomFunction) value;
					return;
				case 21:
					((Horn) o).transZMode = (FloatToFloat) value;
					return;
				case 23:
					((Horn) o).shape = (FloatToFloat) value;
					return;
				case 24:
					((Horn) o).range2 = (CustomFunction) value;
					return;
				case 25:
					((Horn) o).range1 = (CustomFunction) value;
					return;
				case 26:
					((Horn) o).rangeMode = (FloatToFloat) value;
					return;
				case 28:
					((Horn) o).scale2 = (CustomFunction) value;
					return;
				case 29:
					((Horn) o).scale1 = (CustomFunction) value;
					return;
				case 30:
					((Horn) o).scaleMode = (FloatToFloat) value;
					return;
				case 31:
					((Horn) o).steps2 = (CustomFunction) value;
					return;
				case 32:
					((Horn) o).steps1 = (CustomFunction) value;
					return;
				case 33:
					((Horn) o).stepsMode = (FloatToFloat) value;
					return;
				case 34:
					((Horn) o).screw2 = (CustomFunction) value;
					return;
				case 35:
					((Horn) o).screw1 = (CustomFunction) value;
					return;
				case 36:
					((Horn) o).screwMode = (FloatToFloat) value;
					return;
				case 37:
					((Horn) o).flap2 = (CustomFunction) value;
					return;
				case 38:
					((Horn) o).flap1 = (CustomFunction) value;
					return;
				case 39:
					((Horn) o).flapMode = (FloatToFloat) value;
					return;
				case 40:
					((Horn) o).profile = (BSplineCurve) value;
					return;
				case 42:
					((Horn) o).lod = (HornLOD) value;
					return;
				case 43:
					((Horn) o).locationParameter = (LocationParameterBase) value;
					return;
			}
			super.setObjectImpl (o, value);
		}

		@Override
		public Object getObject (Object o)
		{
			switch (id)
			{
				case 0:
					return ((Horn) o).segments;
				case 1:
					return ((Horn) o).numberPerSeg;
				case 2:
					return ((Horn) o).length;
				case 3:
					return ((Horn) o).rotEndAngle;
				case 4:
					return ((Horn) o).rotX2;
				case 5:
					return ((Horn) o).rotX1;
				case 6:
					return ((Horn) o).getRotXMode ();
				case 7:
					return ((Horn) o).rotY2;
				case 8:
					return ((Horn) o).rotY1;
				case 9:
					return ((Horn) o).getRotYMode ();
				case 10:
					return ((Horn) o).rotZ2;
				case 11:
					return ((Horn) o).rotZ1;
				case 12:
					return ((Horn) o).getRotZMode ();
				case 13:
					return ((Horn) o).transX2;
				case 14:
					return ((Horn) o).transX1;
				case 15:
					return ((Horn) o).getTransXMode ();
				case 16:
					return ((Horn) o).transY2;
				case 17:
					return ((Horn) o).transY1;
				case 18:
					return ((Horn) o).getTransYMode ();
				case 19:
					return ((Horn) o).transZ2;
				case 20:
					return ((Horn) o).transZ1;
				case 21:
					return ((Horn) o).getTransZMode ();
				case 23:
					return ((Horn) o).getShape ();
				case 24:
					return ((Horn) o).range2;
				case 25:
					return ((Horn) o).range1;
				case 26:
					return ((Horn) o).getRangeMode ();
				case 28:
					return ((Horn) o).scale2;
				case 29:
					return ((Horn) o).scale1;
				case 30:
					return ((Horn) o).getScaleMode ();
				case 31:
					return ((Horn) o).steps2;
				case 32:
					return ((Horn) o).steps1;
				case 33:
					return ((Horn) o).getStepsMode ();
				case 34:
					return ((Horn) o).screw2;
				case 35:
					return ((Horn) o).screw1;
				case 36:
					return ((Horn) o).getScrewMode ();
				case 37:
					return ((Horn) o).flap2;
				case 38:
					return ((Horn) o).flap1;
				case 39:
					return ((Horn) o).getFlapMode ();
				case 40:
					return ((Horn) o).getProfile ();
				case 42:
					return ((Horn) o).getLod ();
				case 43:
					return ((Horn) o).getLocationParameter ();
			}
			return super.getObject (o);
		}
	}

	static
	{
		$TYPE = new NType (new Horn ());
		$TYPE.addManagedField (segments$FIELD = new _Field ("segments", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (CustomFunction.class), null, 0));
		$TYPE.addManagedField (numberPerSeg$FIELD = new _Field ("numberPerSeg", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (CustomFunction.class), null, 1));
		$TYPE.addManagedField (length$FIELD = new _Field ("length", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (CustomFunction.class), null, 2));
		$TYPE.addManagedField (rotEndAngle$FIELD = new _Field ("rotEndAngle", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (CustomFunction.class), null, 3));
		$TYPE.addManagedField (rotX2$FIELD = new _Field ("rotX2", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (CustomFunction.class), null, 4));
		$TYPE.addManagedField (rotX1$FIELD = new _Field ("rotX1", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (CustomFunction.class), null, 5));
		$TYPE.addManagedField (rotXMode$FIELD = new _Field ("rotXMode", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (FloatToFloat.class), null, 6));
		$TYPE.addManagedField (rotY2$FIELD = new _Field ("rotY2", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (CustomFunction.class), null, 7));
		$TYPE.addManagedField (rotY1$FIELD = new _Field ("rotY1", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (CustomFunction.class), null, 8));
		$TYPE.addManagedField (rotYMode$FIELD = new _Field ("rotYMode", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (FloatToFloat.class), null, 9));
		$TYPE.addManagedField (rotZ2$FIELD = new _Field ("rotZ2", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (CustomFunction.class), null, 10));
		$TYPE.addManagedField (rotZ1$FIELD = new _Field ("rotZ1", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (CustomFunction.class), null, 11));
		$TYPE.addManagedField (rotZMode$FIELD = new _Field ("rotZMode", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (FloatToFloat.class), null, 12));
		$TYPE.addManagedField (transX2$FIELD = new _Field ("transX2", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (CustomFunction.class), null, 13));
		$TYPE.addManagedField (transX1$FIELD = new _Field ("transX1", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (CustomFunction.class), null, 14));
		$TYPE.addManagedField (transXMode$FIELD = new _Field ("transXMode", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (FloatToFloat.class), null, 15));
		$TYPE.addManagedField (transY2$FIELD = new _Field ("transY2", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (CustomFunction.class), null, 16));
		$TYPE.addManagedField (transY1$FIELD = new _Field ("transY1", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (CustomFunction.class), null, 17));
		$TYPE.addManagedField (transYMode$FIELD = new _Field ("transYMode", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (FloatToFloat.class), null, 18));
		$TYPE.addManagedField (transZ2$FIELD = new _Field ("transZ2", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (CustomFunction.class), null, 19));
		$TYPE.addManagedField (transZ1$FIELD = new _Field ("transZ1", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (CustomFunction.class), null, 20));
		$TYPE.addManagedField (transZMode$FIELD = new _Field ("transZMode", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (FloatToFloat.class), null, 21));
		$TYPE.addManagedField (useShape$FIELD = new _Field ("useShape", 0 | _Field.SCO, de.grogra.reflect.Type.BOOLEAN, null, 22));
		$TYPE.addManagedField (shape$FIELD = new _Field ("shape", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (FloatToFloat.class), null, 23));
		$TYPE.addManagedField (range2$FIELD = new _Field ("range2", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (CustomFunction.class), null, 24));
		$TYPE.addManagedField (range1$FIELD = new _Field ("range1", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (CustomFunction.class), null, 25));
		$TYPE.addManagedField (rangeMode$FIELD = new _Field ("rangeMode", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (FloatToFloat.class), null, 26));
		$TYPE.addManagedField (top$FIELD = new _Field ("top", 0 | _Field.SCO, de.grogra.reflect.Type.BOOLEAN, null, 27));
		$TYPE.addManagedField (scale2$FIELD = new _Field ("scale2", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (CustomFunction.class), null, 28));
		$TYPE.addManagedField (scale1$FIELD = new _Field ("scale1", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (CustomFunction.class), null, 29));
		$TYPE.addManagedField (scaleMode$FIELD = new _Field ("scaleMode", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (FloatToFloat.class), null, 30));
		$TYPE.addManagedField (steps2$FIELD = new _Field ("steps2", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (CustomFunction.class), null, 31));
		$TYPE.addManagedField (steps1$FIELD = new _Field ("steps1", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (CustomFunction.class), null, 32));
		$TYPE.addManagedField (stepsMode$FIELD = new _Field ("stepsMode", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (FloatToFloat.class), null, 33));
		$TYPE.addManagedField (screw2$FIELD = new _Field ("screw2", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (CustomFunction.class), null, 34));
		$TYPE.addManagedField (screw1$FIELD = new _Field ("screw1", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (CustomFunction.class), null, 35));
		$TYPE.addManagedField (screwMode$FIELD = new _Field ("screwMode", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (FloatToFloat.class), null, 36));
		$TYPE.addManagedField (flap2$FIELD = new _Field ("flap2", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (CustomFunction.class), null, 37));
		$TYPE.addManagedField (flap1$FIELD = new _Field ("flap1", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (CustomFunction.class), null, 38));
		$TYPE.addManagedField (flapMode$FIELD = new _Field ("flapMode", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (FloatToFloat.class), null, 39));
		$TYPE.addManagedField (profile$FIELD = new _Field ("profile", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (BSplineCurve.class), null, 40));
		$TYPE.addManagedField (geometry$FIELD = new _Field ("geometry", 0 | _Field.SCO, de.grogra.reflect.Type.BOOLEAN, null, 41));
		$TYPE.addManagedField (lod$FIELD = new _Field ("lod", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (HornLOD.class), null, 42));
		$TYPE.addManagedField (locationParameter$FIELD = new _Field ("locationParameter", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (LocationParameterBase.class), null, 43));
		$TYPE.addManagedField (initAll$FIELD = new _Field ("initAll", 0 | _Field.SCO, de.grogra.reflect.Type.BOOLEAN, null, 44));
		$TYPE.validate ();
	}

	@Override
	protected NType getNTypeImpl ()
	{
		return $TYPE;
	}

	@Override
	protected de.grogra.graph.impl.Node newInstance ()
	{
		return new Horn ();
	}

	public boolean isUseShape ()
	{
		return useShape;
	}

	public void setUseShape (boolean value)
	{
		this.useShape = (boolean) value;
	}

	public boolean isTop ()
	{
		return top;
	}

	public void setTop (boolean value)
	{
		this.top = (boolean) value;
	}

	public boolean isGeometry ()
	{
		return geometry;
	}

	public void setGeometry (boolean value)
	{
		this.geometry = (boolean) value;
	}

	public boolean isInitAll ()
	{
		return initAll;
	}

	public void setInitAll (boolean value)
	{
		this.initAll = (boolean) value;
	}

	public void setSegments (CustomFunction value)
	{
		segments$FIELD.setObject (this, value);
	}

	public void setNumberPerSeg (CustomFunction value)
	{
		numberPerSeg$FIELD.setObject (this, value);
	}

	public void setLength (CustomFunction value)
	{
		length$FIELD.setObject (this, value);
	}

	public void setRotEndAngle (CustomFunction value)
	{
		rotEndAngle$FIELD.setObject (this, value);
	}

	public void setRotX2 (CustomFunction value)
	{
		rotX2$FIELD.setObject (this, value);
	}

	public void setRotX1 (CustomFunction value)
	{
		rotX1$FIELD.setObject (this, value);
	}

	public FloatToFloat getRotXMode ()
	{
		return rotXMode;
	}

	public void setRotXMode (FloatToFloat value)
	{
		rotXMode$FIELD.setObject (this, value);
	}

	public void setRotY2 (CustomFunction value)
	{
		rotY2$FIELD.setObject (this, value);
	}

	public void setRotY1 (CustomFunction value)
	{
		rotY1$FIELD.setObject (this, value);
	}

	public FloatToFloat getRotYMode ()
	{
		return rotYMode;
	}

	public void setRotYMode (FloatToFloat value)
	{
		rotYMode$FIELD.setObject (this, value);
	}

	public void setRotZ2 (CustomFunction value)
	{
		rotZ2$FIELD.setObject (this, value);
	}

	public void setRotZ1 (CustomFunction value)
	{
		rotZ1$FIELD.setObject (this, value);
	}

	public FloatToFloat getRotZMode ()
	{
		return rotZMode;
	}

	public void setRotZMode (FloatToFloat value)
	{
		rotZMode$FIELD.setObject (this, value);
	}

	public void setTransX2 (CustomFunction value)
	{
		transX2$FIELD.setObject (this, value);
	}

	public void setTransX1 (CustomFunction value)
	{
		transX1$FIELD.setObject (this, value);
	}

	public FloatToFloat getTransXMode ()
	{
		return transXMode;
	}

	public void setTransXMode (FloatToFloat value)
	{
		transXMode$FIELD.setObject (this, value);
	}

	public void setTransY2 (CustomFunction value)
	{
		transY2$FIELD.setObject (this, value);
	}

	public void setTransY1 (CustomFunction value)
	{
		transY1$FIELD.setObject (this, value);
	}

	public FloatToFloat getTransYMode ()
	{
		return transYMode;
	}

	public void setTransYMode (FloatToFloat value)
	{
		transYMode$FIELD.setObject (this, value);
	}

	public void setTransZ2 (CustomFunction value)
	{
		transZ2$FIELD.setObject (this, value);
	}

	public void setTransZ1 (CustomFunction value)
	{
		transZ1$FIELD.setObject (this, value);
	}

	public FloatToFloat getTransZMode ()
	{
		return transZMode;
	}

	public void setTransZMode (FloatToFloat value)
	{
		transZMode$FIELD.setObject (this, value);
	}

	public FloatToFloat getShape ()
	{
		return shape;
	}

	public void setShape (FloatToFloat value)
	{
		shape$FIELD.setObject (this, value);
	}

	public void setRange2 (CustomFunction value)
	{
		range2$FIELD.setObject (this, value);
	}

	public void setRange1 (CustomFunction value)
	{
		range1$FIELD.setObject (this, value);
	}

	public FloatToFloat getRangeMode ()
	{
		return rangeMode;
	}

	public void setRangeMode (FloatToFloat value)
	{
		rangeMode$FIELD.setObject (this, value);
	}

	public void setScale2 (CustomFunction value)
	{
		scale2$FIELD.setObject (this, value);
	}

	public void setScale1 (CustomFunction value)
	{
		scale1$FIELD.setObject (this, value);
	}

	public FloatToFloat getScaleMode ()
	{
		return scaleMode;
	}

	public void setScaleMode (FloatToFloat value)
	{
		scaleMode$FIELD.setObject (this, value);
	}

	public void setSteps2 (CustomFunction value)
	{
		steps2$FIELD.setObject (this, value);
	}

	public void setSteps1 (CustomFunction value)
	{
		steps1$FIELD.setObject (this, value);
	}

	public FloatToFloat getStepsMode ()
	{
		return stepsMode;
	}

	public void setStepsMode (FloatToFloat value)
	{
		stepsMode$FIELD.setObject (this, value);
	}

	public void setScrew2 (CustomFunction value)
	{
		screw2$FIELD.setObject (this, value);
	}

	public void setScrew1 (CustomFunction value)
	{
		screw1$FIELD.setObject (this, value);
	}

	public FloatToFloat getScrewMode ()
	{
		return screwMode;
	}

	public void setScrewMode (FloatToFloat value)
	{
		screwMode$FIELD.setObject (this, value);
	}

	public void setFlap2 (CustomFunction value)
	{
		flap2$FIELD.setObject (this, value);
	}

	public void setFlap1 (CustomFunction value)
	{
		flap1$FIELD.setObject (this, value);
	}

	public FloatToFloat getFlapMode ()
	{
		return flapMode;
	}

	public void setFlapMode (FloatToFloat value)
	{
		flapMode$FIELD.setObject (this, value);
	}

	public BSplineCurve getProfile ()
	{
		return profile;
	}

	public void setProfile (BSplineCurve value)
	{
		profile$FIELD.setObject (this, value);
	}

	public HornLOD getLod ()
	{
		return lod;
	}

	public void setLod (HornLOD value)
	{
		lod$FIELD.setObject (this, value);
	}

	public LocationParameterBase getLocationParameter ()
	{
		return locationParameter;
	}

	public void setLocationParameter (LocationParameterBase value)
	{
		locationParameter$FIELD.setObject (this, value);
	}

//enh:end

}
