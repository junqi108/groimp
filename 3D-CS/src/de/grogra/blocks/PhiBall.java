/*
 * Copyright (C) 2002 - 2006 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.blocks;

import javax.vecmath.Point2f;
import javax.vecmath.Point3f;
import javax.vecmath.Tuple2f;
import javax.vecmath.Tuple3f;
import javax.vecmath.Vector3d;

import de.grogra.graph.Instantiator;
import de.grogra.graph.impl.Edge;
import de.grogra.graph.impl.Node;
import de.grogra.imp.View;
import de.grogra.imp3d.objects.GlobalTransformation;
import de.grogra.imp3d.objects.Null;
import de.grogra.imp3d.objects.Sphere;
import de.grogra.turtle.RU;
import de.grogra.turtle.Rotate;
import de.grogra.turtle.Scale;
import de.grogra.math.Cos;
import de.grogra.math.Degree;
import de.grogra.math.Id;
import de.grogra.math.Phi;
import de.grogra.persistence.PersistenceField;
import de.grogra.persistence.Transaction;
import de.grogra.rgg.Library;
import de.grogra.rgg.model.Instantiation;
import de.grogra.vecmath.Matrix34d;
import de.grogra.xl.lang.FloatToFloat;

public class PhiBall extends Sphere implements de.grogra.xl.modules.Instantiator<Instantiation> 
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3L;

	CustomFunction number = new CustomFunction(50, 1, 500);
	// enh:field setter

	CustomFunction radiusX = new CustomFunction(0.5, 0, MAX_RADIUS);
	// enh:field setter

	CustomFunction radiusY = new CustomFunction(0.5, 0, MAX_RADIUS);
	// enh:field setter

	CustomFunction radiusZ = new CustomFunction(0.5, 0, MAX_RADIUS);
	// enh:field setter
	
	CustomFunction fan2 = new CustomFunction(0, 0, Math.PI);
	// enh:field setter

	CustomFunction fan1 = new CustomFunction("pi", 0, Math.PI);
	// enh:field setter

	FloatToFloat fanMode = new Id();
	// enh:field getter setter

	CustomFunction angle2 = new CustomFunction(1, -10, 10);
	// enh:field setter

	CustomFunction angle1 = new CustomFunction(1, -10, 10);
	// enh:field setter

	FloatToFloat angleMode = new Phi();
	// enh:field getter setter

	CustomFunction trans2 = new CustomFunction(0, -10, 10);
	// enh:field setter

	CustomFunction trans1 = new CustomFunction(0, -10, 10);
	// enh:field setter

	FloatToFloat transMode = new Id();
	// enh:field getter setter

	CustomFunction scale2 = new CustomFunction(0, -Math.PI, Math.PI);
	// enh:field setter

	CustomFunction scale1 = new CustomFunction(0, -Math.PI, Math.PI);
	// enh:field setter

	FloatToFloat scaleMode = new Cos();
	// enh:field getter setter

	CustomFunction influence = new CustomFunction(1, 0, 1);
	// enh:field setter

	boolean geometry = false;
	// enh:field getter setter
	
	PhiBallLOD lod = new PhiBallLOD();
	// enh:field getter setter
	
	LocationParameterBase locationParameter = new LocationParameterBase();
	// enh:field getter setter
	
	boolean initAll = false;
	// enh:field getter setter
	
	// interne variablen, die von vater uebergeben werden
	private Point2f ids = new Point2f(0, 0);;
	private Float densityValue = new Float(0);
	private Tuple2f height = new Point2f(0, 0);
	private Tuple3f nutrientsValues = new Point3f(0, 0, 0);
	private int childId = 0;
	
	
	private static final float MAX_RADIUS = 50;
	
	public PhiBall() {
		super(BlockTools.RADIUS_SPHERE);
		super.setLayer(1);
	}

	public PhiBall(float n, float radius) {
		super(BlockTools.RADIUS_SPHERE);
		super.setLayer(1);
		number.setFunction(n);
		setRadius(radius);
	}	
	
	public PhiBall(float n) {
		super(BlockTools.RADIUS_SPHERE);
		super.setLayer(1);
		number.setFunction(n);
	}

	private void initAttributes() {
		number = new CustomFunction(50, 1, 500);
		radiusX = new CustomFunction(0.5, 0, MAX_RADIUS);
		radiusY = new CustomFunction(0.5, 0, MAX_RADIUS);
		radiusZ = new CustomFunction(0.5, 0, MAX_RADIUS);
		fan2 = new CustomFunction(0, 0, Math.PI);
		fan1 = new CustomFunction("pi", 0, Math.PI);
		fanMode = new Id();
		angle2 = new CustomFunction(1, -10, 10);
		angle1 = new CustomFunction(1, -10, 10);
		angleMode = new Phi();
		trans2 = new CustomFunction(0, -10, 10);
		trans1 = new CustomFunction(0, -10, 10);
		transMode = new Id();
		scale2 = new CustomFunction(0, -Math.PI, Math.PI);
		scale1 = new CustomFunction(0, -Math.PI, Math.PI);
		scaleMode = new Cos();
		influence = new CustomFunction(1, 0, 1);
		lod = new PhiBallLOD();
		locationParameter = new LocationParameterBase();
	}
	
	public Instantiator getInstantiator() {
		return de.grogra.rgg.model.Instantiation.INSTANTIATOR;
	}

	public void instantiate(Instantiation state) {
		Instantiation inst = (Instantiation) state;
		Library.setSeed(hashCode()); // zufallsgenerator
		
		// uebergenenen werte holen
		ids = (Point2f)inst.getGraphState().getObjectDefault(this, true, Attributes.ID, null);			
		densityValue = (Float)inst.getGraphState().getObjectDefault(this, true, Attributes.DENSITY, 1f);
		
		//aktuelle hoehe des obj ueber null bestimmen
		Matrix34d m = GlobalTransformation.get(this, true, inst.getGraphState(),false);
		Vector3d v = new Vector3d();
		m.get(v);		
		height = new Point2f( (float)v.z,
				(Float)inst.getGraphState().getObjectDefault(this, true, Attributes.HEIGHT, null));

		// locationParameter-werte holen und neu berechnen
		nutrientsValues = (Tuple3f)inst.getGraphState().getObjectDefault(this, true, Attributes.LOCATIONPARAMETER, null);
		Tuple3f nutrientsSetValues = locationParameter.setLocationParameter(nutrientsValues);		

		// gr��e bestimmen
		float rX = radiusX.evaluateFloat(ids, nutrientsValues, height, densityValue);
		float rY = radiusY.evaluateFloat(ids, nutrientsValues, height, densityValue);
		float rZ = radiusZ.evaluateFloat(ids, nutrientsValues, height, densityValue);
		float maxR = ((rX<rY)?rY:rX);
		// lod daten setzen
		lod.set(View.get(inst.getGraphState()), v, (((rZ<maxR)?maxR:rZ))/MAX_RADIUS);
		
		final int n2 = lod.numberToLod(number.evaluateFloat(ids, nutrientsValues, height, densityValue));
//System.out.println("phi num = "+number.evaluateFloat(ids, nutrientsValues, height)+"  n2="+n2);

		float fan11 = fan1.evaluateFloat(ids, nutrientsValues, height, densityValue);
		float fan22 = fan2.evaluateFloat(ids, nutrientsValues, height, densityValue);
		float f1 = (fan11 > fan22) ? fan11 : fan22;
		float f2 = (fan11 > fan22) ? fan22 : fan11;
		float f_fan1 = fanMode.evaluateFloat(f1);
		float f_fan2 = fanMode.evaluateFloat(f2);

		float f_angle1 = angle1.evaluateFloat(ids, nutrientsValues, height, densityValue) / n2;
		float f_angle2 = angle2.evaluateFloat(ids, nutrientsValues, height, densityValue) / n2;
		float f_trans1 = trans1.evaluateFloat(ids, nutrientsValues, height, densityValue) / n2;
		float f_trans2 = trans2.evaluateFloat(ids, nutrientsValues, height, densityValue) / n2;
		float f_scale1 = scale1.evaluateFloat(ids, nutrientsValues, height, densityValue) / n2;
		float f_scale2 = scale2.evaluateFloat(ids, nutrientsValues, height, densityValue) / n2;
		float phi_i = 0;		
		float Ai = 0, An = 0;
				
		// summe aller objektflaechen (skalierungsfaktor [0,1] als flaeche)
		for (int i = 0; i < n2 - 1; i++) {
			An += Math.abs(lod.scaleToLod(ModeList.function(i, n2, f_scale1, f_scale2,	scaleMode, 					
					new Point2f(i,0), nutrientsValues, height, densityValue)));
		}
	
		if (geometry) {
			inst.producer$push();
			inst.instantiate(new Scale(rX, rY, rZ));
			inst.instantiate(new Sphere());
			inst.producer$pop(null);
		}		
		
		for (int i = 0; i < n2 - 1; i++) {
			childId = i;
			// neue nutrientswerte setzen
			inst.getGraphState().setInstanceAttribute (Attributes.NUTRIENTS_TUPLE3F, nutrientsSetValues);			
			// anzahl der angehaengten obj weitergeben
			inst.getGraphState().setInstanceAttribute(Attributes.NUMBER_INT, new Integer(n2));			
			
			float trans = ModeList.function(i, n2, f_trans1, f_trans2,	transMode,
					ids, nutrientsValues, height, densityValue);
			float scale = lod.scaleToLod(ModeList.function(i, n2, f_scale1, f_scale2,	scaleMode,
					ids, nutrientsValues, height, densityValue));

			// summe der objektflaechen [0,i]
			Ai += Math.abs(scale);
			double f_aian = n2 * Ai / An;
			// influence anwenden (also zwichen i und f_aian interpolieren, um
			// influence %)
			float ii = 0;
			if (i < f_aian) {
				ii = (float) (i + influence.evaluateFloat(ids, nutrientsValues, height, densityValue) * 
						Math.abs(i - f_aian));
			} else {
				ii = (float) (i + (1 - influence.evaluateFloat(ids, nutrientsValues, height, densityValue)) * 
						Math.abs(i - f_aian));
			}

			float z1 = (float) ((Math.cos(f_fan1) - ii
					* (Math.cos(f_fan1) - Math.cos(f_fan2)) / n2));
			float z = radiusZ.evaluateFloat(ids, nutrientsValues, height, densityValue) * z1;
			float rz = 1 - z1 * z1;
			rz = (rz > 0) ? (float) Math.sqrt(rz) : 0;
			phi_i = phi_i + 360 / ModeList.function(i, n2, f_angle1, f_angle2, angleMode,
					ids, nutrientsValues, height, densityValue);

			float x = radiusX.evaluateFloat(ids, nutrientsValues, height, densityValue) * 
				(float) Degree.cos(phi_i);
			float y = radiusY.evaluateFloat(ids, nutrientsValues, height, densityValue) * 
				(float) Degree.sin(phi_i);
			float f;
			if (f1 < Math.PI / 2f) {
				if (f1 < 1e-5) {
					f = (float) Math.sqrt(1 - (float) ii / n2);
					z = 0;
					x *= f;
					y *= f;
				} else {
					f = (float) (1 / Math.sin(f1));
					x *= f * rz;
					y *= f * rz;
					z = (float) (f * (z - radiusZ.evaluateFloat(ids, nutrientsValues, height, densityValue) * 
							Math.cos(f1)));
				}
			} else if (f2 > Math.PI / 2f) {
				if (f2 > 3.141f) {
					f = (float) Math.sqrt((float) ii / n2);
					z = 0;
					x *= f;
					y *= f;
				} else {
					f = (float) (1 / Math.sin(f2));
					x *= f * rz;
					y *= f * rz;
					z = (float) (f * (z - radiusZ.evaluateFloat(ids, nutrientsValues, height, densityValue) * 
							Math.cos(f2)));
				}
			} else {
				x *= rz;
				y *= rz;
			}
			float l_xy = (float) Math.sqrt(x * x + y * y);
			float wi = (float) Math.atan2(y, x);

			inst.producer$push();
			inst.instantiate(new Rotate(0, 0, wi * (float)(180/Math.PI)));
			inst.instantiate(new Null(l_xy, 0, z + trans));
			inst.instantiate(new RU((float) Degree.acos(z1)));			
			Scale scaleN = new Scale(scale);
			inst.instantiate(scaleN);
			setHeightLocal(inst, scaleN, height.x);
			
			for (Edge e = getFirstEdge(); e != null; e = e.getNext(this)) {
				Node n = e.getTarget();
				if ((n != this) && (e.testEdgeBits(BlockConst.MULTIPLY) || e.testEdgeBits(BlockConst.CHILD))) {
					inst.producer$push();
					// id des aktuellen Obj weitergeben					
					inst.getGraphState().setInstanceAttribute(Attributes.ID_TUPLE2F, new Point2f(i, ids.x));					
					inst.instantiate(n);
					inst.producer$pop(null);
				}
			}
			inst.producer$pop(null);
		}
	}

	private void setHeightLocal(Instantiation inst, Node n, float height0) {
		Matrix34d m = GlobalTransformation.get(n, true, inst.getGraphState(),false);
		Vector3d heighti = new Vector3d();
		m.get(heighti);
		height.y = (float)Math.abs(heighti.z-height0);
		inst.getGraphState().setInstanceAttribute(Attributes.HEIGHT_FLOAT, new Float(height.y));
	}	
	
	public void setRadius(float value) {
		radiusX.setFunction(value);
		radiusY.setFunction(value);
		radiusZ.setFunction(value);
	}
	
	public void fieldModified (PersistenceField field, int[] indices, Transaction t)
	{
		super.fieldModified (field, indices, t);
		if (!Transaction.isApplying(t))
		{
			if (field.overlaps (indices, initAll$FIELD, null))
			{
				if (isInitAll()) 
				{
					initAttributes();
					setInitAll(false);
				}
			}
		}		
	}
	
	public float getNumber ()
	{
		return number.evaluateZerro();
	}

	public void setNumber (double value)
	{
		number.setFunction(value);
	}	
	
	public void setRadiusX (double value)
	{
		radiusX.setFunction(value);
	}
	
	public float getRadiusX ()
	{
		return radiusX.evaluateZerro();
	}

	public void setRadiusY (double value)
	{
		radiusY.setFunction(value);
	}
	
	public float getRadiusY ()
	{
		return radiusY.evaluateZerro();
	}

	public void setRadiusZ (double value)
	{
		radiusZ.setFunction(value);
	}
	
	public float getRadiusZ ()
	{
		return radiusZ.evaluateZerro();
	}

	public void setRadius (double valueX, double valueY, double valueZ)
	{
		radiusX.setFunction(valueX);
		radiusY.setFunction(valueY);
		radiusZ.setFunction(valueZ);
	}
	
	public float getFan2 ()
	{
		return fan2.evaluateZerro();
	}

	public void setFan2 (double value)
	{
		fan2.setFunction(value);
	}

	public float getFan1 ()
	{
		return fan1.evaluateZerro();
	}

	public void setFan1 (double value)
	{
		fan1.setFunction(value);
	}

	public void setFan (double value1, double value2)
	{
		fan1.setFunction(value1);
		fan2.setFunction(value2);
	}

	public float getAngle2 ()
	{
		return angle2.evaluateZerro();
	}

	public void setAngle2 (double value)
	{
		angle2.setFunction(value);
	}

	public float getAngle1 ()
	{
		return angle1.evaluateZerro();
	}

	public void setAngle1 (double value)
	{
		angle1.setFunction(value);
	}

	public void setAngle (double value1, double value2)
	{
		angle1.setFunction(value1);
		angle2.setFunction(value2);
	}	
	
	public float getTrans2 ()
	{
		return trans2.evaluateZerro();
	}

	public void setTrans2 (double value)
	{
		trans2.setFunction(value);
	}

	public float getTrans1 ()
	{
		return trans1.evaluateZerro();
	}

	public void setTrans1 (double value)
	{
		trans1.setFunction(value);
	}

	public void setTrans (double value1, double value2)
	{
		trans1.setFunction(value1);
		trans2.setFunction(value2);
	}	
	
	public float getScale2 ()
	{
		return scale2.evaluateZerro();
	}

	public void setScale2 (double value)
	{
		scale2.setFunction(value);
	}

	public float getScale1 ()
	{
		return scale1.evaluateZerro();
	}

	public void setScale1  (double value)
	{
		scale1.setFunction(value);
	}
	
	public void setScale (double value1, double value2)
	{
		scale1.setFunction(value1);
		scale2.setFunction(value2);
	}	

	public float getInfluence ()
	{
		return influence.evaluateZerro();
	}

	public void setInfluence  (double value)
	{
		influence.setFunction(value);
	}
	
	public void setNumber (String value)
	{
		number.setFunction(value);
	}	
	
	public void setRadiusX (String value)
	{
		radiusX.setFunction(value);
	}
	
	public void setRadiusY (String value)
	{
		radiusY.setFunction(value);
	}

	public void setRadiusZ (String value)
	{
		radiusZ.setFunction(value);
	}

	public void setRadius (String valueX, String valueY, String valueZ)
	{
		radiusX.setFunction(valueX);
		radiusY.setFunction(valueY);
		radiusZ.setFunction(valueZ);
	}
	
	public void setFan2 (String value)
	{
		fan2.setFunction(value);
	}

	public void setFan1 (String value)
	{
		fan1.setFunction(value);
	}

	public void setFan (String value1, String value2)
	{
		fan1.setFunction(value1);
		fan2.setFunction(value2);
	}

	public void setAngle2 (String value)
	{
		angle2.setFunction(value);
	}

	public void setAngle1 (String value)
	{
		angle1.setFunction(value);
	}

	public void setAngle (String value1, String value2)
	{
		angle1.setFunction(value1);
		angle2.setFunction(value2);
	}	
	

	public void setTrans2 (String value)
	{
		trans2.setFunction(value);
	}

	public void setTrans1 (String value)
	{
		trans1.setFunction(value);
	}

	public void setTrans (String value1, String value2)
	{
		trans1.setFunction(value1);
		trans2.setFunction(value2);
	}	

	public void setScale2 (String value)
	{
		scale2.setFunction(value);
	}

	public void setScale1  (String value)
	{
		scale1.setFunction(value);
	}
	
	public void setScale (String value1, String value2)
	{
		scale1.setFunction(value1);
		scale2.setFunction(value2);
	}	

	public void setInfluence  (String value)
	{
		influence.setFunction(value);
	}
	
	public void useLod (boolean value)
	{
		lod.setUseLOD(value);
	}		
	
	public int getChildId() {
		return childId;
	}
	
	public int getParentId() {
		return (int)ids.x;
	}
	
	public int getThisId() {
		return (int)ids.y;
	}	

	public float getDensity() {
		return densityValue;
	}
	
	public float getAbsoluteHeight() {
		return height.x;
	}
	
	public float getLocalHeight() {
		return height.y;
	}
	
	public float getN1() {
		return nutrientsValues.x;
	}	

	public float getN2() {
		return nutrientsValues.y;
	}
	
	public float getN3() {
		return nutrientsValues.z;
	}	
	
	
	// enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final NType $TYPE;

	public static final NType.Field number$FIELD;
	public static final NType.Field radiusX$FIELD;
	public static final NType.Field radiusY$FIELD;
	public static final NType.Field radiusZ$FIELD;
	public static final NType.Field fan2$FIELD;
	public static final NType.Field fan1$FIELD;
	public static final NType.Field fanMode$FIELD;
	public static final NType.Field angle2$FIELD;
	public static final NType.Field angle1$FIELD;
	public static final NType.Field angleMode$FIELD;
	public static final NType.Field trans2$FIELD;
	public static final NType.Field trans1$FIELD;
	public static final NType.Field transMode$FIELD;
	public static final NType.Field scale2$FIELD;
	public static final NType.Field scale1$FIELD;
	public static final NType.Field scaleMode$FIELD;
	public static final NType.Field influence$FIELD;
	public static final NType.Field geometry$FIELD;
	public static final NType.Field lod$FIELD;
	public static final NType.Field locationParameter$FIELD;
	public static final NType.Field initAll$FIELD;

	private static final class _Field extends NType.Field
	{
		private final int id;

		_Field (String name, int modifiers, de.grogra.reflect.Type type, de.grogra.reflect.Type componentType, int id)
		{
			super (PhiBall.$TYPE, name, modifiers, type, componentType);
			this.id = id;
		}

		@Override
		public void setBoolean (Object o, boolean value)
		{
			switch (id)
			{
				case 17:
					((PhiBall) o).geometry = (boolean) value;
					return;
				case 20:
					((PhiBall) o).initAll = (boolean) value;
					return;
			}
			super.setBoolean (o, value);
		}

		@Override
		public boolean getBoolean (Object o)
		{
			switch (id)
			{
				case 17:
					return ((PhiBall) o).isGeometry ();
				case 20:
					return ((PhiBall) o).isInitAll ();
			}
			return super.getBoolean (o);
		}

		@Override
		protected void setObjectImpl (Object o, Object value)
		{
			switch (id)
			{
				case 0:
					((PhiBall) o).number = (CustomFunction) value;
					return;
				case 1:
					((PhiBall) o).radiusX = (CustomFunction) value;
					return;
				case 2:
					((PhiBall) o).radiusY = (CustomFunction) value;
					return;
				case 3:
					((PhiBall) o).radiusZ = (CustomFunction) value;
					return;
				case 4:
					((PhiBall) o).fan2 = (CustomFunction) value;
					return;
				case 5:
					((PhiBall) o).fan1 = (CustomFunction) value;
					return;
				case 6:
					((PhiBall) o).fanMode = (FloatToFloat) value;
					return;
				case 7:
					((PhiBall) o).angle2 = (CustomFunction) value;
					return;
				case 8:
					((PhiBall) o).angle1 = (CustomFunction) value;
					return;
				case 9:
					((PhiBall) o).angleMode = (FloatToFloat) value;
					return;
				case 10:
					((PhiBall) o).trans2 = (CustomFunction) value;
					return;
				case 11:
					((PhiBall) o).trans1 = (CustomFunction) value;
					return;
				case 12:
					((PhiBall) o).transMode = (FloatToFloat) value;
					return;
				case 13:
					((PhiBall) o).scale2 = (CustomFunction) value;
					return;
				case 14:
					((PhiBall) o).scale1 = (CustomFunction) value;
					return;
				case 15:
					((PhiBall) o).scaleMode = (FloatToFloat) value;
					return;
				case 16:
					((PhiBall) o).influence = (CustomFunction) value;
					return;
				case 18:
					((PhiBall) o).lod = (PhiBallLOD) value;
					return;
				case 19:
					((PhiBall) o).locationParameter = (LocationParameterBase) value;
					return;
			}
			super.setObjectImpl (o, value);
		}

		@Override
		public Object getObject (Object o)
		{
			switch (id)
			{
				case 0:
					return ((PhiBall) o).number;
				case 1:
					return ((PhiBall) o).radiusX;
				case 2:
					return ((PhiBall) o).radiusY;
				case 3:
					return ((PhiBall) o).radiusZ;
				case 4:
					return ((PhiBall) o).fan2;
				case 5:
					return ((PhiBall) o).fan1;
				case 6:
					return ((PhiBall) o).getFanMode ();
				case 7:
					return ((PhiBall) o).angle2;
				case 8:
					return ((PhiBall) o).angle1;
				case 9:
					return ((PhiBall) o).getAngleMode ();
				case 10:
					return ((PhiBall) o).trans2;
				case 11:
					return ((PhiBall) o).trans1;
				case 12:
					return ((PhiBall) o).getTransMode ();
				case 13:
					return ((PhiBall) o).scale2;
				case 14:
					return ((PhiBall) o).scale1;
				case 15:
					return ((PhiBall) o).getScaleMode ();
				case 16:
					return ((PhiBall) o).influence;
				case 18:
					return ((PhiBall) o).getLod ();
				case 19:
					return ((PhiBall) o).getLocationParameter ();
			}
			return super.getObject (o);
		}
	}

	static
	{
		$TYPE = new NType (new PhiBall ());
		$TYPE.addManagedField (number$FIELD = new _Field ("number", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (CustomFunction.class), null, 0));
		$TYPE.addManagedField (radiusX$FIELD = new _Field ("radiusX", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (CustomFunction.class), null, 1));
		$TYPE.addManagedField (radiusY$FIELD = new _Field ("radiusY", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (CustomFunction.class), null, 2));
		$TYPE.addManagedField (radiusZ$FIELD = new _Field ("radiusZ", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (CustomFunction.class), null, 3));
		$TYPE.addManagedField (fan2$FIELD = new _Field ("fan2", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (CustomFunction.class), null, 4));
		$TYPE.addManagedField (fan1$FIELD = new _Field ("fan1", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (CustomFunction.class), null, 5));
		$TYPE.addManagedField (fanMode$FIELD = new _Field ("fanMode", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (FloatToFloat.class), null, 6));
		$TYPE.addManagedField (angle2$FIELD = new _Field ("angle2", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (CustomFunction.class), null, 7));
		$TYPE.addManagedField (angle1$FIELD = new _Field ("angle1", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (CustomFunction.class), null, 8));
		$TYPE.addManagedField (angleMode$FIELD = new _Field ("angleMode", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (FloatToFloat.class), null, 9));
		$TYPE.addManagedField (trans2$FIELD = new _Field ("trans2", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (CustomFunction.class), null, 10));
		$TYPE.addManagedField (trans1$FIELD = new _Field ("trans1", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (CustomFunction.class), null, 11));
		$TYPE.addManagedField (transMode$FIELD = new _Field ("transMode", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (FloatToFloat.class), null, 12));
		$TYPE.addManagedField (scale2$FIELD = new _Field ("scale2", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (CustomFunction.class), null, 13));
		$TYPE.addManagedField (scale1$FIELD = new _Field ("scale1", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (CustomFunction.class), null, 14));
		$TYPE.addManagedField (scaleMode$FIELD = new _Field ("scaleMode", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (FloatToFloat.class), null, 15));
		$TYPE.addManagedField (influence$FIELD = new _Field ("influence", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (CustomFunction.class), null, 16));
		$TYPE.addManagedField (geometry$FIELD = new _Field ("geometry", 0 | _Field.SCO, de.grogra.reflect.Type.BOOLEAN, null, 17));
		$TYPE.addManagedField (lod$FIELD = new _Field ("lod", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (PhiBallLOD.class), null, 18));
		$TYPE.addManagedField (locationParameter$FIELD = new _Field ("locationParameter", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (LocationParameterBase.class), null, 19));
		$TYPE.addManagedField (initAll$FIELD = new _Field ("initAll", 0 | _Field.SCO, de.grogra.reflect.Type.BOOLEAN, null, 20));
		$TYPE.validate ();
	}

	@Override
	protected NType getNTypeImpl ()
	{
		return $TYPE;
	}

	@Override
	protected de.grogra.graph.impl.Node newInstance ()
	{
		return new PhiBall ();
	}

	public boolean isGeometry ()
	{
		return geometry;
	}

	public void setGeometry (boolean value)
	{
		this.geometry = (boolean) value;
	}

	public boolean isInitAll ()
	{
		return initAll;
	}

	public void setInitAll (boolean value)
	{
		this.initAll = (boolean) value;
	}

	public void setNumber (CustomFunction value)
	{
		number$FIELD.setObject (this, value);
	}

	public void setRadiusX (CustomFunction value)
	{
		radiusX$FIELD.setObject (this, value);
	}

	public void setRadiusY (CustomFunction value)
	{
		radiusY$FIELD.setObject (this, value);
	}

	public void setRadiusZ (CustomFunction value)
	{
		radiusZ$FIELD.setObject (this, value);
	}

	public void setFan2 (CustomFunction value)
	{
		fan2$FIELD.setObject (this, value);
	}

	public void setFan1 (CustomFunction value)
	{
		fan1$FIELD.setObject (this, value);
	}

	public FloatToFloat getFanMode ()
	{
		return fanMode;
	}

	public void setFanMode (FloatToFloat value)
	{
		fanMode$FIELD.setObject (this, value);
	}

	public void setAngle2 (CustomFunction value)
	{
		angle2$FIELD.setObject (this, value);
	}

	public void setAngle1 (CustomFunction value)
	{
		angle1$FIELD.setObject (this, value);
	}

	public FloatToFloat getAngleMode ()
	{
		return angleMode;
	}

	public void setAngleMode (FloatToFloat value)
	{
		angleMode$FIELD.setObject (this, value);
	}

	public void setTrans2 (CustomFunction value)
	{
		trans2$FIELD.setObject (this, value);
	}

	public void setTrans1 (CustomFunction value)
	{
		trans1$FIELD.setObject (this, value);
	}

	public FloatToFloat getTransMode ()
	{
		return transMode;
	}

	public void setTransMode (FloatToFloat value)
	{
		transMode$FIELD.setObject (this, value);
	}

	public void setScale2 (CustomFunction value)
	{
		scale2$FIELD.setObject (this, value);
	}

	public void setScale1 (CustomFunction value)
	{
		scale1$FIELD.setObject (this, value);
	}

	public FloatToFloat getScaleMode ()
	{
		return scaleMode;
	}

	public void setScaleMode (FloatToFloat value)
	{
		scaleMode$FIELD.setObject (this, value);
	}

	public void setInfluence (CustomFunction value)
	{
		influence$FIELD.setObject (this, value);
	}

	public PhiBallLOD getLod ()
	{
		return lod;
	}

	public void setLod (PhiBallLOD value)
	{
		lod$FIELD.setObject (this, value);
	}

	public LocationParameterBase getLocationParameter ()
	{
		return locationParameter;
	}

	public void setLocationParameter (LocationParameterBase value)
	{
		locationParameter$FIELD.setObject (this, value);
	}

//enh:end

}
