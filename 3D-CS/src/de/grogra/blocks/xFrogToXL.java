/*
* Copyright (C) 2002 - 2006 Lehrstuhl Grafische Systeme, BTU Cottbus
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 3
* of the License, or any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

package de.grogra.blocks;

import java.io.File;

import de.grogra.blocks.xFrogFileParser.parser;

// de.grogra.blocks.xFrogToXL.main(new String[] {"-t","name"})
public class xFrogToXL {

	public static void main(String[] args) {
		parser p1 = new parser();

		//Falsche Eingaben abfangen
		if (args!=null && args.length!=2) {
			System.out.println("Error: \n\t  xFrogToXL <-t/-s> <filename>");
		} else {
			File file = new File(args[1]);
			if (file.isFile()) {
				p1.parseFile(args[1], new File(file.getPath()), null);		
				String hilf =args[0].toUpperCase();
				if (hilf.compareTo("-S")==0) {
					p1.saveXL(file.getAbsolutePath().replaceAll("xfr","rgg"));
				} else {
					System.out.println(p1.toXL());
				}
			} else {
				System.out.println("Error: "+file+" is no file");	
			}
		}
	}
}
