
/*
 * Copyright (C) 2002 - 2006 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.blocks;

import javax.vecmath.Color3f;
import javax.vecmath.Point2f;
import javax.vecmath.Point3f;
import javax.vecmath.Tuple2f;
import javax.vecmath.Tuple3f;
import javax.vecmath.Vector3d;

import de.grogra.imp3d.objects.GlobalTransformation;
import de.grogra.imp3d.objects.Null;
import de.grogra.imp3d.shading.Phong;
import de.grogra.math.RGBColor;
import de.grogra.rgg.model.Instantiation;
import de.grogra.vecmath.Matrix34d;

public class BlockColor extends Null
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7L;

	CustomFunction addR = new CustomFunction(0, 0, 255);
	//enh:field getter setter

	CustomFunction addG = new CustomFunction(0, 0, 255);
	//enh:field getter setter
	
	CustomFunction addB = new CustomFunction(0, 0, 255);
	//enh:field getter setter
	
	// interne variablen, die von vater uebergeben werden
	private Point2f ids = new Point2f(0, 0);;
	private Float densityValue = new Float(0);
	private Tuple2f height = new Point2f(0, 0);
	private Tuple3f nutrientsValues = new Point3f(0, 0, 0);
	
	
	public BlockColor () {
		this (0,0,0);
	}

	public BlockColor (double valueR, double valueG, double valueB) 
	{		
		addR.setFunction(valueR);
		addG.setFunction(valueG);
		addB.setFunction(valueB);		
	}	
	
	public BlockColor (double value) 
	{		
		addR.setFunction(value);
		addG.setFunction(value);
		addB.setFunction(value);		
	}	
	
	public BlockColor (String valueR, String valueG, String valueB) 
	{		
		addR.setFunction(valueR);
		addG.setFunction(valueG);
		addB.setFunction(valueB);
	}
	
	public BlockColor (String value) 
	{		
		addR.setFunction(value);
		addG.setFunction(value);
		addB.setFunction(value);		
	}	
	
	public void instantiate(Instantiation state) {
		Instantiation inst = (Instantiation) state;
		// uebergenenen werte holen			
		ids = (Point2f)inst.getGraphState().getObjectDefault(this, true, Attributes.ID, null);
		densityValue = (Float)inst.getGraphState().getObjectDefault(this, true, Attributes.DENSITY, 1f);		
		//aktuelle hoehe des obj ueber null bestimmen
		Matrix34d m = GlobalTransformation.get(this, true, inst.getGraphState(),false);
		Vector3d v = new Vector3d();
		m.get(v);		
		height = new Point2f( (float)v.z,
				(Float)inst.getGraphState().getObjectDefault(this, true, Attributes.HEIGHT, null));
		// locationParameter-werte holen und neu berechnen
		nutrientsValues = (Tuple3f)inst.getGraphState().getObjectDefault(this, true, Attributes.LOCATIONPARAMETER, null);

		Color3f color = new Color3f();
		color.x = addR.evaluateFloat(ids, nutrientsValues, height, densityValue) / 255.0f;
		color.y = addG.evaluateFloat(ids, nutrientsValues, height, densityValue) / 255.0f;
		color.z = addB.evaluateFloat(ids, nutrientsValues, height, densityValue) / 255.0f;
				
		Phong ph = new Phong();
		ph.setAmbient(new RGBColor(color));
		ph.setDiffuse(new RGBColor(color));
		ph.setEmissive(new RGBColor(color));
		ph.setSpecular(new RGBColor(color));
		NullWithShaderNode node = new NullWithShaderNode();
		node.setMaterial(ph);
		inst.instantiate(node);

/*		
		System.out.println("COLOR   ids= "+ids+"    height= "+height+"    nV= "+nutrientsValues);
		System.out.println("\t r="+addR.evaluateFloat(ids, nutrientsValues, height)
			+"\t g="+addG.evaluateFloat(ids, nutrientsValues, height)
			+"\t b="+addB.evaluateFloat(ids, nutrientsValues, height)
			+"\t c="+color);
*/
	}

	public void setAddR (double value)
	{
		addR.setFunction(value);
	}

	public void setAddG (double value)
	{
		addG.setFunction(value);
	}

	public void setAddB (double value)
	{
		addB.setFunction(value);
	}
	
	public void setAddR (String value)
	{
		addR.setFunction(value);
	}

	public void setAddG (String value)
	{
		addG.setFunction(value);
	}

	public void setAddB (String value)
	{
		addB.setFunction(value);
	}
	
	public void setColor (double valueR, double valueG, double valueB) 
	{		
		addR.setFunction(valueR);
		addG.setFunction(valueG);
		addB.setFunction(valueB);		
	}	
	
	public void setColor (String valueR, String valueG, String valueB) 
	{		
		addR.setFunction(valueR);
		addG.setFunction(valueG);
		addB.setFunction(valueB);		
	}

	public void setColor (double value) 
	{		
		addR.setFunction(value);
		addG.setFunction(value);
		addB.setFunction(value);		
	}	
	
	public void setColor (String value) 
	{		
		addR.setFunction(value);
		addG.setFunction(value);
		addB.setFunction(value);		
	}	
	
	public int getParentId() {
		return (int)ids.x;
	}
	
	public int getThisId() {
		return (int)ids.y;
	}	

	public float getDensity() {
		return densityValue;
	}
	
	public float getAbsoluteHeight() {
		return height.x;
	}
	
	public float getLocalHeight() {
		return height.y;
	}
	
	public float getN1() {
		return nutrientsValues.x;
	}	

	public float getN2() {
		return nutrientsValues.y;
	}
	
	public float getN3() {
		return nutrientsValues.z;
	}	
	
	
//enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final NType $TYPE;

	public static final NType.Field addR$FIELD;
	public static final NType.Field addG$FIELD;
	public static final NType.Field addB$FIELD;

	private static final class _Field extends NType.Field
	{
		private final int id;

		_Field (String name, int modifiers, de.grogra.reflect.Type type, de.grogra.reflect.Type componentType, int id)
		{
			super (BlockColor.$TYPE, name, modifiers, type, componentType);
			this.id = id;
		}

		@Override
		protected void setObjectImpl (Object o, Object value)
		{
			switch (id)
			{
				case 0:
					((BlockColor) o).addR = (CustomFunction) value;
					return;
				case 1:
					((BlockColor) o).addG = (CustomFunction) value;
					return;
				case 2:
					((BlockColor) o).addB = (CustomFunction) value;
					return;
			}
			super.setObjectImpl (o, value);
		}

		@Override
		public Object getObject (Object o)
		{
			switch (id)
			{
				case 0:
					return ((BlockColor) o).getAddR ();
				case 1:
					return ((BlockColor) o).getAddG ();
				case 2:
					return ((BlockColor) o).getAddB ();
			}
			return super.getObject (o);
		}
	}

	static
	{
		$TYPE = new NType (new BlockColor ());
		$TYPE.addManagedField (addR$FIELD = new _Field ("addR", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (CustomFunction.class), null, 0));
		$TYPE.addManagedField (addG$FIELD = new _Field ("addG", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (CustomFunction.class), null, 1));
		$TYPE.addManagedField (addB$FIELD = new _Field ("addB", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (CustomFunction.class), null, 2));
		$TYPE.validate ();
	}

	@Override
	protected NType getNTypeImpl ()
	{
		return $TYPE;
	}

	@Override
	protected de.grogra.graph.impl.Node newInstance ()
	{
		return new BlockColor ();
	}

	public CustomFunction getAddR ()
	{
		return addR;
	}

	public void setAddR (CustomFunction value)
	{
		addR$FIELD.setObject (this, value);
	}

	public CustomFunction getAddG ()
	{
		return addG;
	}

	public void setAddG (CustomFunction value)
	{
		addG$FIELD.setObject (this, value);
	}

	public CustomFunction getAddB ()
	{
		return addB;
	}

	public void setAddB (CustomFunction value)
	{
		addB$FIELD.setObject (this, value);
	}

//enh:end

}
