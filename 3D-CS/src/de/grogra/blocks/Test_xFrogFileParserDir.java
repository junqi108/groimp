/*
* Copyright (C) 2002 - 2006 Lehrstuhl Grafische Systeme, BTU Cottbus
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 3
* of the License, or any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

package de.grogra.blocks;

import java.io.File;

import de.grogra.blocks.xFrogFileParser.parser;

public class Test_xFrogFileParserDir {

	final static String path0 = "C:\\Dokumente und Einstellungen\\Besitzer\\Eigene Dateien\\Diplomarbeit\\src\\green\\meine\\"; // 29
	final static String path = "C:\\Dokumente und Einstellungen\\Besitzer\\Eigene Dateien\\Diplomarbeit\\src\\green\\"; // 86
	final static String pathAll = "E:\\green\\"; // 461
	final static String path1 = "E:\\green\\Greenworks Xfrog Plants Autumn\\"; // 60
	final static String path2 = "E:\\green\\Greenworks Xfrog Plants Europe2\\"; // 60
	final static String path3 = "E:\\green\\Greenworks Xfrog Plants Houseplants\\"; // 62
	final static String path4 = "E:\\green\\Greenworks Xfrog Plants Mediterranean\\"; // 61
	final static String path5 = "E:\\green\\Greenworks Xfrog Plants Flowers1\\"; // 70  f9 komischen arryNP
	final static String path6 = "E:\\green\\Greenworks Xfrog Plants Flowers2\\"; // 78
	final static String path7 = "E:\\green\\Greenworks Xfrog Plants Usa Southwest\\"; // 70

	static int ii = 0;

	private static void parseDir(String path) {
		File file = new File(path);
		if (file.isDirectory()) {
			String fils[] = file.list();
			for (int i = 0; i < fils.length; ++i) {
				if (fils[i].endsWith(".xfr")) {
					parser p1 = new parser();
					p1.parseFile(path + fils[i]);
					ii++;
					System.out.println(ii + "  " + fils[i]);
				} else {
					if (!fils[i].contains(".")) {
						parseDir(path + fils[i] + "\\");
					}
				}
			}
		}
	}

	public static void main(String[] args) {
		System.out.println("FunktionParser DIR test\n");
		parseDir(pathAll);

		System.out.println("\n\n Summe : " + ii);
		System.exit(0);
	}

}
