
/*
 * Copyright (C) 2002 - 2006 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.blocks;

import javax.vecmath.Point2f;
import javax.vecmath.Point3f;
import javax.vecmath.Tuple2f;
import javax.vecmath.Tuple3f;
import javax.vecmath.Vector3d;

import de.grogra.imp3d.objects.GlobalTransformation;
import de.grogra.imp3d.objects.Null;
import de.grogra.turtle.Scale;
import de.grogra.rgg.model.Instantiation;
import de.grogra.vecmath.Matrix34d;

public class BlockScale extends Null 
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 6L;

	/**
	 * Scaling factor in x-direction.
	 */	
	CustomFunction scaleX = new CustomFunction(1, 0, 25);
	//enh:field

	/**
	 * Scaling factor in y-direction.
	 */	
	CustomFunction scaleY = new CustomFunction(1, 0, 25);
	//enh:field

	/**
	 * Scaling factor in z-direction.
	 */	
	CustomFunction scaleZ = new CustomFunction(1, 0, 25);
	//enh:field
	
	// interne variablen, die von vater uebergeben werden
	private Point2f ids = new Point2f(0, 0);;
	private Float densityValue = new Float(0);
	private Tuple2f height = new Point2f(0, 0);
	private Tuple3f nutrientsValues = new Point3f(0, 0, 0);
	
	
	/**
	 * Creates a new <code>BlockScale</code> node whose scaling
	 * factors are set to one.
	 */	
	public BlockScale ()
	{		
		this (1, 1, 1);
	}

	/**
	 * Creates a new <code>BlockScale</code> node whose scaling
	 * factors are set to <code>scale</code>. This is a
	 * uniform scaling.
	 * 
	 * @param scale scaling factor
	 */	
	public BlockScale (double scale)
	{		
		this.scaleX.setFunction(scale);
		this.scaleY.setFunction(scale);
		this.scaleZ.setFunction(scale);
	}

	/**
	 * Creates a new <code>BlockScale</code> node whose scaling
	 * factors are set to <code>scale</code>. This is a
	 * uniform scaling.
	 * 
	 * @param scale scalingfunction
	 */	
	public BlockScale (String scale)
	{
		this.scaleX.setFunction(scale);
		this.scaleY.setFunction(scale);
		this.scaleZ.setFunction(scale);
	}

	/**
	 * Creates a new <code>BlockScale</code> node whose scaling
	 * factors are set to the specified values.
	 * 
	 * @param scaleX scalingfunction in x-direction
	 * @param scaleY scalingfunction in y-direction
	 * @param scaleZ scalingfunction in z-direction
	 */	
	public BlockScale (String scaleX, String scaleY, String scaleZ) 
	{
		this.scaleX.setFunction(scaleX);
		this.scaleY.setFunction(scaleY);
		this.scaleZ.setFunction(scaleZ);		
	}	

	/**
	 * Creates a new <code>BlockScale</code> node whose scaling
	 * factors are set to the specified values.
	 * 
	 * @param scaleX scaling factor in x-direction
	 * @param scaleY scaling factor in y-direction
	 * @param scaleZ scaling factor in z-direction
	 */	
	public BlockScale (double scaleX, double scaleY, double scaleZ) 
	{
		this.scaleX.setFunction(scaleX);
		this.scaleY.setFunction(scaleY);
		this.scaleZ.setFunction(scaleZ);		
	}	
	
	public void instantiate(Instantiation state) {
		Instantiation inst = (Instantiation) state;
		// uebergenenen werte holen			
		ids = (Point2f)inst.getGraphState().getObjectDefault(this, true, Attributes.ID, null);
		densityValue = (Float)inst.getGraphState().getObjectDefault(this, true, Attributes.DENSITY, 1f);
		//aktuelle hoehe des obj ueber null bestimmen
		Matrix34d m = GlobalTransformation.get(this, true, inst.getGraphState(),false);
		Vector3d v = new Vector3d();
		m.get(v);		
		height = new Point2f( (float)v.z,
				(Float)inst.getGraphState().getObjectDefault(this, true, Attributes.HEIGHT, null));
		// locationParameter-werte holen und neu berechnen
		nutrientsValues = (Tuple3f)inst.getGraphState().getObjectDefault(this, true, Attributes.LOCATIONPARAMETER, null);

		inst.instantiate(new Scale(
				scaleX.evaluateFloat(ids, nutrientsValues, height, densityValue),
				scaleY.evaluateFloat(ids, nutrientsValues, height, densityValue),
				scaleZ.evaluateFloat(ids, nutrientsValues, height, densityValue)));

//System.out.println("ids= "+ids+"    height= "+height+"    nV= "+nutrientsValues+"\t\t  sX="+scaleX.evaluateFloat(ids, nutrientsValues, height, densityValue));
	}

	private static void initType ()
	{
		$TYPE.addIdentityAccessor (de.grogra.turtle.Attributes.TRANSFORMATION);
		$TYPE.addDependency (de.grogra.turtle.Attributes.SCALE, 
				de.grogra.turtle.Attributes.TRANSFORMATION);
	}

	public void setScale (double scale)
	{
		scaleX.setFunction(scale);
		scaleY.setFunction(scale);
		scaleZ.setFunction(scale);
	}
	
	public void setScale (String scale)
	{
		scaleX.setFunction(scale);
		scaleY.setFunction(scale);
		scaleZ.setFunction(scale);
	}

	public void setScale (double scaleX, double scaleY, double scaleZ) 
	{
		this.scaleX.setFunction(scaleX);
		this.scaleY.setFunction(scaleY);
		this.scaleZ.setFunction(scaleZ);		
	}	
	
	public void setScale (String scaleX, String scaleY, String scaleZ) 
	{
		this.scaleX.setFunction(scaleX);
		this.scaleY.setFunction(scaleY);
		this.scaleZ.setFunction(scaleZ);		
	}	

	public void setScaleX(double scaleX) 
	{
		this.scaleX.setFunction(scaleX);
	}	
	
	public void setScaleY(double scaleY) 
	{
		this.scaleY.setFunction(scaleY);
	}	

	public void setScaleZ(double scaleZ) 
	{
		this.scaleZ.setFunction(scaleZ);
	}	
	
	public void setScaleX(String scaleX) 
	{
		this.scaleX.setFunction(scaleX);
	}	
	
	public void setScaleY(String scaleY) 
	{
		this.scaleY.setFunction(scaleY);
	}	

	public void setScaleZ(String scaleZ) 
	{
		this.scaleZ.setFunction(scaleZ);
	}	

	public int getParentId() {
		return (int)ids.x;
	}
	
	public int getThisId() {
		return (int)ids.y;
	}	

	public float getDensity() {
		return densityValue;
	}
	
	public float getAbsoluteHeight() {
		return height.x;
	}
	
	public float getLocalHeight() {
		return height.y;
	}
	
	public float getN1() {
		return nutrientsValues.x;
	}	

	public float getN2() {
		return nutrientsValues.y;
	}
	
	public float getN3() {
		return nutrientsValues.z;
	}	

	
//enh:insert initType ();
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final NType $TYPE;

	public static final NType.Field scaleX$FIELD;
	public static final NType.Field scaleY$FIELD;
	public static final NType.Field scaleZ$FIELD;

	private static final class _Field extends NType.Field
	{
		private final int id;

		_Field (String name, int modifiers, de.grogra.reflect.Type type, de.grogra.reflect.Type componentType, int id)
		{
			super (BlockScale.$TYPE, name, modifiers, type, componentType);
			this.id = id;
		}

		@Override
		protected void setObjectImpl (Object o, Object value)
		{
			switch (id)
			{
				case 0:
					((BlockScale) o).scaleX = (CustomFunction) value;
					return;
				case 1:
					((BlockScale) o).scaleY = (CustomFunction) value;
					return;
				case 2:
					((BlockScale) o).scaleZ = (CustomFunction) value;
					return;
			}
			super.setObjectImpl (o, value);
		}

		@Override
		public Object getObject (Object o)
		{
			switch (id)
			{
				case 0:
					return ((BlockScale) o).scaleX;
				case 1:
					return ((BlockScale) o).scaleY;
				case 2:
					return ((BlockScale) o).scaleZ;
			}
			return super.getObject (o);
		}
	}

	static
	{
		$TYPE = new NType (new BlockScale ());
		$TYPE.addManagedField (scaleX$FIELD = new _Field ("scaleX", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (CustomFunction.class), null, 0));
		$TYPE.addManagedField (scaleY$FIELD = new _Field ("scaleY", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (CustomFunction.class), null, 1));
		$TYPE.addManagedField (scaleZ$FIELD = new _Field ("scaleZ", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (CustomFunction.class), null, 2));
		initType ();
		$TYPE.validate ();
	}

	@Override
	protected NType getNTypeImpl ()
	{
		return $TYPE;
	}

	@Override
	protected de.grogra.graph.impl.Node newInstance ()
	{
		return new BlockScale ();
	}

//enh:end

}
