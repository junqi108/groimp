
/*
 * Copyright (C) 2002 - 2006 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.blocks;

import javax.vecmath.Tuple2f;
import javax.vecmath.Tuple3f;

import de.grogra.graph.ObjectAttribute;
import de.grogra.util.I18NBundle;

public class Attributes extends de.grogra.graph.Attributes {

	public static final I18NBundle I18N = I18NBundle.getInstance(Attributes.class);

	public static final ObjectAttribute NUMBER_INT
		= init (new ObjectAttribute (Integer.class, false, null), "number_int", I18N);

	public static final ObjectAttribute ID_TUPLE2F
		= init (new ObjectAttribute (Tuple2f.class, false, null), "id_tuple2f", I18N);

	public static final ObjectAttribute HEIGHT_FLOAT
		= init (new ObjectAttribute (Float.class, false, null), "height_float", I18N);
	
	public static final ObjectAttribute NUTRIENTS_TUPLE3F
		= init (new ObjectAttribute (Tuple3f.class, false, null), "nutrients_tuple3f", I18N);

	public static final ObjectAttribute TREE_TUPLE3F
		= init (new ObjectAttribute (Tuple3f.class, false, null), "tree_tuple3f", I18N);

	public static final ObjectAttribute DENSITY_FLOAT
	= init (new ObjectAttribute (Float.class, false, null), "density_float", I18N);
	

	
	public static final ObjectAttribute NUMBER = new NumberTreeAttribute();
	
	public static final ObjectAttribute ID = new IdTreeAttribute();

	public static final ObjectAttribute HEIGHT = new HeightTreeAttribute();	
		
	public static final ObjectAttribute LOCATIONPARAMETER = new LocationParameterTreeAttribute ();
	
	public static final ObjectAttribute TREEVALUES = new TreeValuesTreeAttribute ();

	public static final ObjectAttribute DENSITY = new DensityTreeAttribute();
	
}
