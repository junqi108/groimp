/*
* Copyright (C) 2002 - 2006 Lehrstuhl Grafische Systeme, BTU Cottbus
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 3
* of the License, or any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

package de.grogra.blocks;

import javax.vecmath.Point2f;
import javax.vecmath.Point3f;

import de.grogra.blocks.functionParser.parser;

class Test_FunctionParser {

	public static void main(String[] args) {
		System.out.println("Parser test!");
		String s = "";
		
		//Beacht das ';' am ende 
//		s = "(3 + - - 8/2) * (8-2) * 12/10.0 +sin(90)-cos(180) *abs(pi)+ 2*(pow(2,3)- tan(45));"; 
//		s = "3+8;"; 
//		s = "x^2;";
//		s = "(3 + - - 8/2 + x) * (8-2) * 12/10.0 +sin(90)-cos(180-x) *abs(pi)+ 2*(pow(2,3)- tan(45))*rnd(0.5) + pi*x;";		
		
		s = "((x%2==0) || (x==3))?x^2:0;";
		System.out.println(" Ausdruck2 = " + s);
				
		double[] x = { 1, 2, 3, 4, 5, 6 };
		parser parser1 = new parser();
		parser1.initParser(s);
		System.out.println(" out: \n\t"+parser1.toString()+"\n");
		System.out.println(" Ergbnisse ");
		for (int i = 0; i < x.length; i++) {
			System.out.println(i + " (" + x[i] + ", " + parser1.eval(x[i], new Point2f(i,0), new Point3f(0,0,0), new Point2f(0,0), 0, 0) + ")");
		}

		System.exit(0);
	}
}
