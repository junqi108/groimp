/*
* Copyright (C) 2002 - 2006 Lehrstuhl Grafische Systeme, BTU Cottbus
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 3
* of the License, or any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

package de.grogra.blocks.xFrogFileParser;

import de.grogra.graph.impl.Node;
import de.grogra.imp3d.objects.NURBSSurface;
import de.grogra.math.*;

public class Revo1 extends Expr {
	
	private String name = "";
	
	public Revo1(Expr a) {
		this.a = a;
		if (aktKeyFrame == 0) {
			name = new String(aktStructName);
			blocks.put(name, toXL());
			blocksGraphNodes.put(name, toGraph());
			use_GFXColor = false;
			aktTexture = null;
		}		
		if (debug)
			System.out.println(getClass().getSimpleName() + " :: " + a);
	}
	
	private String toXL() {
		String ss = "";
		ss += "setName(" + name + "), ";
		
		// farbe setzen
		if (use_GFXColor) ss += color;
		// letzte komma entfernen
		if (ss.lastIndexOf(',') > 0) {
			ss = ss.substring(0, ss.length() - 2);
		}
		return "createRevo("+toBSCS()+").(" + ss + ")";
	}

	private Node toGraph() {
		NURBSSurface obj = new NURBSSurface();
		Circle cr = new Circle();
		cr.setIntermediateArcs(3);	
		obj.setSurface(new SwungSurface(toBSplineCurve(((SizeFloatTripleRow) a).getXdata(), 
				   ((SizeFloatTripleRow) a).getYdata(), 
				   ((SizeFloatTripleRow) a).getZdata()), cr, 0, 1));
		obj.setVisibleSides(2);
		obj.setName(name);
		
		if (phong!=null && aktTexture!=null) phong.setDiffuse(aktTexture);
		if (use_GFXColor) obj.setMaterial(phong);
		return obj;		
	}
	
	private BSplineCurve toBSplineCurve(float[] xdata, float[] ydata, float[] zdata)
	{
		int flength = xdata.length;
		float[] data = new float[3*flength];
		int j = 0;
		for (int i = 0; i < flength; i++)
		{			
			data[j] = xdata[i];
			j++;
			data[j] = ydata[i];
			j++;
			data[j] = zdata[i];
			j++;
		}
		return new BezierCurve (data, 3);
	}	
	
	private String toBSCS()
	{
		return new String(
			"new float[] " + ((SizeFloatTripleRow) a).getXdataS() + ", " + 
			"new float[] " + ((SizeFloatTripleRow) a).getYdataS() + ", " +
			"new float[] " + ((SizeFloatTripleRow) a).getZdataS());
	}
	
	
	@Override
	public String toString() {
		return "Revo {\n" + a.toString() + "\n" + "}\n";
	}

}
