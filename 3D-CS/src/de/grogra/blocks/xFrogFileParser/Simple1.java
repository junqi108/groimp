/*
* Copyright (C) 2002 - 2006 Lehrstuhl Grafische Systeme, BTU Cottbus
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 3
* of the License, or any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

package de.grogra.blocks.xFrogFileParser;

import javax.vecmath.Matrix3d;
import javax.vecmath.Matrix4d;
import javax.vecmath.Vector3d;

import de.grogra.graph.impl.Node;
import de.grogra.math.TMatrix4d;

public class Simple1 extends Expr {

	private String name = "";

	public Simple1() {
		if (aktKeyFrame == 0) {
			name = new String(aktStructName);
			blocks.put(name, toXL());
			blocksGraphNodes.put(name, toGraph());
			use_GFXColor = false;
			aktTexture = null;
		}
		if (debug)
			System.out.println(getClass().getSimpleName() + " ::  ");
	}

	private String toXL() {
		String ss = "";

		ss += "setName(" + name + "), ";

		// ueberpruefen, ob eine transformation gesetzt
		boolean rot = false;
		boolean tran = false;
		boolean scale = false;
		if (!((FLOAT) (aktTransform.c.a.a.a.a)).equalsF5(0)
				|| !((FLOAT) (aktTransform.c.a.b.a.a)).equalsF5(0)
				|| !((FLOAT) (aktTransform.c.a.c.a.a)).equalsF5(0))
			tran = true;
		if (!((FLOAT) (aktTransform.b.a.a.a.a)).equalsF5(0)
				|| !((FLOAT) (aktTransform.b.a.b.a.a)).equalsF5(0)
				|| !((FLOAT) (aktTransform.b.a.c.a.a)).equalsF5(0))
			rot = true;
		if (!((FLOAT) (aktTransform.a.a.a.a.a)).equalsF5(1)
				|| !((FLOAT) (aktTransform.a.a.b.a.a)).equalsF5(1)
				|| !((FLOAT) (aktTransform.a.a.c.a.a)).equalsF5(1))
			scale = true;

		String transf = "";
		int ii = transformMatrizen.size();

		if (rot) {
			transf += "Matrix3d m3_" + ii + " = new Matrix3d ();\n";
			transf += "m3_" + ii + ".rotX("
					+ ((FLOAT) aktTransform.b.a.a.a.a).getValue() + ");\n";
			transf += "m3_" + ii + ".rotY("
					+ ((FLOAT) aktTransform.b.a.b.a.a).getValue() + ");\n";
			transf += "m3_" + ii + ".rotZ("
					+ ((FLOAT) aktTransform.b.a.c.a.a).getValue() + ");\n";
		}

		if (tran) {
			transf += "Vector3d t" + ii + " = new Vector3d("
					+ ((FLOAT) aktTransform.c.a.a.a.a).getValue() + ", "
					+ ((FLOAT) aktTransform.c.a.b.a.a).getValue() + ", "
					+ ((FLOAT) aktTransform.c.a.c.a.a).getValue() + ");\n";
		}

		if (scale) {
			transf += "Matrix4d m4_" + ii + " = new Matrix4d();\n";
			transf += "m4_" + ii + ".m00 = "
					+ ((FLOAT) aktTransform.a.a.a.a.a).getValue() + ";\n";
			transf += "m4_" + ii + ".m11 = "
					+ ((FLOAT) aktTransform.a.a.b.a.a).getValue() + ";\n";
			transf += "m4_" + ii + ".m22 = "
					+ ((FLOAT) aktTransform.a.a.c.a.a).getValue() + ";\n";
		}

		transf += "TMatrix4d transf" + ii + " = new TMatrix4d ();\n";
		if (rot)
			transf += "transf" + ii + ".setRotationScale(m3_" + ii + ");\n";
		if (scale)
			transf += "transf" + ii + ".mul(m4_" + ii + ");\n";
		if (tran)
			transf += "transf" + ii + ".setTranslation(t" + ii + ");\n";

		if (rot || tran || scale) {
			transformMatrizen.add(transf);
			ss += "setTransform(transf" + ii + "), ";
		}

		// farbe setzen
		if (use_GFXColor) ss += color;
		
		// letzte komma entfernen
		if (ss.lastIndexOf(',') > 0) {
			ss = ss.substring(0, ss.length() - 2);
		}

		ss = aktPrimitive.getClass().getSimpleName() + "()" + ".(" + ss + ")";
		return ss;
	}

	private Node toGraph() {
		aktPrimitive.setName(name);

		// Rotation
		Matrix3d m1 = new Matrix3d();
		m1.rotX(((FLOAT) aktTransform.b.a.a.a.a).getValue() + ((FLOAT) oldTransform.b.a.a.a.a).getValue());
		m1.rotY(((FLOAT) aktTransform.b.a.b.a.a).getValue() + ((FLOAT) oldTransform.b.a.b.a.a).getValue());
		m1.rotZ(((FLOAT) aktTransform.b.a.c.a.a).getValue() + ((FLOAT) oldTransform.b.a.c.a.a).getValue());
		// Translation
		Vector3d t1 = new Vector3d(
				((FLOAT) aktTransform.c.a.a.a.a).getValue() + ((FLOAT) oldTransform.c.a.a.a.a).getValue(),
				((FLOAT) aktTransform.c.a.b.a.a).getValue() + ((FLOAT) oldTransform.c.a.b.a.a).getValue(),
				((FLOAT) aktTransform.c.a.c.a.a).getValue() + ((FLOAT) oldTransform.c.a.c.a.a).getValue());
		// Scale
		Matrix4d m2 = new Matrix4d();
		m2.m00 = ((FLOAT) aktTransform.a.a.a.a.a).getValue() * ((FLOAT) oldTransform.a.a.a.a.a).getValue();
		m2.m11 = ((FLOAT) aktTransform.a.a.b.a.a).getValue() * ((FLOAT) oldTransform.a.a.b.a.a).getValue();
		m2.m22 = ((FLOAT) aktTransform.a.a.c.a.a).getValue() * ((FLOAT) oldTransform.a.a.c.a.a).getValue();

		TMatrix4d transf = new TMatrix4d();
		transf.setRotationScale(m1);
		transf.mul(m2);
		transf.setTranslation(t1);
		
		aktPrimitive.setTransform(transf);	
		aktPrimitive.setTransforming(false);
		
		if (phong!=null && aktTexture!=null) phong.setDiffuse(aktTexture);
		if (use_GFXColor) aktPrimitive.setMaterial(phong);		
		return aktPrimitive;
	} 

	private Node toGraph2() {
		aktPrimitive.setName(name);

		// Rotation
		Matrix3d m1 = new Matrix3d();
		m1.rotX(((FLOAT) aktTransform.b.a.a.a.a).getValue());
		m1.rotY(((FLOAT) aktTransform.b.a.b.a.a).getValue());
		m1.rotZ(((FLOAT) aktTransform.b.a.c.a.a).getValue());
		// Translation
		Vector3d t1 = new Vector3d(((FLOAT) aktTransform.c.a.a.a.a).getValue(),
				((FLOAT) aktTransform.c.a.b.a.a).getValue(),
				((FLOAT) aktTransform.c.a.c.a.a).getValue());
		// Scale
		Matrix4d m2 = new Matrix4d();
		m2.m00 = ((FLOAT) aktTransform.a.a.a.a.a).getValue();
		m2.m11 = ((FLOAT) aktTransform.a.a.b.a.a).getValue();
		m2.m22 = ((FLOAT) aktTransform.a.a.c.a.a).getValue();

		TMatrix4d transf = new TMatrix4d();
		transf.setRotationScale(m1);
		transf.mul(m2);
		transf.setTranslation(t1);

		aktPrimitive.setTransform(transf);	
		if (phong!=null && aktTexture!=null) phong.setDiffuse(aktTexture);
		if (use_GFXColor) aktPrimitive.setMaterial(phong);		
		return aktPrimitive;
	}	
	
	@Override
	public String toString() {
		return "";
	}

}
