
package de.grogra.blocks.xFrogFileParser;

import java_cup.runtime.*;

%%

%cup

%class scanner2
%function next_token
%type Symbol

%{
private final boolean debug = false; // ist ueberall definiert
%}

alnum2 = [a-zA-Z_~0-9\/\\\+\-\*\/\(\)\.\^ ]
dec =	[0-9]
hex =	[0-9a-fA-F]
sign =	[+-]?
exp =	([eE]{sign}{dec}+)
X =	[xX]

%%

","						{ return new Symbol(sym.KOMMA); }
"{"						{ return new Symbol(sym.SKAUF); }
"}"						{ return new Symbol(sym.SKZU); }
"\"id\""				{ return new Symbol(sym.ID); }
"\"sin\""				{ return new Symbol(sym.SIN); }
"\"cos\""				{ return new Symbol(sym.COS); }
"\"tan\""				{ return new Symbol(sym.TAN); }
"\"asin\""				{ return new Symbol(sym.ASIN); }
"\"acos\""				{ return new Symbol(sym.ACOS); }
"\"atan\""				{ return new Symbol(sym.ATAN); }
"\"sinh\""				{ return new Symbol(sym.SINH); }
"\"cosh\""				{ return new Symbol(sym.COSH); }
"\"tanh\""				{ return new Symbol(sym.TANH); }
"\"exp\""				{ return new Symbol(sym.EXP); }
"\"exp2\""				{ return new Symbol(sym.EXP2); }
"\"log\""				{ return new Symbol(sym.LOG); }
"\"sqr\""				{ return new Symbol(sym.SQR); }
"\"sqrt\""				{ return new Symbol(sym.SQRT); }
"\"ceil\""				{ return new Symbol(sym.CEIL); }
"\"floor\""				{ return new Symbol(sym.FLOOR); }
"\"rndabs\""			{ return new Symbol(sym.RNDABS); }
"\"rnd\""				{ return new Symbol(sym.RND); }
"\"abs\""				{ return new Symbol(sym.ABS); }
"\"phi\""				{ return new Symbol(sym.PHI); }
"\"pi\""				{ return new Symbol(sym.PI); }
"\"rad\""				{ return new Symbol(sym.RAD); }
"\"chdm\""				{ return new Symbol(sym.CHDM); }
"\"iter\""				{ return new Symbol(sym.ITER); }
"\"rec\""				{ return new Symbol(sym.REC); }
"\"ftime\""				{ return new Symbol(sym.FTIME); }
"\"frame\""				{ return new Symbol(sym.FRAME); }
"\"smooth\""			{ return new Symbol(sym.SMOOTHN); }
"\"clamp\""				{ return new Symbol(sym.CLAMP); }
"Version"				{ if (debug) System.out.println("VERSION L"); return new Symbol(sym.VERSION); }
"Queue"					{ if (debug) System.out.println("QUEUE L"); return new Symbol(sym.QUEUE); }
"Frames"				{ if (debug) System.out.println("FRAMES L"); return new Symbol(sym.FRAMES); }
"Unit"					{ if (debug) System.out.println("UNIT L"); return new Symbol(sym.UNIT); }
"Entity"				{ if (debug) System.out.println("ENTITY L"); return new Symbol(sym.ENTITY); }
"KeySequence"			{ if (debug) System.out.println("KEYSEQUENCE L"); return new Symbol(sym.KEYSEQUENCE); }
"KeyFrame"				{ if (debug) System.out.println("KEYFRAME L"); return new Symbol(sym.KEYFRAME); }
"Model"					{ if (debug) System.out.println("MODEL L"); return new Symbol(sym.MODEL); }
"Struct"				{ if (debug) System.out.println("STRUCT L"); return new Symbol(sym.STRUCT); }
"AccessInfo"			{ return new Symbol(sym.ACCESSINFO); }
"Primitive"				{ if (debug) System.out.println("PRIMITIVE L"); return new Symbol(sym.PRIMITIVE); }
"Horn"					{ if (debug) System.out.println("HORN_0 L"); return new Symbol(sym.HORN); }
"Tree"					{ if (debug) System.out.println("TREE_0 L"); return new Symbol(sym.TREE); }
"Leaf"					{ if (debug) System.out.println("LEAF_0 L"); return new Symbol(sym.LEAF); }
"PhiBall"				{ if (debug) System.out.println("PHIBALL_0 L"); return new Symbol(sym.PHIBALL); }
"Hydra"					{ if (debug) System.out.println("HYDRA_0 L"); return new Symbol(sym.HYDRA); }
"FFD"					{ if (debug) System.out.println("FFD_0 L"); return new Symbol(sym.FFD); }
"Revo"					{ if (debug) System.out.println("REVO_0 L"); return new Symbol(sym.REVO); }
"Wreath"				{ if (debug) System.out.println("WREATH_0 L"); return new Symbol(sym.WREATH); }
"HyperPatch"			{ if (debug) System.out.println("HYPERPATCH_0 L"); return new Symbol(sym.HYPERPATCH); }
"Branchiness"			{ return new Symbol(sym.BRANCHINESS); }
"Forkiness"				{ return new Symbol(sym.FORKINESS); }
"GeomScale"				{ return new Symbol(sym.GEOMSCALE); }
"Deviate"				{ return new Symbol(sym.DEVIATE); }
"Angle"					{ return new Symbol(sym.ANGLE); }
"ForkAngle"				{ return new Symbol(sym.FORKANGLE); }
"Phototropism"			{ return new Symbol(sym.PHOTOTROPISM); }
"Gravitropism"			{ return new Symbol(sym.GRAVITROPISM); }
"Kruschtl"				{ return new Symbol(sym.KRUSCHTL); }
"Profil"				{ return new Symbol(sym.PROFIL); }
"Knots"					{ return new Symbol(sym.KNOTS); }
"Characteristic"		{ return new Symbol(sym.CHARACTERISTIC); }
"Branch_Num"			{ return new Symbol(sym.BRANCH_NUM); }
"Fork_Num"				{ return new Symbol(sym.FORK_NUM); }
"Len"					{ return new Symbol(sym.LEN); }
"TransZ"				{ return new Symbol(sym.TRANSZ); }
"KruschtlKnots"			{ return new Symbol(sym.KRUSCHTLKNOTS); }
"IniScale"				{ return new Symbol(sym.INISCALE); }
"MoveScale"				{ return new Symbol(sym.MOVESCALE); }
"Length"				{ return new Symbol(sym.LENGTH); }
"RadDeform"				{ return new Symbol(sym.RADDEFORM); }
"Segments"				{ return new Symbol(sym.SEGMENTS); }
"Top"					{ return new Symbol(sym.TOP); }
"Scale"					{ return new Symbol(sym.SCALE); }
"Shape"					{ return new Symbol(sym.SHAPE); }
"Dense"					{ return new Symbol(sym.DENSE); }
"Fan"					{ return new Symbol(sym.FAN); }
"Spin"					{ return new Symbol(sym.SPIN); }
"Twist"					{ return new Symbol(sym.TWIST); }
"Trans"					{ return new Symbol(sym.TRANS); }
"Steps"					{ return new Symbol(sym.STEPS); }
"Flap"					{ return new Symbol(sym.FLAP); }
"Screw"					{ return new Symbol(sym.SCREW); }
"RotX"					{ return new Symbol(sym.ROTX); }
"RotY"					{ return new Symbol(sym.ROTY); }
"RotZ"					{ return new Symbol(sym.ROTZ); }
"TrX"					{ return new Symbol(sym.TRX); }
"TrY"					{ return new Symbol(sym.TRY); }
"TrZ"					{ return new Symbol(sym.TRZ); }
"Smooth"				{ return new Symbol(sym.SMOOTH); }
"Size"					{ return new Symbol(sym.SIZE); }
"Points"				{ return new Symbol(sym.POINTS); }
"Separator"				{ return new Symbol(sym.SEPARATOR); }
"Deformable"			{ return new Symbol(sym.DEFORMABLE); }
"TorusMinor"			{ return new Symbol(sym.TORUSMINOR); }
"Resolution"			{ return new Symbol(sym.RESOLUTION); }
"Displace"				{ return new Symbol(sym.DISPLACE); }
"Texture"				{ return new Symbol(sym.TEXTURE); }
"Texture_File"			{ return new Symbol(sym.TEXTURE_FILE); }
"Texture_Handler"		{ return new Symbol(sym.TEXTURE_HANDLER); }
"Texture_Mode"			{ return new Symbol(sym.TEXTURE_MODE); }
"Texture_ScaleS"		{ return new Symbol(sym.TEXTURE_SCALES); }
"Texture_ScaleT"		{ return new Symbol(sym.TEXTURE_SCALET); }
"Texture_ShiftS"		{ return new Symbol(sym.TEXTURE_SHIFTS); }
"Texture_ShiftT"		{ return new Symbol(sym.TEXTURE_SHIFTT); }
"Texture_Speed"			{ return new Symbol(sym.TEXTURE_SPEED); }
"Color_Select"			{ return new Symbol(sym.COLOR_SELECT); }
"Attractor_Center"		{ return new Symbol(sym.ATTRACTOR_CENTER); }
"Attractor_Strength"	{ return new Symbol(sym.ATTRACTOR_STRENGTH); }
"Attractor_Mode"		{ return new Symbol(sym.ATTRACTOR_MODE); }
"NewAlphaDef"			{ return new Symbol(sym.NEWALPHADEF); }
"Simple_Triangulation"	{ return new Symbol(sym.SIMPLE_TRIANGULATION); }
"Use_Color_Spline"		{ return new Symbol(sym.USE_COLOR_SPLINE); }
"Culling"				{ return new Symbol(sym.CULLING); }
"CullRadius"			{ return new Symbol(sym.CULLRADIUS); }
"CullPos"				{ return new Symbol(sym.CULLPOS); }
"Photo_Dir"				{ return new Symbol(sym.PHOTO_DIR); }
"Photo_Mode"			{ return new Symbol(sym.PHOTO_MODE); }
"Photo"					{ return new Symbol(sym.PHOTO); }
"Existence"				{ return new Symbol(sym.EXISTENCE); }
"Radius"				{ return new Symbol(sym.RADIUS); }
"Number"				{ return new Symbol(sym.NUMBER); }
"Influence"				{ return new Symbol(sym.INFLUENCE); }
"Struct_Mut_Info"		{ return new Symbol(sym.STRUCT_MUT_INFO); }
"Trigger"				{ return new Symbol(sym.TRIGGER); }
"Trigger_Method"		{ return new Symbol(sym.TRIGGER_METHOD); }
"Acceleration"			{ return new Symbol(sym.ACCELERATION); }
"Time"					{ if (debug) System.out.println("TIME L"); return new Symbol(sym.TIME); }
"Animate_Camera"		{ if (debug) System.out.println("ANIMATE_CAMERA L");return new Symbol(sym.ANIMATE_CAMERA); }
"Active"				{ if (debug) System.out.println("ACTIVE L"); return new Symbol(sym.ACTIVE); }
"Deform_Points"			{ return new Symbol(sym.DEFORM_POINTS); }
"Bezier"				{ return new Symbol(sym.BEZIER); }
"Attractor"				{ return new Symbol(sym.ATTRACTOR); }
"Transform"				{ return new Symbol(sym.TRANSFORM); }
"ShapeKnots"			{ return new Symbol(sym.SHAPEKNOTS); }
"Patch_Points"			{ return new Symbol(sym.PATCH_POINTS); }
"Rotation"				{ return new Symbol(sym.ROTATION); }
"Translation"			{ return new Symbol(sym.TRANSLATION); }
"Type"					{ return new Symbol(sym.TYPE); }
"\"Undefined\""			{ return new Symbol(sym.UNDEFINED_GF); }
"\"Tube\""				{ return new Symbol(sym.TUBE_GF); }
"\"Area\""				{ return new Symbol(sym.AREA_GF); }
"\"Square\""			{ return new Symbol(sym.SQUQRE_GF); }
"\"Circle\""			{ return new Symbol(sym.CIRCLE_GF); }
"\"Box\""				{ return new Symbol(sym.BOX_GF); }
"\"Sphere\""			{ return new Symbol(sym.SPHERE_GF); }
"\"Torus\""				{ return new Symbol(sym.TORUS_GF); }
"\"Cylinder\""			{ return new Symbol(sym.CYLINDER_GF); }
"\"Cone\""				{ return new Symbol(sym.CONE_GF); }
"\"Attractor\""			{ return new Symbol(sym.ATTRACTOR_GF); }
"\"Triangle Up\""		{ return new Symbol(sym.TRIANGLEUP_GF); }
"\"Triangle Down\""		{ return new Symbol(sym.TRIANGLEDOWN_GF); }
"Ribs"					{ return new Symbol(sym.RIBS); }
"Children"				{ if (debug) System.out.println("CHILDREN L"); return new Symbol(sym.CHILDREN); }
"Branches"				{ if (debug) System.out.println("BRANCHES L"); return new Symbol(sym.BRANCHES); }
"Export_GroupName"		{ if (debug) System.out.println("EXPORT_GROUPNAME L"); return new Symbol(sym.EXPORT_GROUPNAME); }
"Name"					{ if (debug) System.out.println("NAME L"); return new Symbol(sym.NAME); }
"Camera"				{ if (debug) System.out.println("CAMERA L"); return new Symbol(sym.CAMERA); }
"View_Vector"			{ return new Symbol(sym.VIEW_VECTOR); }
"Foot_Vector"			{ return new Symbol(sym.FOOT_VECTOR); }
"View_Point"			{ return new Symbol(sym.VIEW_POINT); }
"Zoom"					{ return new Symbol(sym.ZOOM); }
"Near"					{ return new Symbol(sym.NEAR); }
"Shift_Right"			{ return new Symbol(sym.SHIFT_RIGHT); }
"Shift_Up"				{ return new Symbol(sym.SHIFT_UP); }
"Far"					{ return new Symbol(sym.FAR); }
"OccularDistance"		{ return new Symbol(sym.OCCULARDISTANCE); }
"EyePlane"				{ return new Symbol(sym.EYEPLANE); }
"PlaneOrigin"			{ return new Symbol(sym.PLANEORIGIN); }
"BrushConstant"			{ return new Symbol(sym.BRUSHCONSTANT); }
"GFXColor"				{ return new Symbol(sym.GFXCOLOR); }
"Use_GFXColor"			{ return new Symbol(sym.USE_GFXCOLOR); }
"ApplyProfil"			{ return new Symbol(sym.APPLYPROFIL); }
"ApplyProfil3D"			{ return new Symbol(sym.APPLYPROFIL3D); }
"AMBIENT"				{ return new Symbol(sym.AMBIENT1); }
"DIFFUSE"				{ return new Symbol(sym.DIFFUSE1); }
"SPECULAR"				{ return new Symbol(sym.SPECULAR1); }
"EMISSION"				{ return new Symbol(sym.EMISSION1); }
"ALPHA"					{ return new Symbol(sym.ALPHA1); }
"Alpha"					{ return new Symbol(sym.ALPHA); }
"TRUE"					{ return new Symbol(sym.TRUE); }
"FALSE"					{ return new Symbol(sym.FALSE); }
"Basis"					{ return new Symbol(sym.BASIS); }
"TG_NONE"				{ return new Symbol(sym.TG_NONE); }
"TG_SPHEREMAP"			{ return new Symbol(sym.TG_SPHEREMAP); }
"X"						{ return new Symbol(sym.X); }
"Y"						{ return new Symbol(sym.Y); }
"Z"						{ return new Symbol(sym.Z); }
"Vector"				{ if (debug) System.out.println("VECTOR L"); return new Symbol(sym.VECTOR); }
"Background"			{ return new Symbol(sym.BACKGROUND); }
"Pairs"					{ return new Symbol(sym.PAIRS); }
"ApplyShape"			{ return new Symbol(sym.APPLYSHAPE); }
"Spline"				{ return new Symbol(sym.SPLINE); }
"Color"					{ return new Symbol(sym.COLOR); }
"On"					{ return new Symbol(sym.ON); }
"Off"					{ return new Symbol(sym.OFF); }
"Reference"				{ return new Symbol(sym.REFERENCE); }
"Group"					{ return new Symbol(sym.GROUP); }
"Plane"					{ return new Symbol(sym.PLANE); }
"Hide"					{ return new Symbol(sym.HIDE); }
"Texture_VGeneric"		{ return new Symbol(sym.TEXTURE_VGENERIC); }
"Switch"				{ return new Symbol(sym.SWITCH); }
"World"					{ return new Symbol(sym.WORLD); }
"Gravitation"			{ return new Symbol(sym.GRAVITATION); }
"Func_X"				{ return new Symbol(sym.FUNC_X); }
"Func_Y"				{ return new Symbol(sym.FUNC_Y); }
"Func_Z"				{ return new Symbol(sym.FUNC_Z); }
"Light_Scale"			{ return new Symbol(sym.LIGHT_SCALE); }
"Gravi_Scale"			{ return new Symbol(sym.GRAVI_SCALE); }
"TG_LINEAR"				{ return new Symbol(sym.TG_LINEAR); }
"TG_UVW"				{ return new Symbol(sym.TG_UVW); }
"Lights"				{ return new Symbol(sym.LIGHTS); }
"Light"					{ return new Symbol(sym.LIGHT); }
"Attractor_Hot_Spot"	{ return new Symbol(sym.ATTRACTOR_HOT_SPOT); }
"LCOLOR"				{ return new Symbol(sym.LCOLOR); }
"POSITION"				{ return new Symbol(sym.POSITION); }

0{X}{hex}+				{ return new Symbol(sym.HEX, new String(yytext())); }
"-"?{dec}+"."{dec}*{exp}?|"-"?{dec}*"."{dec}+{exp}?|"-"?{dec}+{exp}|"-"?{dec}+
						{ return new Symbol(sym.FLOAT, new String(yytext())); }						

"\""{alnum2}":"?{alnum2}*"."?{alnum2}*"\"" 	{ if (debug) System.out.println("STRING GF L"); return new Symbol(sym.STRING_GF, new String(yytext())); }

[ \t\r\n\f]	{ /* white spaces ingnorieren */ }
.   { System.err.println("xFrogFileParser: Unerwartetes Zeichen: "  + yytext()); }

