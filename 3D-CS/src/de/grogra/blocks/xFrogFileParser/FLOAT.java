/*
* Copyright (C) 2002 - 2006 Lehrstuhl Grafische Systeme, BTU Cottbus
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 3
* of the License, or any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

package de.grogra.blocks.xFrogFileParser;

public class FLOAT extends Expr {

	private float value;

	private final int precision = 1000;

	public FLOAT(float value) {
		this.value = value;
//		if (debug)
//			System.out.println(getClass().getSimpleName() + " :: " + value);
	}

	public boolean equalsF5(double bi) {
		return ((int) (value * precision)) == ((int) (bi * precision));
	}

	public float getValue() {
		return value;
	}

	@Override
	public String toString() {
		return "" + value;
	}

}
