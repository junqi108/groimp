/*
* Copyright (C) 2002 - 2006 Lehrstuhl Grafische Systeme, BTU Cottbus
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 3
* of the License, or any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

package de.grogra.blocks.xFrogFileParser;

import java.util.StringTokenizer;

public class SizeFloatTripleRow extends Expr {

	private final String[] typeList = { "Branchiness", "Forkiness", "shape",
			"Scale", "GeomScale", "Deviate", "Screw", "Angle", "Light_Scale",
			"Gravi_Scale", "ForkAngle", "Phototropism", "Gravitropism",
			"Kruschtl", "Dense", "Profil", "Knots", "ShapeKnots", "Patch_Points"};

	private int type = 0;

	private float[] xdata;

	private float[] ydata;
	
	private float[] zdata;

	private String xdataS = "{";

	private String ydataS = "{";
	
	private String zdataS = "{";

	public SizeFloatTripleRow(Expr a, Expr b, int type) {
		this.a = a;
		this.b = b;
		this.type = type;

		xdata = new float[((INT) a.a).getValue()];
		ydata = new float[((INT) a.a).getValue()];
		zdata = new float[((INT) a.a).getValue()];

		String ss = b.toString().replaceAll("\n", " ");
		StringTokenizer sn = new StringTokenizer(ss.replaceAll(",", " "));
		int anz = 0;
		while (sn.hasMoreTokens()) {
			xdata[anz] = Float.parseFloat((String) sn.nextElement());
			ydata[anz] = Float.parseFloat((String) sn.nextElement());
			zdata[anz] = Float.parseFloat((String) sn.nextElement());			
			xdataS += xdata[anz];
			ydataS += ydata[anz];
			zdataS += zdata[anz];
			if (anz < sn.countTokens()) {
				xdataS += ", ";
				ydataS += ", ";
				zdataS += ", ";
			}
			anz++;
		}
		xdataS += "}";
		ydataS += "}";
		zdataS += "}";
		if (debug)
			System.out.println(getClass().getSimpleName() + " :: " + a + " = " + typeList[type]);
	}

	public float[] getXdata() {
		return xdata;
	}

	public float[] getYdata() {
		return ydata;
	}

	public float[] getZdata() {
		return zdata;
	}

	public String getXdataS() {
		return xdataS;
	}

	public String getYdataS() {
		return ydataS;
	}

	public String getZdataS() {
		return zdataS;
	}
	
	@Override
	public String toString() {
		return typeList[type] + " {\n" + a.toString() + "\n" + b.toString()
				+ "\n" + "}\n";
	}

}
