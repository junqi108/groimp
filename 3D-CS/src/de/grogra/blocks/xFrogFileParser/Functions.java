/*
* Copyright (C) 2002 - 2006 Lehrstuhl Grafische Systeme, BTU Cottbus
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 3
* of the License, or any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

package de.grogra.blocks.xFrogFileParser;

import de.grogra.blocks.CustomFunction;
import de.grogra.math.Abs;
import de.grogra.math.Acos;
import de.grogra.math.Asin;
import de.grogra.math.Atan;
import de.grogra.math.Ceil;
import de.grogra.math.Cos;
import de.grogra.math.Cosh;
import de.grogra.math.Exp;
import de.grogra.math.Exp2;
import de.grogra.math.Floor;
import de.grogra.math.Id;
import de.grogra.math.Log;
import de.grogra.math.Phi;
import de.grogra.math.Pi;
import de.grogra.math.Rad;
import de.grogra.math.Rnd;
import de.grogra.math.Rndabs;
import de.grogra.math.Sin;
import de.grogra.math.Sinh;
import de.grogra.math.Sqr;
import de.grogra.math.Sqrt;
import de.grogra.math.Tan;
import de.grogra.math.Tanh;
import de.grogra.xl.lang.FloatToFloat;

public class Functions extends Expr {

	private final String[] functionListString = { "\"id\"", "\"sin\"",
			"\"cos\"", "\"Tan\"", "\"asin\"", "\"acos\"", "\"atan\"",
			"\"sinh\"", "\"cosh\"", "\"tanh\"", "\"exp\"", "\"exp2\"",
			"\"log\"", "\"sqr\"", "\"sqrt\"", "\"ceil\"", "\"floor\"",
			"\"rndabs\"", "\"rnd\"", "\"abs\"", "\"phi\"", "\"pi\"", "\"rad\"",
			"\"chdm\"", "\"iter\"", "\"rec\"", "\"ftime\"", "\"frame\"",
			"\"smooth\"", "\"clamp\"" };

	private final String[] functionListXL = { "new Id()", "new Sin()",
			"new Cos()", "new Tan()", "new Asin()", "new Acos()", "new Atan()",
			"new Sinh()", "new Cosh()", "new Tanh()", "new Exp()",
			"new Exp2()", "new Log()", "new Sqr()", "new Sqrt()", "new Ceil()",
			"new Floor()", "new Rndabs()", "new Rnd()", "new Abs()", "new Phi()",
			"new Pi()", "new Rad()", "new Id()", "new Id()", "new Id()",
			"new Id()", "new Id()", "new Id()", "new Id()" };

	private final String[] notSupportFuctions = { "iter", "rec", "time", "frame", 
			"chdrn", "ftime", "smooth", "clamp"};
	
	private int ii = 0;

	private String ss = "";

	private FloatToFloat function = null;
	
	public Functions(int ii) {
		this.ii = ii;
		makeFunction();
		if (debug)
			System.out.println(getClass().getSimpleName() + " :: "+ii+" => "+functionListString[ii]);
	}

	public Functions(String ss) {
		ii = -1;
		this.ss = ss;
		makeFunction();
		if (debug)
			System.out.println(getClass().getSimpleName() + " :: custom = "+ss);
	}

	
	private void makeFunction() {
		switch (ii) {
		case -1:
			function = new CustomFunction(cleanFunctions(ss));
			break;
		case 0:
			function = new Id();
			break;
		case 1:
			function = new Sin();
			break;
		case 2:
			function = new Cos();
			break;
		case 3:
			function = new Tan();
			break;
		case 4:
			function = new Asin();
			break;
		case 5:
			function = new Acos();
			break;
		case 6:
			function = new Atan();
			break;
		case 7:
			function = new Sinh();
			break;
		case 8:
			function = new Cosh();
			break;
		case 9:
			function = new Tanh();
			break;
		case 10:
			function = new Exp();
			break;
		case 11:
			function = new Exp2();
			break;
		case 12:
			function = new Log();
			break;
		case 13:
			function = new Sqr();
			break;
		case 14:
			function = new Sqrt();
			break;
		case 15:
			function = new Ceil();
			break;
		case 16:
			function = new Floor();
			break;
		case 17:
			function = new Rndabs();
			break;
		case 18:
			function = new Rnd();
			break;
		case 19:
			function = new Abs();
			break;
		case 20:
			function = new Phi();
			break;
		case 21:
			function = new Pi();
			break;
		case 22:
			function = new Rad();
			break;
		case 23:
			function = new Id();
			break; // chdm
		case 24:
			function = new Id();
			break; // iter
		case 25:
			function = new Id();
			break; // rec
		case 26:
			function = new Id();
			break; // ftime
		case 27:
			function = new Id();
			break; // frame
		case 28:
			function = new Id();
			break; // smooth
		case 29:
			function = new Id();
			break; // clamp
		}
	}

	private String cleanFunctions (String string)
	{
		// '"' entfernen
		string = string.substring(1,string.length()-1);
		
		// alle funktionen klein schreiben
		string = string.toLowerCase();
		
		// nicht unterstuetzte funktionen durch id ersetzen
		for (int i=0; i<notSupportFuctions.length; i++) 
		{
			string = string.replaceAll(notSupportFuctions[i], "id");
		}
		return string;
	}

	public FloatToFloat getFunction() {
		return function;
	}
	
	public String toXL() {
		if (ii == -1)
			return "new CustomFunction(" + ss + ")";
		return functionListXL[ii];
	}

	@Override
	public String toString() {
		if (ii == -1)
			return ss;
		return functionListString[ii];
	}

}
