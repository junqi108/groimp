/*
* Copyright (C) 2002 - 2006 Lehrstuhl Grafische Systeme, BTU Cottbus
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 3
* of the License, or any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

package de.grogra.blocks.xFrogFileParser;

public class StructChildren2 extends Expr {

	public StructChildren2(Expr a, Expr b, Expr c, Expr d, Expr e, Expr f) {
		this.a = a;
		this.b = b;
		this.c = c;
		this.d = d;
		this.e = e;
		this.f = f;		
		
		if (aktKeyFrame == 0) {
			// cameraparams setzen
			camera.z = (e!=null)?((FLOAT)(e.e.a)).getValue():0;
		}
		
		if (debug)
			System.out.println(getClass().getSimpleName() + " :: " + a);
	}

	@Override
	public String toString() {
		if (debugS)
			System.out.println("TS  " + getClass().getSimpleName());
		return "Struct {\n" + a.toString() + "\n" + b.toString() + "\n"
				+ c.toString() + "\n" + d.toString() + "\n" + e.toString()
				+ "\n" + f.toString() + "\n" + "}\n";
	}

}
