/*
* Copyright (C) 2002 - 2006 Lehrstuhl Grafische Systeme, BTU Cottbus
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 3
* of the License, or any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

package de.grogra.blocks.xFrogFileParser;

import de.grogra.imp3d.shading.Phong;
import de.grogra.math.Graytone;
import de.grogra.math.RGBColor;

public class GFXColor extends Expr {
	
	public GFXColor(Expr a, Expr b, Expr c, Expr d, Expr e) {
		this.a = a;
		this.b = b;
		this.c = c;
		this.d = d;
		this.e = e; // ist der falsche alpha wert - dieser wird nicht gesetzt!
		color = toXL();
		phong = toGraph();
		if (debug)
			System.out.println(getClass().getSimpleName() + " :: " + a);
	}

	private String toXL() {
		return
		"setMaterial(new Phong().(setAmbient(new RGBColor(" +
		((FLOAT) a.a.a).getValue() +", "+ ((FLOAT) a.a.b).getValue() +", "+ ((FLOAT) a.a.c).getValue() +
		")), setDiffuse(new RGBColor(" +
		((FLOAT) b.a.a).getValue() +", "+ ((FLOAT) b.a.b).getValue() +", "+ ((FLOAT) b.a.c).getValue() + 
		")), setSpecular(new RGBColor(" +
		((FLOAT) c.a.a).getValue() +", "+ ((FLOAT) c.a.b).getValue() +", "+ ((FLOAT) c.a.c).getValue() +
		")), setEmissive(new RGBColor(" +
		((FLOAT) d.a.a).getValue() +", "+ ((FLOAT) d.a.b).getValue() +", "+ ((FLOAT) d.a.c).getValue() +		
		")), setTransparency(new Graytone(" + alpha +")))), ";
	}

	private Phong toGraph() {
		Phong ph = new Phong();
		ph.setAmbient(new RGBColor(((FLOAT) a.a.a).getValue(), ((FLOAT) a.a.b).getValue(), ((FLOAT) a.a.c).getValue()));
		ph.setDiffuse(new RGBColor(((FLOAT) b.a.a).getValue(), ((FLOAT) b.a.b).getValue(), ((FLOAT) b.a.c).getValue()));
		ph.setSpecular(new RGBColor(((FLOAT) c.a.a).getValue(), ((FLOAT) c.a.b).getValue(), ((FLOAT) c.a.c).getValue()));
		ph.setEmissive(new RGBColor(((FLOAT) d.a.a).getValue(), ((FLOAT) d.a.b).getValue(), ((FLOAT) d.a.c).getValue()));
		ph.setTransparency(new Graytone(alpha));
		return ph;
	}	
	
	@Override
	public String toString() {
		if (debugS)
			System.out.println("TS  " + getClass().getSimpleName());
		return "GFXColor {\n" + a.toString() + "\n" + b.toString() + "\n"
				+ c.toString() + "\n" + d.toString() + "\n" + e.toString()
				+ "\n" + "}\n";
	}

}
