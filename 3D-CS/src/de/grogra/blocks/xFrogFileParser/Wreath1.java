/*
* Copyright (C) 2002 - 2006 Lehrstuhl Grafische Systeme, BTU Cottbus
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 3
* of the License, or any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

package de.grogra.blocks.xFrogFileParser;

import javax.vecmath.Point2f;

import de.grogra.blocks.Hydra;
import de.grogra.graph.impl.Node;
import de.grogra.math.BSplineCurve;
import de.grogra.math.Circle;
import de.grogra.math.SplineFunction;

public class Wreath1 extends Expr {

	private final String wreathImport = "import de.grogra.blocks.Wreath;";

	private String name = "";

	public Wreath1(Expr a, Expr b) {
		this.a = a;
		this.b = b;

		if (aktKeyFrame == 0) {
			name = new String(aktStructName);
			blocks.put(name, toXL());
			blocksGraphNodes.put(name, toGraph());
			if (!imports.contains(wreathImport))
				imports.add(wreathImport);
			use_GFXColor = false;
			aktTexture = null;
		}
		if (debug)
			System.out.println(getClass().getSimpleName() + " :: " + a);
	}

	private String toXL() {
		Hydra obj = new Hydra();
		String ss = "";

		ss += "setName(" + name + "), ";

		// anz Segmente
		if (!((FLOAT) (a.a.a.a)).equalsF5(obj.getNumber()))
			ss += "setNumber(" + a.a.a.a + "), ";

		// radius
		if (!((FLOAT) (b.a.a.a)).equalsF5(obj.getRadius()))
			ss += "setRadius(" + b.a.a.a + "), ";
		
		ss += "setSlope("+toSplineFunctionString()+")), ";		
		
		// farbe setzen
		if (use_GFXColor) ss += color;
		
		// letzte komma entfernen
		if (ss.lastIndexOf(',') > 0) {
			ss = ss.substring(0, ss.length() - 2);
		}
		return "Hydra().(" + ss + ")";
	}

	private Node toGraph() {
		Hydra obj = new Hydra(false);
		obj.setName(name);
		obj.setNumber(((FLOAT) a.a.a.a).getValue());
		BSplineCurve trajectory = new Circle();
		((Circle)trajectory).setRadius(((FLOAT) b.a.a.a).getValue());
		obj.setTrajectory(trajectory);
		obj.setSlopeFunction(toSplineFunction());
		
		if (phong!=null && aktTexture!=null) phong.setDiffuse(aktTexture);
		if (use_GFXColor) obj.setMaterial(phong);
		return obj;
	}

	private SplineFunction toSplineFunction()
	{
		float[] xdata = {0, 0.5f, 1};
		float[] ydata = {0.5f, 0.5f, 0.5f};
		Point2f[] data = new Point2f[xdata.length];
		for (int i = 0; i < xdata.length; i++)
		{
			data[i] = new Point2f (xdata[i], ydata[i]);
		}
		return new SplineFunction(data, SplineFunction.HERMITE);
	}	
	
	private String toSplineFunctionString()
	{
		float[] xdata = {0, 0.5f, 1};
		float[] ydata = {0.5f, 0.5f, 0.5f};
		String dataString = "new SplineFunction(new Point2f[] {";
		for (int i = 0; i < xdata.length; i++)
		{
			dataString += "new Point2f("+xdata[i]+", "+ydata[i]+")";
			if (i+1<xdata.length) {
				dataString += ", ";
			}
		}
		return dataString+"})";
	}	
	
	@Override
	public String toString() {
		return "Wreath {\n" + a.toString() + "\n" + b.toString() + "\n" + "}\n";
	}

}
