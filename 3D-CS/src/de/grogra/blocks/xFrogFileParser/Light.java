/*
* Copyright (C) 2002 - 2006 Lehrstuhl Grafische Systeme, BTU Cottbus
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 3
* of the License, or any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

package de.grogra.blocks.xFrogFileParser;

import de.grogra.graph.impl.Node;
import de.grogra.imp3d.objects.LightNode;
import de.grogra.imp3d.objects.PointLight;

public class Light extends Expr {

	private String lightName = "";

	private final String l1Import = "import de.grogra.imp3d.objects.LightNode;";
	private final String l2Import = "import de.grogra.imp3d.objects.PointLight;";

	public Light(Expr a, Expr b, Expr c, Expr d) {
		this.a = a;
		this.b = b;
		this.c = c;
		this.d = d;
		if (aktKeyFrame == 0) {
			lightsGraphNodes.put(a.toString(), toGraph());
			lightName = "light" + lightsXL.size();
			lightsXL.put(lightName, toXL());
			if (!imports.contains(l1Import))
				imports.add(l1Import);
			if (!imports.contains(l2Import))
				imports.add(l2Import);
		}
		if (debug)
			System.out.println(getClass().getSimpleName() + " :: " + a);
	}

	private String toXL() {
		String ss = "\tLightNode " + lightName + " = new LightNode();\n";
		ss += "\t" + lightName + ".setTransform(" + ((FLOAT) d.a.a).getValue()
				+ ", " + ((FLOAT) d.a.b).getValue() + ", "
				+ ((FLOAT) d.a.c).getValue() + ");\n";
		ss += "\tPointLight pp" + lightsXL.size() + " = new PointLight();\n";
		ss += "\tpp" + lightsXL.size() + ".getColor().set("
				+ (float) (((FLOAT) c.a.a).getValue() / 256.0) + ", "
				+ (float) (((FLOAT) c.a.b).getValue() / 256.0) + ", "
				+ (float) (((FLOAT) c.a.c).getValue() / 256.0) + ");\n";
		ss += "\t" + lightName + ".setLight(pp" + lightsXL.size() + ");\n";
		return ss;
	}

	private Node toGraph() {
		LightNode cc = new LightNode();
		cc.setTransform(((FLOAT) d.a.a).getValue(), ((FLOAT) d.a.b).getValue(),
				((FLOAT) d.a.c).getValue());
		PointLight pp = new PointLight();
		pp.getColor().set((float) (((FLOAT) c.a.a).getValue() / 256.0),
				(float) (((FLOAT) c.a.b).getValue() / 256.0),
				(float) (((FLOAT) c.a.c).getValue() / 256.0));
		cc.setLight(pp);
		return cc;
	}	
	
	@Override
	public String toString() {
		if (debugS)
			System.out.println("TS  " + getClass().getSimpleName());
		return "Light {\n" + a.toString() + "\n" + b.toString() + "\n"
				+ c.toString() + "\n" + d.toString() + "\n" + "}\n";
	}

}
