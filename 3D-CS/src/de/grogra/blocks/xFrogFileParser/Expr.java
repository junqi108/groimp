/*
* Copyright (C) 2002 - 2006 Lehrstuhl Grafische Systeme, BTU Cottbus
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 3
* of the License, or any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

package de.grogra.blocks.xFrogFileParser;

import java.io.File;
import java.util.Hashtable;
import java.util.TreeSet;
import java.util.Vector;

import javax.vecmath.Point3f;
import javax.vecmath.Tuple3f;

import de.grogra.imp3d.objects.ShadedNull;
import de.grogra.imp3d.shading.Phong;
import de.grogra.math.ChannelMap;
import de.grogra.pf.registry.Registry;

public abstract class Expr {

	protected Expr a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t,	u, v, w, x;

	protected static final boolean debug = false; // ist ueberall definiert (nicht in: float, floattriple)
	protected static final boolean debugS = false; // ist nicht ueberall definiert - debug in toString

	protected static int aktKeyFrame;
	protected static Transform aktTransform;
	protected static Transform oldTransform;
	protected static ShadedNull aktPrimitive;
	protected static TreeSet transformMatrizen;
	protected static TreeSet trajectory;
	protected static TreeSet imports;
	protected static String aktStructName;
	protected static Hashtable blocks;
	protected static Hashtable children;
	protected static Hashtable ribs;
	protected static Hashtable branches;
	protected static Hashtable blocksGraphNodes;
	protected static Hashtable lightsGraphNodes;
	protected static float alpha;
	protected static String sky;
	protected static String color;
	protected static Phong phong;
	protected static boolean use_GFXColor;
	protected static ChannelMap aktTexture;
	protected static String aktTextureName;
	protected static TreeSet textureImports;
	protected static Vector textures;
	protected static Tuple3f camera;
	protected static Hashtable lightsXL;
	protected static File xfrogDir;
	protected static Registry registry;
	
	public static void init() {
		aktKeyFrame = 0;
		aktTransform = null;
		aktPrimitive = null;
		transformMatrizen = new TreeSet();
		trajectory = new TreeSet();
		imports = new TreeSet();
		aktStructName = "";
		blocks = new Hashtable();
		children = new Hashtable();
		ribs = new Hashtable();
		branches = new Hashtable();
		blocksGraphNodes = new Hashtable();
		lightsGraphNodes = new Hashtable();
		alpha = 0;
		sky = "";
		color = "";
		phong = null;
		use_GFXColor = false;
		aktTexture = null;
		aktTextureName = "";
		textureImports = new TreeSet();
		textures = new Vector();
		camera = new Point3f(0,0,0);
		lightsXL = new Hashtable();
		xfrogDir =null;
		registry = null;		
	}
	
	public static void setSrcReg(File dir, Registry reg) {
 		xfrogDir = dir;
 		registry = reg;
	}
		
	public abstract String toString();
}
