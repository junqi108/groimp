/*
* Copyright (C) 2002 - 2006 Lehrstuhl Grafische Systeme, BTU Cottbus
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 3
* of the License, or any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

package de.grogra.blocks.xFrogFileParser;

import java.io.File;
import java.io.IOException;

import de.grogra.imp.io.ImageReader;
import de.grogra.imp.objects.ImageAdapter;
import de.grogra.imp3d.shading.ImageMap;
import de.grogra.math.ChannelMap;
import de.grogra.pf.ui.Workbench;

public class Texture_File extends Expr {

	public Texture_File(Expr a) {
		this.a = a;
		if (debug)
			System.out.println(getClass().getSimpleName() + " :: " + a);
		if (aktKeyFrame == 0) {
			aktTexture = makeTexture();
			if (aktTexture!=null) {
				aktTextureName = "texture" + textures.size();
				textures.add(aktTexture);
				textureImports.add("const MaterialRef leafmat = material ("+aktTextureName+");");
			}
		}
	}

	private ChannelMap makeTexture ()
	{		
		if (registry!=null) {
			File file = new File(xfrogDir, a.toString().substring(1, a.toString().length()-1));
			ChannelMap cm = new ImageMap();
			if (file.exists()) {
				ImageAdapter img = null;
				try {
					img = (ImageAdapter) ImageReader.getFactory(registry).addFromURL(registry, file.toURI().toURL(), null, Workbench.current());
				} catch (IOException ex) {
					System.err.println(getClass().getSimpleName()+"  "+ex);
				}
				((ImageMap)cm).setImageAdapter(img);
				return cm;
			} else {
				if (!a.toString().substring(a.toString().length()-8, a.toString().length()-1).equals("Default")) {					
					System.err.println(getClass().getSimpleName()+"  Can't find texture file: "+file);
				}
			}
		}
		return null;
	}

	@Override
	public String toString() {
		if (debugS)
			System.out.println("TS  " + getClass().getSimpleName());
		return "Texture_File " + a.toString();
	}

}
