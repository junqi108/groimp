/*
* Copyright (C) 2002 - 2006 Lehrstuhl Grafische Systeme, BTU Cottbus
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 3
* of the License, or any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

package de.grogra.blocks.arrangeBlock;

import de.grogra.persistence.SCOType;

public class BetaProbability extends ShareableProbabilityArrangeBase implements ProbabilityArrangeMethod
{
	//enh:sco SCOType
	
	float shape_a = 0.5f;
	// enh:field getter setter min=0 max=5

	float shape_b = 1;
	// enh:field getter setter min=0 max=5
	
	public void calculate() {
		for (int i=0; i<probabilityValues.length; i++) {
			probabilityValues[i] = (int)Math.abs(Math.round(source.beta(shape_a, shape_b)));			
		}
	}
	
	//enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final Type $TYPE;

	public static final Type.Field shape_a$FIELD;
	public static final Type.Field shape_b$FIELD;

	public static class Type extends SCOType
	{
		public Type (Class c, de.grogra.persistence.SCOType supertype)
		{
			super (c, supertype);
		}

		public Type (BetaProbability representative, de.grogra.persistence.SCOType supertype)
		{
			super (representative, supertype);
		}

		Type (Class c)
		{
			super (c, SCOType.$TYPE);
		}

		private static final int SUPER_FIELD_COUNT = SCOType.FIELD_COUNT;
		protected static final int FIELD_COUNT = SCOType.FIELD_COUNT + 2;

		static Field _addManagedField (Type t, String name, int modifiers, de.grogra.reflect.Type type, de.grogra.reflect.Type componentType, int id)
		{
			return t.addManagedField (name, modifiers, type, componentType, id);
		}

		@Override
		protected void setFloat (Object o, int id, float value)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 0:
					((BetaProbability) o).shape_a = (float) value;
					return;
				case Type.SUPER_FIELD_COUNT + 1:
					((BetaProbability) o).shape_b = (float) value;
					return;
			}
			super.setFloat (o, id, value);
		}

		@Override
		protected float getFloat (Object o, int id)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 0:
					return ((BetaProbability) o).getShape_a ();
				case Type.SUPER_FIELD_COUNT + 1:
					return ((BetaProbability) o).getShape_b ();
			}
			return super.getFloat (o, id);
		}

		@Override
		public Object newInstance ()
		{
			return new BetaProbability ();
		}

	}

	public de.grogra.persistence.ManageableType getManageableType ()
	{
		return $TYPE;
	}


	static
	{
		$TYPE = new Type (BetaProbability.class);
		shape_a$FIELD = Type._addManagedField ($TYPE, "shape_a", 0 | Type.Field.SCO, de.grogra.reflect.Type.FLOAT, null, Type.SUPER_FIELD_COUNT + 0);
		shape_b$FIELD = Type._addManagedField ($TYPE, "shape_b", 0 | Type.Field.SCO, de.grogra.reflect.Type.FLOAT, null, Type.SUPER_FIELD_COUNT + 1);
		shape_a$FIELD.setMinValue (new Float (0));
		shape_a$FIELD.setMaxValue (new Float (5));
		shape_b$FIELD.setMinValue (new Float (0));
		shape_b$FIELD.setMaxValue (new Float (5));
		$TYPE.validate ();
	}

	public float getShape_a ()
	{
		return shape_a;
	}

	public void setShape_a (float value)
	{
		this.shape_a = (float) value;
	}

	public float getShape_b ()
	{
		return shape_b;
	}

	public void setShape_b (float value)
	{
		this.shape_b = (float) value;
	}

//enh:end

}
