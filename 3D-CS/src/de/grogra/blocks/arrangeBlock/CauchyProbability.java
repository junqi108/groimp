/*
* Copyright (C) 2002 - 2006 Lehrstuhl Grafische Systeme, BTU Cottbus
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 3
* of the License, or any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

package de.grogra.blocks.arrangeBlock;

import de.grogra.persistence.SCOType;

public class CauchyProbability extends ShareableProbabilityArrangeBase implements ProbabilityArrangeMethod
{
	//enh:sco SCOType
	
	float median = 0.5f;
	// enh:field getter setter min=0 max=5

	float scale = 4;
	// enh:field getter setter min=0 max=5
	
	public void calculate() {
		for (int i=0; i<probabilityValues.length; i++) {
			probabilityValues[i] = (int)Math.abs(Math.round(source.cauchy(median, scale)));
		}
	}
	
	//enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final Type $TYPE;

	public static final Type.Field median$FIELD;
	public static final Type.Field scale$FIELD;

	public static class Type extends SCOType
	{
		public Type (Class c, de.grogra.persistence.SCOType supertype)
		{
			super (c, supertype);
		}

		public Type (CauchyProbability representative, de.grogra.persistence.SCOType supertype)
		{
			super (representative, supertype);
		}

		Type (Class c)
		{
			super (c, SCOType.$TYPE);
		}

		private static final int SUPER_FIELD_COUNT = SCOType.FIELD_COUNT;
		protected static final int FIELD_COUNT = SCOType.FIELD_COUNT + 2;

		static Field _addManagedField (Type t, String name, int modifiers, de.grogra.reflect.Type type, de.grogra.reflect.Type componentType, int id)
		{
			return t.addManagedField (name, modifiers, type, componentType, id);
		}

		@Override
		protected void setFloat (Object o, int id, float value)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 0:
					((CauchyProbability) o).median = (float) value;
					return;
				case Type.SUPER_FIELD_COUNT + 1:
					((CauchyProbability) o).scale = (float) value;
					return;
			}
			super.setFloat (o, id, value);
		}

		@Override
		protected float getFloat (Object o, int id)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 0:
					return ((CauchyProbability) o).getMedian ();
				case Type.SUPER_FIELD_COUNT + 1:
					return ((CauchyProbability) o).getScale ();
			}
			return super.getFloat (o, id);
		}

		@Override
		public Object newInstance ()
		{
			return new CauchyProbability ();
		}

	}

	public de.grogra.persistence.ManageableType getManageableType ()
	{
		return $TYPE;
	}


	static
	{
		$TYPE = new Type (CauchyProbability.class);
		median$FIELD = Type._addManagedField ($TYPE, "median", 0 | Type.Field.SCO, de.grogra.reflect.Type.FLOAT, null, Type.SUPER_FIELD_COUNT + 0);
		scale$FIELD = Type._addManagedField ($TYPE, "scale", 0 | Type.Field.SCO, de.grogra.reflect.Type.FLOAT, null, Type.SUPER_FIELD_COUNT + 1);
		median$FIELD.setMinValue (new Float (0));
		median$FIELD.setMaxValue (new Float (5));
		scale$FIELD.setMinValue (new Float (0));
		scale$FIELD.setMaxValue (new Float (5));
		$TYPE.validate ();
	}

	public float getMedian ()
	{
		return median;
	}

	public void setMedian (float value)
	{
		this.median = (float) value;
	}

	public float getScale ()
	{
		return scale;
	}

	public void setScale (float value)
	{
		this.scale = (float) value;
	}

//enh:end

}
