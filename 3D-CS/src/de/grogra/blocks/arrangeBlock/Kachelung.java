/*
 * Copyright (C) 2002 - 2006 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.blocks.arrangeBlock;

import raskob.geometry.Point;
import raskob.geometry.PointArrayList;
import de.grogra.persistence.PersistenceField;
import de.grogra.persistence.SCOType;
import de.grogra.persistence.Transaction;
import de.grogra.rgg.Library;

public final class Kachelung extends ShareableAdditionalArrangeBase implements AdditionalArrangeMethod
{
	//enh:sco SCOType
	
	int number = 5;
	// enh:field getter setter min=1 max=MAX_NUMBER
	
	float minRadius = 0.5f;
	// enh:field quantity=LENGTH getter setter min=0 max=10
	
	int level = 1;
	// enh:field getter setter min=1 max=15

	boolean checkBorders = false;
	// enh:field getter setter 
	
	private boolean changed = true;

	public void calculate() {
		if (changed) {
			changed = false;			
			//level --> (level+1)*(level+1)=anzSegmente
			int anzSegmente = (level+1)*(level+1);
			double swX = maxX / (level+1);
			double swY = maxY / (level+1);
			
			PointArrayList kachel = new PointArrayList();
			float aktMinRadius = minRadius;
			if (!checkBorders) {
				// kachel berechnen, ohne kantenbeachtung
				int j = 0, k = 0;			
				while (j < number) {
					Point p = new Point(Library.random(0, (float) swX), 
										Library.random(0, (float) swY));
					if (!kachel.contains(p) && kachel.isRadiusFree(p, aktMinRadius)) {
						kachel.add(p);
						j++;
						k = 0;
					}
					// wenn maxF versuche lang kein Punkt platziert werden konnte
					if (k > maxF) { 
						if ((aktMinRadius- aktMinRadius * prozent) > 0) {
							// den mindestradius um prozent verringern
							aktMinRadius -= aktMinRadius * prozent;
						}
						k = 0;
					}
					k++;
				}
			} else {
				// kachel berechnen, mit kantenbeachtung
				int j = 0, k = 0;			
				while (j < number) {
					Point p = new Point(swX + Library.random(0, (float) swX), 
										swY + Library.random(0, (float) swY));
					if (!kachel.contains(p) && kachel.isRadiusFree(p, aktMinRadius)) {
						addPoint(swX, swY, kachel, p);
						j++;
						k = 0;					
					}
					// wenn maxF versuche lang kein Punkt platziert werden konnte
					if (k > maxF) { 
						if ((aktMinRadius- aktMinRadius * prozent) > 0) {
							// den mindestradius um prozent verringern
							aktMinRadius -= aktMinRadius * prozent;
						}
						k = 0;
					}
					k++;
				}
				// nur die mittelst kachel betrachten
				kachel = trimmKachel(swX, swY, kachel);
			}
			
			pointList = new PointArrayList();
			for (int i=0; i<anzSegmente; i++) {
				// feld mit mit den punkten belegen			
				float y1 = i / (level+1);
				float x1 = i % (level+1);			
				x1 *= swX;
				y1 *= swY;
				// punkte der kachel auf das segment verteilen
				for (int m = 0; m < kachel.size(); m++) {
					Point p = kachel.get(m);
					if (checkDensityField(x1 + p.getX(), y1 + p.getY())) {
						pointList.add(new Point(x1 + p.getX(), y1 + p.getY()));
					}
				}
			}
		}
	}
	
	public void fieldModified (PersistenceField field, int[] indices, Transaction t)
	{
		super.fieldModified (field, indices, t);
		if (!Transaction.isApplying(t))
		{
			if (field.overlaps (indices, number$FIELD, null) || 
				field.overlaps (indices, minRadius$FIELD, null) ||
				field.overlaps (indices, level$FIELD, null) ||
				field.overlaps (indices, checkBorders$FIELD, null))
			{
				changed = true;
			}
		}
	}	
	
	private PointArrayList trimmKachel(double swX, double swY, PointArrayList kachel) {
		PointArrayList newList = new PointArrayList();
		for (int i = 0; i < kachel.size(); i++) {
			Point p = kachel.get(i);
			if (p.getX()<=swX && p.getY()<=swY) {
				newList.add(p);
			}
		}
		return newList;
	}

	private void addPoint(double swX, double swY, PointArrayList kachel, Point p) {		
		kachel.add(new Point(p.getX() - swX, p.getY() - swY));
		kachel.add(new Point(p.getX(),       p.getY() - swY));		
		kachel.add(new Point(p.getX() + swX, p.getY() - swY));
		
		kachel.add(new Point(p.getX() - swX, p.getY()));
		kachel.add(p);		
		kachel.add(new Point(p.getX() + swX, p.getY()));

		kachel.add(new Point(p.getX() - swX, p.getY() + swY));
		kachel.add(new Point(p.getX(),       p.getY() + swY));
		kachel.add(new Point(p.getX() + swX, p.getY() + swY));
	}
	
	
	//enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final Type $TYPE;

	public static final Type.Field number$FIELD;
	public static final Type.Field minRadius$FIELD;
	public static final Type.Field level$FIELD;
	public static final Type.Field checkBorders$FIELD;

	public static class Type extends SCOType
	{
		public Type (Class c, de.grogra.persistence.SCOType supertype)
		{
			super (c, supertype);
		}

		public Type (Kachelung representative, de.grogra.persistence.SCOType supertype)
		{
			super (representative, supertype);
		}

		Type (Class c)
		{
			super (c, SCOType.$TYPE);
		}

		private static final int SUPER_FIELD_COUNT = SCOType.FIELD_COUNT;
		protected static final int FIELD_COUNT = SCOType.FIELD_COUNT + 4;

		static Field _addManagedField (Type t, String name, int modifiers, de.grogra.reflect.Type type, de.grogra.reflect.Type componentType, int id)
		{
			return t.addManagedField (name, modifiers, type, componentType, id);
		}

		@Override
		protected void setBoolean (Object o, int id, boolean value)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 3:
					((Kachelung) o).checkBorders = (boolean) value;
					return;
			}
			super.setBoolean (o, id, value);
		}

		@Override
		protected boolean getBoolean (Object o, int id)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 3:
					return ((Kachelung) o).isCheckBorders ();
			}
			return super.getBoolean (o, id);
		}

		@Override
		protected void setInt (Object o, int id, int value)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 0:
					((Kachelung) o).number = (int) value;
					return;
				case Type.SUPER_FIELD_COUNT + 2:
					((Kachelung) o).level = (int) value;
					return;
			}
			super.setInt (o, id, value);
		}

		@Override
		protected int getInt (Object o, int id)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 0:
					return ((Kachelung) o).getNumber ();
				case Type.SUPER_FIELD_COUNT + 2:
					return ((Kachelung) o).getLevel ();
			}
			return super.getInt (o, id);
		}

		@Override
		protected void setFloat (Object o, int id, float value)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 1:
					((Kachelung) o).minRadius = (float) value;
					return;
			}
			super.setFloat (o, id, value);
		}

		@Override
		protected float getFloat (Object o, int id)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 1:
					return ((Kachelung) o).getMinRadius ();
			}
			return super.getFloat (o, id);
		}

		@Override
		public Object newInstance ()
		{
			return new Kachelung ();
		}

	}

	public de.grogra.persistence.ManageableType getManageableType ()
	{
		return $TYPE;
	}


	static
	{
		$TYPE = new Type (Kachelung.class);
		number$FIELD = Type._addManagedField ($TYPE, "number", 0 | Type.Field.SCO, de.grogra.reflect.Type.INT, null, Type.SUPER_FIELD_COUNT + 0);
		minRadius$FIELD = Type._addManagedField ($TYPE, "minRadius", 0 | Type.Field.SCO, de.grogra.reflect.Type.FLOAT, null, Type.SUPER_FIELD_COUNT + 1);
		level$FIELD = Type._addManagedField ($TYPE, "level", 0 | Type.Field.SCO, de.grogra.reflect.Type.INT, null, Type.SUPER_FIELD_COUNT + 2);
		checkBorders$FIELD = Type._addManagedField ($TYPE, "checkBorders", 0 | Type.Field.SCO, de.grogra.reflect.Type.BOOLEAN, null, Type.SUPER_FIELD_COUNT + 3);
		number$FIELD.setMinValue (new Integer (1));
		number$FIELD.setMaxValue (new Integer (MAX_NUMBER));
		minRadius$FIELD.setQuantity (de.grogra.util.Quantity.LENGTH);
		minRadius$FIELD.setMinValue (new Float (0));
		minRadius$FIELD.setMaxValue (new Float (10));
		level$FIELD.setMinValue (new Integer (1));
		level$FIELD.setMaxValue (new Integer (15));
		$TYPE.validate ();
	}

	public boolean isCheckBorders ()
	{
		return checkBorders;
	}

	public void setCheckBorders (boolean value)
	{
		this.checkBorders = (boolean) value;
	}

	public int getNumber ()
	{
		return number;
	}

	public void setNumber (int value)
	{
		this.number = (int) value;
	}

	public int getLevel ()
	{
		return level;
	}

	public void setLevel (int value)
	{
		this.level = (int) value;
	}

	public float getMinRadius ()
	{
		return minRadius;
	}

	public void setMinRadius (float value)
	{
		this.minRadius = (float) value;
	}

//enh:end
	
}
