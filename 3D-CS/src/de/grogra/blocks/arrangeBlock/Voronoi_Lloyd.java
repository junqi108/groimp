/*
 * Copyright (C) 2002 - 2006 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.blocks.arrangeBlock;

import raskob.geometry.Point;
import raskob.geometry.PointArrayList;
import raskob.geometry.Polygon;
import raskob.geometry.Voronoi;
import de.grogra.persistence.PersistenceField;
import de.grogra.persistence.SCOType;
import de.grogra.persistence.Transaction;
import de.grogra.rgg.Library;

public class Voronoi_Lloyd extends ShareableAdditionalArrangeBase implements AdditionalArrangeMethod
{
	//enh:sco SCOType
	
	int number = ((superNumber!=-1)? superNumber: 25);
	// enh:field getter setter min=1 max=MAX_NUMBER
	
	int anzIterationen = 40;
	// enh:field getter setter min=2 max=150	

	private boolean changed = true;
	
	public void calculate() {
		if (changed) {
			changed = false;
			double[] pol1D = { 0, 0, 0, (maxY+0.5), (maxX+0.5), (maxY+0.5), (maxX+0.5), 0};
			final Polygon pol = new Polygon(null);
			final Polygon pol1 = new Polygon(null, pol1D);
			pol.addHolePolygon(pol1);
			final Voronoi vd = new Voronoi(pol);
	
			while (vd.size() < number) {
				vd.insertSite(new Point(Library.random(0, maxX), Library.random(0, maxY)));
			}
			vd.lloydIterate(anzIterationen);
	
			pointList = new PointArrayList();
			for (int i = 0; i < vd.size(); i++) {
				Point p = vd.get(i);
				if (checkDensityField(p.getX(), p.getY())) {
					pointList.add(p);
				}
			}
		}
	}
	
	public void fieldModified (PersistenceField field, int[] indices, Transaction t)
	{
		super.fieldModified (field, indices, t);
		if (!Transaction.isApplying(t))
		{
			if (field.overlaps (indices, number$FIELD, null) || 
				field.overlaps (indices, anzIterationen$FIELD, null))
			{
				changed = true;
			}
		}
	}	
	
	//enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final Type $TYPE;

	public static final Type.Field number$FIELD;
	public static final Type.Field anzIterationen$FIELD;

	public static class Type extends SCOType
	{
		public Type (Class c, de.grogra.persistence.SCOType supertype)
		{
			super (c, supertype);
		}

		public Type (Voronoi_Lloyd representative, de.grogra.persistence.SCOType supertype)
		{
			super (representative, supertype);
		}

		Type (Class c)
		{
			super (c, SCOType.$TYPE);
		}

		private static final int SUPER_FIELD_COUNT = SCOType.FIELD_COUNT;
		protected static final int FIELD_COUNT = SCOType.FIELD_COUNT + 2;

		static Field _addManagedField (Type t, String name, int modifiers, de.grogra.reflect.Type type, de.grogra.reflect.Type componentType, int id)
		{
			return t.addManagedField (name, modifiers, type, componentType, id);
		}

		@Override
		protected void setInt (Object o, int id, int value)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 0:
					((Voronoi_Lloyd) o).number = (int) value;
					return;
				case Type.SUPER_FIELD_COUNT + 1:
					((Voronoi_Lloyd) o).anzIterationen = (int) value;
					return;
			}
			super.setInt (o, id, value);
		}

		@Override
		protected int getInt (Object o, int id)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 0:
					return ((Voronoi_Lloyd) o).getNumber ();
				case Type.SUPER_FIELD_COUNT + 1:
					return ((Voronoi_Lloyd) o).getAnzIterationen ();
			}
			return super.getInt (o, id);
		}

		@Override
		public Object newInstance ()
		{
			return new Voronoi_Lloyd ();
		}

	}

	public de.grogra.persistence.ManageableType getManageableType ()
	{
		return $TYPE;
	}


	static
	{
		$TYPE = new Type (Voronoi_Lloyd.class);
		number$FIELD = Type._addManagedField ($TYPE, "number", 0 | Type.Field.SCO, de.grogra.reflect.Type.INT, null, Type.SUPER_FIELD_COUNT + 0);
		anzIterationen$FIELD = Type._addManagedField ($TYPE, "anzIterationen", 0 | Type.Field.SCO, de.grogra.reflect.Type.INT, null, Type.SUPER_FIELD_COUNT + 1);
		number$FIELD.setMinValue (new Integer (1));
		number$FIELD.setMaxValue (new Integer (MAX_NUMBER));
		anzIterationen$FIELD.setMinValue (new Integer (2));
		anzIterationen$FIELD.setMaxValue (new Integer (150));
		$TYPE.validate ();
	}

	public int getNumber ()
	{
		return number;
	}

	public void setNumber (int value)
	{
		this.number = (int) value;
	}

	public int getAnzIterationen ()
	{
		return anzIterationen;
	}

	public void setAnzIterationen (int value)
	{
		this.anzIterationen = (int) value;
	}

//enh:end

	

}
