/*
 * Copyright (C) 2002 - 2006 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.blocks.arrangeBlock;

import raskob.geometry.Point;
import raskob.geometry.PointArrayList;
import de.grogra.rgg.Library;

public class RandomDither extends ArrangeBase {

	public RandomDither(float maxX, float maxY, 
			float threshold, float maxThreshold, float[][] field) {			
		final int FIELD_LENGTH = field.length;
		threshold *= maxThreshold;

		for (int i = 0; i < FIELD_LENGTH - 1; i++) {
			for (int j = 1; j < FIELD_LENGTH - 1; j++) {
				if (field[j][i] < Library.random(0, threshold)) {
					field[j][i] = 0;
				} else {
					field[j][i] = maxThreshold;
				}
			}
		}

		// berechneten Punkte uebertragen
		pointList = new PointArrayList();
		for (int i = 0; i < FIELD_LENGTH; i++) {
			for (int j = 0; j < FIELD_LENGTH; j++) {
				if (field[j][i] == maxThreshold) {
					pointList.add(new Point(i*(maxX-1)/FIELD_LENGTH, j*(maxY-1)/FIELD_LENGTH));
				}
			}
		}
		
		pointListToArrays();
	}

}