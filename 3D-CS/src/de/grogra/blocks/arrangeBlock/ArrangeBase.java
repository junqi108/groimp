
/*
 * Copyright (C) 2002 - 2006 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.blocks.arrangeBlock;

import raskob.geometry.Point;
import raskob.geometry.PointArrayList;

public abstract class ArrangeBase implements ArrangeMethod {

	protected float[] xx = null;
	protected float[] yy = null;	
	
	PointArrayList pointList = null;
	
	public final static int maxF = 15;
	public final static float prozent = 0.025f; // 2.5%
	
	public float[] getXx() 
	{
		return xx;
	}

	public float[] getYy() 
	{
		return yy;
	}

	public PointArrayList getPointList ()
	{
		return pointList;
	}
	
	protected void pointListToArrays() 
	{
		xx = new float[pointList.size()];
		yy = new float[pointList.size()];
		for (int ii = 0; ii < pointList.size(); ii++) {
			Point p = pointList.get(ii);
			xx[ii] = (float) p.getX();
			yy[ii] = (float) p.getY();
		}		
	}
	
}
