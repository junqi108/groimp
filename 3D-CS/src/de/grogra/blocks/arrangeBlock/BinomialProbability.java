/*
* Copyright (C) 2002 - 2006 Lehrstuhl Grafische Systeme, BTU Cottbus
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 3
* of the License, or any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

package de.grogra.blocks.arrangeBlock;

import de.grogra.persistence.SCOType;

public class BinomialProbability extends ShareableProbabilityArrangeBase implements ProbabilityArrangeMethod
{
	//enh:sco SCOType
	
	float prob = 0.5f;
	// enh:field getter setter min=0 max=5

	int trials = 20;
	// enh:field getter setter min=0 max=50
	
	public void calculate() {
		for (int i=0; i<probabilityValues.length; i++) {
			probabilityValues[i] = (int)Math.abs(Math.round(source.binomial(prob, trials)));
		}
	}
	
	//enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final Type $TYPE;

	public static final Type.Field prob$FIELD;
	public static final Type.Field trials$FIELD;

	public static class Type extends SCOType
	{
		public Type (Class c, de.grogra.persistence.SCOType supertype)
		{
			super (c, supertype);
		}

		public Type (BinomialProbability representative, de.grogra.persistence.SCOType supertype)
		{
			super (representative, supertype);
		}

		Type (Class c)
		{
			super (c, SCOType.$TYPE);
		}

		private static final int SUPER_FIELD_COUNT = SCOType.FIELD_COUNT;
		protected static final int FIELD_COUNT = SCOType.FIELD_COUNT + 2;

		static Field _addManagedField (Type t, String name, int modifiers, de.grogra.reflect.Type type, de.grogra.reflect.Type componentType, int id)
		{
			return t.addManagedField (name, modifiers, type, componentType, id);
		}

		@Override
		protected void setInt (Object o, int id, int value)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 1:
					((BinomialProbability) o).trials = (int) value;
					return;
			}
			super.setInt (o, id, value);
		}

		@Override
		protected int getInt (Object o, int id)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 1:
					return ((BinomialProbability) o).getTrials ();
			}
			return super.getInt (o, id);
		}

		@Override
		protected void setFloat (Object o, int id, float value)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 0:
					((BinomialProbability) o).prob = (float) value;
					return;
			}
			super.setFloat (o, id, value);
		}

		@Override
		protected float getFloat (Object o, int id)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 0:
					return ((BinomialProbability) o).getProb ();
			}
			return super.getFloat (o, id);
		}

		@Override
		public Object newInstance ()
		{
			return new BinomialProbability ();
		}

	}

	public de.grogra.persistence.ManageableType getManageableType ()
	{
		return $TYPE;
	}


	static
	{
		$TYPE = new Type (BinomialProbability.class);
		prob$FIELD = Type._addManagedField ($TYPE, "prob", 0 | Type.Field.SCO, de.grogra.reflect.Type.FLOAT, null, Type.SUPER_FIELD_COUNT + 0);
		trials$FIELD = Type._addManagedField ($TYPE, "trials", 0 | Type.Field.SCO, de.grogra.reflect.Type.INT, null, Type.SUPER_FIELD_COUNT + 1);
		prob$FIELD.setMinValue (new Float (0));
		prob$FIELD.setMaxValue (new Float (5));
		trials$FIELD.setMinValue (new Integer (0));
		trials$FIELD.setMaxValue (new Integer (50));
		$TYPE.validate ();
	}

	public int getTrials ()
	{
		return trials;
	}

	public void setTrials (int value)
	{
		this.trials = (int) value;
	}

	public float getProb ()
	{
		return prob;
	}

	public void setProb (float value)
	{
		this.prob = (float) value;
	}

//enh:end

}
