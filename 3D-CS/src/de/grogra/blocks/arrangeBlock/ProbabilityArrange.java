
/*
 * Copyright (C) 2002 - 2006 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.blocks.arrangeBlock;

import raskob.geometry.Point;
import raskob.geometry.PointArrayList;
import de.grogra.persistence.SCOType;
import de.grogra.rgg.Library;

public class ProbabilityArrange extends ShareableArrangeBase implements ArrangeMethod 
{
	//enh:sco SCOType
	
	ShareableProbabilityArrangeBase method = new NormalProbability();
	//enh:field getter setter
	
	int level = 1;
	// enh:field getter setter min=0 max=15

	float minRadius = 0.5f;
	// enh:field getter setter min=0 max=5	
	
	boolean hierarchic = false; 
	// enh:field getter setter
	
	private PointArrayList pointList = null;
	
	public void calculate() {
		pointList = new PointArrayList();
		if (hierarchic) {
			calculateHierarchic();
		} else {
			calculateSimple();
		}
		xx = new float[pointList.size()];
		yy = new float[pointList.size()];		
		for (int i = 0; i < pointList.size(); i++) {
			Point p = pointList.get(i);
			xx[i] = (float) p.getX();
			yy[i] = (float) p.getY();
		}
	}
	
	private void calculateSimple() {
		//level --> (level+1)*(level+1)=anzSegmente
		int anzSegmente = (level+1)*(level+1);
		int[] probabilityValues = method.getProbabilityValues(anzSegmente, seed);
		double swX = (maxX-1) / (level+1);
		double swY = (maxY-1) / (level+1);
		
		for (int i=0; i<anzSegmente; i++) {
			// feld mit mit den punkten belegen
			int j = 0, k = 0;
			float y1 = i / (level+1);
			float x1 = i % (level+1);
			x1 *= swX;
			y1 *= swY;
			float aktMinRadius = minRadius;
			while (j < probabilityValues[i]) {
				Point p = new Point(x1 + Library.random(0, (float) swX), 
									y1 + Library.random(0, (float) swY));
				if (!pointList.contains(p) && pointList.isRadiusFree(p, aktMinRadius)) {
					pointList.add(p);
					j++;
					k = 0;					
				}
				// wenn maxF versuche lang kein Punkt platziert werden konnte
				if (k > maxF) { 
					if ((aktMinRadius- aktMinRadius * prozent) > 0) {
						// den mindestradius um prozent verringern
						aktMinRadius -= aktMinRadius * prozent;
					}
					k = 0;
				}
				k++;
			}
		}
	}	

	private void calculateHierarchic() {
		//level --> (level+2)*(level+2)=anzSegmente
		int anzSegmente = (level+2)*(level+2);
		double swX = (maxX-1) / (level+2);
		double swY = (maxY-1) / (level+2);
		
		for (int i=0; i<anzSegmente; i++) {
			calculateInnerSegments((i % (level+2))*swX, swX, (i / (level+2))*swY, swY);				
		}	
	}		
	
	private void calculateInnerSegments(double x0, double maxX, double y0, double maxY) {
		//level --> (level+1)*(level+1)=anzSegmente
		int anzSegmente = (level+1)*(level+1);
		int[] probabilityValues = method.getProbabilityValues(anzSegmente, seed);
		double swX = (maxX-1) / (level+1);
		double swY = (maxY-1) / (level+1);
		
		for (int i=0; i<anzSegmente; i++) {
			// feld mit mit den punkten belegen
			int j = 0, k = 0;
			float y1 = i / (level+1);
			float x1 = i % (level+1);			
			x1 *= swX + x0;
			y1 *= swY + y0;
			float aktMinRadius = minRadius;
			while (j < probabilityValues[i]) {
				Point p = new Point(x1 + Library.random(0, (float) swX), 
									y1 + Library.random(0, (float) swY));
				if (!pointList.contains(p) && pointList.isRadiusFree(p, aktMinRadius)) {
					pointList.add(p);
					j++;
					k = 0;					
				}
				// wenn maxF versuche lang kein Punkt platziert werden konnte
				if (k > maxF) { 
					if ((aktMinRadius- aktMinRadius * prozent) > 0) {
						// den mindestradius um prozent verringern
						aktMinRadius -= aktMinRadius * prozent;
					}
					k = 0;
				}
				k++;
			}
		}
	}
			
	//enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final Type $TYPE;

	public static final Type.Field method$FIELD;
	public static final Type.Field level$FIELD;
	public static final Type.Field minRadius$FIELD;
	public static final Type.Field hierarchic$FIELD;

	public static class Type extends SCOType
	{
		public Type (Class c, de.grogra.persistence.SCOType supertype)
		{
			super (c, supertype);
		}

		public Type (ProbabilityArrange representative, de.grogra.persistence.SCOType supertype)
		{
			super (representative, supertype);
		}

		Type (Class c)
		{
			super (c, SCOType.$TYPE);
		}

		private static final int SUPER_FIELD_COUNT = SCOType.FIELD_COUNT;
		protected static final int FIELD_COUNT = SCOType.FIELD_COUNT + 4;

		static Field _addManagedField (Type t, String name, int modifiers, de.grogra.reflect.Type type, de.grogra.reflect.Type componentType, int id)
		{
			return t.addManagedField (name, modifiers, type, componentType, id);
		}

		@Override
		protected void setBoolean (Object o, int id, boolean value)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 3:
					((ProbabilityArrange) o).hierarchic = (boolean) value;
					return;
			}
			super.setBoolean (o, id, value);
		}

		@Override
		protected boolean getBoolean (Object o, int id)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 3:
					return ((ProbabilityArrange) o).isHierarchic ();
			}
			return super.getBoolean (o, id);
		}

		@Override
		protected void setInt (Object o, int id, int value)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 1:
					((ProbabilityArrange) o).level = (int) value;
					return;
			}
			super.setInt (o, id, value);
		}

		@Override
		protected int getInt (Object o, int id)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 1:
					return ((ProbabilityArrange) o).getLevel ();
			}
			return super.getInt (o, id);
		}

		@Override
		protected void setFloat (Object o, int id, float value)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 2:
					((ProbabilityArrange) o).minRadius = (float) value;
					return;
			}
			super.setFloat (o, id, value);
		}

		@Override
		protected float getFloat (Object o, int id)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 2:
					return ((ProbabilityArrange) o).getMinRadius ();
			}
			return super.getFloat (o, id);
		}

		@Override
		protected void setObject (Object o, int id, Object value)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 0:
					((ProbabilityArrange) o).method = (ShareableProbabilityArrangeBase) value;
					return;
			}
			super.setObject (o, id, value);
		}

		@Override
		protected Object getObject (Object o, int id)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 0:
					return ((ProbabilityArrange) o).getMethod ();
			}
			return super.getObject (o, id);
		}

		@Override
		public Object newInstance ()
		{
			return new ProbabilityArrange ();
		}

	}

	public de.grogra.persistence.ManageableType getManageableType ()
	{
		return $TYPE;
	}


	static
	{
		$TYPE = new Type (ProbabilityArrange.class);
		method$FIELD = Type._addManagedField ($TYPE, "method", 0 | Type.Field.SCO, de.grogra.reflect.ClassAdapter.wrap (ShareableProbabilityArrangeBase.class), null, Type.SUPER_FIELD_COUNT + 0);
		level$FIELD = Type._addManagedField ($TYPE, "level", 0 | Type.Field.SCO, de.grogra.reflect.Type.INT, null, Type.SUPER_FIELD_COUNT + 1);
		minRadius$FIELD = Type._addManagedField ($TYPE, "minRadius", 0 | Type.Field.SCO, de.grogra.reflect.Type.FLOAT, null, Type.SUPER_FIELD_COUNT + 2);
		hierarchic$FIELD = Type._addManagedField ($TYPE, "hierarchic", 0 | Type.Field.SCO, de.grogra.reflect.Type.BOOLEAN, null, Type.SUPER_FIELD_COUNT + 3);
		level$FIELD.setMinValue (new Integer (0));
		level$FIELD.setMaxValue (new Integer (15));
		minRadius$FIELD.setMinValue (new Float (0));
		minRadius$FIELD.setMaxValue (new Float (5));
		$TYPE.validate ();
	}

	public boolean isHierarchic ()
	{
		return hierarchic;
	}

	public void setHierarchic (boolean value)
	{
		this.hierarchic = (boolean) value;
	}

	public int getLevel ()
	{
		return level;
	}

	public void setLevel (int value)
	{
		this.level = (int) value;
	}

	public float getMinRadius ()
	{
		return minRadius;
	}

	public void setMinRadius (float value)
	{
		this.minRadius = (float) value;
	}

	public ShareableProbabilityArrangeBase getMethod ()
	{
		return method;
	}

	public void setMethod (ShareableProbabilityArrangeBase value)
	{
		method$FIELD.setObject (this, value);
	}

//enh:end

}
