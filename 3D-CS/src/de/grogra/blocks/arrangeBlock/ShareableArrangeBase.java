
/*
 * Copyright (C) 2002 - 2006 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.blocks.arrangeBlock;

import de.grogra.persistence.ShareableBase;

public abstract class ShareableArrangeBase extends ShareableBase implements ArrangeMethod {

	protected float maxX = 0;
	protected float maxY = 0;	
	protected float maxXHalbe = 0;
	protected float maxYHalbe = 0;	
	
	public final static int MAX_NUMBER = 500;
	
	protected float[] xx = null;
	protected float[] yy = null;	

	protected float[][] densityField = null;	
	
	protected final static int maxF = 10; 	// Anzahl der Versuche 
	protected final static float prozent = 0.05f; // 5% Verkleinerungsfaktor des Mindestradius
	
	protected long seed = 0;
	
	protected int superNumber = -1;
	
	public float[] getXx() {
		return xx;
	}

	public float[] getYy() {
		return yy;
	}
	
	protected float[][] copyField(float[][] field) {
		int fieldLength = field.length;
		float[][] copy = new float[fieldLength][fieldLength];
		for (int i = 0; i < fieldLength; i++) {
			for (int j = 0; j < fieldLength; j++) {
				copy[j][i] = field[j][i];
			}
		}
		return copy;
	}
	
	public abstract void calculate();
	
	public void setAll(float maxX, float maxY, float[][] densityField, long seed) {
		this.maxX = maxX;
		this.maxY = maxY;
		this.densityField = copyField(densityField);		
		this.seed = seed;

		maxXHalbe = (float)((maxX-1) / 2.0);
		maxYHalbe = (float)((maxY-1) / 2.0);
		calculate();		
	}

	public void setNumber(int number) {
		superNumber = number;
	}
	
}
