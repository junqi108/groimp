/*
 * Copyright (C) 2002 - 2006 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.blocks.arrangeBlock;

import raskob.geometry.Point;
import raskob.geometry.PointArrayList;
import de.grogra.persistence.SCOType;
import de.grogra.rgg.Library;

public class DartThrowing extends ShareableAdditionalArrangeBase implements AdditionalArrangeMethod
{
	//enh:sco SCOType
	
	int number = ((superNumber!=-1)? superNumber: 25);
	// enh:field getter setter min=1 max=MAX_NUMBER
	
	float minRadius = 0.5f;
	// enh:field quantity=LENGTH getter setter min=0 max=10

	public void calculate() {	
		pointList = new PointArrayList();
		int j = 0, k = 0;
		float aktMinRadius = minRadius;
		while (j < number) {
			Point p = new Point(Library.random(0, maxX),
								Library.random(0, maxY));
			if (!pointList.contains(p) && pointList.isRadiusFree(p, aktMinRadius) && checkDensityField(p.getX(), p.getY())) {
				pointList.add(p);
				j++;
				k = 0;
			}
			// wenn maxF versuche lang kein Punkt platziert werden konnte
			if (k > maxF) {
				if ((aktMinRadius- aktMinRadius * prozent) > 0) {
					// den mindestradius um prozent verringern
					aktMinRadius -= aktMinRadius * prozent;
				}				
				k = 0;
			}
			k++;
		}
	}
	
	//enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final Type $TYPE;

	public static final Type.Field number$FIELD;
	public static final Type.Field minRadius$FIELD;

	public static class Type extends SCOType
	{
		public Type (Class c, de.grogra.persistence.SCOType supertype)
		{
			super (c, supertype);
		}

		public Type (DartThrowing representative, de.grogra.persistence.SCOType supertype)
		{
			super (representative, supertype);
		}

		Type (Class c)
		{
			super (c, SCOType.$TYPE);
		}

		private static final int SUPER_FIELD_COUNT = SCOType.FIELD_COUNT;
		protected static final int FIELD_COUNT = SCOType.FIELD_COUNT + 2;

		static Field _addManagedField (Type t, String name, int modifiers, de.grogra.reflect.Type type, de.grogra.reflect.Type componentType, int id)
		{
			return t.addManagedField (name, modifiers, type, componentType, id);
		}

		@Override
		protected void setInt (Object o, int id, int value)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 0:
					((DartThrowing) o).number = (int) value;
					return;
			}
			super.setInt (o, id, value);
		}

		@Override
		protected int getInt (Object o, int id)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 0:
					return ((DartThrowing) o).getNumber ();
			}
			return super.getInt (o, id);
		}

		@Override
		protected void setFloat (Object o, int id, float value)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 1:
					((DartThrowing) o).minRadius = (float) value;
					return;
			}
			super.setFloat (o, id, value);
		}

		@Override
		protected float getFloat (Object o, int id)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 1:
					return ((DartThrowing) o).getMinRadius ();
			}
			return super.getFloat (o, id);
		}

		@Override
		public Object newInstance ()
		{
			return new DartThrowing ();
		}

	}

	public de.grogra.persistence.ManageableType getManageableType ()
	{
		return $TYPE;
	}


	static
	{
		$TYPE = new Type (DartThrowing.class);
		number$FIELD = Type._addManagedField ($TYPE, "number", 0 | Type.Field.SCO, de.grogra.reflect.Type.INT, null, Type.SUPER_FIELD_COUNT + 0);
		minRadius$FIELD = Type._addManagedField ($TYPE, "minRadius", 0 | Type.Field.SCO, de.grogra.reflect.Type.FLOAT, null, Type.SUPER_FIELD_COUNT + 1);
		number$FIELD.setMinValue (new Integer (1));
		number$FIELD.setMaxValue (new Integer (MAX_NUMBER));
		minRadius$FIELD.setQuantity (de.grogra.util.Quantity.LENGTH);
		minRadius$FIELD.setMinValue (new Float (0));
		minRadius$FIELD.setMaxValue (new Float (10));
		$TYPE.validate ();
	}

	public int getNumber ()
	{
		return number;
	}

	public void setNumber (int value)
	{
		this.number = (int) value;
	}

	public float getMinRadius ()
	{
		return minRadius;
	}

	public void setMinRadius (float value)
	{
		this.minRadius = (float) value;
	}

//enh:end

	
}
