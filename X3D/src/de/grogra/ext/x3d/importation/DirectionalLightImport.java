package de.grogra.ext.x3d.importation;

import java.util.HashMap;
import javax.vecmath.Vector3d;
import de.grogra.ext.x3d.ProtoInstanceImport;
import de.grogra.ext.x3d.X3DImport;
import de.grogra.ext.x3d.util.Util;
import de.grogra.ext.x3d.xmlbeans.DirectionalLightDocument.DirectionalLight;
import de.grogra.graph.EdgePatternImpl;
import de.grogra.graph.impl.Node;
import de.grogra.imp3d.objects.LightNode;
import de.grogra.math.RGBColor;

public class DirectionalLightImport {

	private static float intensityFactor = 10f;
	private static Vector3d upVec = new Vector3d(0, 0, 1);
	
	public static LightNode createInstance(DirectionalLight directionalLight) {
		LightNode l = null;

		HashMap<String, Object> referenceMap = X3DImport.getTheImport().getCurrentParser().getReferenceMap();
		
		if (directionalLight.isSetUSE()) {
			try {
				l = (LightNode) ((Node) referenceMap.get(directionalLight.getUSE())).cloneGraph(EdgePatternImpl.TREE, true);
			} catch (CloneNotSupportedException e) {e.printStackTrace();}
		}
		else {
			l = new LightNode();

			HashMap<String, String> finalValueMap = new HashMap<String, String>();
			if (directionalLight.isSetIS()) {
				ProtoInstanceImport.calcFinalValueMap(finalValueMap, directionalLight);
			}
			
			setValues(l, directionalLight, finalValueMap);
			if (directionalLight.isSetDEF()) {
				referenceMap.put(directionalLight.getDEF(), l);
			}
		}
		
		l.setName("X3DDirectionalLight");
		return l;
	}
	
	private static void setValues(LightNode node, DirectionalLight directionalLight, HashMap<String, String> valueMap) {
		de.grogra.imp3d.objects.DirectionalLight l = new de.grogra.imp3d.objects.DirectionalLight();
		
		// color
		RGBColor color = new RGBColor();
		String colorString = (valueMap.get("color") != null) ? valueMap.get("color") : directionalLight.getColor();
		Util.splitStringToTuple3f(color, colorString);
		l.getColor().set(color);
		
		// direction
		Vector3d direction = new Vector3d();
		String directionString = (valueMap.get("direction") != null) ? valueMap.get("direction") : directionalLight.getDirection();
		Util.convertStringToTuple3d(direction, directionString);
		node.setTransform(Util.vectorsToTransMatrix(upVec, direction));
		
		// intensity
		float intensity;
		intensity = (valueMap.get("intensity") != null) ? Float.valueOf(valueMap.get("intensity")) : directionalLight.getIntensity();
		l.setPowerDensity(intensity * intensityFactor);
		
		node.setLight(l);
	}
}
