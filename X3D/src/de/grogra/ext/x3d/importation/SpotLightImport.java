package de.grogra.ext.x3d.importation;

import java.util.HashMap;
import javax.vecmath.Matrix4d;
import javax.vecmath.Vector3d;
import de.grogra.ext.x3d.ProtoInstanceImport;
import de.grogra.ext.x3d.X3DImport;
import de.grogra.ext.x3d.util.Util;
import de.grogra.ext.x3d.xmlbeans.SpotLightDocument.SpotLight;
import de.grogra.graph.EdgePatternImpl;
import de.grogra.graph.impl.Node;
import de.grogra.imp3d.objects.LightNode;
import de.grogra.math.RGBColor;

public class SpotLightImport {

	private static float intensityFactor = 100f;
	private static Vector3d upVec = new Vector3d(0, 0, 1);
	
	public static LightNode createInstance(SpotLight spotLight) {
		LightNode l = null;

		HashMap<String, Object> referenceMap = X3DImport.getTheImport().getCurrentParser().getReferenceMap();
		
		if (spotLight.isSetUSE()) {
			try {
				l = (LightNode) ((Node) referenceMap.get(spotLight.getUSE())).cloneGraph(EdgePatternImpl.TREE, true);
			} catch (CloneNotSupportedException e) {e.printStackTrace();}
		}
		else {
			l = new LightNode();

			HashMap<String, String> finalValueMap = new HashMap<String, String>();
			if (spotLight.isSetIS()) {
				ProtoInstanceImport.calcFinalValueMap(finalValueMap, spotLight);
			}
			
			setValues(l, spotLight, finalValueMap);
			if (spotLight.isSetDEF()) {
				referenceMap.put(spotLight.getDEF(), l);
			}
		}
		
		l.setName("X3DSpotLight");
		return l;
	}
	
	private static void setValues(LightNode node, SpotLight spotLight, HashMap<String, String> valueMap) {
		de.grogra.imp3d.objects.SpotLight l = new de.grogra.imp3d.objects.SpotLight();
		
		// color
		RGBColor color = new RGBColor();
		String colorString = (valueMap.get("color") != null) ? valueMap.get("color") : spotLight.getColor();
		Util.splitStringToTuple3f(color, colorString);
		l.getColor().set(color);
		
		// direction
		Vector3d direction = new Vector3d();
		String directionString = (valueMap.get("direction") != null) ? valueMap.get("direction") : spotLight.getDirection();
		Util.convertStringToTuple3d(direction, directionString);
		Matrix4d transform = Util.vectorsToTransMatrix(upVec, direction);
		
		// location
		Vector3d location = new Vector3d();
		String locationString = (valueMap.get("location") != null) ? valueMap.get("location") : spotLight.getLocation();
		Util.convertStringToTuple3d(location, locationString);
		transform.setTranslation(location);
		node.setTransform(transform);
		
		// intensity
		float intensity;
		intensity = (valueMap.get("intensity") != null) ? Float.valueOf(valueMap.get("intensity")) : spotLight.getIntensity();
		l.setPower(intensity * intensityFactor);
		
		// light area
		float beamWidth;
		beamWidth = (valueMap.get("beamWidth") != null) ? Float.valueOf(valueMap.get("beamWidth")) : spotLight.getBeamWidth();
		l.setInnerAngle(beamWidth);
		float cutOffAngle;
		cutOffAngle = (valueMap.get("cutOffAngle") != null) ? Float.valueOf(valueMap.get("cutOffAngle")) : spotLight.getCutOffAngle();
		l.setInnerAngle(cutOffAngle);
		
		node.setLight(l);
	}
	
}
