package de.grogra.ext.x3d.importation;

import java.util.HashMap;
import de.grogra.ext.x3d.X3DImport;
import de.grogra.ext.x3d.xmlbeans.AppearanceDocument.Appearance;
import de.grogra.ext.x3d.xmlbeans.ImageTextureDocument.ImageTexture;
import de.grogra.ext.x3d.xmlbeans.MaterialDocument.Material;
import de.grogra.ext.x3d.xmlbeans.TextureTransformDocument.TextureTransform;
import de.grogra.graph.impl.Node;
import de.grogra.imp.objects.ImageAdapter;
import de.grogra.imp3d.shading.AffineUVTransformation;
import de.grogra.imp3d.shading.ImageMap;
import de.grogra.imp3d.shading.Phong;

public class AppearanceImport {

	public static Phong createInstance(Appearance app) {
		HashMap<String, Object> referenceMap = X3DImport.getTheImport().getCurrentParser().getReferenceMap();

		Phong p = null;
		if (app.isSetUSE()) {
			try {
				p = (Phong) ((Node) referenceMap.get(app.getUSE())).clone(true);
			} catch (CloneNotSupportedException e) {e.printStackTrace();}
		}
		else {
			p = new Phong();
			
			setValues(p, app);
			if (app.isSetDEF()) {
				referenceMap.put(app.getDEF(), p);
			}
		}
		return p;
	}
	
	private static void setValues(Phong p, Appearance app) {
		
		// material
		if (app.getMaterialArray().length > 0) {
			Phong mat = handleMaterial(app.getMaterialArray(0));
			p.setAmbient(mat.getAmbient());
			p.setDiffuse(mat.getDiffuse());
			p.setEmissive(mat.getEmissive());
			p.setShininess(mat.getShininess());
			p.setSpecular(mat.getSpecular());
			p.setTransparency(mat.getTransparency());
		}
		
		// texture
		if (app.getImageTextureArray().length > 0) {
			ImageAdapter texture = handleImageTexture(app.getImageTextureArray(0));
			ImageMap im = new ImageMap();
			im.setImageAdapter(texture);
			p.setDiffuse(im);
		}
		
		// texture transform
		if (app.getTextureTransformArray().length > 0) {
			AffineUVTransformation textureTransform = handleTextureTransform(app.getTextureTransformArray(0));
			if (p.getDiffuse() instanceof ImageMap) {
				ImageMap im = (ImageMap) p.getDiffuse();
				im.setInput(textureTransform);
			}
		}
	}
	
	private static Phong handleMaterial(Material material) {
		Phong child = MaterialImport.createInstance(material);
		return child;
	}
	
	private static ImageAdapter handleImageTexture(ImageTexture imageTexture) {
		ImageAdapter ia = ImageTextureImport.createInstance(imageTexture);
		return ia;
	}
	
	private static AffineUVTransformation handleTextureTransform(TextureTransform textureTransform) {
		AffineUVTransformation uv = TextureTransformImport.createInstance(textureTransform);
		return uv;		
	}
	
}
