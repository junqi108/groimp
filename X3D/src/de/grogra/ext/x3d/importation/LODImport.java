package de.grogra.ext.x3d.importation;

import java.util.HashMap;
import org.apache.xmlbeans.XmlCursor;
import de.grogra.ext.x3d.ProtoInstanceImport;
import de.grogra.ext.x3d.X3DImport;
import de.grogra.ext.x3d.X3DParser;
import de.grogra.ext.x3d.xmlbeans.LODDocument.LOD;
import de.grogra.graph.EdgePatternImpl;
import de.grogra.graph.impl.Node;

public class LODImport {

	public static Node createInstance(LOD lod) {
		Node n = null;
		
		HashMap<String, Object> referenceMap = X3DImport.getTheImport().getCurrentParser().getReferenceMap();
		
		if (lod.isSetUSE()) {
			try {
				n = ((Node) referenceMap.get(lod.getUSE())).cloneGraph(EdgePatternImpl.TREE, true);
			}
			catch (CloneNotSupportedException e) {e.printStackTrace();}
		}
		else {
			n = new Node();
			
			HashMap<String, String> finalValueMap = new HashMap<String, String>();
			if (lod.isSetIS()) {
				ProtoInstanceImport.calcFinalValueMap(finalValueMap, lod);
			}
			
			setValues(n, lod, finalValueMap);
			if (lod.isSetDEF()) {
				referenceMap.put(lod.getDEF(), n);
			}
		}
	
		n.setName("X3DLOD");
		return n;
	}
	
	private static void setValues(Node n, LOD lod, HashMap<String, String> valueMap) {
		// parse first child - has highest LOD
		X3DImport importer = X3DImport.getTheImport();
		
		XmlCursor cursor = lod.newCursor();
		cursor.toFirstChild();
		X3DParser parser = new X3DParser(cursor.getObject(), importer.getCurrentParser().getFile());
		parser.parseScene(n);
	}
	
}
