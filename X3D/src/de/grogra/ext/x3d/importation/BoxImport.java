package de.grogra.ext.x3d.importation;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import javax.imageio.ImageIO;
import de.grogra.ext.x3d.ProtoInstanceImport;
import de.grogra.ext.x3d.X3DImport;
import de.grogra.ext.x3d.util.Util;
import de.grogra.ext.x3d.xmlbeans.BoxDocument.Box;
import de.grogra.graph.impl.Node;
import de.grogra.imp.objects.ImageAdapter;
import de.grogra.imp3d.objects.ShadedNull;
import de.grogra.imp3d.shading.AffineUVTransformation;
import de.grogra.imp3d.shading.ImageMap;
import de.grogra.imp3d.shading.Phong;

public class BoxImport {

	public static ShadedNull createInstance(Box box, Object parent) {
		de.grogra.imp3d.objects.Box b = null;
		
		HashMap<String, Object> referenceMap = X3DImport.getTheImport().getCurrentParser().getReferenceMap();
		
		if (box.isSetUSE()) {
			try {
				b = (de.grogra.imp3d.objects.Box) ((Node) referenceMap.get(box.getUSE())).clone(true);
			} catch (CloneNotSupportedException e) {e.printStackTrace();}
		}
		else {
			b = new de.grogra.imp3d.objects.Box();
			
			HashMap<String, String> finalValueMap = new HashMap<String, String>();
			if (box.isSetIS()) {
				ProtoInstanceImport.calcFinalValueMap(finalValueMap, box);
			}
			
			setValues(b, box, finalValueMap, parent);
			if (box.isSetDEF()) {
				referenceMap.put(box.getDEF(), b);
			}				
		}
	
		b.setName("X3DBox");
		return b;
	}
	
	private static void setValues(de.grogra.imp3d.objects.Box node, Box box, HashMap<String, String> valueMap, Object parent) {
		// size
		float[] size = null;
		String sizeString = (valueMap.get("size") != null) ? valueMap.get("size") : box.getSize();
		size = Util.splitStringToArrayOfFloat(sizeString, new float[]{2, 2, 2});
		node.setLength(size[1]);
		node.setWidth(size[0]);
		node.setHeight(size[2]);
		
		// groimp-specific attributes
		node.setShiftPivot(false);
		node.setStartPosition(-0.5f);
		node.setEndPosition(0);
		
		// redraw texture image to correct uv's
		if ((parent instanceof ShadedNull) && (((ShadedNull) parent).getShader() instanceof Phong)) {
			
			Phong phong = (Phong) ((ShadedNull) parent).getShader();
			if (phong.getDiffuse() instanceof ImageMap) {
				// parent of box is shape and has an image texture
				ImageMap im = (ImageMap) phong.getDiffuse();
				BufferedImage image = im.getImageAdapter().getBufferedImage();
				
				float scaleU = 1;
				float scaleV = 1;
				
				// texture transformation is given, so use this scale factor for calc new texture and remove it
				if (im.getInput() instanceof AffineUVTransformation) {
					AffineUVTransformation uvTrans = (AffineUVTransformation) im.getInput();
					scaleU = uvTrans.getScaleU();
					scaleV = uvTrans.getScaleV();
					uvTrans.setScaleU(1);
					uvTrans.setScaleV(1);
				}
				
				int imageWidth = image.getWidth();
				int imageHeight = image.getHeight();
				
				int resultWidth = imageWidth * 4;
				int resultHeight = imageHeight * 3;
				
				BufferedImage result = new BufferedImage(resultWidth, resultHeight, BufferedImage.TYPE_INT_ARGB);
				
				// loop over pixels of new image and calc color value
				// box mapping is in groimp done like in pov-ray
				for (int w = 0; w < resultWidth; w++) {
					
					for (int h = 0; h < resultHeight; h++) {
						
						double u = (double) w / (double) (resultWidth - 1);
						double v = (double) h / (double) (resultHeight - 1);
						
						double x, y;
						
						// top
						if ((u >= 0.25) && (u <= 0.5) && (v >= 1.0/3.0) && (v <= 2.0/3.0)) {
							x = (u - 0.25) / 0.25;
							y = (v - 1.0/3.0) / (1.0/3.0);
							x *= scaleU; x = x - Math.floor(x);
							y *= scaleV; y = y - Math.floor(y);
							x *= imageWidth - 1;
							y *= imageHeight - 1;
							result.setRGB(w, h, image.getRGB(Util.round(x), Util.round(y)));
						}
						
						// bottom
						else if ((u >= 0.75) && (u <= 1) && (v >= 1.0/3.0) && (v <= 2.0/3.0)) {
							x = (u - 0.75) / 0.25;
							y = (v - 1.0/3.0) / (1.0/3.0);
							x *= scaleU; x = x - Math.floor(x);
							y *= scaleV; y = y - Math.floor(y);
							x *= imageWidth - 1; x = imageWidth - 1 - x;
							y *= imageHeight - 1; y = imageHeight - 1 - y;
							result.setRGB(w, h, image.getRGB(Util.round(x), Util.round(y)));
						}
						
						// front
						else if ((u >= 0.25) && (u <= 0.5) && (v >= 2.0/3.0) && (v <= 1)) {
							x = (u - 0.25) / 0.25;
							y = (v - 2.0/3.0) / (1.0/3.0);
							x *= scaleU; x = x - Math.floor(x);
							y *= scaleV; y = y - Math.floor(y);
							x *= imageWidth - 1;
							y *= imageHeight - 1;
							result.setRGB(w, h, image.getRGB(Util.round(x), Util.round(y)));
						}
						
						// back
						else if ((u >= 0.25) && (u <= 0.5) && (v >= 0) && (v <= 1.0/3.0)) {
							x = (u - 0.25) / 0.25;
							y = v / (1.0/3.0);
							x *= scaleU; x = x - Math.floor(x);
							y *= scaleV; y = y - Math.floor(y);
							x *= imageWidth - 1; x = imageWidth - 1 - x;
							y *= imageHeight - 1; y = imageHeight - 1 - y;
							result.setRGB(w, h, image.getRGB(Util.round(x), Util.round(y)));
						}
						
						// right
						else if ((u >= 0.5) && (u <= 0.75) && (v >= 1.0/3.0) && (v <= 2.0/3.0)) {
							y = (u - 0.5) / 0.25;
							x = (v - 1.0/3.0) / (1.0/3.0);
							x *= scaleU; x = x - Math.floor(x);
							y *= scaleV; y = y - Math.floor(y);
							x *= imageWidth - 1; x = imageWidth - 1 - x;
							y *= imageHeight - 1;
							result.setRGB(w, h, image.getRGB(Util.round(x), Util.round(y)));
						}
						
						// left
						else if ((u >= 0) && (u <= 0.25) && (v >= 1.0/3.0) && (v <= 2.0/3.0)) {
							y = u / 0.25;
							x = (v - 1.0/3.0) / (1.0/3.0);
							x *= scaleU; x = x - Math.floor(x);
							y *= scaleV; y = y - Math.floor(y);
							x *= imageWidth - 1;
							y *= imageHeight - 1; y = imageHeight - 1 - y;
							result.setRGB(w, h, image.getRGB(Util.round(x), Util.round(y)));
						}
					} // for h
				} // for w
				
				File tmpImage = null;
				try {
					tmpImage = File.createTempFile("x3dsurface", ".png");
					ImageIO.write(result, "png", tmpImage);
				} catch (IOException e) {
					e.printStackTrace();
					return;
				}
				
				ImageAdapter ia = Util.getImageForURL(tmpImage.getPath(), true);
				im.setImageAdapter(ia);
			}
		}
	} // setValues
	
}
