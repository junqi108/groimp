package de.grogra.ext.x3d.importation;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import javax.imageio.ImageIO;
import javax.vecmath.Color3f;
import javax.vecmath.Tuple3f;
import de.grogra.ext.x3d.ProtoInstanceImport;
import de.grogra.ext.x3d.X3DImport;
import de.grogra.ext.x3d.util.Util;
import de.grogra.ext.x3d.xmlbeans.BackgroundDocument.Background;
import de.grogra.graph.impl.Node;
import de.grogra.imp.objects.ImageAdapter;
import de.grogra.imp3d.objects.Sky;
import de.grogra.imp3d.shading.ImageMap;
import de.grogra.imp3d.shading.Phong;
import de.grogra.math.Tuple3fType;

public class BackgroundImport {

	public static Sky createInstance(Background background) {
		Sky s = null;
		
		HashMap<String, Object> referenceMap = X3DImport.getTheImport().getCurrentParser().getReferenceMap();
		
		if (background.isSetUSE()) {
			try {
				s = (Sky) ((Node) referenceMap.get(background.getUSE())).clone(true);
			} catch (CloneNotSupportedException e) {e.printStackTrace();}
		}
		else {
			s = new Sky();
			
			HashMap<String, String> finalValueMap = new HashMap<String, String>();
			if (background.isSetIS()) {
				ProtoInstanceImport.calcFinalValueMap(finalValueMap, background);
			}
			
			setValues(s, background, finalValueMap);
			if (background.isSetDEF()) {
				referenceMap.put(background.getDEF(), s);
			}				
		}
	
		s.setName("X3DBackground");
		return s;	
	}
	
	@SuppressWarnings("unchecked")
	private static void setValues(Sky s, Background b, HashMap<String, String> valueMap) {
		X3DImport importer = X3DImport.getTheImport();
		
		// spherical image for sky
		int width = 2048, height = 1024;
		BufferedImage result = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		
		
		// skyColor
		float[] skyColor = new float[]{0, 0, 0};
		String skyColorString = valueMap.containsKey("skyColor") ? valueMap.get("skyColor") : b.getSkyColor();
		if (skyColorString != null) {
			skyColor = Util.splitStringToArrayOfFloat(skyColorString);
		}
		
		// skyAngle
		float[] skyAngle = new float[]{};
		String skyAngleString = valueMap.containsKey("skyAngle") ? valueMap.get("skyAngle") : b.getSkyAngle();
		if (skyAngleString != null) {
			skyAngle = Util.splitStringToArrayOfFloat(skyAngleString);
		}
		
		importer.increaseProgress();
		
		// no skyAngle, so only one color for whole sky
		if (skyAngle.length == 0) {
			Tuple3f rgb = new Color3f(skyColor[0], skyColor[1], skyColor[2]);
			int color = Tuple3fType.colorToInt(rgb);
			for (int w = 0; w < width; w++) {
				for (int h = 0; h < height; h++) {
					result.setRGB(w, h, color);
				}
			}
		}
		else {
			int i = 0;
			
			// loop over lines of result image and calculate color of this line
			for (int h = 0; h < height; h++) {
				
				double v = (double) h / (double) height;
	
				double[] color1 = new double[]{skyColor[3*i+0], skyColor[3*i+1], skyColor[3*i+2]};
				double[] color2;
				double angle;
				double preAngle;
				if (i == 0) {
					angle = skyAngle[i];
					preAngle = 0;
					color2 = new double[]{skyColor[3*(i+1)+0], skyColor[3*(i+1)+1], skyColor[3*(i+1)+2]};
				}
				else if (i < skyAngle.length) {
					angle = skyAngle[i];
					preAngle = skyAngle[i-1];
					color2 = new double[]{skyColor[3*(i+1)+0], skyColor[3*(i+1)+1], skyColor[3*(i+1)+2]};
				}
				else {
					angle = Math.PI;
					preAngle = skyAngle[i-1];
					color2 = new double[]{skyColor[3*i+0], skyColor[3*i+1], skyColor[3*i+2]};				
				}
				
				double ratio = (v - preAngle/Math.PI) / ((angle - preAngle) / Math.PI);
				Tuple3f rgb = new Color3f();
				rgb.x = (float) ((1 - ratio) * color1[0] + ratio * color2[0]);
				rgb.y = (float) ((1 - ratio) * color1[1] + ratio * color2[1]);
				rgb.z = (float) ((1 - ratio) * color1[2] + ratio * color2[2]);
				for (int w = 0; w < width; w++) {
					result.setRGB(w, h, Tuple3fType.colorToInt(rgb));
				}
				
				if (v >= angle / Math.PI)
					i++;
			}
		}
		
		
		// groundColor
		float[] groundColor = new float[]{0, 0, 0};
		String groundColorString = valueMap.containsKey("groundColor") ? valueMap.get("groundColor") : b.getGroundColor();
		if (groundColorString != null) {
			groundColor = Util.splitStringToArrayOfFloat(groundColorString);
		}
		
		// groundAngle
		float[] groundAngle = new float[]{};
		String groundAngleString = valueMap.containsKey("groundAngle") ? valueMap.get("groundAngle") : b.getGroundAngle();
		if (groundAngleString != null) {
			groundAngle = Util.splitStringToArrayOfFloat(groundAngleString);
		}
		
		importer.increaseProgress();
		
		// paint only ground color in the region of 0 until last value of ground angle
		if (groundAngle.length == 0) {
			// do nothing
		}
		else {
			int i = 0;
			
			// loop over lines of result image and calculate color of this line
			int maxHeight = (int) (groundAngle[groundAngle.length-1] / Math.PI * height);
			for (int h = 0; h < maxHeight; h++) {
				
				double v = (double) h / (double) height;
	
				double[] color1 = new double[]{groundColor[3*i+0], groundColor[3*i+1], groundColor[3*i+2]};
				double[] color2;
				double angle;
				double preAngle;
				if (i == 0) {
					angle = groundAngle[i];
					preAngle = 0;
					color2 = new double[]{groundColor[3*(i+1)+0], groundColor[3*(i+1)+1], groundColor[3*(i+1)+2]};
				}
				else if (i < groundAngle.length) {
					angle = groundAngle[i];
					preAngle = groundAngle[i-1];
					color2 = new double[]{groundColor[3*(i+1)+0], groundColor[3*(i+1)+1], groundColor[3*(i+1)+2]};
				}
				else {
					angle = Math.PI;
					preAngle = groundAngle[i-1];
					color2 = new double[]{groundColor[3*i+0], groundColor[3*i+1], groundColor[3*i+2]};				
				}
				
				double ratio = (v - preAngle/Math.PI) / ((angle - preAngle) / Math.PI);
				Tuple3f rgb = new Color3f();
				rgb.x = (float) ((1 - ratio) * color1[0] + ratio * color2[0]);
				rgb.y = (float) ((1 - ratio) * color1[1] + ratio * color2[1]);
				rgb.z = (float) ((1 - ratio) * color1[2] + ratio * color2[2]);
				for (int w = 0; w < width; w++) {
					result.setRGB(w, height - h - 1, Tuple3fType.colorToInt(rgb));
				}
				
				if (v >= angle / Math.PI)
					i++;
			}
		}
		
		X3DImport.getTheImport().increaseProgress();		

		// background images
		if (b.isSetTopUrl() || b.isSetLeftUrl() || b.isSetFrontUrl()
				|| b.isSetRightUrl() || b.isSetBackUrl() || b.isSetBottomUrl()) {

			BufferedImage imgTop = null, imgLeft = null, imgFront = null, imgRight = null, imgBack = null, imgBottom = null;

			ImageAdapter iaTop = null;
			if (b.isSetTopUrl()) {
				iaTop = Util.getImageForURL(b.getTopUrl(), valueMap, false);
				imgTop = iaTop.getBufferedImage();
				importer.increaseProgress();
			}
			ImageAdapter iaLeft = null;
			if (b.isSetLeftUrl()) {
				iaLeft = Util.getImageForURL(b.getLeftUrl(), valueMap, false);
				imgLeft = iaLeft.getBufferedImage();
				importer.increaseProgress();
			}
			ImageAdapter iaFront = null;
			if (b.isSetFrontUrl()) {
				iaFront = Util.getImageForURL(b.getFrontUrl(), valueMap, false);
				imgFront = iaFront.getBufferedImage();
				importer.increaseProgress();
			}
			ImageAdapter iaRight = null;
			if (b.isSetRightUrl()) {
				iaRight = Util.getImageForURL(b.getRightUrl(), valueMap, false);
				imgRight = iaRight.getBufferedImage();
			}
			ImageAdapter iaBack = null;
			if (b.isSetBackUrl()) {
				iaBack = Util.getImageForURL(b.getBackUrl(), valueMap, false);
				imgBack = iaBack.getBufferedImage();
				importer.increaseProgress();
			}
			ImageAdapter iaBottom = null;
			if (b.isSetBottomUrl()) {
				iaBottom = Util.getImageForURL(b.getBottomUrl(), valueMap, false);
				imgBottom = iaBottom.getBufferedImage();
				importer.increaseProgress();
			}
			
			// loop over pixels of output image for sky object
			for (int w = 0; w < width; w++) {
				importer.increaseProgress();
				for (int h = 0; h < height; h++) {
					
					double uResult = (double) w / (double) (width - 1);
					double vResult = (double) h / (double) (height - 1);
					
					double lambda = uResult * 2.0 * Math.PI;
					double phi = vResult * Math.PI - Math.PI / 2.0;
					
					double lambda0;
					double phi1;
					double cosc;
					double x;
					double y;
					int color;
					
					if ((lambda <= 3 * Math.PI / 4) || (lambda > 7 *Math.PI / 4))  {
					
						// back
						if (imgBack != null) {
							lambda0 = Math.PI/2;
							phi1 = 0;
							cosc = Math.sin(phi1) * Math.sin(phi) + Math.cos(phi1) * Math.cos(phi) * Math.cos(lambda - lambda0);
							x = Math.cos(phi) * Math.sin(lambda - lambda0) / cosc;
							y = (Math.cos(phi1) * Math.sin(phi) - Math.sin(phi1) * Math.cos(phi) * Math.cos(lambda - lambda0)) / cosc;
							if ((x >= -1) && (x < 1) && (y > -1) && (y < 1)) {
								x = (x + 1) / 2 * (imgBack.getWidth()-1);
								y = (y + 1) / 2 * (imgBack.getHeight()-1);
								color = imgBack.getRGB((int) Math.round(x), (int) Math.round(y));
								color = Util.overlapPixel(result.getRGB(w, h), color);
								result.setRGB(w, h, color);
								continue;
							}
						}
						
						// right
						if (imgRight != null) {
							lambda0 = 0;
							phi1 = 0;
							cosc = Math.sin(phi1) * Math.sin(phi) + Math.cos(phi1) * Math.cos(phi) * Math.cos(lambda - lambda0);
							x = Math.cos(phi) * Math.sin(lambda - lambda0) / cosc;
							y = (Math.cos(phi1) * Math.sin(phi) - Math.sin(phi1) * Math.cos(phi) * Math.cos(lambda - lambda0)) / cosc;
							if ((x >= -1) && (x < 1) && (y > -1) && (y < 1)) {
								x = (x + 1) / 2 * (imgRight.getWidth()-1);
								y = (y + 1) / 2 * (imgRight.getHeight()-1);
								color = imgRight.getRGB((int) Math.round(x), (int) Math.round(y));
								color = Util.overlapPixel(result.getRGB(w, h), color);
								result.setRGB(w, h, color);
								continue;
							}
						}
					
					}
					else {
					
						// front
						if (imgFront != null) {
							lambda0 = 3 * Math.PI/2;
							phi1 = 0;
							cosc = Math.sin(phi1) * Math.sin(phi) + Math.cos(phi1) * Math.cos(phi) * Math.cos(lambda - lambda0);
							x = Math.cos(phi) * Math.sin(lambda - lambda0) / cosc;
							y = (Math.cos(phi1) * Math.sin(phi) - Math.sin(phi1) * Math.cos(phi) * Math.cos(lambda - lambda0)) / cosc;
							if ((x >= -1) && (x < 1) && (y > -1) && (y < 1)) {
								x = (x + 1) / 2 * (imgFront.getWidth()-1);
								y = (y + 1) / 2 * (imgFront.getHeight()-1);
								color = imgFront.getRGB((int) Math.round(x), (int) Math.round(y));
								color = Util.overlapPixel(result.getRGB(w, h), color);
								result.setRGB(w, h, color);
								continue;
							}
						}
						
						// left
						if (imgLeft != null) {
							lambda0 = Math.PI;
							phi1 = 0;
							cosc = Math.sin(phi1) * Math.sin(phi) + Math.cos(phi1) * Math.cos(phi) * Math.cos(lambda - lambda0);
							x = Math.cos(phi) * Math.sin(lambda - lambda0) / cosc;
							y = (Math.cos(phi1) * Math.sin(phi) - Math.sin(phi1) * Math.cos(phi) * Math.cos(lambda - lambda0)) / cosc;
							if ((x >= -1) && (x < 1) && (y > -1) && (y < 1)) {
								x = (x + 1) / 2 * (imgLeft.getWidth()-1);
								y = (y + 1) / 2 * (imgLeft.getHeight()-1);
								color = imgLeft.getRGB((int) Math.round(x), (int) Math.round(y));
								color = Util.overlapPixel(result.getRGB(w, h), color);
								result.setRGB(w, h, color);
								continue;
							}
						}
					
					}
	
					if (phi <= Math.PI / 4) {
						// top
						if (imgTop != null) {
							lambda0 = 3*Math.PI/2;
							phi1 = -Math.PI/2;
							cosc = Math.sin(phi1) * Math.sin(phi) + Math.cos(phi1) * Math.cos(phi) * Math.cos(lambda - lambda0);
							x = Math.cos(phi) * Math.sin(lambda - lambda0) / cosc;
							y = (Math.cos(phi1) * Math.sin(phi) - Math.sin(phi1) * Math.cos(phi) * Math.cos(lambda - lambda0)) / cosc;
							if ((x >= -1) && (x < 1) && (y > -1) && (y < 1)) {
								x = (x + 1) / 2 * (imgTop.getWidth()-1);
								y = (y + 1) / 2 * (imgTop.getHeight()-1);
								color = imgTop.getRGB((int) Math.round(x), (int) Math.round(y));
								color = Util.overlapPixel(result.getRGB(w, h), color);
								result.setRGB(w, h, color);
								continue;
							}
						}
					}
					
					if (phi >= Math.PI / 4) {
						// bottom
						if (imgBottom != null) {
							lambda0 = 3*Math.PI/2;
							phi1 = Math.PI/2;
							cosc = Math.sin(phi1) * Math.sin(phi) + Math.cos(phi1) * Math.cos(phi) * Math.cos(lambda - lambda0);
							x = Math.cos(phi) * Math.sin(lambda - lambda0) / cosc;
							y = (Math.cos(phi1) * Math.sin(phi) - Math.sin(phi1) * Math.cos(phi) * Math.cos(lambda - lambda0)) / cosc;
							if ((x >= -1) && (x < 1) && (y > -1) && (y < 1)) {
								x = (x + 1) / 2 * (imgBottom.getWidth()-1);
								y = (y + 1) / 2 * (imgBottom.getHeight()-1);
								color = imgBottom.getRGB((int) Math.round(x), (int) Math.round(y));
								color = Util.overlapPixel(result.getRGB(w, h), color);
								result.setRGB(w, h, color);
								continue;
							}
						}
					}
				}
			}
		}
		
		importer.increaseProgress();
		
		File tmpImage = null;
		try {
			tmpImage = File.createTempFile("x3dbackground", ".png");
			importer.increaseProgress();
			ImageIO.write(result, "png", tmpImage);
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}
		
		importer.increaseProgress();
		
		ImageAdapter ia = Util.getImageForURL(tmpImage.getPath(), true);
		ImageMap im = new ImageMap();
		im.setImageAdapter(ia);
		im.setBilinearFiltering(false);
		Phong p = new Phong();
		p.setDiffuse(im);
		s.setShader(p);

	}
	
}
