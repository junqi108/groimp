package de.grogra.ext.x3d.importation;

import java.util.HashMap;

import de.grogra.ext.x3d.ProtoInstanceImport;
import de.grogra.ext.x3d.X3DImport;
import de.grogra.ext.x3d.xmlbeans.SphereDocument.Sphere;
import de.grogra.graph.impl.Node;
import de.grogra.imp3d.objects.ShadedNull;

public class SphereImport {

	public static ShadedNull createInstance(Sphere sphere) {
		de.grogra.imp3d.objects.Sphere s = null;

		HashMap<String, Object> referenceMap = X3DImport.getTheImport().getCurrentParser().getReferenceMap();
		
		if (sphere.isSetUSE()) {
			try {
				s = (de.grogra.imp3d.objects.Sphere) ((Node) referenceMap.get(sphere)).clone(true);
			} catch (CloneNotSupportedException e) {e.printStackTrace();}
		}
		else {
			s = new de.grogra.imp3d.objects.Sphere();
			
			HashMap<String, String> finalValueMap = new HashMap<String, String>();
			if (sphere.isSetIS()) {
				ProtoInstanceImport.calcFinalValueMap(finalValueMap, sphere);
			}
			
			setValues(s, sphere, finalValueMap);
			if (sphere.isSetDEF()) {
				referenceMap.put(sphere.getDEF(), s);
			}
		}
		
		s.setName("X3DSphere");	
		return s;
	}
	
	private static void setValues(de.grogra.imp3d.objects.Sphere node, Sphere sphere, HashMap<String, String> valueMap) {
		// radius
		float radius;
		radius = valueMap.containsKey("radius") ?
				Float.valueOf(valueMap.get("radius")) :
					sphere.isSetRadius() ?
						sphere.getRadius() :
							1f;
		node.setRadius(radius);
	}
	
}