package de.grogra.ext.x3d.importation;

import java.util.HashMap;
import de.grogra.ext.x3d.ProtoInstanceImport;
import de.grogra.ext.x3d.X3DImport;
import de.grogra.ext.x3d.util.Util;
import de.grogra.ext.x3d.xmlbeans.ColorDocument.Color;

public class ColorImport {

	public static float[] createInstance(Color color) {
		float[] c = null;
		
		HashMap<String, Object> referenceMap = X3DImport.getTheImport().getCurrentParser().getReferenceMap();
		
		if (color.isSetUSE()) {
			c = (float[]) referenceMap.get(color.getUSE());
		}
		else {
			HashMap<String, String> finalValueMap = new HashMap<String, String>();
			if (color.isSetIS()) {
				ProtoInstanceImport.calcFinalValueMap(finalValueMap, color);
			}
			
			c = setValues(color, finalValueMap);
			if (color.isSetDEF()) {
				referenceMap.put(color.getDEF(), c);
			}				
		}
	
		return c;
	}
	
	private static float[] setValues(Color color, HashMap<String, String> valueMap) {
		// color
		float[] c;
		String cString = valueMap.containsKey("color") ? valueMap.get("color") : color.getColor();
		c = Util.splitStringToArrayOfFloat(cString);
		return c;
	}
	
}
