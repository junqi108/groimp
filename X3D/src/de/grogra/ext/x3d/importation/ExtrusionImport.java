package de.grogra.ext.x3d.importation;

import java.util.ArrayList;
import java.util.HashMap;
import javax.vecmath.Point3d;
import javax.vecmath.Point3f;
import de.grogra.ext.x3d.ProtoInstanceImport;
import de.grogra.ext.x3d.X3DImport;
import de.grogra.ext.x3d.util.ExtrusionComputation;
import de.grogra.ext.x3d.util.Util;
import de.grogra.ext.x3d.util.ExtrusionComputation.CrossSection;
import de.grogra.ext.x3d.xmlbeans.ExtrusionDocument.Extrusion;
import de.grogra.graph.impl.Node;
import de.grogra.imp3d.objects.PolygonMesh;
import de.grogra.imp3d.objects.ShadedNull;
import de.grogra.xl.util.FloatList;
import de.grogra.xl.util.IntList;

public class ExtrusionImport {

	public static ShadedNull createInstance(Extrusion extrusion) {
		de.grogra.imp3d.objects.MeshNode m = null;

		HashMap<String, Object> referenceMap = X3DImport.getTheImport().getCurrentParser().getReferenceMap();
		
		if (extrusion.isSetUSE()) {
			try {
				m = (de.grogra.imp3d.objects.MeshNode) ((Node) referenceMap.get(extrusion.getUSE())).clone(true);
			} catch (CloneNotSupportedException e) {e.printStackTrace();}
		}
		else {
			m = new de.grogra.imp3d.objects.MeshNode();
			
			HashMap<String, String> finalValueMap = new HashMap<String, String>();
			if (extrusion.isSetIS()) {
				ProtoInstanceImport.calcFinalValueMap(finalValueMap, extrusion);
			}
			
			setValues(m, extrusion, finalValueMap);
			if (extrusion.isSetDEF()) {
				referenceMap.put(extrusion.getDEF(), m);
			}				
		}
	
		m.setName("X3DExtrusion");
		return m;
	}
	
	
	private static void setValues(de.grogra.imp3d.objects.MeshNode node, Extrusion extrusion, HashMap<String, String> valueMap) {

		//TODO: nur 'true' in x3d erlaubt, aber nicht 'TRUE' -> Exception
		
		// beginCap
		boolean beginCap;
		beginCap = valueMap.containsKey("beginCap") ?
				Boolean.valueOf(valueMap.get("beginCap")) :
					extrusion.isSetBeginCap() ?
							extrusion.getBeginCap() :
								true;
		
		// ccw
		boolean ccw;
		ccw = valueMap.containsKey("ccw") ?
			Boolean.valueOf(valueMap.get("ccw")) :
				extrusion.isSetCcw() ? 
						extrusion.getCcw() :
							true;
		
		// crossSection
		float[] crossSection;
		String crossSectionString = valueMap.containsKey("crossSection") ?
				valueMap.get("crossSection") : 
					extrusion.getCrossSection();
		crossSection = Util.splitStringToArrayOfFloat(crossSectionString, new float[]{1, 1, 1, -1, -1, -1, -1, 1, 1, 1});

		// endCap
		boolean endCap;
		endCap = valueMap.containsKey("endCap") ?
				Boolean.valueOf(valueMap.get("endCap")) :
					extrusion.isSetEndCap() ?
							extrusion.getEndCap() :
								true;
		
		// orientation
		float[] orientation;
		String orientationString = valueMap.containsKey("orientation") ?
				valueMap.get("orientation") :
					extrusion.getOrientation();
		orientation = Util.splitStringToArrayOfFloat(orientationString, new float[]{0, 0, 1, 0});
		
		// scale
		float[] scale;
		String scaleString = valueMap.containsKey("scale") ?
				valueMap.get("scale") :
					extrusion.getScale();
		scale = Util.splitStringToArrayOfFloat(scaleString, new float[]{1, 1});
		
		// solid
		boolean solid;
		solid = valueMap.containsKey("solid") ?
				Boolean.valueOf(valueMap.get("solid")) :
					extrusion.isSetSolid() ?
							extrusion.getSolid() :
								true;

		// spine
		float[] spine;
		String spineString = valueMap.containsKey("spine") ?
				valueMap.get("spine") :
					extrusion.getSpine();
		spine = Util.splitStringToArrayOfFloat(spineString, new float[]{0, 0, 0, 0, 1, 0});

		PolygonMesh p = new PolygonMesh();
		
		IntList coordIndexList = new IntList();
		FloatList vertexList = new FloatList();
		FloatList normalList = new FloatList();
		FloatList uvList = new FloatList();
			
		ExtrusionComputation ec = new ExtrusionComputation(coordIndexList, vertexList, normalList,
				uvList, crossSection, scale, ccw);
			
		// calculate some variables
		int scaleLength = scale.length;
		int orientationLength = orientation.length;
		int spineLength = spine.length;	
	
		// if too less scale values given, use the first two for all
		float[] newScale = null;
		int newScaleLength = 0;
		if (scaleLength*3 < spineLength*2) {
			newScaleLength = spineLength*2/3;
			newScale = new float[newScaleLength];
			for (int i = 0; i < newScaleLength; i=i+2) {
				newScale[i+0] = scale[0];
				newScale[i+1] = scale[1];
			}
		}
		else {
			newScale = scale.clone();
			newScaleLength = scaleLength;
		}
		
		// flip y and z coordinates in orientation
		float[] tmpOrientation = orientation.clone();
		for (int i = 0; i < orientationLength; i=i+4) {
			float temp = tmpOrientation[i+1];
			tmpOrientation[i+1] = tmpOrientation[i+2];
			tmpOrientation[i+2] = temp;
			tmpOrientation[i+0] = tmpOrientation[i+0];
		}
		
		// if too less orientation values given, use the first four for all
		float[] newOrientation = null;
		int newOrientationLength = 0;
		if (orientationLength*3 < spineLength*4) {
			newOrientationLength = spineLength*4/3;
			newOrientation = new float[newOrientationLength];
			for (int i = 0; i < newOrientationLength; i=i+4) {
				newOrientation[i+0] = tmpOrientation[0];
				newOrientation[i+1] = tmpOrientation[1];
				newOrientation[i+2] = tmpOrientation[2];
				newOrientation[i+3] = tmpOrientation[3];
			}
		}
		else {
			newOrientation = orientation.clone();
			newOrientationLength = orientationLength;
		}
		
		// flip y and z coordinates in spine points
		float[] newSpine = spine.clone();
		for (int i = 0; i < spineLength; i=i+3) {
			float temp = newSpine[i+1];
			newSpine[i+1] = -newSpine[i+2];
			newSpine[i+2] = temp;
		}
		
		// test if entire spline is collinear
		ArrayList<Point3d> spinePointList = new ArrayList<Point3d>();
		for (int sn = 0; sn < spineLength; sn = sn + 3) {
			spinePointList.add(new Point3d(newSpine[sn], newSpine[sn+1], newSpine[sn+2]));
		}
		boolean pointsOnLine = Util.pointsOnLine(spinePointList);
		
		ArrayList<CrossSection> cs = new ArrayList<CrossSection>();
		
		// create for every spine a crossSection
		ec.createCrossSections(newScale, newOrientation, newSpine, cs, pointsOnLine);
		
		// copy vertices from cs list to vertexList
		int k = 0;
		
		// create begin cap
		if (beginCap) {
			ArrayList<Point3f> vertList = cs.get(0).getVertices();
			k = ec.createCap(k, vertList, true);
		}
		
		// create triangles from one cs to the next
		k = ec.createSides(cs, k);
		
		// create end cap
		if (endCap) {
			ArrayList<Point3f> vertList = cs.get(cs.size()-1).getVertices();
			k = ec.createCap(k, vertList, false);
		}
		
		p.setVertexData(vertexList);
		p.setIndexData(coordIndexList);
		p.setNormalData(normalList.toArray());
		p.setTextureData(uvList.toArray());
		
		node.setVisibleSides(solid?0:2);
		
		node.setPolygons(p);
	}
	
}
