package de.grogra.ext.x3d.importation;

import java.util.HashMap;
import javax.vecmath.Vector2f;
import de.grogra.ext.x3d.ProtoInstanceImport;
import de.grogra.ext.x3d.X3DImport;
import de.grogra.ext.x3d.util.Util;
import de.grogra.ext.x3d.xmlbeans.TextureTransformDocument.TextureTransform;
import de.grogra.imp3d.shading.AffineUVTransformation;

public class TextureTransformImport {

	public static AffineUVTransformation createInstance(TextureTransform textureTransform) {
		AffineUVTransformation uv = null;
		
		HashMap<String, Object> referenceMap = X3DImport.getTheImport().getCurrentParser().getReferenceMap();
		
		if (textureTransform.isSetUSE()) {
			uv = (AffineUVTransformation) referenceMap.get(textureTransform.getUSE());
		}
		else {
			uv = new AffineUVTransformation();
			
			HashMap<String, String> finalValueMap = new HashMap<String, String>();
			if (textureTransform.isSetIS()) {
				ProtoInstanceImport.calcFinalValueMap(finalValueMap, textureTransform);
			}
			
			setValues(uv, textureTransform, finalValueMap);
			if (textureTransform.isSetDEF()) {
				referenceMap.put(textureTransform.getDEF(), uv);
			}
		}
		return uv;
	}
	
	private static void setValues(AffineUVTransformation uv, TextureTransform textureTransform, HashMap<String, String> valueMap) {
		// rotation
		float rotation;
		rotation = (valueMap.get("rotation") != null) ? Float.valueOf(valueMap.get("rotation")) : textureTransform.getRotation();
		uv.setAngle(-rotation);
		
		// translation
		Vector2f translation = new Vector2f();
		String translationString = (valueMap.get("translation") != null) ? valueMap.get("translation") : textureTransform.getTranslation();
		Util.splitStringToTuple2f(translation, translationString);
		uv.setOffsetU(-translation.x);
		uv.setOffsetV(-translation.y);
		
		// scale
		Vector2f scale = new Vector2f();
		String scaleString = (valueMap.get("scale") != null) ? valueMap.get("scale") : textureTransform.getScale();
		Util.splitStringToTuple2f(scale, scaleString);
		uv.setScaleU(scale.x);
		uv.setScaleV(scale.y);
	}
	
}
