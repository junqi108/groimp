package de.grogra.ext.x3d.importation;

import java.util.HashMap;
import de.grogra.ext.x3d.ProtoInstanceImport;
import de.grogra.ext.x3d.X3DImport;
import de.grogra.ext.x3d.util.Util;
import de.grogra.ext.x3d.xmlbeans.TriangleFanSetDocument.TriangleFanSet;
import de.grogra.graph.impl.Node;
import de.grogra.imp3d.objects.MeshNode;
import de.grogra.imp3d.objects.PolygonMesh;
import de.grogra.imp3d.objects.ShadedNull;
import de.grogra.xl.util.FloatList;
import de.grogra.xl.util.IntList;

public class TriangleFanSetImport {

	public static ShadedNull createInstance(TriangleFanSet triangleFanSet, Node parent) {
		MeshNode m = null;

		HashMap<String, Object> referenceMap = X3DImport.getTheImport().getCurrentParser().getReferenceMap();
		
		if (triangleFanSet.isSetUSE()) {
			try {
				m = (MeshNode) ((Node) referenceMap.get(triangleFanSet.getUSE())).clone(true);
			} catch (CloneNotSupportedException e) {e.printStackTrace();}
		}
		else {
			m = new MeshNode();
			
			HashMap<String, String> finalValueMap = new HashMap<String, String>();
			if (triangleFanSet.isSetIS()) {
				ProtoInstanceImport.calcFinalValueMap(finalValueMap, triangleFanSet);
			}
			
			setValues(m, triangleFanSet, finalValueMap, parent);
			if (triangleFanSet.isSetDEF()) {
				referenceMap.put(triangleFanSet.getDEF(), m);
			}
		}
		
		m.setName("X3DTriangleFanSet");
		return m;
	}
	
	private static void setValues(MeshNode node, TriangleFanSet triangleFanSet, HashMap<String, String> valueMap, Node parent) {
		
		// coords
		float[] coords = null;
		if (triangleFanSet.getCoordinateArray().length > 0)
			coords = CoordinateImport.createInstance(triangleFanSet.getCoordinateArray(0));
		
		// normals
		float[] normals = null;
		if (triangleFanSet.getNormalArray().length > 0)
			normals = NormalImport.createInstance(triangleFanSet.getNormalArray(0));
		
		// texCoords
		float[] texCoords = null;
		if (triangleFanSet.getTextureCoordinateArray().length > 0)
			texCoords = TextureCoordinateImport.createInstance(triangleFanSet.getTextureCoordinateArray(0));

		// fanCount
		int[] fanCount;
		String fanCountString = valueMap.containsKey("fanCount") ? valueMap.get("fanCount") : triangleFanSet.getFanCount();
		fanCount = Util.splitStringToArrayOfInt(fanCountString, new int[]{});		
		
		// ccw
		boolean ccw;
		ccw = valueMap.containsKey("ccw") ?
				Boolean.valueOf(valueMap.get("ccw")) :
					triangleFanSet.isSetCcw() ?
							triangleFanSet.getCcw() :
								true;
		
		// normalPerVertex
		boolean normalPerVertex;
		normalPerVertex = valueMap.containsKey("normalPerVertex") ?
				Boolean.valueOf(valueMap.get("normalPerVertex")) :
					triangleFanSet.isSetNormalPerVertex() ?
							triangleFanSet.getNormalPerVertex() :
								true;
		
		// solid
		boolean solid;
		solid = valueMap.containsKey("solid") ?
				Boolean.valueOf(valueMap.get("solid")) :
					triangleFanSet.isSetSolid() ?
							triangleFanSet.getSolid() :
								true;

		// if no coordinates exist, no mesh is created
//		if (coords == null)
//			return;
		
		PolygonMesh p = new PolygonMesh();
		
		IntList indexData = new IntList();
		FloatList vertexData = new FloatList();
		FloatList normalData = new FloatList();
		FloatList textureData = new FloatList();
		
		int vertexCount = 0;
		int startVertex = 0;
		
		for (int i = 0; i < fanCount.length; i++) {
			// loop over fans
			
			// count of vertices at current fan
			int fanLength = fanCount[i];
			
			for (int j = (startVertex + 1); j < (startVertex + fanLength - 1); j++) {
				// loop over vertices of current fan
				
				// set vertices
				indexData.add(vertexCount);
				vertexData.add( coords[3*startVertex+0]);
				vertexData.add(-coords[3*startVertex+2]);
				vertexData.add( coords[3*startVertex+1]);
				
				indexData.add(vertexCount + (ccw ? 1 : 2));
				vertexData.add( coords[3*j+0]);
				vertexData.add(-coords[3*j+2]);
				vertexData.add( coords[3*j+1]);

				indexData.add(vertexCount + (ccw ? 2 : 1));
				vertexData.add( coords[3*(j+1)+0]);
				vertexData.add(-coords[3*(j+1)+2]);
				vertexData.add( coords[3*(j+1)+1]);
				
				// set normals
				if ((normals != null) && normalPerVertex) {
					normalData.add( normals[3*startVertex+0]);
					normalData.add(-normals[3*startVertex+2]);
					normalData.add( normals[3*startVertex+1]);
					
					normalData.add( normals[3*j+0]);
					normalData.add(-normals[3*j+2]);
					normalData.add( normals[3*j+1]);

					normalData.add( normals[3*(j+1)+0]);
					normalData.add(-normals[3*(j+1)+2]);
					normalData.add( normals[3*(j+1)+1]);
				}
				
				// set texture coordinates
				if (texCoords != null) {
					textureData.add(texCoords[2*startVertex+0]);
					textureData.add(texCoords[2*startVertex+1]);
					
					textureData.add(texCoords[2*j+0]);
					textureData.add(texCoords[2*j+1]);

					textureData.add(texCoords[2*(j+1)+0]);
					textureData.add(texCoords[2*(j+1)+1]);
				}
				
				vertexCount += 3;
			}
			
			startVertex = fanLength;
			
		}
		
		p.setIndexData(indexData);
		p.setVertexData(vertexData);
		if ((normals != null) && normalPerVertex)
			p.setNormalData(normalData.elements);
		if (texCoords != null)
			p.setTextureData(textureData.elements);
		
		node.setVisibleSides(solid ? 0 : 2);
		
		node.setPolygons(p);
	}
}
