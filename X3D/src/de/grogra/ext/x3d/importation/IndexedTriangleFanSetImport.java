package de.grogra.ext.x3d.importation;

import java.util.HashMap;
import de.grogra.ext.x3d.ProtoInstanceImport;
import de.grogra.ext.x3d.X3DImport;
import de.grogra.ext.x3d.util.Util;
import de.grogra.ext.x3d.xmlbeans.IndexedTriangleFanSetDocument.IndexedTriangleFanSet;
import de.grogra.graph.impl.Node;
import de.grogra.imp3d.objects.MeshNode;
import de.grogra.imp3d.objects.PolygonMesh;
import de.grogra.imp3d.objects.ShadedNull;
import de.grogra.xl.util.FloatList;
import de.grogra.xl.util.IntList;

public class IndexedTriangleFanSetImport {

	public static ShadedNull createInstance(IndexedTriangleFanSet triangleFanSet, Node parent) {
		MeshNode m = null;
		
		HashMap<String, Object> referenceMap = X3DImport.getTheImport().getCurrentParser().getReferenceMap();
		
		if (triangleFanSet.isSetUSE()) {
			try {
				m = (MeshNode) ((Node) referenceMap.get(triangleFanSet.getUSE())).clone(true);
			} catch (CloneNotSupportedException e) {e.printStackTrace();}
		}
		else {
			m = new MeshNode();
			
			HashMap<String, String> finalValueMap = new HashMap<String, String>();
			if (triangleFanSet.isSetIS()) {
				ProtoInstanceImport.calcFinalValueMap(finalValueMap, triangleFanSet);
			}
			
			setValues(m, triangleFanSet, finalValueMap, parent);
			if (triangleFanSet.isSetDEF()) {
				referenceMap.put(triangleFanSet.getDEF(), m);
			}
		}
		
		m.setName("X3DIndexedTriangleFanSet");
		return m;
	}
	
	private static void setValues(MeshNode node, IndexedTriangleFanSet triangleFanSet, HashMap<String, String> valueMap, Node parent) {
		
		// coords
		float[] coords = null;
		if (triangleFanSet.getCoordinateArray().length > 0)
			coords = CoordinateImport.createInstance(triangleFanSet.getCoordinateArray(0));
		
		// normals
		float[] normals = null;
		if (triangleFanSet.getNormalArray().length > 0)
			normals = NormalImport.createInstance(triangleFanSet.getNormalArray(0));
		
		// texCoords
		float[] texCoords = null;
		if (triangleFanSet.getTextureCoordinateArray().length > 0)
			texCoords = TextureCoordinateImport.createInstance(triangleFanSet.getTextureCoordinateArray(0));

		// index
		int[] index;
		String indexString = valueMap.containsKey("index") ? valueMap.get("index") : triangleFanSet.getIndex();
		index = Util.splitStringToArrayOfInt(indexString, new int[]{});		
		
		// ccw
		boolean ccw;
		ccw = valueMap.containsKey("ccw") ?
				Boolean.valueOf(valueMap.get("ccw")) :
					triangleFanSet.isSetCcw() ?
							triangleFanSet.getCcw() :
								true;
		
		// normalPerVertex
		boolean normalPerVertex;
		normalPerVertex = valueMap.containsKey("normalPerVertex") ?
				Boolean.valueOf(valueMap.get("normalPerVertex")) :
					triangleFanSet.isSetNormalPerVertex() ?
							triangleFanSet.getNormalPerVertex() :
								true;
		
		// solid
		boolean solid;
		solid = valueMap.containsKey("solid") ?
				Boolean.valueOf(valueMap.get("solid")) :
					triangleFanSet.isSetSolid() ?
							triangleFanSet.getSolid() :
								true;

		// if no coordinates exist, no mesh is created
//		if (coords == null)
//			return;
		
		PolygonMesh p = new PolygonMesh();
		
		int coordCount = index.length;
		
		IntList indexData = new IntList();
		FloatList vertexData = new FloatList();
		FloatList normalData = new FloatList();
		FloatList textureData = new FloatList();
		
		int vertexCount = 0;
		int startVertex = 0;
		
		for (int i = 0; i < coordCount - 2; i++) {
			// loop over fans
			
			if (index[i+2] != -1) {
				// create triangle
				
				// set vertices
				indexData.add(vertexCount);
				vertexData.add( coords[3*index[startVertex]+0]);
				vertexData.add(-coords[3*index[startVertex]+2]);
				vertexData.add( coords[3*index[startVertex]+1]);

				indexData.add(vertexCount + (ccw ? 1 : 2));
				vertexData.add( coords[3*index[i+1]+0]);
				vertexData.add(-coords[3*index[i+1]+2]);
				vertexData.add( coords[3*index[i+1]+1]);

				indexData.add(vertexCount + (ccw ? 2 : 1));
				vertexData.add( coords[3*index[i+2]+0]);
				vertexData.add(-coords[3*index[i+2]+2]);
				vertexData.add( coords[3*index[i+2]+1]);

				// set normals
				if ((normals != null) && normalPerVertex) {
					normalData.add( normals[3*index[startVertex]+0]);
					normalData.add(-normals[3*index[startVertex]+2]);
					normalData.add( normals[3*index[startVertex]+1]);
					
					normalData.add( normals[3*index[i+1]+0]);
					normalData.add(-normals[3*index[i+1]+2]);
					normalData.add( normals[3*index[i+1]+1]);

					normalData.add( normals[3*index[i+2]+0]);
					normalData.add(-normals[3*index[i+2]+2]);
					normalData.add( normals[3*index[i+2]+1]);
				}
				
				// set texture coordinates
				if (texCoords != null) {
					textureData.add(texCoords[2*index[startVertex]+0]);
					textureData.add(texCoords[2*index[startVertex]+1]);
					
					textureData.add(texCoords[2*index[i+1]+0]);
					textureData.add(texCoords[2*index[i+1]+1]);

					textureData.add(texCoords[2*index[i+2]+0]);
					textureData.add(texCoords[2*index[i+2]+1]);
				}
				
				vertexCount += 3;
			}
			else {
				// end of fan
				i += 2;
				startVertex = i + 1;
			}
		}
		
		p.setIndexData(indexData);
		p.setVertexData(vertexData);
		if ((normals != null) && normalPerVertex)
			p.setNormalData(normalData.elements);
		if (texCoords != null)
			p.setTextureData(textureData.elements);
		
		node.setVisibleSides(solid ? 0 : 2);
		
		node.setPolygons(p);
	}
}
