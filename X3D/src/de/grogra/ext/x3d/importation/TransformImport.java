package de.grogra.ext.x3d.importation;

import java.util.HashMap;
import javax.vecmath.AxisAngle4d;
import javax.vecmath.Vector3d;

import de.grogra.ext.x3d.ProtoInstanceImport;
import de.grogra.ext.x3d.X3DImport;
import de.grogra.ext.x3d.util.Util;
import de.grogra.ext.x3d.xmlbeans.TransformDocument.Transform;
import de.grogra.graph.EdgePatternImpl;
import de.grogra.graph.impl.Node;
import de.grogra.imp3d.objects.Null;
import de.grogra.math.TMatrix4d;

public class TransformImport {
	
	public static Node createInstance(Transform transform) {
		Null t = null;

		HashMap<String, Object> referenceMap = X3DImport.getTheImport().getCurrentParser().getReferenceMap();
		
		if (transform.isSetUSE()) {
			try {
				t = (Null) ((Node) referenceMap.get(transform.getUSE())).cloneGraph(EdgePatternImpl.TREE, true);
			} catch (CloneNotSupportedException e) {e.printStackTrace();}
		}
		else {
			t = new Null();
			
			HashMap<String, String> finalValueMap = new HashMap<String, String>();
			if (transform.isSetIS()) {
				ProtoInstanceImport.calcFinalValueMap(finalValueMap, transform);
			}
			
			setValues(t, transform, finalValueMap);
			if (transform.isSetDEF()) {
				referenceMap.put(transform.getDEF(), t);
			}
		}
		
		t.setName("X3DTransform");
		return t;
	}
	
	private static void setValues(Null node, Transform transform, HashMap<String, String> valueMap) {
		// calculate attributes (T * C * R * SR * S * -SR * -C)
		TMatrix4d m = new TMatrix4d();
		
		// translation
		Vector3d translation = new Vector3d();
		String translationString = (valueMap.get("translation") != null) ? valueMap.get("translation") : transform.getTranslation();  
		Util.convertStringToTuple3d(translation, translationString);
		m.setTranslation(translation);

		// center
		TMatrix4d tmp = new TMatrix4d();
		Vector3d center = new Vector3d();
		String centerString = (valueMap.get("center") != null) ? valueMap.get("center") : transform.getCenter();
		Util.convertStringToTuple3d(center, centerString);
		tmp.setTranslation(center);
		m.mul(tmp);

		// rotation
		tmp.setIdentity();
		AxisAngle4d rotation = new AxisAngle4d();
		String rotationString = (valueMap.get("rotation") != null) ? valueMap.get("rotation") : transform.getRotation();
		Util.convertStringToAxisAngle4d(rotation, rotationString);
		tmp.set(rotation);
		m.mul(tmp);		

		// scale-orientation
		tmp = new TMatrix4d();
		AxisAngle4d scaleOrientation = new AxisAngle4d();
		String scaleOrientationString = (valueMap.get("scaleOrientation") != null) ? valueMap.get("scaleOrientation") : transform.getScaleOrientation();
		Util.convertStringToAxisAngle4d(scaleOrientation, scaleOrientationString);
		tmp.set(scaleOrientation);
		m.mul(tmp);			

		// scale
		tmp.setIdentity();
		Vector3d scale = new Vector3d();
		String scaleString = (valueMap.get("scale") != null) ? valueMap.get("scale") : transform.getScale();
		Util.splitStringToTuple3d(scale, scaleString);
		tmp.setElement(0, 0, scale.x);
		tmp.setElement(1, 1, scale.z);
		tmp.setElement(2, 2, scale.y);
		m.mul(tmp);	

		// -scaleOrientation
		tmp.setIdentity();
		tmp.set(scaleOrientation);
		tmp.invert();
		m.mul(tmp);			

		// -center
		tmp.setIdentity();
		tmp.setTranslation(center);
		tmp.invert();
		m.mul(tmp);
		
		node.setTransform(m);
	}
	
}
