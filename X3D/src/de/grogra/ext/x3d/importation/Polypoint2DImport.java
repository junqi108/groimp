package de.grogra.ext.x3d.importation;

import java.util.HashMap;
import de.grogra.ext.x3d.ProtoInstanceImport;
import de.grogra.ext.x3d.X3DImport;
import de.grogra.ext.x3d.util.Util;
import de.grogra.ext.x3d.xmlbeans.Polypoint2DDocument.Polypoint2D;
import de.grogra.graph.EdgePatternImpl;
import de.grogra.graph.Graph;
import de.grogra.graph.impl.Node;
import de.grogra.imp3d.objects.Null;
import de.grogra.imp3d.objects.Point;
import de.grogra.imp3d.objects.ShadedNull;
import de.grogra.imp3d.shading.Phong;
import de.grogra.imp3d.shading.Shader;
import de.grogra.math.ChannelMap;
import de.grogra.math.RGBColor;

public class Polypoint2DImport {

	public static Null createInstance(Polypoint2D polypoint, Node parent) {
		Null n = null;

		HashMap<String, Object> referenceMap = X3DImport.getTheImport().getCurrentParser().getReferenceMap();
		
		if (polypoint.isSetUSE()) {
			try {
				n = (Null) ((Node) referenceMap.get(polypoint.getUSE())).cloneGraph(EdgePatternImpl.TREE, true);
			} catch (CloneNotSupportedException e) {e.printStackTrace();}
		}
		else {
			n = new Null();
			
			HashMap<String, String> finalValueMap = new HashMap<String, String>();
			if (polypoint.isSetIS()) {
				ProtoInstanceImport.calcFinalValueMap(finalValueMap, polypoint);
			}
			
			setValues(n, polypoint, finalValueMap, parent);
			if (polypoint.isSetDEF()) {
				referenceMap.put(polypoint.getDEF(), n);
			}
		}
		
		n.setName("X3DPolypoint2D");
		return n;
	}
	
	private static void setValues(Null node, Polypoint2D polypoint, HashMap<String, String> valueMap, Node parent) {
		// point
		float[] point = null;
		String pointString = valueMap.containsKey("point") ? valueMap.get("point") : polypoint.getPoint();
		point = Util.splitStringToArrayOfFloat(pointString, new float[]{});
		
		RGBColor appColor = new RGBColor(0, 0, 0);
		if (parent instanceof ShadedNull) {
			Shader s = ((ShadedNull) parent).getShader();
			if (s instanceof Phong) {
				ChannelMap cm = ((Phong) s).getEmissive();
				if (cm instanceof RGBColor) {
					appColor = (RGBColor) cm; 
				}
			}
		}
		
		// create points, set translations
		int pointCount = point.length;
		for (int i = 0; i < pointCount; i=i+2) {
			Point newPoint = new Point();
			newPoint.setTranslation(point[i], 0, point[i+1]);
			newPoint.setColor(appColor.x, appColor.y, appColor.z);
			newPoint.setName("X3DPoint");
			node.addEdgeBitsTo(newPoint, Graph.BRANCH_EDGE, null);
		}
	}
	
}
