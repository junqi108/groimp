package de.grogra.ext.x3d.importation;

import java.util.HashMap;
import java.util.List;
import de.grogra.ext.x3d.ProtoInstanceImport;
import de.grogra.ext.x3d.X3DImport;
import de.grogra.ext.x3d.util.Util;
import de.grogra.ext.x3d.xmlbeans.ImageTextureDocument.ImageTexture;
import de.grogra.imp.objects.ImageAdapter;

public class ImageTextureImport {

	public static ImageAdapter createInstance(ImageTexture img) {
		ImageAdapter ia = null;

		HashMap<String, Object> referenceMap = X3DImport.getTheImport().getCurrentParser().getReferenceMap();
		
		if (img.isSetUSE()) {
			ia = (ImageAdapter) referenceMap.get(img.getUSE());
		}
		else {
			HashMap<String, String> finalValueMap = new HashMap<String, String>();
			if (img.isSetIS()) {
				ProtoInstanceImport.calcFinalValueMap(finalValueMap, img);
			}
			
			ia = setValues(img, finalValueMap);
			if (img.isSetDEF()) {
				referenceMap.put(img.getDEF(), ia);
			}
		}
		
		return ia;
	}
	
	@SuppressWarnings("unchecked")
	private static ImageAdapter setValues(ImageTexture img, HashMap<String, String> valueMap) {
		List<String> imgURL = img.getUrl();
		return Util.getImageForURL(imgURL, valueMap, true);
	}
	
}
