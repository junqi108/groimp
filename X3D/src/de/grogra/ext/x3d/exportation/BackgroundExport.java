package de.grogra.ext.x3d.exportation;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import de.grogra.ext.x3d.X3DExport;
import de.grogra.ext.x3d.util.Util;
import de.grogra.ext.x3d.xmlbeans.BackgroundDocument.Background;
import de.grogra.ext.x3d.xmlbeans.SceneDocument.Scene;
import de.grogra.ext.x3d.xmlbeans.ShapeDocument.Shape;
import de.grogra.ext.x3d.xmlbeans.TransformDocument.Transform;
import de.grogra.imp.objects.ImageAdapter;
import de.grogra.imp3d.objects.Attributes;
import de.grogra.imp3d.objects.Sky;
import de.grogra.imp3d.objects.SceneTree.Leaf;
import de.grogra.imp3d.shading.AffineUVTransformation;
import de.grogra.imp3d.shading.ImageMap;
import de.grogra.imp3d.shading.Phong;
import de.grogra.imp3d.shading.Shader;
import de.grogra.imp3d.shading.SideSwitchShader;
import de.grogra.imp3d.shading.SurfaceMap;
import de.grogra.math.ChannelMap;
import de.grogra.math.RGBColor;
import de.grogra.persistence.SharedObjectProvider;
import de.grogra.pf.registry.Item;
import de.grogra.xl.util.ObjectList;

public class BackgroundExport extends BaseExport {

	@SuppressWarnings("unchecked")
	@Override
	protected void exportImpl(Leaf node, X3DExport export, Shape shapeNode,
			Transform transformNode) throws IOException {
		Scene sceneNode = export.getScene();
		
		Background b = null;
		if (sceneNode.getBackgroundArray().length == 0)
			b = sceneNode.addNewBackground();
		else
			b = sceneNode.getBackgroundArray(0);
		
		int col;
		
		Shader sh = ((Sky) export.getGraphState ().getObjectDefault
				 (node.object, true, Attributes.SHAPE, null)).getShader ();
		Shader mat;
		if (sh instanceof SideSwitchShader)
			mat = ((SideSwitchShader) sh).getFrontShader ();
		else if (sh instanceof Phong)
			mat = sh;
		else
			mat = null;
		if (mat instanceof Phong)
		{
			Phong phong = (Phong) mat;
			if (phong.getDiffuse() instanceof SurfaceMap) {
				exportBGImages(b, (SurfaceMap) phong.getDiffuse(), export);
			}
			else {
				col = phong.getAverageColor();
				RGBColor rgb = Util.intToRGB(col);
				b.setSkyColor(rgb.x + " " + rgb.y + " " + rgb.z);
			}
		}
		else {
			col = ((sh != null) ? sh.getAverageColor () : 0xff8080ff);
			RGBColor rgb = Util.intToRGB(col);
			b.setSkyColor(rgb.x + " " + rgb.y + " " + rgb.z);
		}
	}

	private void exportBGImages(Background b, SurfaceMap sm, X3DExport export) {
		String fileType = "png";
		String imgName = "backgroundmap." + fileType;
		ChannelMap in = sm.getInput ();
		int drawRes = 1024;
		
		// generateImage == true: a new image file has to be created
		boolean generateImage;
		if (in instanceof AffineUVTransformation)
		{
			generateImage = true;
		}
		else if (in != null)
		{
			// some unsupported input transformation, have to create an image
			generateImage = true;
		}
		else
		{
			// no input transformation
			generateImage = false;
		}
		BufferedImage img;
		
		if (generateImage)
		{
			// draw the possibly transformed image
			img = new BufferedImage (drawRes, drawRes, BufferedImage.TYPE_INT_ARGB);
			export.increaseProgress();
			sm.drawImage (img, 1, true);
			export.increaseProgress();
		}
		else if (sm instanceof ImageMap)
		{
			// ImageMap: we can use GroIMP's image immediately
			ImageAdapter a = ((ImageMap) sm).getImageAdapter ();
			img = a.getBufferedImage ();
			SharedObjectProvider sop = a.getProvider();
			String itemName = ((Item) sop).getAbsoluteName();
			itemName = itemName.substring(itemName.lastIndexOf("/")+1, itemName.length());
			imgName = itemName + "." + fileType;
		}
		else
		{
			// make an image of the untransformed general surface map
			img = new BufferedImage (drawRes, drawRes, BufferedImage.TYPE_INT_ARGB);
			sm.drawImage (img, 1, false);
		}
		
		// if needed, write image to file
		if (img != null)
		{
			File file1, file2, file3, file4, file5, file6;
			
			int imageRes = 512;
			
			BufferedImage img1 = new BufferedImage(imageRes, imageRes, BufferedImage.TYPE_INT_ARGB);
			BufferedImage img2 = new BufferedImage(imageRes, imageRes, BufferedImage.TYPE_INT_ARGB);
			BufferedImage img3 = new BufferedImage(imageRes, imageRes, BufferedImage.TYPE_INT_ARGB);
			BufferedImage img4 = new BufferedImage(imageRes, imageRes, BufferedImage.TYPE_INT_ARGB);
			BufferedImage img5 = new BufferedImage(imageRes, imageRes, BufferedImage.TYPE_INT_ARGB);
			BufferedImage img6 = new BufferedImage(imageRes, imageRes, BufferedImage.TYPE_INT_ARGB);
			
			export.increaseProgress();
			calcGnomonic(img, img2, 0, 0);
			export.increaseProgress();
			calcGnomonic(img, img3, Math.PI / 2, 0);
			export.increaseProgress();
			calcGnomonic(img, img4, Math.PI, 0);
			export.increaseProgress();
			calcGnomonic(img, img5, 3 * Math.PI / 2, 0);
			export.increaseProgress();
			calcGnomonic(img, img1, 3 * Math.PI / 2, -Math.PI/2);
			export.increaseProgress();
			calcGnomonic(img, img6, Math.PI / 2, Math.PI/2);
			export.increaseProgress();
			
			try {
				file1 = (File) export.getFile (imgName);
				file2 = (File) export.getFile (imgName);
				file3 = (File) export.getFile (imgName);
				file4 = (File) export.getFile (imgName);
				file5 = (File) export.getFile (imgName);
				file6 = (File) export.getFile (imgName);
				
				ImageIO.write (img1, fileType, file1);
				ImageIO.write (img2, fileType, file2);
				ImageIO.write (img3, fileType, file3);
				ImageIO.write (img4, fileType, file4);
				ImageIO.write (img5, fileType, file5);
				ImageIO.write (img6, fileType, file6);
				
				b.setTopUrl(new ObjectList<String>(new String[]{file1.getName()}));
				b.setRightUrl(new ObjectList<String>(new String[]{file2.getName()}));
				b.setBackUrl(new ObjectList<String>(new String[]{file3.getName()}));
				b.setLeftUrl(new ObjectList<String>(new String[]{file4.getName()}));
				b.setFrontUrl(new ObjectList<String>(new String[]{file5.getName()}));
				b.setBottomUrl(new ObjectList<String>(new String[]{file6.getName()}));
				
			} catch (IOException e) {
				System.err.println("Couldn't write background image files for x3d export.");
			}			
		}
	}
	
	private void calcGnomonic(BufferedImage in, BufferedImage out, double lambda0, double phi1) {
		int width = out.getWidth();
		int height = out.getHeight();
		
		double x, y, rho, c, lambda, phi, u, v;
		int color;
		
		// loop over pixel of output image
		for (int w = 0; w < width; w++) {
			for (int h = 0; h < height; h++) {
				x = w / (double) (width - 1);
				x = 2 * x - 1;
				y = h / (double) (height - 1);
				y = 2 * y - 1;
				
				// calc lamba and phi for pixel
				rho = Math.sqrt((x*x) + (y*y));
				c = Math.atan(rho);
				
				lambda = 0;
				phi = 0;
				
				if (rho != 0) {
					phi = Math.asin( Math.cos(c) * Math.sin(phi1) + ((y * Math.sin(c) * Math.cos(phi1)) / (rho)) );
					lambda = lambda0 + Math.atan((x * Math.sin(c)) / (rho * Math.cos(phi1) * Math.cos(c) - y * Math.sin(phi1) * Math.sin(c)));
				}
				
				if ((phi1 == Math.PI / 2) && (y <= 0))
					lambda += Math.PI;
				if ((phi1 == -Math.PI / 2) && (y <= 0))
					lambda += Math.PI;
				if (lambda < 0)
					lambda += 2 * Math.PI;
				if (lambda > 2 * Math.PI)
					lambda -= 2 * Math.PI;
				
				// calc uv for lambda and phi of input image
				u = lambda / (2 * Math.PI);
				v = (phi + Math.PI / 2) / Math.PI;
	
				// get pixel at uv from input image, set to output image
				color = in.getRGB((int) Math.round(u * (in.getWidth() - 1)), (int) Math.round(v * (in.getHeight() - 1)));
				out.setRGB(w, h, color);
			}
		}
	}
	
}
