package de.grogra.ext.x3d.exportation;

import java.io.IOException;

import javax.vecmath.Color3f;
import javax.vecmath.Vector3f;

import de.grogra.ext.x3d.X3DExport;
import de.grogra.ext.x3d.xmlbeans.AppearanceDocument.Appearance;
import de.grogra.ext.x3d.xmlbeans.CoordinateDocument.Coordinate;
import de.grogra.ext.x3d.xmlbeans.LineSetDocument.LineSet;
import de.grogra.ext.x3d.xmlbeans.MaterialDocument.Material;
import de.grogra.ext.x3d.xmlbeans.ShapeDocument.Shape;
import de.grogra.ext.x3d.xmlbeans.TransformDocument.Transform;
import de.grogra.imp3d.objects.Attributes;
import de.grogra.imp3d.objects.SceneTree.Leaf;

public class LineExport extends BaseExport {

	@Override
	protected void exportImpl(Leaf node, X3DExport export, Shape shapeNode,
			Transform transformNode) throws IOException {
		LineSet ls = shapeNode.addNewLineSet();
		
		// coordinate
		ls.setVertexCount("2");
		Vector3f vec = (Vector3f) node.getObject(Attributes.AXIS);
		Coordinate c = ls.addNewCoordinate();
		c.setPoint("0 0 0 " + vec.x + " " + vec.z + " " + -vec.y);
				
		// color
		Color3f col = (Color3f) node.getObject(de.grogra.imp.objects.Attributes.COLOR);

		Appearance a = shapeNode.getAppearance();
		Material m = a.getMaterialArray(0);

		if (m.isSetDiffuseColor())
			m.unsetDiffuseColor();
		if (m.isSetAmbientIntensity())
			m.unsetAmbientIntensity();
		if (m.isSetShininess())
			m.unsetShininess();
		if (m.isSetSpecularColor())
			m.unsetSpecularColor();
		m.setEmissiveColor(col.x + " " + col.y + " " + col.z);
	}

}
