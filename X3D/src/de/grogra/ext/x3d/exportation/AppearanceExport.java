package de.grogra.ext.x3d.exportation;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.imageio.ImageIO;
import javax.vecmath.Tuple2f;
import javax.vecmath.Vector2f;

import de.grogra.ext.x3d.X3DExport;
import de.grogra.ext.x3d.util.Util;
import de.grogra.ext.x3d.xmlbeans.AppearanceDocument.Appearance;
import de.grogra.ext.x3d.xmlbeans.ImageTextureDocument.ImageTexture;
import de.grogra.ext.x3d.xmlbeans.MaterialDocument.Material;
import de.grogra.ext.x3d.xmlbeans.ShapeDocument.Shape;
import de.grogra.ext.x3d.xmlbeans.TextureTransformDocument.TextureTransform;
import de.grogra.graph.GraphState;
import de.grogra.imp.objects.ImageAdapter;
import de.grogra.imp3d.shading.AffineUVTransformation;
import de.grogra.imp3d.shading.ImageMap;
import de.grogra.imp3d.shading.Phong;
import de.grogra.imp3d.shading.RGBAShader;
import de.grogra.imp3d.shading.Shader;
import de.grogra.imp3d.shading.SurfaceMap;
import de.grogra.math.ChannelMap;
import de.grogra.math.ColorMap;
import de.grogra.math.Graytone;
import de.grogra.math.RGBColor;
import de.grogra.persistence.SharedObjectProvider;
import de.grogra.pf.registry.Item;

public class AppearanceExport {

	/**
	 * Map for linking channel maps to exported image files to avoid duplicated exporting of same maps.
	 */
	public static HashMap<ChannelMap, String> imageFileMap = new HashMap<ChannelMap, String>();
	
	public static void handleAppearance(Shape parent, Object node, Shader shader, X3DExport export, GraphState gs) {
		Appearance a = parent.addNewAppearance();
		
		Material m = a.addNewMaterial();
		
		// RGBA
		if (shader instanceof RGBAShader) {
			RGBAShader rgba = (RGBAShader) shader;
			
			m.setDiffuseColor(rgba.x + " " + rgba.y + " " + rgba.z);
			if (1.0f-rgba.w != 0.0f)
				m.setTransparency(1.0f-rgba.w);
		}
		// Phong
		else if (shader instanceof Phong) {
			Phong p = (Phong) shader;
			
			// ambientIntensity
			ChannelMap c = p.getAmbient();
			if (c == null) {
				c = Phong.DEFAULT_AMBIENT;
			}
			if (c instanceof ColorMap) {
				int color = ((ColorMap) c).getAverageColor();
				float gray = Util.intToGray(color);
				if (gray != 0.2f)
					m.setAmbientIntensity(gray);
			}
			
			// diffuseColor
			c = p.getDiffuse();
			if (c instanceof SurfaceMap) {
				String s;
				try {
					s = exportChannelMap(c, export, a);
					ImageTexture i = a.addNewImageTexture();
					ArrayList<String> sl = new ArrayList<String>();
					sl.add(s);
					i.setUrl(sl);
				} catch (Exception e) {
					System.err.println("Couldn't write image file for x3d export.");
				}
			}
			else {
				if (c == null) {
					c = Phong.DEFAULT_DIFFUSE;
				}
				if (c instanceof ColorMap) {
					int color = ((ColorMap) c).getAverageColor();
					RGBColor rgb = Util.intToRGB(color);
					if (!rgb.equals(new RGBColor(0.8f, 0.8f, 0.8f)))
						m.setDiffuseColor(rgb.x + " " + rgb.y + " " + rgb.z);
				}
			}
			
			// emissiveColor
			c = p.getEmissive();
			if (c == null) {
				c = Phong.DEFAULT_EMISSIVE;
			}
			if (c instanceof ColorMap) {
				int color = ((ColorMap) c).getAverageColor();
				RGBColor rgb = Util.intToRGB(color);
				if (!rgb.equals(new RGBColor(0.0f, 0.0f, 0.0f)))
					m.setEmissiveColor(rgb.x + " " + rgb.y + " " + rgb.z);
			}
			
			// shininess
			c = p.getShininess();
			if (c == null) {
				c = new Graytone(0.5f);
			}
			if (c instanceof ColorMap) {
				int color = ((ColorMap) c).getAverageColor();
				float gray = Util.intToGray(color);
				if (gray != 0.2f)
					m.setShininess(gray);
			}
			
			// specularColor
			c = p.getSpecular();
			if (c == null) {
				c = Phong.DEFAULT_SPECULAR;
			}
			if (c instanceof ColorMap) {
				int color = ((ColorMap) c).getAverageColor();
				RGBColor rgb = Util.intToRGB(color);
				if (!rgb.equals(new RGBColor(0.0f, 0.0f, 0.0f)))
					m.setSpecularColor(rgb.x + " " + rgb.y + " " + rgb.z);
			}
			
			// transparency
			c = p.getTransparency();
			if (c == null) {
				c = Phong.DEFAULT_TRANSPARENCY;
			}
			if (c instanceof ColorMap) {
				int color = ((ColorMap) c).getAverageColor();
				float gray = Util.intToGray(color);
				if (gray != 0.0f)
					m.setTransparency(gray);
			}
			
		} // if shader phong
	}

	private static String exportChannelMap (ChannelMap c, X3DExport export, Appearance appearance)
			throws IOException {
		String fileName = null;
		String fileType = "png";
		String imgName = "surfacemap." + fileType;
		SurfaceMap sm = (SurfaceMap) c;
		ChannelMap in = sm.getInput ();
		AffineUVTransformation t;
		
		// generateImage == true: a new image file has to be created
		boolean generateImage;
		if (in instanceof AffineUVTransformation)
		{
			t = (AffineUVTransformation) in;
			if (t.getInput () != null)
			{
				t = null;
				// chained inputs: cannot be handled
				generateImage = true;
			}
			else
			{
				// in is just an affine uv transformation, can be handled
				generateImage = false;
			}
		}
		else if (in != null)
		{
			// some unsupported input transformation, have to create an image
			generateImage = true;
			t = null;
		}
		else
		{
			// no input transformation
			generateImage = false;
			t = null;
		}
		BufferedImage img;
		
		if (generateImage)
		{
			// draw the possibly transformed image
			img = new BufferedImage (256, 256, BufferedImage.TYPE_INT_ARGB);
			sm.drawImage (img, 1, true);
		}
		else if (sm instanceof ImageMap)
		{
			// ImageMap: we can use GroIMP's image immediately
			ImageAdapter a = ((ImageMap) sm).getImageAdapter ();
			img = a.getBufferedImage ();
			SharedObjectProvider sop = a.getProvider();
			String itemName = ((Item) sop).getAbsoluteName();
			itemName = itemName.substring(itemName.lastIndexOf("/")+1, itemName.length());
			imgName = itemName + "." + fileType;
		}
		else
		{
			// make an image of the untransformed general surface map
			img = new BufferedImage (256, 256, BufferedImage.TYPE_INT_ARGB);
			sm.drawImage (img, 1, false);
		}
		
		// if needed, write image to file
		if (imageFileMap.get(c) == null) {
			if (img != null) {
				File file = (File) export.getFile (imgName);
				ImageIO.write (img, fileType, file);
				String s = file.getCanonicalPath();
				int pathEnd = s.lastIndexOf(File.separator) + 1;
				fileName = s.substring(pathEnd, s.length());
				imageFileMap.put(c, fileName);
			}
		}
		else {
			fileName = imageFileMap.get(c);
		}
		if (t != null)
		{
			TextureTransform tt = appearance.addNewTextureTransform();
			
			float angle = t.getAngle();
			if (angle != 0.0f)
				tt.setRotation(-angle);
			Tuple2f translation = new Vector2f(t.getOffsetU(), t.getOffsetV());
			if (!translation.equals(new Vector2f(0.0f, 0.0f)))
				tt.setTranslation(-translation.x + " " + -translation.y);
			Tuple2f scale = new Vector2f(t.getScaleU(), t.getScaleV());
			if (!scale.equals(new Vector2f(1.0f, 1.0f)))
				tt.setScale(scale.x + " " + scale.y);
			
		}
		
		return fileName;
	}
	
}
