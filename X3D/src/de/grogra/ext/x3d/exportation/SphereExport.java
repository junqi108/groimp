package de.grogra.ext.x3d.exportation;

import java.io.IOException;
import de.grogra.ext.x3d.X3DExport;
import de.grogra.ext.x3d.xmlbeans.ShapeDocument.Shape;
import de.grogra.ext.x3d.xmlbeans.SphereDocument.Sphere;
import de.grogra.ext.x3d.xmlbeans.TransformDocument.Transform;
import de.grogra.imp3d.objects.SceneTree.Leaf;

public class SphereExport extends BaseExport {

	@Override
	protected void exportImpl(Leaf node, X3DExport export, Shape shapeNode, Transform transformNode)
			throws IOException {
		Sphere s = shapeNode.addNewSphere();
		
		// radius
		float radius = node.getFloat (de.grogra.imp.objects.Attributes.RADIUS);
		if (radius != 1.0f)
			s.setRadius(radius);
		
	}

}
