package de.grogra.ext.x3d;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Stack;

import javax.vecmath.Matrix4d;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.xmlbeans.XmlOptions;

import de.grogra.ext.x3d.exportation.AppearanceExport;
import de.grogra.ext.x3d.exportation.IndexedFaceSetExport;
import de.grogra.ext.x3d.exportation.LightExport;
import de.grogra.ext.x3d.exportation.TransformExport;
import de.grogra.ext.x3d.xmlbeans.ProfileNames;
import de.grogra.ext.x3d.xmlbeans.SceneDocument.Scene;
import de.grogra.ext.x3d.xmlbeans.TransformDocument.Transform;
import de.grogra.ext.x3d.xmlbeans.X3DDocument;
import de.grogra.ext.x3d.xmlbeans.X3DDocument.X3D;
import de.grogra.ext.x3d.xmlbeans.X3DVersion;
import de.grogra.imp3d.Polygonizable;
import de.grogra.imp3d.View3D;
import de.grogra.imp3d.objects.Attributes;
import de.grogra.imp3d.objects.SceneTree;
import de.grogra.imp3d.objects.SceneTree.InnerNode;
import de.grogra.imp3d.objects.SceneTreeWithShader;
import de.grogra.imp3d.shading.Light;
import de.grogra.pf.io.FileWriterSource;
import de.grogra.pf.io.FilterItem;
import de.grogra.pf.io.FilterSource;
import de.grogra.pf.ui.Workbench;

public class VRML97Export extends X3DExport implements FileWriterSource {

	private File outFile = null;
	private Scene scene = null;
	private Stack<Transform> transformStack = null;
	private Workbench workbench = null;

	/**
     * This collection is used to give every exported bitmap an unique name.
     */
    private final HashSet<String> createdFiles = new HashSet<String>();
    
	public VRML97Export(FilterItem item, FilterSource source) {
		super(item, source);
		setFlavor (item.getOutputFlavor ());
		this.transformStack = new Stack<Transform>();
		this.workbench = Workbench.current();
	}

	@Override
	protected SceneTree createSceneTree(View3D scene) {
		SceneTree t = new SceneTreeWithShader(scene) {
			@Override
			protected boolean acceptLeaf(Object object, boolean asNode) {
				if (asNode) {
					return getExportFor(object, asNode) != null;
				}
				return false;
			}
			@Override
			protected Leaf createLeaf(Object object, boolean asNode, long id) {
				Leaf l = new Leaf(object, asNode, id);
				init(l);
				return l;
			}
		};
		t.createTree(false);
		return t;
	}

	@SuppressWarnings("unchecked")
	@Override
	public NodeExport getExportFor (Object object, boolean asNode)
	{
		Object s = getGraphState ().getObjectDefault
			(object, asNode, Attributes.SHAPE, null);
		if (s != null)
		{
			NodeExport ex = super.getExportFor (s, asNode);
			if (ex != null)
			{
				return ex;
			}
			if (s instanceof Polygonizable)
			{
				return new IndexedFaceSetExport();
			}
		}
		Light light = (Light) getGraphState ().getObjectDefault
			(object, asNode, Attributes.LIGHT, null);
		return ((light != null) && (light.getLightType () != Light.SKY)) ? new LightExport() : null;
	}
	
	@Override
	protected void beginGroup(InnerNode group) throws IOException {
		Matrix4d transMatrix = new Matrix4d();
		group.get(transMatrix);
		Transform transformNode = TransformExport.handleTransform(transMatrix, transformStack.peek());
		transformStack.push(transformNode);
	}

	@Override
	protected void endGroup(InnerNode group) throws IOException {
		transformStack.pop();
	}

	@Override
	public void write(File out) throws IOException {
		workbench.beginStatus(this);
		workbench.setStatus(this, "Export X3D", -1);
		
		this.outFile = out;
		
		// create x3d instance
		X3DDocument x3dDocument = X3DDocument.Factory.newInstance();
		X3D x3d = x3dDocument.addNewX3D();
		x3d.setProfile(ProfileNames.IMMERSIVE);
		x3d.setVersion(X3DVersion.X_3_0);
		scene = x3d.addNewScene();
		
		// additional transform node, needed only for stack
		Transform transform = scene.addNewTransform();
		transformStack.add(transform);
		
		// delete map of imageFiles
		AppearanceExport.imageFileMap.clear();
	
		// execute groimp's scene graph exporter
		write();
		
		// set appearance of x3d file
		XmlOptions xmlOptions = new XmlOptions();
		xmlOptions.setSavePrettyPrint();
		xmlOptions.setSavePrettyPrintIndent(2);
		
		// save x3d file to temporary location
		String tempPath = out.getParent() + File.separator + "temp.x3d";
		File tempFile = new File(tempPath);
		x3dDocument.save(tempFile, xmlOptions);
		
		// translating to VRML 97 using xslt
		// set xalan as transformer. see constant declaration in 'X3DExport.java' for 
				// more details in library choice.
		//String originalTransformer = System.getProperty(TRANSFORMERFACTORY_JAVA);
		System.setProperty(TRANSFORMERFACTORY_JAVA, TRANSFORMERFACTORY_XALAN);
		
		// get sources for translation
		Source x3dSource = new StreamSource(tempFile);
		
		File xsltFile = new File(XSLT_PATH_X3D_TO_VRML97);
		if(!xsltFile.exists()) {
			// try alternative path
			xsltFile = new File(XSLT_PATH_X3D_TO_VRML97_A);
		}
        Source xsltSource = new StreamSource(xsltFile);
		
        // init transformer factory (from saxon library)
        TransformerFactory transFact = TransformerFactory.newInstance();
        Transformer trans;
		try 
		{
			// create transformer using xslt file
			trans = transFact.newTransformer(xsltSource);
			
			// execute transform
			trans.transform(x3dSource, new StreamResult(out));
			
		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerException e) {
			e.printStackTrace();
		}
        
        //delete temporary file
        tempFile.delete();
        
        //reset xslt transformer class
        //System.setProperty(TRANSFORMERFACTORY_JAVA, originalTransformer);
        
        //update workbench
		workbench.clearStatusAndProgress(this);
	}

	@Override
	public File getOutFile() {
		return outFile;
	}

	@Override
	public Transform getLastTransform() {
		return transformStack.peek();
	}
	
	@Override
	public Scene getScene() {
		return scene;
	}
	
	@Override
	public String getDirectory () {
		return outFile.getParent();
	}
	
	@Override
	public Object getFile (String name) throws IOException {
		int i = name.lastIndexOf ('.');
		String base = (i < 0) ? name : name.substring (0, i);
		String ext = (i < 0) ? "" : name.substring (i);
		i = 0;
		name = base + i + ext;
		while (!createdFiles.add (name))		{
			name = base + ++i + ext;
		}
		String dir = getDirectory();
		File f = new File(dir + File.separator + name);
		return f;
	}
	
	@Override
	public void increaseProgress() {
		workbench.setIndeterminateProgress(this);
	}
}
