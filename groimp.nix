{ lib, stdenv, makeWrapper, jdk8, xorg, ant, libxslt, javaPackages }:

stdenv.mkDerivation rec {
  pname = "groimp";
  version = "1.6";
  
  src = ./.;
  
  nativeBuildInputs = [ jdk8 makeWrapper ant libxslt ];
  
  buildPhase = ''
    cd Build
    ls lib
    ant 
    cd ..
  '';
  
  installPhase = ''
    mkdir -p $out/bin
    cp -r app $out

    makeWrapper ${jdk8}/bin/java $out/bin/${pname} \
      --add-flags "-jar $out/app/core.jar"
  '';
}
