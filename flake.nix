{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs";
  };

  outputs = { self, nixpkgs }: {
    packages.x86_64-linux.groimp = 
      let
        pkgs = import nixpkgs {
          system = "x86_64-linux";
          overlays = [ (final: prev: { groimp = final.callPackage ./groimp.nix {}; } ) ];
        };
      in pkgs.groimp;
    
    
    apps.x86_64-linux.groimp = {
      type = "app";
      program = "${self.packages.x86_64-linux.groimp}/bin/groimp";
    };
    
    packages.x86_64-linux.default = self.packages.x86_64-linux.groimp;
    apps.x86_64-linux.default = self.apps.x86_64-linux.groimp;
  };
}
