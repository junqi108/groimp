# GroIMP

## Description

The modelling platform GroIMP is designed as an integrated platform which incorporates modelling, visualisation and interaction. It exhibits several features which makes itself suitable for the field of biological or ALife modelling:

The “modelling backbone” consists in the language XL. It is fully integrated, e.g., the user can interactively select the rules to be applied.
GroIMP provides classes that can be used in modelling: Turtle commands, further geometrical classes like bicubic surfaces, the class Cell which has been used in the Game Of Life implementation, and so on.
The outcome of a model is visualised within GroIMP.

In the visual representation of the model output, users can interact with the dynamics of the model, e.g., by selecting or deleting elements. A networked mode is available, allowing different users to interact with the modelled world synchronously. This may be an interesting feature to be used in the field of e-learning.

Visit the [grogra website](http://grogra.de) for more information on the XL language and the GroIMP development.

## Features

* Interactive editing of scenes
* Rich set of 3D objects, including primitives, NURBS curves and surfaces, and height fields
* Material options like colours, and textures
* Java + L-System gramma support
* Real-time rendering using OpenGL
* Support of several improt and exprot formats
* Build-in raytracing implementations (CPU. and GPU-based)
* Full spectral raytracing (down to 1nm buckets)

## project samples

GroIMP's virtual laser scanner and generated point cloud.  |  Screenshot of GroIMP user interface showing a tree model.
:------------------------------:|:------------------------------:
<img src="https://gitlab.com/grogra/groimp/-/raw/master/images/prj_sample1.jpeg" width="500px">  |  <img src="https://gitlab.com/grogra/groimp/-/raw/master/images/prj_sample2.jpeg" width="500px">

image rendered with raytracer Twilight from within GroIMP.  |  image rendered with raytracer Twilight from within GroIMP.
:------------------------------:|:------------------------------:
<img src="https://gitlab.com/grogra/groimp/-/raw/master/images/prj_sample3.jpeg" width="500px">  |  <img src="https://gitlab.com/grogra/groimp/-/raw/master/images/ong_multiscale.png" width="500px">


## Installation

### Users installation

Download the [last release](https://gitlab.com/grogra/groimp/-/releases) adapted to your distribution. 


### Developpers installation

You can clone the repository on your local device with:

```
git clone https://gitlab.com/grogra/groimp.git
```

Then, you can either compile the project with ant (see [this wiki](https://gitlab.com/grogra/groimp/-/wikis/Making-a-GroIMP-release) for more detailed information),

or, import the project into your IDE (see [this wiki](https://gitlab.com/grogra/groimp/-/wikis/Setup-GPUFlux-for-Eclipse) for integration in Eclipse).


## Documentation

You can see the complete documentation with tutorials in the [wiki pages](https://gitlab.com/grogra/groimp/-/wikis/home).

## Contributing

All contributions, bug reports, bug fixes, documentation improvements, enhancements, and ideas are welcome.
