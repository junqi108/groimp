/**
 * All abstract classes defining interpolation strategies as well as the
 * available implementations are located in this package.
 * 
 * @author Dominick Leppich
 * 
 */
package de.grogra.video.interpolation;