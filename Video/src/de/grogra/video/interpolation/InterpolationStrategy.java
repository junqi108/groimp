package de.grogra.video.interpolation;

import java.io.IOException;
import java.util.List;

import de.grogra.video.model.ImageSequence;
import de.grogra.video.model.ImageSequenceControl;
import de.grogra.video.model.ImageSequenceView;
import de.grogra.video.model.VideoImage;
import de.grogra.video.util.ProgressObservable;

/**
 * This abstract class describes classes which can generate images between all
 * images of an {@link ImageSequence} given by an {@link ImageSequenceView} and
 * an {@link ImageSequenceControl} object.
 * 
 * ImageGeneratorStrategy classes are observable and can update observers with
 * the image generating progress.
 * 
 * @author Dominick Leppich
 *
 */
public abstract class InterpolationStrategy extends ProgressObservable {
	/**
	 * Compute {@code n} images between every two images on a list of images. This
	 * method checks, if the dimensions of the images in the list match.
	 * 
	 * @param images
	 *            - {@link List} of images
	 * @param n
	 *            - number of images to generate between two images
	 * @return {@link List} of generated images
	 * @throws IOException
	 *             if at least two images mismatch in their dimension
	 */
	public final List<VideoImage> generateImages(List<VideoImage> images, int n) throws IOException {
		// Comparing image sizes
		for (int i = 0; i < images.size() - 1; i++)
			if (!images.get(i).getDimension().equals(images.get(1).getDimension()))
				throw new IllegalArgumentException("Image dimension mismatch!");

		return computeImages(images, n);
	}

	/**
	 * Compute {@code n} images between every two images on the list.
	 * 
	 * Observers have to be notified after each generated image with a call of
	 * {@link #setProgress}.
	 * 
	 * @param a
	 *            - Start image
	 * @param b
	 *            - End image
	 * @param n
	 *            - number of images to generate
	 * @return List of generated images
	 */
	protected abstract List<VideoImage> computeImages(List<VideoImage> images, int n) throws IOException;

	/**
	 * Get the name of the image generating strategy instance.
	 * 
	 * @return Name
	 */
	public abstract String getName();

	/**
	 * The {@link #toString()} method returns the name of the image generating
	 * strategy in order to correctly display it on the user interface.
	 * 
	 * @return Name of the image provider
	 */
	public final String toString() {
		return getName();
	}
}
