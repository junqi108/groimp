package de.grogra.video.test;

/**
 * The {@code HeapMonitor} can be used to measure the heap usage of the java
 * application. To program lifetime there can only be one instance of this
 * class, which can be created using the {@link #instance()} method. To make a
 * measure you have to call the {@link #measure()} method. To print the results,
 * just print the {@code HeapMonitor} object or directly call the
 * {@link #toString()} method. The output format can be set using the
 * {@link #setFormat(Format)} method.
 * 
 * @author Dominick Leppich
 *
 */
public class HeapMonitor {
	/**
	 * Enumeration of available output formats of the {@link HeapMonitor}.
	 * 
	 * @author Dominick Leppich
	 *
	 */
	public enum Format {
		RAW, HUMAN_READABLE;
	}

	// ------------------------------------------------------------

	private static HeapMonitor instance;

	private Runtime runtime;
	private long maxJavaSpace;
	private long maxSpace;
	private long space;

	private Format format = Format.HUMAN_READABLE;

	// ------------------------------------------------------------

	/**
	 * Default ctr.
	 */
	private HeapMonitor() {
		runtime = Runtime.getRuntime();
		maxJavaSpace = runtime.maxMemory();
		maxSpace = runtime.totalMemory();
	}

	/**
	 * Get (and create on first call) an instance of the {@link HeapMonitor} class.
	 * 
	 * @return A {@link HeapMonitor} instance
	 */
	public static synchronized HeapMonitor instance() {
		if (instance == null)
			instance = new HeapMonitor();
		return instance;
	}

	// ------------------------------------------------------------

	/**
	 * Set the output format of the monitor.
	 * 
	 * @param format
	 *            - Output format as {@link Format} enumeration
	 */
	public void setFormat(Format format) {
		this.format = format;
	}

	// ------------------------------------------------------------

	/**
	 * Measure the current heap usage.
	 */
	public void measure() {
		maxJavaSpace = runtime.maxMemory();
		maxSpace = runtime.totalMemory();
		space = maxSpace - runtime.freeMemory();
	}

	/**
	 * Get a string representation of the last measured result in the defined
	 * format.
	 * 
	 * @return Result of the last measurement
	 */
	public String toString() {
		if (format == Format.HUMAN_READABLE)
			return "Heap measured:   S = " + humanReadable(space) + "   CM = " + humanReadable(maxSpace) + "   M = "
					+ humanReadable(maxJavaSpace) + "   S/CM = "
					+ String.format("%.2f", ((double) 100 * space / maxSpace)) + "%";
		else
			return "" + space + ";" + maxSpace + ";" + maxJavaSpace;
	}

	// ------------------------------------------------------------

	/**
	 * Create a human readable string representation of memory usage.
	 * 
	 * @param bytes
	 *            - Bytes
	 * @return Human readalbe string representing an amount of bytes
	 */
	public static String humanReadable(long bytes) {
		String[] units = new String[] { "B", "KB", "MB", "GB" };
		long pre = bytes;
		long post = 0L;
		int i = 0;

		while (pre > 999L) {
			post = pre % 1000L;
			pre /= 1000L;
			i++;
		}

		return String.format("%3d.%03d ", pre, post) + units[i];
	}
}
