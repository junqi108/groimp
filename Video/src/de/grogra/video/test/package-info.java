/**
 * All test implementations of interfaces of this plugin are located in this
 * test package. The classes {@link HeapMonitor} and {@link TimeStopper}, which
 * are used for benchmarks, belong to this package as well.
 * 
 * @author Dominick Leppich
 * 
 */
package de.grogra.video.test;