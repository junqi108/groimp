package de.grogra.video.test;

import java.io.File;

import javax.swing.JFrame;

import de.grogra.video.ui.VideoPanel;

/**
 * This test class creates a {@link VideoPanel} with a {@link TestConnector} and
 * showing the panel in a swing {@link JFrame} using the {@link TestFrame}. The
 * {@link TestConnector} loads files from the directory provided by the first
 * program argument.
 * 
 * @author Dominick Leppich
 *
 */
public class VideoPanelTest {
	public static void main(String[] args) {
		TestFrame tf = new TestFrame(new VideoPanel(new TestConnector(new File(args[0]))));
		tf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}
