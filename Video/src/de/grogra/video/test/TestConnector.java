package de.grogra.video.test;

import java.awt.Dimension;
import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.imageio.ImageIO;

import de.grogra.video.connector.VideoPluginConnector;
import de.grogra.video.model.VideoImage;
import de.grogra.video.render.ImageProvider;
import de.grogra.video.simulation.SimulationMethod;

/**
 * Simple test connector class, which loads images from disk instead of
 * rendering them.
 * 
 * @author Dominick Leppich
 *
 */
public class TestConnector implements VideoPluginConnector {
	private int index;
	private List<File> files;

	// ------------------------------------------------------------

	/**
	 * Create a {@code TestConnector} which can read files from the directory
	 * provided as argument.
	 * 
	 * @param path
	 *            - Directory to read files from
	 */
	public TestConnector(File path) {
		index = 0;
		if (path.exists() && path.isDirectory()) {
			File[] fileArray = path.listFiles();
			files = new ArrayList<>(fileArray.length);
			for (File f : fileArray)
				files.add(f);
			Collections.sort(files);
		}
	}

	// ------------------------------------------------------------

	/**
	 * <p>
	 * {@inheritDoc}
	 * </p>
	 * 
	 * <p>
	 * The only provided {@link ImageProvider} reads images from the directory
	 * defined at object creation time. This {@link ImageProvider} doesn't check if
	 * the file is an image and fails, if it's not.
	 * </p>
	 */
	@Override
	public List<ImageProvider> getImageProviders() {
		ArrayList<ImageProvider> list = new ArrayList<>();
		list.add(new ImageProvider() {
			@Override
			public VideoImage createImage(Dimension dimension) {
				try {
					if (files == null)
						return null;
					File file = files.get(index % files.size());
					return new VideoImage(ImageIO.read(file).getScaledInstance(dimension.width, dimension.height,
							Image.SCALE_SMOOTH));
				} catch (IOException e) {
					e.printStackTrace();
				}
				return null;
			}

			@Override
			public String getName() {
				return "ImageFromFileLoader";
			}

		});
		return list;
	}

	/**
	 * <p>
	 * {@inheritDoc}
	 * </p>
	 * 
	 * <p>
	 * The only provided {@link SimulationMethod} let the {@link TestConnector} go
	 * to the next file on the next call of the
	 * {@link ImageProvider#createImage(Dimension)} method.
	 * </p>
	 */
	@Override
	public List<SimulationMethod> getSimulationMethods() {
		ArrayList<SimulationMethod> list = new ArrayList<>();
		list.add(new SimulationMethod() {

			@Override
			public String getName() {
				return "GoToNextFile";
			}

			@Override
			public void execute() {
				index++;
			}
		});
		return list;
	}
}
