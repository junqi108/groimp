/**
 * This package defines the {@link VideoPluginConnector} interface and its
 * GroIMP implementation {@link GroIMPConnector}.
 * 
 * @author Dominick Leppich
 * 
 */
package de.grogra.video.connector;