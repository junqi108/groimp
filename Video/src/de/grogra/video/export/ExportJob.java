package de.grogra.video.export;

import java.io.File;
import java.io.IOException;
import java.util.Observable;
import java.util.Observer;

import de.grogra.video.VideoPlugin;
import de.grogra.video.model.ImageSequenceView;
import de.grogra.video.util.Job;
import de.grogra.video.util.Progress;

/**
 * The {@code ExportJob} encapsulates the export of the video into a {@link Job}
 * in order to be processed by a {@link Worker}.
 * 
 * @author Dominick Leppich
 *
 */
public class ExportJob extends Job implements Observer {
	private ImageSequenceView view;
	private VideoExporter exporter;
	private VideoSettings settings;
	private File file;

	// ------------------------------------------------------------

	/**
	 * Ctr.
	 * 
	 * @param view
	 *            - {@link ImageSequenceView} of the {@link ImageSequence} to export
	 * @param exporter
	 *            - {@link VideoExporter} to use
	 * @param settings
	 *            - {@link VideoSettings} of the video
	 * @param file
	 *            - Output file
	 */
	public ExportJob(ImageSequenceView view, VideoExporter exporter, VideoSettings settings, File file) {
		this.view = view;
		this.exporter = exporter;
		this.settings = settings;
		this.file = file;
	}

	// ------------------------------------------------------------

	@Override
	public void process() {
		// observe the progress of the exporter
		exporter.addObserver(this);

		// do the concrete export
		try {
			exporter.createVideo(view, settings, file);
		} catch (IOException e) {
			VideoPlugin.handleError(e);
		}

		// stop observing the exporter
		exporter.deleteObserver(this);
	}

	// ------------------------------------------------------------

	/**
	 * <p>
	 * {@inheritDoc}
	 * </p>
	 * 
	 * <p>
	 * The progress of the {@link VideoExporter} is passed to the observing object
	 * of this job.
	 * </p>
	 */
	@Override
	public void update(Observable o, Object arg) {
		if (o instanceof VideoExporter && arg instanceof Progress) {
			Progress p = (Progress) arg;
			setProgress(p);
		}
	}
}
