package de.grogra.video.export;

import java.util.LinkedList;
import java.util.List;

/**
 * This is a data class for video file formats. Each format has a name and an
 * arbitrary list of file extensions.
 * 
 * @author Dominick Leppich
 *
 */
public class FileFormat {
	private String name;
	private List<String> extensions;

	// ------------------------------------------------------------

	/**
	 * Ctr with {@link List} of file extensions.
	 * 
	 * @param name
	 *            - Name of the file format
	 * @param extensions
	 *            - {@link List} of file extensions
	 */
	public FileFormat(String name, List<String> extensions) {
		this.name = name;
		this.extensions = extensions;
	}

	/**
	 * Ctr with an array or arbitrary number of arguments of file extensions.
	 * 
	 * @param name
	 *            - Name of the file format
	 * @param extensions
	 *            - Arguments of file extensions
	 */
	public FileFormat(String name, String... extensions) {
		this.name = name;
		this.extensions = new LinkedList<>();
		for (String ex : extensions)
			this.extensions.add(ex);
	}

	// ------------------------------------------------------------

	/**
	 * Return the name of the file format.
	 * 
	 * @return Name of the file format
	 */
	public String getName() {
		return name;
	}

	/**
	 * Return the {@link List} of file extensions.
	 * 
	 * @return {@link List} of file extensions
	 */
	public List<String> getExtensions() {
		return extensions;
	}

	// ------------------------------------------------------------

	@Override
	public String toString() {
		String s = name;
		for (int i = 0; i < extensions.size(); i++) {
			if (i == 0)
				s += ": ";
			s += extensions.get(i);
			if (i < extensions.size() - 1)
				s += ",";
		}
		return s;
	}
}
