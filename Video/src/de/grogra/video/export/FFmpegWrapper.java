package de.grogra.video.export;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.grogra.video.VideoPlugin;
import de.grogra.video.model.ImageSequenceView;
import de.grogra.video.util.Progress;

/**
 * A Java wrapper for the FFmpeg tool. Different operating systems will use
 * different binaries of this tool. The progress of the external tool is
 * observable.
 * 
 * @author Dominick Leppich
 *
 */
public class FFmpegWrapper extends VideoExporter {
	public static final File FFMPEG_LINUX = new File("../Video/ffmpeg/linux/ffmpeg");
	public static final File FFMPEG_WIN = new File("../Video/ffmpeg/win/bin/ffmpeg.exe");
	public static final File FFMPEG_MAC_64 = new File("../Video/ffmpeg/osx/ffmpeg");

	// ------------------------------------------------------------

	@Override
	public void createVideo(final ImageSequenceView view, final VideoSettings settings, final File outputFile)
			throws IOException {

		// Delete output file if existing
		if (outputFile.exists())
			outputFile.delete();

		// Reorder
		view.reorder();

		// Call FFMPEG
		callFFMPEG(view, settings, outputFile);
	}

	/**
	 * Generate the correct FFmpeg call for the running operating system and the
	 * given parameters.
	 * 
	 * @param view
	 *            - {@link ImageSequenceView} to the {@link ImageSequence} to export
	 * @param settings
	 *            - {@link VideoSettings}
	 * @param file
	 *            - Output file
	 * @return Command line FFmpeg call
	 * @throws IOException
	 *             if the running operating system is not supported
	 */
	private String generateFFMPEGCommand(ImageSequenceView view, VideoSettings settings, File file) throws IOException {
		return getFFMPEGPath() + " -framerate " + settings.getInputFps() + " -i " + view.getRegex() + " -r "
				+ settings.getOutputFps() + " " + file.getAbsolutePath();
	}

	/**
	 * Call the FFmpeg command line tool, create the video and update the observers
	 * progress.
	 * 
	 * @param view
	 *            - {@link ImageSequenceView} to the {@link ImageSequence} to export
	 * @param settings
	 *            - {@link VideoSettings}
	 * @param outputFile
	 *            - Output {@link File}
	 * @throws IOException
	 *             if the running operating system is not supported
	 */
	private void callFFMPEG(ImageSequenceView view, VideoSettings settings, File outputFile) throws IOException {
		String command = generateFFMPEGCommand(view, settings, outputFile);

		// Regex pattern to find progress output of the ffmpeg process
		final Pattern p = Pattern.compile("frame=\\s*\\d+");

		Runtime rt = Runtime.getRuntime();

		// Determine max number of frames
		int n = (int) (view.size() * settings.getOutputFps() / settings.getInputFps());

		// Reset progress
		setProgress(new Progress(0.0, "Exporting Video: FFMPEG"));

		// Call FFmpeg with correct arguments
		try {
			final Process pr = rt.exec(command);

			BufferedReader input = new BufferedReader(new InputStreamReader(pr.getErrorStream()));
			String line = null;

			while ((line = input.readLine()) != null) {
				System.err.println(line);
				Matcher m = p.matcher(line);
				// When match found, update progress
				while (m.find()) {
					int frame = Integer.parseInt(m.group().replaceAll("frame=\\s*", ""));
					setProgress(new Progress((double) frame / n, "Exporting Video: FFMPEG"));
				}
			}

			// Wait for process to end
			pr.waitFor();

			// .. and set progress to 100%
			setProgress(new Progress(1.0, "Exporting Video: FFMPEG"));
		} catch (IOException e) {
			VideoPlugin.handleError(e);
		} catch (InterruptedException e) {
			VideoPlugin.handleError(e);
		}
	}

	/**
	 * <p>
	 * {@inheritDoc}
	 * </p>
	 * 
	 * <p>
	 * In future FFmpeg can be queried to get all possible formats.
	 * </p>
	 */
	@Override
	public List<FileFormat> getFileFormats() {
		List<FileFormat> formats = new LinkedList<>();

		// Add two common formats, which are available in FFmpeg
		formats.add(new FileFormat("AVI (Audio Video Interleaved)", "avi"));
		formats.add(new FileFormat("MP4 (MPEG-4 Part 14)", "mp4"));

		return formats;
	}

	// ------------------------------------------------------------

	/**
	 * Determine the correct FFmpeg path for the running operating system.
	 * 
	 * @return The path to the FFmpeg binary for the running operating system
	 * @throws IOException
	 */
	public static String getFFMPEGPath() throws IOException {
		String os = System.getProperty("os.name").toLowerCase();
		if (os.contains("nix") || os.contains("nux") || os.contains("aix"))
			return FFMPEG_LINUX.getAbsolutePath();
		if (os.contains("win"))
			return FFMPEG_WIN.getAbsolutePath();
		if (os.contains("mac"))
			return FFMPEG_MAC_64.getAbsolutePath();

		throw new IOException("Unsupported operating system");
	}
}
