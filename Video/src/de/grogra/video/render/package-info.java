/**
 * This package contains all classes which are used in the context of image
 * creation.
 * 
 * @author Dominick Leppich
 * 
 */
package de.grogra.video.render;