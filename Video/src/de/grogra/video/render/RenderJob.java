package de.grogra.video.render;

import java.awt.Dimension;
import java.io.IOException;

import de.grogra.video.model.ImageSequence;
import de.grogra.video.model.ImageSequenceControl;
import de.grogra.video.util.Job;
import de.grogra.video.util.Progress;
import de.grogra.video.util.Worker;

/**
 * The {@code RenderJob} encapsulates the automatic simulation and rendering of
 * images into a {@link Job} in order to be processed by a {@link Worker}.
 * 
 * @author Dominick Leppich
 *
 */
public class RenderJob extends Job {
	private ImageProvider provider;
	private Dimension dimension;
	private ImageSequenceControl control;

	// ------------------------------------------------------------

	/**
	 * Ctr.
	 * 
	 * @param provider
	 *            - {@link ImageProvider} (Source of image creation)
	 * @param dimension
	 *            - {@link Dimension} of the image
	 * @param control
	 *            - {@link ImageSequenceControl} to the {@link ImageSequence}, where
	 *            the images will be added
	 */
	public RenderJob(ImageProvider provider, Dimension dimension, ImageSequenceControl control) {
		this.provider = provider;
		this.dimension = dimension;
		this.control = control;
	}

	// ------------------------------------------------------------

	@Override
	public void process() throws IOException, InterruptedException {
		control.add(provider.createImage(dimension));
		setProgress(new Progress(1.0, "Render: (" + provider.getName() + ", " + dimension + ")"));
	}
}
