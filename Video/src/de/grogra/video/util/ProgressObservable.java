package de.grogra.video.util;

import java.util.Observable;

/**
 * A {@code ProgressObservable} is just an {@link Observable} object, with an
 * additional method {@link #setProgress(Progress)} to make the use easier.
 * 
 * @author Dominick Leppich
 *
 */
public class ProgressObservable extends Observable {
	/**
	 * Set the objects progress and automatically notify all its observers.
	 * 
	 * @param progress
	 *            - New {@link Progress}
	 */
	protected final void setProgress(Progress progress) {
		setChanged();
		notifyObservers(progress);
	}
}
