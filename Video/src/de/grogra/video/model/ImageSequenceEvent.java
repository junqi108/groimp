package de.grogra.video.model;

/**
 * TODO
 * 
 * @author Dominick Leppich
 *
 */
public class ImageSequenceEvent {
	public static final int IMAGE_ADDED = 0;
	public static final int IMAGE_CHANGED = 1;
	public static final int IMAGE_REMOVED = 2;
	
	private int type;
	private int index;
	private int newSize;
	
	// ----
	
	public ImageSequenceEvent(int type, int index, int newSize) {
		this.type = type;
		this.index = index;
		this.newSize = newSize;
	}
	
	// ----
	
	public int getType() {
		return type;
	}
	
	public int getIndex() {
		return index;
	}
	
	public int getNewSize() {
		return newSize;
	}
}
