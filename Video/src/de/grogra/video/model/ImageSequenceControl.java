package de.grogra.video.model;

/**
 * Controller for an {@link ImageSequence} which grants write only access to the
 * sequence.
 * 
 * @author Dominick Leppich
 *
 */
public interface ImageSequenceControl {
	/**
	 * Add a {@link VideoImage} at the end of the sequence.
	 * 
	 * @param image
	 *            - {@link VideoImage}
	 */
	void add(VideoImage image);

	/**
	 * Add a {@link VideoImage} at a specified position in the sequence.
	 * 
	 * @param index
	 *            - Index to insert the image
	 * @param image
	 *            - {@link VideoImage}
	 */
	void add(int index, VideoImage image);

	/**
	 * Clear the sequence of images.
	 */
	void clear();

	/**
	 * Remove the image at the specified position.
	 * 
	 * @param index
	 *            - Index of the image to remove
	 * @return The {@link VideoImage} which was removed
	 */
	VideoImage remove(int index);
}
