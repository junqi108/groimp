package de.grogra.video.model;

import java.awt.Dimension;
import java.util.List;

/**
 * Implements an {@link ImageSequenceView} which grants read only access to the
 * sequence given by objects creation by simply passing all method calls to the
 * sequence.
 * 
 * @author Dominick Leppich
 *
 */
public class ImageSequenceViewer implements ImageSequenceView {
	private ImageSequence imageSequence;
	
	// ------------------------------------------------------------

	ImageSequenceViewer(ImageSequence imageSequence) {
		this.imageSequence = imageSequence;
	}

	@Override
	public boolean contains(Object arg0) {
		return imageSequence.contains(arg0);
	}

	@Override
	public VideoImage get(int arg0) {
		return imageSequence.get(arg0);
	}

	@Override
	public List<VideoImage> getList() {
		return imageSequence.getList();
	}

	@Override
	public boolean isEmpty() {
		return imageSequence.isEmpty();
	}

	@Override
	public int size() {
		return imageSequence.size();
	}

	@Override
	public Dimension getDimension() {
		return imageSequence.getDimension();
	}
	
	@Override
	public void reorder() {
		imageSequence.reorder();
	}

	@Override
	public String getRegex() {
		return imageSequence.getRegex();
	}
}
