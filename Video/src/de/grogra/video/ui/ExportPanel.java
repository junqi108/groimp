package de.grogra.video.ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.HashSet;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileNameExtensionFilter;

import de.grogra.video.VideoPlugin;
import de.grogra.video.export.ExportJob;
import de.grogra.video.export.FileFormat;
import de.grogra.video.export.VideoExporter;
import de.grogra.video.export.VideoSettings;
import de.grogra.video.model.ImageSequence;
import de.grogra.video.model.ImageSequenceView;
import de.grogra.video.util.Worker;

/**
 * This panel controls the video generation process
 * 
 * @author Dominick Leppich
 *
 */
public class ExportPanel extends JPanel implements Observer, InteractionPanel {
	private static final long serialVersionUID = 1L;

	private Worker worker;

	private ImageSequenceView view;
	private VideoExporter exporter;
	private SpinnerNumberModel inputFramesModel;
	private JTextField fileName;
	private JButton pathButton;
	private JButton applyPreviewFpsButton;
	private JLabel videoLength;
	private VideoSettings settings;
	private JButton advancedButton;
	private JButton generateButton;

	private boolean inputEnabled;

	private Set<JComponent> components;

	// ------------------------------------------------------------

	/**
	 * Create the {@code ExportPanel} and position all user interface elements on
	 * the panel.
	 * 
	 * @param worker
	 *            - {@link Worker} to use for the export
	 * @param slideshow
	 *            - Reference to the {@link PreviewPanel} to get the current speed
	 * @param view
	 *            - {@link ImageSequenceView} to get reading access to the
	 *            {@link ImageSequence}
	 */
	public ExportPanel(Worker worker, final PreviewPanel slideshow, final ImageSequenceView view) {
		/*
		 * Creating swing objects and positioning them on the panel using some layout as
		 * well as adding some simple listeners to them won't be explained in detail.
		 */
		this.worker = worker;
		this.view = view;

		components = new HashSet<>();

		exporter = VideoPlugin.createDefaultExporter();
		settings = new VideoSettings();

		setBorder(BorderFactory.createTitledBorder("Video generation"));

		Box vBox = Box.createVerticalBox();

		Box pathBox = Box.createHorizontalBox();
		JLabel pathLabel = new JLabel("Path:");
		components.add(pathLabel);
		pathBox.add(pathLabel);
		pathBox.add(Box.createHorizontalStrut(5));
		fileName = new JTextField(System.getProperty("user.home") + File.separator + "video.mp4");
		components.add(fileName);
		pathBox.add(fileName);
		pathBox.add(Box.createHorizontalStrut(5));
		final JFileChooser savePathChooser = new JFileChooser();

		// Add file filters
		for (FileFormat ff : exporter.getFileFormats()) {
			List<String> extList = ff.getExtensions();
			String[] ext = new String[extList.size()];
			extList.toArray(ext);
			savePathChooser.setFileFilter(new FileNameExtensionFilter(ff.getName(), ext));
		}
		pathButton = new JButton(new ImageIcon("../Platform/build/icons/16x16/filesystems/folder_open.png"));
		pathButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int rVal = savePathChooser.showSaveDialog(ExportPanel.this);
				if (rVal == JFileChooser.APPROVE_OPTION) {
					fileName.setText(savePathChooser.getSelectedFile().getAbsolutePath());
				}
			}
		});
		components.add(pathButton);
		pathBox.add(pathButton);
		vBox.add(pathBox);

		vBox.add(Box.createVerticalStrut(5));

		Box fpsBox = Box.createHorizontalBox();
		vBox.add(fpsBox);
		JLabel fpsLabel = new JLabel("FPS:");
		components.add(fpsLabel);
		fpsBox.add(fpsLabel);
		fpsBox.add(Box.createHorizontalStrut(5));
		inputFramesModel = new SpinnerNumberModel(VideoSettings.FPS_INIT, VideoSettings.FPS_MIN, VideoSettings.FPS_MAX,
				1);
		JSpinner inputFramesSpinner = new JSpinner(inputFramesModel);
		fpsBox.add(inputFramesSpinner);
		components.add(inputFramesSpinner);
		fpsBox.add(Box.createHorizontalStrut(5));
		applyPreviewFpsButton = new JButton("Apply Preview Speed");
		applyPreviewFpsButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				inputFramesModel.setValue(slideshow.getPreviewFps());
			}
		});
		components.add(applyPreviewFpsButton);
		fpsBox.add(applyPreviewFpsButton);
		fpsBox.add(Box.createHorizontalStrut(5));
		JLabel videoLengthLabel = new JLabel("Video Length:");
		components.add(videoLengthLabel);
		fpsBox.add(videoLengthLabel);
		fpsBox.add(Box.createHorizontalStrut(5));
		videoLength = new JLabel("   --:--:--   ");
		components.add(videoLength);
		videoLength.setBorder(BorderFactory.createLoweredBevelBorder());
		videoLength.setHorizontalAlignment(JLabel.CENTER);
		inputFramesModel.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				ExportPanel.this.settings.setInputFps(inputFramesModel.getNumber().intValue());
				recalculateVideoLength();
			}
		});
		fpsBox.add(videoLength);

		vBox.add(Box.createVerticalStrut(5));

		Box lastBox = Box.createHorizontalBox();
		advancedButton = new JButton("Advanced...");
		advancedButton.setIcon(new ImageIcon("../Platform/build/icons/16x16/actions/configure.png"));
		advancedButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				new AdvancedSettings(ExportPanel.this, settings).setVisible(true);
			}
		});
		components.add(advancedButton);
		lastBox.add(advancedButton);
		lastBox.add(Box.createHorizontalStrut(5));
		generateButton = new JButton("Generate Video");
		generateButton.setIcon(new ImageIcon("../Platform/build/icons/16x16/actions/filesave.png"));
		generateButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				generateVideo();
			}
		});
		components.add(generateButton);
		lastBox.add(generateButton);
		vBox.add(lastBox);

		inputEnabled = false;
		setInteractionEnabled(inputEnabled);

		add(vBox);
	}

	// ------------------------------------------------------------

	/**
	 * Let the video generation begin by creating an {@link ExportJob} and pass it
	 * to the {@link Worker}.
	 */
	private void generateVideo() {
		worker.addJob(new ExportJob(view, exporter, settings, new File(fileName.getText())));
	}

	/**
	 * Update the video length and display it.
	 */
	private void recalculateVideoLength() {
		if (view.isEmpty())
			videoLength.setText("   --:--:--   ");
		else {
			int h, m, s;
			int t = view.size() / inputFramesModel.getNumber().intValue();
			s = t % 60;
			t /= 60;
			m = t % 60;
			t /= 60;
			h = t;
			videoLength.setText("   " + String.format("%02d", h) + ":" + String.format("%02d", m) + ":"
					+ String.format("%02d", s) + "   ");
		}
	}

	/**
	 * Recalculate the video length if the image sequence changes. Also update the
	 * interaction status.
	 */
	@Override
	public void update(Observable o, Object arg) {
		if (o instanceof ImageSequence) {
			recalculateVideoLength();
			setInteractionEnabled(inputEnabled);
		}
	}

	@Override
	public void setInteractionEnabled(boolean enabled) {
		inputEnabled = enabled && !view.isEmpty();
		this.setEnabled(inputEnabled);
		for (JComponent c : components)
			c.setEnabled(inputEnabled);
	}
}
