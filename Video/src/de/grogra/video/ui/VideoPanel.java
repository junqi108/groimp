package de.grogra.video.ui;

import java.awt.Dimension;
import java.util.HashSet;
import java.util.Set;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JPanel;

import de.grogra.video.connector.GroIMPConnector;
import de.grogra.video.connector.VideoPluginConnector;
import de.grogra.video.model.ImageSequence;
import de.grogra.video.model.ImageSequenceControl;
import de.grogra.video.model.ImageSequenceView;
import de.grogra.video.util.Job;
import de.grogra.video.util.JobListener;
import de.grogra.video.util.Worker;

/**
 * The {@code VideoPanel} is the graphical user interface of the video export
 * plugin. All features of the plugin are located in different panels, which are
 * lying on this main panel.
 * 
 * @author Dominick Leppich
 *
 */
public class VideoPanel extends JPanel implements InteractionPanel {
	private static final long serialVersionUID = 1L;

	public static final Dimension IMAGE_PREVIEW_SIZE = new Dimension(640, 360);
	public static final int PANEL_SPACER = 10;

	/**
	 * The {@link ImageSequence}
	 */
	private ImageSequence imageSequence;

	private PreviewPanel previewPanel;
	private RenderPanel renderPanel;
	private InterpolationPanel interpolationPanel;
	private ExportPanel exportPanel;
	private ProgressPanel progressPanel;

	private Set<InteractionPanel> panels;

	// ------------------------------------------------------------

	/**
	 * Default ctr. All sub panels are added to this panel inside the constructor.
	 */
	public VideoPanel(VideoPluginConnector connector) {
		/*
		 * Creating swing objects and positioning them on the panel using some layout as
		 * well as adding some simple listeners to them won't be explained in detail.
		 */
		panels = new HashSet<>();

		Worker worker = Worker.instance();

		imageSequence = new ImageSequence();
		ImageSequenceView view = imageSequence.view();
		ImageSequenceControl control = imageSequence.control();

		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

		previewPanel = new PreviewPanel(view, control, IMAGE_PREVIEW_SIZE.width, IMAGE_PREVIEW_SIZE.height);
		imageSequence.addObserver(previewPanel);
		add(previewPanel);

		add(Box.createVerticalStrut(PANEL_SPACER));
		renderPanel = new RenderPanel(worker, connector, view, control);
		imageSequence.addObserver(renderPanel);
		add(renderPanel);
		add(Box.createVerticalStrut(PANEL_SPACER));
		interpolationPanel = new InterpolationPanel(worker, view, control);
		imageSequence.addObserver(interpolationPanel);
		add(interpolationPanel);
		add(Box.createVerticalStrut(PANEL_SPACER));
		exportPanel = new ExportPanel(worker, previewPanel, view);
		imageSequence.addObserver(exportPanel);
		add(exportPanel);
		add(Box.createVerticalStrut(PANEL_SPACER));
		progressPanel = new ProgressPanel(((GroIMPConnector) connector).getWorkbench());
		progressPanel.setInteractionEnabled(false);
		add(progressPanel);

		panels.add(previewPanel);
		panels.add(renderPanel);
		panels.add(interpolationPanel);
		panels.add(exportPanel);

		worker.addJobListener(new JobListener() {

			@Override
			public void startJob(Job job) {
				job.addObserver(progressPanel);
			}

			@Override
			public void finishedWork() {
			}

			@Override
			public void endJob(Job job) {
				job.deleteObserver(progressPanel);
			}
		});
		
		
		worker.addJobListener(new JobListener() {

			@Override
			public void startJob(Job job) {
				setInteractionEnabled(false);
				progressPanel.setInteractionEnabled(true);
			}

			@Override
			public void finishedWork() {
				setInteractionEnabled(true);
				progressPanel.setInteractionEnabled(false);
			}

			@Override
			public void endJob(Job job) {
			}
		});
	}

	/**
	 * This method is called just before this panel is closed. Before this could
	 * happen, all observers to the {@link ImageSequence} are removed.
	 */
	public void dispose() {
		imageSequence.deleteObserver(previewPanel);
		imageSequence.deleteObserver(renderPanel);
		imageSequence.deleteObserver(interpolationPanel);
		imageSequence.deleteObserver(exportPanel);
	}

	@Override
	public void setInteractionEnabled(boolean enabled) {
		for (InteractionPanel p : panels)
			p.setInteractionEnabled(enabled);
	}
}
