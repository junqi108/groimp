package de.grogra.video.ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;

import de.grogra.pf.ui.Workbench;
import de.grogra.video.util.Job;
import de.grogra.video.util.Progress;
import de.grogra.video.util.Worker;

/**
 * The {@code ProgressPanel} shows the progress of the job currently processed.
 * Running {@link Job}s can be canceled.
 * 
 * @author Dominick Leppich
 *
 */
public class ProgressPanel extends JPanel implements Observer, InteractionPanel {
	private static final long serialVersionUID = 1L;

	private JLabel label;
	private JProgressBar progressBar;
	private JButton cancelButton;
	
	private Workbench workbench;

	// ------------------------------------------------------------

	/**
	 * Ctr.
	 */
	public ProgressPanel(Workbench workbench) {
		this.workbench = workbench;
		/*
		 * Creating swing objects and positioning them on the panel using some layout as
		 * well as adding some simple listeners to them won't be explained in detail.
		 */
		setBorder(BorderFactory.createTitledBorder("Progress"));

		Box hBox = Box.createHorizontalBox();

		label = new JLabel();
		hBox.add(label);
		hBox.add(Box.createHorizontalStrut(5));
		progressBar = new JProgressBar(0, 100);
		progressBar.setStringPainted(true);
		hBox.add(progressBar);
		hBox.add(Box.createHorizontalStrut(5));
		cancelButton = new JButton(new ImageIcon("../Video/build/icons/16x16/cancel.png"));
		cancelButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					Worker.instance().cancel();
				} catch (InterruptedException e1) {
					return;
				}
			}
		});
		hBox.add(cancelButton);
		add(hBox);
	}

	// ------------------------------------------------------------

	/**
	 * Update the progress bar, if it is updated by a {@link Job}.
	 */
	@Override
	public void update(Observable o, Object arg) {
		if (o instanceof Job && arg instanceof Progress) {
			Progress p = (Progress) arg;
			label.setText(p.getName());
			progressBar.setValue((int) (100 * p.getValue()));
			repaint();
		}
	}

	@Override
	public void setInteractionEnabled(boolean enabled) {
		// Just en- or disable all components
		this.setEnabled(enabled);
		label.setEnabled(enabled);
		progressBar.setEnabled(enabled);
		cancelButton.setEnabled(enabled);
	}
}
