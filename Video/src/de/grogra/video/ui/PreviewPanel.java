package de.grogra.video.ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.io.IOException;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import de.grogra.video.VideoPlugin;
import de.grogra.video.export.VideoSettings;
import de.grogra.video.model.ImageSequence;
import de.grogra.video.model.ImageSequenceControl;
import de.grogra.video.model.ImageSequenceEvent;
import de.grogra.video.model.ImageSequenceView;

/**
 * The {@code PreviewPanel} displays all collected images. It is possible to
 * navigate through the images and delete them by one or all together. An
 * animation showing the resulting video can be started.
 * 
 * @author Dominick Leppich
 *
 */
public class PreviewPanel extends JPanel implements Observer, InteractionPanel {
	private static final long serialVersionUID = 1L;

	private ImageSequenceView view;
	private ImageSequenceControl control;
	private ImagePanel imagePanel;
	private ImageIcon playIcon, pauseIcon;
	private JButton firstButton, prevButton, nextButton, lastButton, playPauseButton, stopButton, removeButton,
			clearButton;
	private JLabel sliderLabel;
	private JSlider speedSlider;
	// Set displaying image index to zero (first image)
	private int displayIndex = 0;
	private boolean inputEnabled;

	private AnimationThread animationThread = new AnimationThread(VideoSettings.FPS_INIT);

	// ------------------------------------------------------------

	/**
	 * Ctr.
	 * 
	 * @param view
	 *            - {@link ImageSequenceView} to get reading access to the
	 *            {@link ImageSequence}
	 * @param control-
	 *            {@link ImageSequenceControl} to get writing access to the
	 *            {@link ImageSequence}
	 * @param width
	 *            - Width of the preview image
	 * @param height
	 *            - Height of the preview image
	 */
	public PreviewPanel(ImageSequenceView view, ImageSequenceControl control, int width, int height) {
		// Set view and control objects
		this.view = view;
		this.control = control;

		inputEnabled = true;

		setBorder(BorderFactory.createTitledBorder("Preview"));

		Box vBox = Box.createVerticalBox();

		// Image panel to display the images
		Box imageBox = Box.createHorizontalBox();
		imagePanel = new ImagePanel(width, height);
		imageBox.add(Box.createHorizontalGlue());
		imageBox.add(imagePanel);
		imageBox.add(Box.createHorizontalGlue());
		vBox.add(imageBox);

		vBox.add(Box.createVerticalStrut(10));

		// Add the control buttons
		vBox.add(createControlButtonBox());

		// Add mouse listener
		addMouseWheelListener(new MouseWheelListener() {
			@Override
			public void mouseWheelMoved(MouseWheelEvent arg0) {
				if (arg0.getWheelRotation() == 1)
					onControl(ControlCommand.NEXT_IMAGE);
				else if (arg0.getWheelRotation() == -1)
					onControl(ControlCommand.PREV_IMAGE);
			}
		});

		add(vBox);

		// Initialize button state
		updateControls();
	}

	// ------------------------------------------------------------

	/**
	 * Get the current preview speed (as fps).
	 * 
	 * @return Preview speed in frames per second
	 */
	public int getPreviewFps() {
		return speedSlider.getValue();
	}

	// ------------------------------------------------------------

	/**
	 * Creates all control buttons under the image panel.
	 * 
	 * @return {@link Box} with control buttons
	 */
	private Box createControlButtonBox() {
		Box box = Box.createHorizontalBox();
		// firstButton = new JButton("<<");
		firstButton = new JButton(new ImageIcon("../Video/build/icons/16x16/first.png"));
		// firstButton.setIcon(new ImageIcon());
		firstButton.addActionListener(new ControlActionListener(ControlCommand.FIRST_IMAGE));
		box.add(firstButton);
		// prevButton = new JButton("<");
		prevButton = new JButton(new ImageIcon("../Video/build/icons/16x16/previous.png"));
		prevButton.addActionListener(new ControlActionListener(ControlCommand.PREV_IMAGE));
		box.add(prevButton);
		// nextButton = new JButton(">");
		nextButton = new JButton(new ImageIcon("../Video/build/icons/16x16/next.png"));
		nextButton.addActionListener(new ControlActionListener(ControlCommand.NEXT_IMAGE));
		box.add(nextButton);
		// lastButton = new JButton(">>");
		lastButton = new JButton(new ImageIcon("../Video/build/icons/16x16/last.png"));
		lastButton.addActionListener(new ControlActionListener(ControlCommand.LAST_IMAGE));
		box.add(lastButton);
		box.add(Box.createHorizontalGlue());
		// playButton = new JButton("▶");
		playIcon = new ImageIcon("../Video/build/icons/16x16/play.png");
		pauseIcon = new ImageIcon("../Video/build/icons/16x16/pause.png");
		playPauseButton = new JButton(playIcon);
		// TODO rework
		playPauseButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (animationThread.isRunning())
					onControl(ControlCommand.PAUSE);
				else
					onControl(ControlCommand.PLAY);
			}
		});
		box.add(playPauseButton);
		stopButton = new JButton("■");
		stopButton = new JButton(new ImageIcon("../Video/build/icons/16x16/stop.png"));
		stopButton.addActionListener(new ControlActionListener(ControlCommand.STOP));
		box.add(stopButton);
		box.add(Box.createHorizontalStrut(5));
		sliderLabel = new JLabel("Preview Speed:");
		box.add(sliderLabel);
		box.add(Box.createHorizontalStrut(5));
		speedSlider = new JSlider(JSlider.HORIZONTAL, VideoSettings.FPS_MIN, VideoSettings.FPS_MAX,
				VideoSettings.FPS_INIT);
		speedSlider.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				JSlider source = (JSlider) e.getSource();
				animationThread.setFps(source.getValue());
			}
		});
		speedSlider.setMajorTickSpacing(30);
		speedSlider.setMinorTickSpacing(1);
		speedSlider.setPaintTicks(true);
		box.add(speedSlider);
		box.add(Box.createHorizontalGlue());
		// removeButton = new JButton("X");
		removeButton = new JButton(new ImageIcon("../Video/build/icons/16x16/delete.png"));
		removeButton.addActionListener(new ControlActionListener(ControlCommand.REMOVE_IMAGE));
		box.add(removeButton);
		// clearButton = new JButton("⌫");
		clearButton = new JButton(new ImageIcon("../Video/build/icons/16x16/clear.png"));
		clearButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (JOptionPane.showConfirmDialog(PreviewPanel.this,
						"Are you sure you want to clear ALL collected images?", "VideoPlugin",
						JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
					onControl(ControlCommand.CLEAR_IMAGES);
			}
		});
		box.add(clearButton);
		return box;
	}

	/**
	 * Enables or disables control buttons dependent of the current state.
	 */
	private void updateControls() {
		if (!inputEnabled) {
			firstButton.setEnabled(false);
			prevButton.setEnabled(false);
			nextButton.setEnabled(false);
			lastButton.setEnabled(false);
			removeButton.setEnabled(false);
			clearButton.setEnabled(false);
			playPauseButton.setEnabled(false);
			playPauseButton.setIcon(playIcon);
			stopButton.setEnabled(false);
			sliderLabel.setEnabled(false);
			speedSlider.setEnabled(false);
		} else if (animationThread.isRunning()) {
			firstButton.setEnabled(false);
			prevButton.setEnabled(false);
			nextButton.setEnabled(false);
			lastButton.setEnabled(false);
			removeButton.setEnabled(false);
			clearButton.setEnabled(false);
			playPauseButton.setEnabled(true);
			playPauseButton.setIcon(pauseIcon);
			stopButton.setEnabled(true);
			sliderLabel.setEnabled(false);
			speedSlider.setEnabled(false);
		} else {
			firstButton.setEnabled(!view.isEmpty() && displayIndex > 0);
			prevButton.setEnabled(!view.isEmpty() && displayIndex > 0);
			nextButton.setEnabled(!view.isEmpty() && displayIndex < view.size() - 1);
			lastButton.setEnabled(!view.isEmpty() && displayIndex < view.size() - 1);
			removeButton.setEnabled(!view.isEmpty());
			clearButton.setEnabled(!view.isEmpty());
			playPauseButton.setEnabled(!view.isEmpty());
			playPauseButton.setIcon(playIcon);
			stopButton.setEnabled(false);
			sliderLabel.setEnabled(!view.isEmpty());
			speedSlider.setEnabled(!view.isEmpty());
		}
	}

	/**
	 * Show the image with the provided {@code newIndex} at the preview area.
	 * 
	 * @param newIndex
	 *            - New index of image to show
	 * @return True, if the new index is shown
	 */
	private boolean showPreview(int newIndex) {
		int size = view.size();

		// No image to show
		if (size == 0) {
			// imagePanel.setTitle(null);
			imagePanel.clear();
			return false;
		}

		// Check if index can be changed
		if (newIndex < 0 || newIndex >= size)
			return false;

		displayIndex = newIndex;

		updateControls();
		try {
			imagePanel.draw(view.get(newIndex).getPreviewImage(), displayIndex + 1, size);
		} catch (IndexOutOfBoundsException | IOException e) {
			VideoPlugin.handleError(this, e);
		}

		return true;
	}

	/**
	 * Reaction to user interface control events.
	 * 
	 * @param cmd
	 *            - {@link ControlCommand}
	 */
	public void onControl(ControlCommand cmd) {
		// Mousewheel should not work
		if (!inputEnabled)
			return;

		switch (cmd) {
		case FIRST_IMAGE:
			showPreview(0);
			break;
		case LAST_IMAGE:
			showPreview(view.size() - 1);
			break;
		case PREV_IMAGE:
			showPreview(displayIndex - 1);
			break;
		case NEXT_IMAGE:
			showPreview(displayIndex + 1);
			break;
		case PLAY:
			// Start at beginning if current image is the last one
			if (displayIndex == view.size() - 1)
				showPreview(0);
			animationThread.startAnimation();
			imagePanel.setBorder(true);
			updateControls();
			break;
		case PAUSE:
			animationThread.stopAnimation();
			imagePanel.setBorder(false);
			updateControls();
			break;
		case STOP:
			animationThread.stopAnimation();
			imagePanel.setBorder(false);
			showPreview(0);
			updateControls();
			break;
		case REMOVE_IMAGE:
			control.remove(displayIndex);
			break;
		case CLEAR_IMAGES:
			control.clear();
			break;
		}
	}

	// ----

	/**
	 * Show the newest image, if the newest image was shown before and a new image
	 * is added to the {@link ImageSequence}. Update the control buttons as well.
	 */
	@Override
	public void update(Observable observable, Object obj) {
		// Test if event object was passed
		if (obj != null && obj instanceof ImageSequenceEvent) {
			ImageSequenceEvent event = (ImageSequenceEvent) obj;

			if (event.getType() == ImageSequenceEvent.IMAGE_ADDED) {
				if (displayIndex >= view.size() - 2)
					// Jump to last image automatically
					showPreview(view.size() - 1);
				else
					// Update preview only (for image counter)
					showPreview(displayIndex);
			} else if (event.getType() == ImageSequenceEvent.IMAGE_REMOVED)
				showPreview(event.getIndex() < view.size() ? event.getIndex() : event.getIndex() - 1);

			// Update the buttons at the end
			updateControls();
		}
	}

	@Override
	public void setInteractionEnabled(boolean enabled) {
		// Do command first, because it won't work after inputEnabled is turned off!
		if (!enabled)
			onControl(ControlCommand.PAUSE);
		inputEnabled = enabled;
		this.setEnabled(inputEnabled);
		updateControls();
		if (!enabled)
			onControl(ControlCommand.PAUSE);
	}

	// ------------------------------------------------------------

	/**
	 * Enumeration of controls available for this {@link PreviewPanel}.
	 * 
	 * @author Dominick Leppich
	 *
	 */
	enum ControlCommand {
		NEXT_IMAGE, PREV_IMAGE, FIRST_IMAGE, LAST_IMAGE, REMOVE_IMAGE, CLEAR_IMAGES, PLAY, PAUSE, STOP
	}

	// ------------------------------------------------------------

	/**
	 * Action listener which executes a specific command on action.
	 * 
	 * @author Dominick Leppich
	 *
	 */
	class ControlActionListener implements ActionListener {
		private ControlCommand command;

		public ControlActionListener(ControlCommand command) {
			this.command = command;
		}

		@Override
		public void actionPerformed(ActionEvent arg0) {
			onControl(command);
		}
	}

	// ------------------------------------------------------------

	/**
	 * Thread to animate the preview.
	 * 
	 * @author Dominick Leppich
	 *
	 */
	class AnimationThread extends Thread {
		private boolean running;
		private long startTime, frameTime, endTime;
		private int startIndex, showIndex, maxIndex;

		public AnimationThread(int fps) {
			setFps(fps);
			running = false;
			start();
		}

		@Override
		public synchronized void run() {
			int index;
			long time;

			while (true) {
				try {
					// Go to sleep if animation is not running anymore
					if (!running)
						wait();

					// Timeout before changing image
					Thread.sleep(1);

					time = System.nanoTime();
					
					// Calculate index to show
					index = startIndex + (int) ((time - startTime) / frameTime);

					// Stop if last frame is shown
					if (index == maxIndex || time > endTime)
						onControl(ControlCommand.STOP);

					else if (index != showIndex) {
						showIndex = index;
						showPreview(showIndex);
					}
				} catch (InterruptedException e) {
					VideoPlugin.handleError(e);
				}
			}
		}

		public synchronized void startAnimation() {
			running = true;
			notify();
			startIndex = displayIndex;
			showIndex = startIndex;
			maxIndex = view.size();
			startTime = System.nanoTime();
			endTime = startTime + frameTime * (maxIndex - showIndex);
		}

		public void stopAnimation() {
			running = false;
		}

		private void setFrameTime(long frameTime) {
			// this.timeout = timeout;
			this.frameTime = frameTime;
		}

		public void setFps(int fps) {
			setFrameTime(1000000000L / fps);
		}

		public boolean isRunning() {
			return running;
		}
	}
}
