/*
 * Copyright (C) 2016 GroIMP Developer Team
 *
 * Department Ecoinformatics, Biometrics and Forest Growth,
 * University of Göttingen, Germany
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

package de.grogra.lignum.stlLignum;

/**
 * Translated from orignal C++ Lignum code.
 *
 * @author Michael Henke
 */
public final class ScotsPineParameters extends TreeParameters {

	public ScotsPineParameters() {
		LGPaf = 2.0;
		LGPapical = 0.9;
		LGPar = 0.5;
		LGPlen_random = 0.0;
		LGPLmin = 0.02;
		LGPlr = 0.01;
		LGPmf = 0.24;
		LGPmr = 0.24;
		LGPms = 0.024;
		LGPna = 0.7854;
		LGPnl = 0.03;
		LGPpr = 0.0006;
		LGPq = 0.1;
		LGPrhoW = 200.0;
		LGPsf = 28.0;
		LGPsr = 1.33;
		LGPss = 0.07;
		LGPxi = 0.6;
		LGPzbrentEpsilon = 0.01;
	}

}
