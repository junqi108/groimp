<chapter><title>Using properties</title>

<para>
Each node in GroIMP's graph has a set of <firstterm>properties</firstterm>.
For example, every node has a name. A sphere additionally has a radius,
a location in space, a <firstterm>shader</firstterm> which defines
its appearance, and some other properties.
</para>

<section><title>Initializing properties</title>

<para>
The basic geometric properties
of a geometric node can be set in its constructor: The class
<literal>Sphere</literal> declares the constructor
<literal>Sphere(float radius)</literal> which initializes the
radius with the specified value, for the turtle command
<literal>RU</literal> the constructor
<literal>RU(float radius)</literal> is defined which initializes the
rotation angle. However, there is no constructor in these classes
which initializes the name or (in case of the <literal>Sphere</literal>)
the shader. This is because generally it would not be feasible to provide
constructors for all combinations of properties which the modeller wants
to initialize.
</para>

<para>
Nevertheless, one often wants to initialize properties which are not
accessible through the constructor. For this purpose, the classes define
<firstterm>setter methods</firstterm> like
<literal>setRadius(float radius)</literal> in class
<literal>Sphere</literal> to initialize values of these properties.
These can be invoked immediately after the constructor with the help
of a special syntax (<firstterm>instance scope expressions</firstterm>
of the XL programming language) as in
<programlisting>
Axiom ==> Sphere(1).(setShader(RED), setTransform(0, 0, 1));
</programlisting>
As in this example, a constructor expression
(or in fact any expression) can be followed
by a <literal>"."</literal> and a parenthesized comma-separated list of method
invocations (or in fact statements). Within the parenthesized list,
the result of the constructor is used as the implicit <literal>this</literal>
for the method invocations. The example creates a sphere of radius 1,
sets its shader to the colour red and translates it to the relative location
(0, 0, 1). Such a coloured sphere is used in
<ulink url="command:/rggtutorial/selectfiles?pfs:ColouredObjects.rgg">
this simple example</ulink>, where you should have a look at the
method <literal>init</literal>. The methods <literal>swap</literal>
and <literal>grow</literal> will be discussed in the next sections.
</para>

</section>

<section><title>Reading properties</title>

<para>
Knowing how to initialize properties, the question arises how they can
be accessed. As we already know, a cylinder of length
<replaceable>l</replaceable> and radius
<replaceable>r</replaceable> can be replaced by a cone of
the same length and radius by the rule
<programlisting>
Cylinder(l, r) ==> Cone(l, r);
</programlisting>
In this case, the properties length and radius of the cylinder
can be read quite easily.
But what if we want to set not only these properties,
but also the shader of the cone to
the shader of the cylinder? GroIMP does not provide the possibility
to write a pattern like <literal>Cylinder(l, r, s)</literal> on the
left hand side of a rule where <literal>s</literal> is linked to the
cylinder's shader. As with constructors, providing patterns for all possible
combinations of properties would not be feasible. However, there is
a solution to this problem which reads as follows:
<programlisting>
x:Cylinder(l, r) ==> Cone(l, r).(setShader(x[shader]));
</programlisting>
This rule contains two new features of the XL programming language:
<orderedlist>
  <listitem><para>
    A node pattern on the left hand side can be prefixed by an
    <firstterm>identifier</firstterm> (and an additional colon inbetween)
    in order to name the nodes which match the pattern during rule application.
    This name is just the name of a local variable, so it can be used
    to invoke methods on matching nodes, or to modify their contents.
    The example uses the identifier <literal>x</literal> to name
    the cylinders which match the left hand side.
  </para></listitem>
  <listitem><para>
    Properties can be accessed using a square bracket notation.
    <literal>x[shader]</literal> denotes a
    <firstterm>property variable</firstterm> whose value is the shader
    of cylinder <literal>x</literal>.
  </para></listitem>
</orderedlist>
The rule is integrated in the method <literal>swap</literal> of
<ulink url="command:/rggtutorial/selectfiles?pfs:ColouredObjects.rgg">
this example</ulink>.
<ulink url="command:/workbench/rgg/methods/swap">
Click here</ulink> to see how cylinders and cones are swapped,
thereby adopting size and colour of their predecessor.
</para>

</section>

<section><title>Setting properties</title>

<para>
Now you have learned how to initialize properties of a new node,
and how to read out properties of existing nodes. The latter
is done using a syntax with square brackets. This syntax can also
be used to set new values for properties.
But at first, consider the following example:
<programlisting>
Sphere(r) ==> Sphere(r/2);
</programlisting>
Obviously, the aim is to divide the radius of each sphere by two.
The rule implements this behaviour by replacing each sphere of
radius <literal>r</literal> by a new sphere of radius
<literal>r/2</literal>, using the constructor invocation
<literal>new Sphere(r/2)</literal>. Although the rule does its
job correctly, there is a better solution: What if we keep the
existing original sphere in the graph and just divide its radius by two?
This is more efficient, because the existing sphere can be re-used
and less changes to the graph have to be done. And there is another
advantage: If the original sphere has some other properties which
differ from their default values (e.g., a shader has been set), these
properties get lost by application of the replacement rule because
the rule just uses the radius property of the original sphere. However,
if we just modify the radius of the existing sphere, all other
properties remain unchanged, so nothing is lost. Here is how the better
solution looks like:
<programlisting>
x:Sphere ::> x[radius] /= 2;
</programlisting>
The most apparent difference to the previous rule is the new arrow symbol
<literal>::></literal>: It indicates a new type of rule, namely
an <firstterm>execution rule</firstterm>. Execution rules treat their
right hand sides as statements which are to be executed for every match
of their left hand side. Think of this as a <literal>for</literal> loop
which iterates over all matches and has the right hand side as body:
<programlisting>
for (<emphasis>all matches of x:Sphere in graph</emphasis>)
    x[radius] /= 2;
</programlisting>
Now the statement <literal>x[radius] /= 2;</literal> divides
<literal>x</literal>'s radius property by two as desired. 
The execution rule is contained in the method <literal>grow</literal> of
<ulink url="command:/rggtutorial/selectfiles?pfs:ColouredObjects.rgg">
this example</ulink>.
<ulink url="command:/workbench/rgg/methods/grow">
Click here</ulink> to see a sphere shrinking and a cylinder growing.
</para>
<para>
You may wonder if it is also possible to write
<literal>x.setRadius(x[radius]/2);</literal> instead of
<literal>x[radius] /= 2;</literal>. In principle, this works. But
in this case, GroIMP is not informed about the modification of the
radius and, thus, does not redraw the 3D visualization. As a rule of thumb,
the modification of properties of nodes which are already part of
GroIMP's graph should be done using
<firstterm>property assignments</firstterm> as in
<literal>x[radius] /= 2;</literal>. For newly created nodes which are
not yet part of GroIMP's graph, it is OK to use setter methods for
the purpose of initialization as in
<literal>Sphere(1).(setShader(RED))</literal>.
</para>

</section>

</chapter>
