<chapter><title>Introduction</title>

<section><title>Overview</title>

<para>
<firstterm>Growth grammars</firstterm> are a rule-based approach to the
modelling of dynamic systems. In contrast to conventional imperative
programming languages, rule-based approaches provide a natural and concise way
of modelling: Most systems to be modelled behave according to a set of rules,
and our perception works this way and not imperatively using a list of
operations which have to be processed step by step.</para>
<para>
<firstterm>Relational
growths grammars</firstterm> (RGG) are defined as a
unification of L-systems and graph grammars, which
follow the rule-based
paradigm and were developed during the last decades.
This tutorial gives an introduction into RGGs and how to
implement them within the modelling platform GroIMP. Using these
techniques, the following snapshots have been created:
</para>

<informaltable frame="none">
<tgroup cols="2">
<tbody>
<row>
<entry>
<mediaobject>
  <imageobject>
    <imagedata fileref="res/barley.png" format="PNG"/>
  </imageobject>
  <caption>
   <para>A model of barley.
</para>
  </caption>
</mediaobject>
</entry>
<entry>
<mediaobject>
  <imageobject>
    <imagedata fileref="res/gol.png" format="PNG"/>
  </imageobject>
  <caption>
   <para>Game Of Life.
</para>
  </caption>
</mediaobject>
</entry>
</row>
<row>
<entry>
<mediaobject>
  <imageobject>
    <imagedata fileref="res/steps.png" format="PNG"/>
  </imageobject>
  <caption>
   <para>A looped ladder.
</para>
  </caption>
</mediaobject>
</entry>
<entry>
<mediaobject>
  <imageobject>
    <imagedata fileref="res/tree.png" format="PNG"/>
  </imageobject>
  <caption>
   <para>A tree, based on the pipe model.
</para>
  </caption>
</mediaobject>
</entry>
</row>
</tbody>
</tgroup>
</informaltable>

</section>

<section><title>First example: The snowflake curve</title>

<figure id="f-snowflake"><title>Sequence of structures</title>

<mediaobject>
  <imageobject>
    <imagedata fileref="res/snowflake.png" format="PNG"/>
  </imageobject>
</mediaobject>

</figure>

<para>
The snowflake curve is a good starting point to enter the world of
    rule-based modelling. The swedish mathematician Helge von Koch introduced
    his construction in 1904; nowadays, this construction is
    known as the origin of a family of generalized Koch
    constructions based on the simple rule-based principle of
    <firstterm>initiator</firstterm> and <firstterm>generator</firstterm>.
</para>

<para>
    Koch's construction starts with a straight line, the
    initiator. This line is divided into three parts,
    the middle part then replaced by an equilateral triangle
    without baseline, see <xref linkend="f-koch"/>. The resulting
    shape, the generator, consists of four lines, and by
    repeatedly replacing single lines by this shape, we
    reproduce Koch's construction.
</para>

<figure id="f-koch"><title>Koch's construction step</title>
<mediaobject>
  <imageobject>
    <imagedata fileref="res/koch.png" format="PNG"/>
  </imageobject>
</mediaobject>
</figure>

<para>
    If we combine three initiator lines to an initial triangle and
    repeatedly apply Koch's construction on this structure,
    we get the snowflake curve (<xref linkend="f-snowflake"/>).
</para>

<section><title>Snowflake curve within GroIMP</title>

<para>
    In order to see the snowflake curve within GroIMP,
    <ulink url="command:/rggtutorial/selectfiles?pfs:Koch.rgg">
    click here</ulink> to load the example and
    <ulink url="command:/workbench/rgg/methods/derivation">
    here</ulink> to apply Koch's construction step.
</para>
<para>
The description of Koch&apos;s construction is coded in the four
lines of source code which you can see in the text editor below. They
should look like:
</para>
<programlisting>
public void derivation() [
    Axiom ==> F RU(120) F RU(120) F;
    F ==> F RU(-60) F RU(120) F RU(-60) F;
]
</programlisting>
<para>
This is the translation of Koch&apos;s construction into a
relational growth grammar (RGG)
programme which can be executed within GroIMP. The source code is written
in the <firstterm>RGG dialect</firstterm> of the
<firstterm>XL programming language</firstterm>.
The relation between
these lines and Koch's construction is as follows:
</para>
<itemizedlist>
<listitem>
<para>
RGGs (and L-systems) operate on sets of <firstterm>symbols</firstterm>. The
symbols of the example are
<classname>F</classname>, <classname>RU</classname>
and <classname>Axiom</classname>. In the context of RGGs, symbols
are in fact <firstterm>nodes</firstterm> of a <firstterm>graph</firstterm>.
</para>
</listitem>
<listitem>
<para>
To obtain a geometric outcome, RGGs and L-systems use a
so-called <firstterm>turtle graphics interpretation</firstterm>
of their symbols: The symbol <classname>F</classname>
commands a virtual turtle to move
forward and draw a line, the symbol
<classname>RU</classname>
leads to a turn by the given angle. Thus, lines in Koch's construction
are represented by symbols <classname>F</classname> in the RGG model.
These special symbols are called <firstterm>turtle commands</firstterm>
(for a list of all turtle commands, see
<xref linkend="c-turtlecommands"/>).
</para>
</listitem>
<listitem>
<para>
Turtles read symbols
from left to right and obey the commands: When a turtle
reads the sequence
<literal>F RU(120) F RU(120) F</literal>, it draws three lines with
angles of 120&deg; inbeetween, which is an equilateral triangle.
The sequence <literal>F RU(-60) F RU(120) F RU(-60) F</literal>
exactly corresponds to the generator of Koch's construction.
</para>
</listitem>
<listitem>
<para>
Now the arrow <literal>==></literal> indicates a
<firstterm>replacement rule</firstterm>:
All symbols which look like the left hand side of the arrow
are replaced by the
right hand side. Initially, in every RGG there exists just a single symbol
<classname>Axiom</classname>,
so the first rule replaces this initial symbol with a triangle:
</para>
<programlisting>
    Axiom ==> F RU(120) F RU(120) F;
</programlisting>
<para>
Koch's construction step is implemented by the second replacement rule:
</para>
<programlisting>
    F ==> F RU(-60) F RU(120) F RU(-60) F;
</programlisting>
<para>
This is a straightforward translation of the statement
&quot;Replace a line by four lines with certain
angles in-between&quot; into the symbolic notation of RGGs.
</para>
</listitem>
</itemizedlist>

<para>
Both replacement rules lead to the desired development of the structure:
</para>

<formalpara><title>Before first replacement</title>
<para>
The initial structure is <literal>Axiom</literal>. One cannot see
any geometric outcome, because <literal>Axiom</literal> is invisible.
</para>
</formalpara>

<formalpara><title>After first replacement</title>
<para>
The rule <literal>Axiom ==> F RU(120) F RU(120) F;</literal>
has been applied to <literal>Axiom</literal>, this results in
<literal>F RU(120) F RU(120) F</literal>. The geometry which is produced
by this sequence of turtle commands is an equilateral triangle.
<informalfigure>
<mediaobject>
  <imageobject>
    <imagedata fileref="res/triangle.gif" format="GIF"/>
  </imageobject>
</mediaobject>
</informalfigure>
</para>
</formalpara>

<formalpara><title>After second replacement</title>
<para>
The rule <literal>F ==> F RU(-60) F RU(120) F RU(-60) F;</literal>
has been applied to every <literal>F</literal>, this results in
<literal>F RU(-60) F RU(120) F RU(-60) F RU(120) F RU(-60) F RU(120) F RU(-60) F RU(120) F RU(-60) F RU(120) F RU(-60) F</literal>.
The geometry which is produced
by this sequence of turtle commands is a star.
<informalfigure>
<mediaobject>
  <imageobject>
    <imagedata fileref="res/star.gif" format="GIF"/>
  </imageobject>
</mediaobject>
</informalfigure>
</para>
</formalpara>

<para>
The two rules for the initial structure and the construction step
are surrounded by a so-called <firstterm>method declaration</firstterm>:
</para>
<programlisting>
public void derivation() [
    ...
]
</programlisting>
<para>
This leads to a
menu button with the same name <guimenuitem>derivation</guimenuitem> in the
RGG toolbar which is displayed in the upper right part of this window.
By clicking on this item, the method is <firstterm>invoked</firstterm>,
i.e., its contained rules are applied to the structure.
</para>
</section>

<section><title>Parametrization</title>
    <para>
    As you may have noticed, the replacement of one line by four
    lines should come along with a shortening of the lines to one third
    of the original length. However, this is not yet included
    in the example: The structure becomes very large after a number
    of construction steps.
    </para>
<para>
This problem can be solved in a parametrized
    version of the snowflake RGG: If <literal>F(x)</literal> stands
    for a line of length <literal>x</literal>, we can simply begin with
    the start word <literal>F(1) RU(120) F(1) RU(120) F(1)</literal> and write
    </para>
<programlisting>
F(x) ==> F(x/3) RU(-60) F(x/3) RU(120) F(x/3) RU(-60) F(x/3);
</programlisting>
    <para>
This reduces the length of the <classname>F</classname> segments to
one third of their previous length.
    Now we have created a simple
<firstterm>parametric L-system</firstterm>.
<ulink url="command:/rggtutorial/selectfiles?pfs:Koch2.rgg">
Click here</ulink> to load the improved, parametric version.
The construction step can be performed by clicking
<ulink url="command:/workbench/rgg/methods/derivation">here</ulink>
or on the menu button <guimenuitem>derivation</guimenuitem>.
</para>

</section>

<section><title>Modification of the model</title>

<para>

At this point, you should be able to play with the model:
<orderedlist>
<listitem>
<para>
Modify the source code in the text editor. For example, modify
the angles in the contruction step:
<programlisting>
    F(x) ==> F(x/3) RU(-30) F(x/3) RU(120) F(x/3) RU(-90) F(x/3);
</programlisting>
</para>
</listitem>
<listitem>
<para>
After a modification of source code, you have to save it within
the text editor. Within the text editor panel,
click on <guimenuitem>File/Save</guimenuitem>
or the corresponding button
<inlinemediaobject>
<imageobject>
<imagedata fileref="res/filesave.png"/>
</imageobject>
</inlinemediaobject>
in the tool bar of the text editor.
This <firstterm>compiles</firstterm> the source code into an
internal representation (Java byte-code);
if there are errors in the source code, they
are displayed within GroIMP.
</para>
</listitem>
<listitem>
<para>
Invoke methods of the source code in the RGG tool bar which
is displayed above the 3D view. Here, the method
<literal>derivation</literal> can be invoked to apply
the construction step.
</para>
</listitem>
<listitem>
<para>
Use the mouse to click on a single line of the snowflake. This brings
up the <firstterm>attribute editor</firstterm>, which displays the
attributes of the selected line. Modify the length interactively and
see how this influences the further development of the model.
</para>
</listitem>
</orderedlist>

</para>

</section>

</section>

<section><title>Second example: Getting three-dimensional
- Fractal cuboids</title>

<para>
The snowflake curve is a two-dimensional curve. This is because we have
only used rotations <classname>RU</classname> about a single axis,
namely the y-axis of the three-dimensional coordinate system. In this way
we cannot leave the two-dimensional x-z-plane.
Nevertheless, the curve was drawn in a three-dimensional world. To see this,
you may want to modify the snowflake by the inclusion of rotations
about the x- or z-axis which are called <classname>RL</classname>
and <classname>RH</classname>. For example, add a rotation
<classname>RH</classname> about the z-axis as in
</para>
<programlisting>
    F(x) ==> F(x/3) RU(-60) RH(20) F(x/3) RU(120) F(x/3) RU(-60) F(x/3);
</programlisting>
<para>
Afterwards, invoke the rules by a click on
<guimenuitem>derivation</guimenuitem> for a number of times: Do you see
how the curve extends into three dimensions? If not, click on the
button
<inlinemediaobject>
<imageobject>
<imagedata fileref="res/camerarotate.png"/>
</imageobject>
</inlinemediaobject>
above the 3D view, keep the mouse button pressed and move the mouse. This
rotates the virtual camera so that you can see the curve from another
perspective.
</para>
<para>
In any case, since the snowflake is designed as a two-dimensional curve,
the extension to three dimensions looks somewhat odd. So let's consider
a new example, fractal cuboids.
<ulink url="command:/rggtutorial/selectfiles?pfs:Cuboids.rgg">
Click here</ulink> to load the cuboids and
<ulink url="command:/workbench/rgg/methods/step">
here</ulink> to apply the replacement rule of the example.
</para>
<para>
The cuboids example uses two new turtle commands:
<literal>Box(x, true)</literal> draws a box (a cuboid) having
length <literal>x</literal> (<literal>true</literal>
means that the faces of consecutive boxes adjoin), and
<literal>Scale(x)</literal> scales the following symbols in size
by a factor of <literal>x</literal>. The initial cuboidal structure
is created by the replacement rule
<programlisting>
Axiom ==>
    Scale(1)
    RU(90) Box(L, true)
    RH(-90) RU(-90) Box(L, true)
    RH(90) RU(-90) Box(L, true)
    RH(-90) RU(-90) Box(L, true) Box(L, true)
    RH(90) RU(-90) Box(L, true) Box(L, true)
    RH(-90) RU(-90) Box(L, true) Box(L, true)
    RH(90) RU(-90) Box(L, true) Box(L, true)
    RH(90) RL(-90) Box(L, true);
</programlisting>
This rule is part of the special method <literal>init</literal>: This
method is invoked automatically by GroIMP and, thus, should contain
instructions to initialize the structure.
</para>
<para>
The length of the boxes is specified by a <literal>constant</literal>
named <literal>L</literal>. Such a constant has to be declared
in the RGG source code. In this example the declaration is simply
<programlisting>
const int L = 2;
</programlisting>
<literal>int</literal> is the <firstterm>type</firstterm> of the
constant, where <literal>int</literal> stands for integral numbers.
Every type of the Java programming language can be used. Other types are, e.g.,
<literal>float</literal> for single-precision floating-point numbers,
<literal>double</literal> for double-precision floating-point numbers,
or <literal>String</literal> for character sequences.
</para>
<para>
The development of the fractal cuboids is implemented by two rules contained
in the method <literal>derive</literal>. The first rule is responsible
for the structural development:
<programlisting>
Box ==> Box(L, true) RU(90) ... Box(L, true);
</programlisting>
It works similarly to the rule of the previous example which implemented
Koch's construction step. Every box (ignoring its length parameter)
is replaced by a sequence of boxes and rotations.
</para>
<para>
The second rule is responsible for the overall size of the structure:
In order to keep this size constant, a scaling of all boxes to one third
of their previous size is necessary in each step. This can be achieved
globally for all boxes by the rule
<programlisting>
Scale(x) ==> Scale(x/3);
</programlisting>
This is a difference to the Koch example, where each <literal>F</literal>
was individually reduced in size.
</para>

</section>

<section id="s-branched">
<title>Third example: Creating branched structures - The graph</title>

<para>
Both the snowflake curve and the fractal cuboids are composed
of a simple sequence of symbols: The turtle reads this sequence
from left to right and draws the corresponding objects. Thus,
we see a set of three-dimensional objects which are stringed
together one after another.
</para>
<para>
While this is sufficient for very simple structures, it would not
be feasible to model more complex structures this way. For example,
the side branches of plants and trees cannot be represented easily
within a single sequence of symbols: At every branching, a new
sequence for the side branch starts, while the bearing branch
continues with its own sequence of symbols.
</para>
<para>
As an example,
consider the following two figures: The left one
shows a structure consisting of a quite abstract stem (three
consecutive elements) and four side branches (each a single element).
The stem is representable by the sequence
<literal>F F F</literal> of turtle commands. Each branch is
representable by the single turtle command <literal>F</literal>,
and the branching rotations can be encoded by <literal>RU</literal>
commands.
</para>
<informaltable frame="none">
<tgroup cols="2">
<tbody>
<row>
<entry>
<mediaobject>
  <imageobject>
    <imagedata fileref="res/branch.gif" format="GIF"/>
  </imageobject>
  <caption>
   <para>Stem with four branches</para>
  </caption>
</mediaobject>
</entry>
<entry>
<mediaobject>
  <imageobject>
    <imagedata fileref="res/branch_topo.gif" format="GIF"/>
  </imageobject>
  <caption>
   <para>Internal graph structure using turtle commands</para>
  </caption>
</mediaobject>
</entry>
</row>
</tbody>
</tgroup>
</informaltable>
<para>
The figure on the right hand side shows a
<firstterm>graph</firstterm> which combines the turtle commands
of the stem and its branches into a single structure. The graph
has to be understood as follows:
<itemizedlist>
<listitem>
<para>
Each symbol (e.g., <literal>F</literal>, <literal>RU</literal>)
is a <firstterm>node</firstterm> of the graph.
</para>
</listitem>
<listitem>
<para>
Nodes are connected by <firstterm>directed edges</firstterm>
(the arrows in the figure).
</para>
</listitem>
<listitem>
<para>
The direction of edges reflects the direction in which the turtle
executes its commands.
</para>
</listitem>
<listitem>
<para>
Branchings occur at a node when there are two or more edges
starting at the node. In this case, the turtle handles each
branch separately and independently.
</para>
</listitem>
</itemizedlist>
</para>
<para>
Now relational growth grammars use a textual notation for
branches with the help of square brackets: Each branch is
included in a pair of square brackets and appended behind
its bearing symbol. E.g., the graph in the figure above is written
textually as
<programlisting>
F [RU(90) F] [RU(-90) F] F [RU(90) F] [RU(-90) F] F
</programlisting>
The three <literal>F</literal>'s of the stem can be seen when
ignoring the bracketed parts. In order to see that this works, click
<ulink url="command:/rggtutorial/selectfiles?pfs:TreeGraph.rgg">here</ulink>
and have a look at the source code in the text editor and at the
3D view.
</para>
<para>
This very simple example of branched structures has no development,
it just creates a fixed structure. But there is a second,
<ulink url="command:/rggtutorial/selectfiles?pfs:SimpleTree.rgg">
more interesting example of branching</ulink> with a simple
<ulink url="command:/workbench/rgg/methods/run">
development</ulink>: In every step of development, an
<literal>F</literal> segment is split into two
<literal>F</literal> segments, and two side branches are
created at the splitting location.
</para>

</section>

<section>
<title>Fourth example: Declaring own symbols - Nodes and classes</title>

<para>
The previous sections made use of some predefined symbols
which have a meaning as turtle commands:
<literal>F</literal>, <literal>RU</literal>,
<literal>RH</literal>, <literal>RL</literal>,
<literal>Box</literal>, <literal>Scale</literal>. In
<xref linkend="s-branched"/>, it was clarified that these symbols
are nodes of a graph. Now the following question arises: What is
the possible set of symbols which can be used as nodes in an RGG?
</para>

<para>
In order to give the precise answer, we have to digress slightly
to Java: GroIMP and its RGG features are implemented in Java.
Thus, the graph of nodes which represents the visible structure
in GroIMP is also implemented in Java. As Java is an object-oriented
programming language, Java objects are <firstterm>instances</firstterm>
of <firstterm>classes</firstterm>, and so are the nodes of the graph:
Actually, the turtle command <literal>F</literal> is a class
<literal>F</literal> which is defined in the Java package
<literal>de.grogra.turtle</literal>.
The turtle command <literal>Box</literal> is a class
<literal>Box</literal> which is defined in the Java package
<literal>de.grogra.imp3d.objects</literal>. So a symbol
<literal>F</literal> is a node of class <literal>de.grogra.turtle.F</literal>
in the graph.
If you are not familiar with these Java-related terms, please consult
the numerous literature on Java.
</para>

<para>
In general, every instance of a class which is a
<firstterm>subclass</firstterm> of <literal>de.grogra.graph.impl.Node</literal>
can be used as a node. GroIMP provides a lot of useful classes,
some of which are described in <xref linkend="c-turtlecommands"/>.
But you can also define your own node classes: In an RGG file,
the simplest way to do this is to write a
<firstterm>module declaration</firstterm>:
<programlisting>
module X;
</programlisting>
This declares a simple node class <literal>X</literal>. This class
can then be used like any other node classes (e.g., the turtle
commands). However, <literal>X</literal> has no gemetrical
meaning, it is invisible. Nevertheless, such an invisible node can be
very useful.
<ulink url="command:/rggtutorial/selectfiles?pfs:BinaryTree.rgg">
This example</ulink> uses invisible <literal>X</literal> nodes
which represent the growing tips of a binary tree. The
<ulink url="command:/workbench/rgg/methods/grow">
growth</ulink>
is implemented by replacing an <literal>X</literal> node with
an <literal>F</literal> line which bears two new
<literal>X</literal> nodes as its branches:
</para>

<programlisting>
X ==> F(1) [RU(30) X] [RU(-30) X];
</programlisting>

<para>
It is also possible to declare <firstterm>parametrized modules</firstterm>:
<programlisting>
module X(float length);
</programlisting>
With the help of such a parametrized <literal>X</literal>, the
example of a binary tree can be improved such that
child branches are shorter than their bearing parent branches:
<programlisting>
X(len) ==> F(len) [RU(30) X(len*0.7)] [RU(-30) X(len*0.7)];
</programlisting>
<ulink url="command:/rggtutorial/selectfiles?pfs:BinaryTree2.rgg">
Click here</ulink> to load the improved example and
<ulink url="command:/workbench/rgg/methods/grow">here</ulink>
to apply the growth rule.
</para>

</section>

</chapter>
