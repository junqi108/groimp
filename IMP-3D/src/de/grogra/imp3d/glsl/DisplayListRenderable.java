package de.grogra.imp3d.glsl;

abstract class DisplayListRenderable
{
	abstract void render (float lod);
}
