
/*
 * Copyright (C) 2002 - 2007 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.ext.povray;

import java.io.*;
import de.grogra.persistence.*;
import de.grogra.graph.impl.*;
import de.grogra.imp3d.objects.SceneTreeWithShader.Leaf;
import de.grogra.imp3d.objects.SceneTree.InnerNode;
import de.grogra.pf.registry.*;

public abstract class POVObject
{
	private final String type, refType;


	public POVObject (String type, String refType)
	{
		this.type = type;
		this.refType = refType;
	}


	protected String getType ()
	{
		return type;
	}


	protected boolean shouldDeclareObject (Object object)
	{
		return (object instanceof Shareable)
			&& (((Shareable) object).getProvider () != null);
	}


	protected String createIdentifier (Object object, POVExport export)
	{
		if (object instanceof Shareable)
		{
			Shareable o = (Shareable) object;
			SharedObjectProvider sop = o.getProvider ();
			if (sop instanceof ObjectItem)
			{
				return ((ObjectItem) sop).getName ();
			}
			else if (sop instanceof SharedObjectNode)
			{
				NodeReference r = NodeReference
					.get (export, "/objects", (SharedObjectNode) sop);
				if (r != null)
				{
					return r.getName ();
				}
			}
		}
		return null;
	}


	protected void export (Object object, Leaf node, InnerNode transform,
						   String typeOverride,
						   POVExport sge) throws IOException
	{
		POVWriter out = sge.out;
		if ((object != null) && shouldDeclareObject (object))
		{
			String name = sge.getDeclaredName (object);
			if (name == null)
			{
				name = sge.declareObject
					(object, createIdentifier (object, sge));
				out.print ("#declare ");
				out.write (name);
				out.write (" = ");
				out.write (sge.getObjectType (type));
				out.write ('{');
				out.println ();
				out.indent ();
				exportImpl (object, node, sge);
				out.unindent ();
				out.print ("};");
				out.println ();
			}
			out.print (sge.getObjectType
					   ((typeOverride != null) ? typeOverride : refType));
			out.write ('{');
			exportHeader (object, node, sge);
			out.write (name);
			out.println ();
			out.indent ();
		}
		else
		{
			out.print (sge.getObjectType
					   ((typeOverride != null) ? typeOverride : type));
			out.write ('{');
			exportHeader (object, node, sge);
			out.println ();
			out.indent ();
			exportImpl (object, node, sge);
		}
		exportFooter (object, node, transform, sge);
		out.unindent ();
		out.print ('}');
		out.println ();
	}


	protected void exportHeader (Object object, Leaf leaf, POVExport export)
		throws IOException
	{
	}


	protected abstract void exportFooter
		(Object object, Leaf node, InnerNode transform, POVExport export)
		throws IOException;


	protected abstract void exportImpl (Object object, Leaf leaf, POVExport export)
		throws IOException;

}
