
/*
 * Copyright (C) 2002 - 2007 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.ext.povray;

import java.io.IOException;

import de.grogra.imp3d.objects.Attributes;
import de.grogra.imp3d.objects.SceneTreeWithShader.Leaf;

public class Box extends POVNode
{
	public Box ()
	{
		super ("box");
	}


	@Override
	protected void exportImpl (Object object, Leaf leaf, POVExport export)
		throws IOException
	{
		float w2 = (float) leaf.getDouble (Attributes.WIDTH) * 0.5f;
		float l2 = (float) leaf.getDouble (Attributes.HEIGHT) * 0.5f;
		float h = (float) leaf.getDouble (Attributes.LENGTH);
		export.out.print (-w2, -l2, 0);
		export.out.print (w2, l2, h);
		export.out.println ();
	}

}
