
/*
 * Copyright (C) 2002 - 2007 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.ext.povray;

import java.io.IOException;

import de.grogra.imp3d.io.SceneGraphExport;
import de.grogra.imp3d.objects.SceneTree;
import de.grogra.imp3d.objects.SceneTree.InnerNode;
import de.grogra.imp3d.objects.SceneTreeWithShader.Leaf;
import de.grogra.imp3d.shading.AffineUVTransformation;
import de.grogra.imp3d.shading.Phong;
import de.grogra.imp3d.shading.Shader;
import de.grogra.imp3d.shading.SideSwitchShader;
import de.grogra.math.ChannelMap;
import de.grogra.ray.physics.Spectrum3f;

public abstract class POVNode extends POVObject
	implements SceneGraphExport.NodeExport
{

	public POVNode (String type)
	{
		super (type, type);
	}


	public POVNode (String type, String refType)
	{
		super (type, refType);
	}


	@Override
	protected void exportFooter (Object object, Leaf node, InnerNode transform,
								 POVExport export) throws IOException
	{
		Shader s = node.shader;
		if (s instanceof SideSwitchShader)
		{
			SideSwitchShader cs = (SideSwitchShader) s;
			exportMaterial (node, cs.getFrontShader (), cs.getBackShader (),
						  null, export);
		}
		else if (s instanceof Phong)
		{
			exportMaterial (node, s, null, null, export);
		}
		else
		{
			export.export (s);
			if (node.interior != null)
			{
				float ior = node.interior.getIndexOfRefraction (new Spectrum3f (1, 1, 1));
				if (ior != 1)
				{
					export.out.print ("interior{ior ");
					export.out.print (ior);
					export.out.print ('}');
					export.out.println ();
				}
			}
		}
		exportTransform (node, transform, export);
	}


	protected void exportTransform (Leaf node, InnerNode transform,
									POVExport export) throws IOException
	{
		if (transform != null)
		{
			export.out.print (transform);
			export.out.println ();
		}
	}


	protected void exportMaterial (Leaf node, Shader front, Shader back,
								   ChannelMap mapping, POVExport export)
		throws IOException
	{
		POVWriter out = export.out;
		out.print ("material{");
		out.println ();
		out.indent ();

		if (front != null)
		{
			String t = "texture";
			Shader m = front;
			for (int i = 0; i <= 1; i++)
			{
				SceneGraphExport.ObjectExport ex = export.getExportForObject ((Object) m);
				if (ex instanceof POVPhong)
				{
					((POVPhong) ex).export (m, mapping, node, t, this, export);
				}
				else
				{
					out.print (t);
					out.write ("{pigment{color rgbt");
					out.printRGBT (m.getAverageColor ());
					out.print ("}}");
					out.println ();
				}
				m = back;
				if (m == null)
				{
					break;
				}
				t = "interior_texture";
			}
		}
		else
		{
			out.print ("texture{pigment{color rgb <1,1,1>}}");
			out.println ();
		}

		if (node.interior != null)
		{
			float ior = node.interior.getIndexOfRefraction (new Spectrum3f (1, 1, 1));
			if (ior != 1)
			{
				out.print ("interior{ior ");
				out.print (ior);
				out.print ('}');
				out.println ();
			}
		}
		out.unindent ();
		out.print ('}');
		out.println ();
	}


	protected void exportTextureHeader (Phong mat, Leaf node, POVExport export)
		throws IOException
	{
		if (POVPhong.usesUVMapping (mat))
		{
			export.out.write ("uv_mapping ");
		}
	}


	protected void exportTextureFooter (Phong mat, ChannelMap mapping,
										Leaf node, POVExport export)
		throws IOException
	{
		if (mapping instanceof AffineUVTransformation)
		{
			export.out.print ((AffineUVTransformation) mapping);
		}
	}


	public void export (SceneTree.Leaf node, InnerNode transform, SceneGraphExport sge)
		throws IOException
	{
		export (null, (Leaf) node, transform, null, (POVExport) sge);
	}

}
