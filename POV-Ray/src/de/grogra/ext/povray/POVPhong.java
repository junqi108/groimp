/*
 * Copyright (C) 2002 - 2007 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.ext.povray;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.vecmath.Matrix4d;
import javax.vecmath.Vector3f;

import de.grogra.imp.objects.ImageAdapter;
import de.grogra.imp3d.objects.SceneTree.InnerNode;
import de.grogra.imp3d.objects.SceneTreeWithShader.Leaf;
import de.grogra.imp3d.shading.AffineUVTransformation;
import de.grogra.imp3d.shading.BlendItem;
import de.grogra.imp3d.shading.BumpMap;
import de.grogra.imp3d.shading.ChannelBlend;
import de.grogra.imp3d.shading.ChannelMapNode;
import de.grogra.imp3d.shading.Gradient;
import de.grogra.imp3d.shading.Granite;
import de.grogra.imp3d.shading.ImageMap;
import de.grogra.imp3d.shading.Leopard;
import de.grogra.imp3d.shading.Phong;
import de.grogra.imp3d.shading.Shader;
import de.grogra.imp3d.shading.SurfaceMap;
import de.grogra.imp3d.shading.Turbulence;
import de.grogra.imp3d.shading.VolumeFunction;
import de.grogra.imp3d.shading.VolumeTurbulence;
import de.grogra.imp3d.shading.Wood;
import de.grogra.imp3d.shading.XYZTransformation;
import de.grogra.math.ChannelMap;
import de.grogra.math.ColorMap;
import de.grogra.math.Cubic;
import de.grogra.math.Graytone;
import de.grogra.math.Scallop;
import de.grogra.math.Sin01;
import de.grogra.math.Transform3D;
import de.grogra.math.Triangle;
import de.grogra.pf.ui.registry.FileObjectItem;
import de.grogra.xl.lang.FloatToFloat;

public class POVPhong extends POVAttribute
{
	public POVPhong ()
	{
		super ("texture");
	}

	public void export (Shader m, ChannelMap mapping, Leaf node,
			String textureType, POVNode nodeExport, POVExport export)
			throws IOException
	{
		export.phongNode = nodeExport;
		export.phongMapping = mapping;
		export (m, node, null, textureType, export);
	}

	static boolean usesUVMapping (Phong mat)
	{
		return (mat.getDiffuse () instanceof SurfaceMap)
			|| ((mat.getInput () instanceof BumpMap) && (((BumpMap) mat
				.getInput ()).getDisplacement () instanceof SurfaceMap));
	}

	static boolean exportChannelMap (ChannelMap c, POVExport export,
			String mapType, String bitmapType, int povMapType, String additional)
			throws IOException
	{
		POVWriter out = export.out;
		if (c instanceof SurfaceMap)
		{
			SurfaceMap sm = (SurfaceMap) c;
			out.print (mapType);
			out.write ('{');
			out.println ();
			out.indent ();
			out.print (bitmapType);
			out.write ('{');
			ChannelMap in = sm.getInput ();
			AffineUVTransformation t;

			// generateImage == true: a new image file has to be created
			boolean generateImage;
			if (in instanceof AffineUVTransformation)
			{
				t = (AffineUVTransformation) in;
				if (t.getInput () != null)
				{
					t = null;
					// chained inputs: cannot be handled by POV-Ray
					generateImage = true;
				}
				else
				{
					// in is just an affine uv transformation, can be handled by POV-Ray
					generateImage = false;
				}
			}
			else if (in != null)
			{
				// some unsupported input transformation, have to create an image
				generateImage = true;
				t = null;
			}
			else
			{
				// no input transformation
				generateImage = false;
				t = null;
			}
			BufferedImage img;
			String type = null;
			String path = null;
			boolean interpolate = true;
			if (generateImage)
			{
				// draw the possibly transformed image
				img = new BufferedImage (256, 256, BufferedImage.TYPE_INT_ARGB);
				sm.drawImage (img, 1, true);
			}
			else if (sm instanceof ImageMap)
			{
				// ImageMap: we can use GroIMP's image immediately
				ImageAdapter a = ((ImageMap) sm).getImageAdapter ();
				interpolate = ((ImageMap) sm).hasBilinearFiltering();
				img = a.getBufferedImage ();
				if (a.getProvider () instanceof FileObjectItem)
				{
					FileObjectItem fo = (FileObjectItem) a.getProvider ();
					String mt = fo.getMimeType ().getMediaType ();
					if (mt.equals ("image/png") || mt.equals ("image/x-png"))
					{
						type = "png";
					}
					else if (mt.equals ("image/jpeg"))
					{
						type = "jpeg";
					}
					else if (mt.equals ("image/tiff"))
					{
						type = "tiff";
					}
					else if (mt.equals ("image/gif"))
					{
						type = "gif";
					}
					if (type != null)
					{
						path = export.getPath (fo);
						img = null;
					}
				}
			}
			else
			{
				// make an image of the untransformed general surface map
				img = new BufferedImage (256, 256, BufferedImage.TYPE_INT_ARGB);
				sm.drawImage (img, 1, false);
			}

			// if needed, write image to file
			if (img != null)
			{
				File file = (File) export.getFile ("surfacemap.png");
				type = "png";
				path = export.getPath (file);
				ImageIO.write (img, "png", file);
			}
			out.write (type);
			out.write (" \"");
			out.write (path);
			out.write ("\" ");
			if (interpolate)
				out.write ("interpolate 2 ");
			if (povMapType > 0)
			{
				out.write ("map_type ");
				out.print (povMapType);
			}
			out.write ('}');
			out.write (additional);
			out.println ();
			if (t != null)
			{
				if (povMapType == 2)
				{
					out.print ("scale ");
					float u = 1 / t.getScaleU ();
					out.print (u, 1 / t.getScaleV (), u);
					out.println ();
				}
				else
				{
					out.print (t);
				}
			}
			out.unindent ();
			out.print ('}');
			out.println ();
			return true;
		}
		else if (c instanceof ChannelBlend)
		{
			out.print (mapType);
			out.write ('{');
			out.println ();
			out.indent ();
			export ((ChannelBlend) c, export);
			out.unindent ();
			out.print ('}');
			out.println ();
			return true;
		}
		return false;
	}

	@Override
	protected void exportImpl (Object object, Leaf leaf, POVExport export)
			throws IOException
	{
		POVWriter out = export.out;
		Phong m = (Phong) object;
		ChannelMap c = m.getDiffuse ();
		if (c == null)
		{
			c = Phong.DEFAULT_DIFFUSE;
		}
		if (!exportChannelMap (c, export, "pigment", "image_map", 0, ""))
		{
			out.print ("pigment{color rgb");
			int ac = (c instanceof ColorMap) ? ((ColorMap) c)
				.getAverageColor () : 0xff808080;
			c = m.getTransparency ();
			if (c instanceof ColorMap)
			{
				out.print (m.isInterpolatedTransparency () ? "t<" : "f<");
				out.printRGBValues (ac);
				out.print (',');
				int t = ((ColorMap) c).getAverageColor ();
				out.print (((t & 255) + ((t >> 8) & 255) + ((t >> 16) & 255))
					* (1f / (3 * 255)));
				out.print ('>');
			}
			else
			{
				out.printRGB (ac);
			}
			out.print ("}");
			out.println ();
		}
		c = m.getInput ();
		if (c instanceof BumpMap)
		{
			exportChannelMap (((BumpMap) c).getDisplacement (), export,
				"normal", "bump_map", 0, " bump_size " + 2
					* ((BumpMap) c).getStrength ());
		}
		out.print ("finish{diffuse 1");
		c = m.getSpecular ();
		if (c == null)
		{
			c = Phong.DEFAULT_SPECULAR;
		}
		if (c instanceof ColorMap)
		{
			int rgba = ((ColorMap) c).getAverageColor ();
			out.write (" phong ");
			out
				.print ((((rgba >> 16) & 255) + ((rgba >> 8) & 255) + (rgba & 255)) / 765f);
			out.write (" phong_size ");
			c = m.getShininess ();
			out.print ((c instanceof Graytone) ? Phong.convertShininess (((Graytone) c).getValue ()) : 10);
		}
		out.write ('}');
		out.println ();
	}

	@Override
	protected void exportHeader (Object object, Leaf leaf, POVExport export)
			throws IOException
	{
		if (export.phongNode != null)
		{
			export.phongNode.exportTextureHeader ((Phong) object, leaf,
				export);
		}
	}

	@Override
	protected void exportFooter (Object object, Leaf leaf, InnerNode transform,
			POVExport export) throws IOException
	{
		if (export.phongNode != null)
		{
			export.phongNode.exportTextureFooter ((Phong) object,
				export.phongMapping, leaf, export);
			export.phongNode = null;
			export.phongMapping = null;
		}
	}

	static void export (ChannelBlend b, POVExport export) throws IOException
	{
		POVWriter out = export.out;
		ChannelMap in = b.getInput ();
		exportPattern:
		{
			boolean wave = false;
			if (in instanceof Gradient)
			{
				Vector3f v = export.pool.v3f0;
				v.set (((Gradient) in).getDirection ());
				float f = 1 / v.length ();
				v.scale (f);
				out.print ("gradient ");
				out.print (v);
				out.print (" scale ");
				out.print (f);
			}
			else if (in instanceof Granite)
			{
				out.print ("granite");
			}
			else if (in instanceof VolumeTurbulence)
			{
				out.print ("bumps");
			}
			else if (in instanceof Leopard)
			{
				out.print ("leopard");
				wave = true;
			}
			else if (in instanceof Wood)
			{
				out.print ("wood");
				wave = true;
			}
			else
			{
				out.print ("gradient x");
				break exportPattern;
			}
			if (wave)
			{
				VolumeFunction w = (VolumeFunction) in;
				out.print (" frequency ");
				out.print (w.getFrequency ());
				out.print (" phase ");
				out.print (w.getPhase ());
				FloatToFloat wf = w.getWaveForm ();
				if (wf instanceof Triangle)
				{
					out.print (" triangle_wave");
				}
				else if (wf instanceof Sin01)
				{
					out.print (" sine_wave");
				}
				else if (wf instanceof Scallop)
				{
					out.print (" scallop_wave");
				}
				else if (wf instanceof Cubic)
				{
					out.print (" cubic_wave");
				}
				else
				{
					out.print (" ramp_wave");
				}
			}
		}
		out.println ();
		exportTransformations (b, export);
		BlendItem i = b.getBlend ();
		if (i != null)
		{
			out.print ("pigment_map{");
			out.indent ();
			out.println ();
			while (i != null)
			{
				out.print ('[');
				out.print ((i.getValue () + 1) / 2);
				out.print (' ');
				exportPigment (i.getInput (), export);
				out.print (']');
				out.println ();
				i = i.getNext ();
			}
			out.unindent ();
			out.print ('}');
			out.println ();
		}
	}

	static void exportPigment (ChannelMap p, POVExport export)
			throws IOException
	{
		if (p instanceof ChannelBlend)
		{
			export ((ChannelBlend) p, export);
			exportTransformations ((ChannelBlend) p, export);
		}
		else if (p instanceof ColorMap)
		{
			export.out.print ("color rgbt");
			export.out.printRGBT (((ColorMap) p).getAverageColor ());
		}
		else
		{
			export.out.print ("color rgb<1,1,1>");
		}
	}

	static void exportTransformations (ChannelMapNode n, POVExport export)
			throws IOException
	{
		ChannelMap c = n.getInput ();
		POVWriter out = export.out;
		while (c instanceof ChannelMapNode)
		{
			if (c instanceof XYZTransformation)
			{
				Transform3D t = ((XYZTransformation) c).getTransform ();
				if (t != null)
				{
					Matrix4d m = export.pool.m4d0;
					m.setIdentity ();
					t.transform (m, m);
					de.grogra.vecmath.Math2.invertAffine (m, m);
					out.print (m);
					out.println ();
				}
			}
			else if (c instanceof Turbulence)
			{
				Turbulence t = (Turbulence) c;
				out.print ("warp{turbulence ");
				out.print (t.getAmount ());
				out.print (" octaves ");
				out.print (t.getOctaves ());
				out.print (" omega ");
				out.print (1 / t.getFrequencyRatio ());
				out.print (" lambda ");
				out.print (t.getNoiseRatio ());
				out.print ('}');
				out.println ();
			}
			c = ((ChannelMapNode) c).getInput ();
		}
	}

}
