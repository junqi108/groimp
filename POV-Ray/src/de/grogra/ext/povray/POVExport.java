
/*
 * Copyright (C) 2002 - 2007 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.ext.povray;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.util.Collection;

import javax.vecmath.Color3f;
import javax.vecmath.Matrix4d;

import de.grogra.graph.ContextDependent;
import de.grogra.graph.GraphState;
import de.grogra.imp.ExternalRenderer;
import de.grogra.imp3d.PolygonArray;
import de.grogra.imp3d.Polygonizable;
import de.grogra.imp3d.Polygonization;
import de.grogra.imp3d.View3D;
import de.grogra.imp3d.io.SceneGraphExport;
import de.grogra.imp3d.objects.Attributes;
import de.grogra.imp3d.objects.DirectionalLight;
import de.grogra.imp3d.objects.GlobalTransformation;
import de.grogra.imp3d.objects.PointLight;
import de.grogra.imp3d.objects.SceneTree;
import de.grogra.imp3d.objects.SceneTreeWithShader;
import de.grogra.imp3d.objects.Sky;
import de.grogra.imp3d.objects.SpotLight;
import de.grogra.imp3d.objects.SceneTree.InnerNode;
import de.grogra.imp3d.objects.SceneTreeWithShader.Leaf;
import de.grogra.imp3d.shading.Light;
import de.grogra.imp3d.shading.Phong;
import de.grogra.imp3d.shading.Shader;
import de.grogra.imp3d.shading.SideSwitchShader;
import de.grogra.math.Pool;
import de.grogra.math.Tuple3fType;
import de.grogra.pf.io.FileWriterSource;
import de.grogra.pf.io.FilterItem;
import de.grogra.pf.io.FilterSource;
import de.grogra.pf.io.IOFlavor;
import de.grogra.pf.io.StreamAdapter;
import de.grogra.pf.io.WriterSource;
import de.grogra.pf.io.FilterSource.MetaDataKey;
import de.grogra.pf.ui.Context;
import de.grogra.util.MimeType;
import de.grogra.util.StringMap;
import de.grogra.vfs.FileSystem;
import de.grogra.vfs.LocalFileSystem;

public class POVExport extends SceneGraphExport
	implements FileWriterSource, WriterSource
{
	public static final MetaDataKey<Float> FLATNESS = new MetaDataKey<Float> ("flatness");
	public static final MetaDataKey<Boolean> LAYERS = new MetaDataKey<Boolean> ("layers");
	public static final MetaDataKey<Float> ASPECT = new MetaDataKey<Float> ("aspect");
	public static final MetaDataKey<Boolean> TEMPORARY = new MetaDataKey<Boolean> ("temporary");

	public final Pool pool = new Pool ();
 
	POVWriter out;

	private final java.util.Map declaredObjects = new java.util.IdentityHashMap ();
	private final StringMap declaredNames = new StringMap (64, true);

	public static final MimeType MIME_TYPE = new MimeType ("model/x-pov", null);
	public static final IOFlavor FLAVOR
		= new IOFlavor (MIME_TYPE, IOFlavor.FILE_WRITER, null);

	private Object sky;
	private boolean skyIsNode;
	private boolean hasLights;

	private String objectType;

	POVNode phongNode;
	de.grogra.math.ChannelMap phongMapping;
	final Color3f tmpColor = new Color3f ();


	public POVExport (FilterItem item, FilterSource source)
	{
		super (item, source);
		setFlavor (FLAVOR);
	}


	@Override
	protected SceneTree createSceneTree (View3D scene)
	{
		SceneTree t = new SceneTreeWithShader (scene)
		{
			@Override
			protected boolean acceptLeaf (Object object, boolean asNode)
			{
				return getExportFor (object, asNode) != null;
			}

			@Override
			protected SceneTree.Leaf createLeaf (Object object, boolean asNode, long id)
			{
				Leaf l = new Leaf (object, asNode, id);
				init (l);
				return l;
			}
		};
		t.createTree (true, this.getMetaData (LAYERS, true));
		return t;
	}


	@Override
	protected void beginGroup (InnerNode group) throws IOException
	{
		String s;
		switch (group.flags & InnerNode.CSG_MASK)
		{
			case Attributes.CSG_UNION:
				 s = "merge{";
				 break;
			case Attributes.CSG_INTERSECTION:
				 s = "intersection{";
				 break;
			case Attributes.CSG_DIFFERENCE:
				 s = "difference{";
				 break;
			default:
				 s = "union{";
				 break;
		}			
		out.print (s);
		out.println ();
		out.indent ();
	}


	@Override
	protected void endGroup (InnerNode group) throws IOException
	{
		out.print (group);
		out.println ();
		out.unindent ();
		out.print ('}');
		out.println ();
	}


	private File outFile;
	private Collection<File> files;
	boolean temporary;

	@Override
	protected FileSystem initFileSystem () throws IOException
	{
		return new LocalFileSystem (null, outFile.getParentFile ());
	}


	@Override
	protected Object initDirectory () throws IOException
	{
		File dir = new File (outFile.getParentFile (), outFile.getName () + "-files");
		if (!dir.isDirectory ())
		{
			if (dir.exists ())
			{
				dir.delete ();
			}
			dir.mkdir ();
			if (!dir.isDirectory ())
			{
				throw new IOException ("Cannot create directory " + dir);
			}
			if (files != null)
			{
				files.add (dir);
			}
			if (temporary)
			{
				dir.deleteOnExit ();
			}
		}
		return dir;
	}


	@Override
	public Object getFile (String name) throws IOException
	{
		File file = (File) super.getFile (name);
		if (files != null)
		{
			files.add (file);
		}
		if (temporary)
		{
			file.deleteOnExit ();
		}
		return file;
	}


	@Override
	protected void write () throws IOException
	{
		out.print ("#version 3.5;");
		out.println ();
		super.write ();
	}


	public void write (File out) throws IOException
	{
		outFile = out;
		files = getMetaData (ExternalRenderer.FILELIST, null);
		temporary = getMetaData (TEMPORARY, false);
		StreamAdapter.write (this, out);
	}


	public void write (Writer w) throws IOException
	{
		out = new POVWriter (w);
		write ();
		if (sky != null)
		{
			out.print ("sky_sphere{rotate <90,0,0>");
			out.println ();
			out.indent ();
			Shader sh = ((Sky) getGraphState ().getObjectDefault
						 (sky, skyIsNode, Attributes.SHAPE, null))
				.getShader ();
			Shader mat;
			if (sh instanceof SideSwitchShader)
			{
				mat = ((SideSwitchShader) sh).getFrontShader ();
			}
			else if (sh instanceof Phong)
			{
				mat = sh;
			}
			else
			{
				mat = null;
			}
			if ((mat instanceof Phong)
				&& POVPhong.exportChannelMap
				   (((Phong) mat).getDiffuse (), this, "pigment",
					"image_map", 1, ""))
			{
			}
			else
			{
				out.print ("pigment{color rgbt");
				out.printRGBT ((sh != null) ? sh.getAverageColor ()
							   : 0xff8080ff);
				out.write ("}");
			}
			out.println ();
			Matrix4d m = pool.m4d0;
			GlobalTransformation.get (sky, skyIsNode, getGraphState (), false)
				.get (m);
			m.get (pool.m3f0);
			out.print (pool.m3f0);
			out.println ();
			out.unindent ();
			out.print ('}');
			out.println ();
		}
		if (!hasLights)
		{
			Matrix4d m = new Matrix4d ();
			Light light = getView ().getDefaultLight (m);
			out.print ("light_source");
			out.write ('{');
			out.println ();
			out.indent ();
			exportLight (light);
			out.print (m);
			out.unindent ();
			out.print ('}');
			out.println ();
		}
		export (getView ().getCamera ());
	}


	public String getDeclaredName (Object object)
	{
		return (String) declaredObjects.get (object);
	}


	public String declareObject (Object object, String name)
	{
		StringBuffer b = new StringBuffer (); 
		if (name == null)
		{
			name = object.getClass ().getName ();
			b.append (name);
			b.delete (0, name.lastIndexOf ('.') + 1);
		}
		else
		{
			b.append (name);
		}
		int i, n = b.length ();
		for (i = 0; i < n; i++)
		{
			char c = b.charAt (i);
			if (!(((c >= '0') && (c <= '9'))
				  || ((c >= 'A') && (c <= 'Z'))
				  || ((c >= 'a') && (c <= 'z'))
				  || (c == '_')))
			{
				b.setCharAt (i, '_');
			}
		}
		if (n == 0)
		{
			b.append ("Unnamed");
		}
		else
		{
			char c = b.charAt (0);
			if ((c >= 'a') && (c <= 'z'))
			{
				b.setCharAt (0, Character.toUpperCase (c));
			}
			else if (!((c >= 'A') && (c <= 'Z')))
			{
				b.insert (0, "X_");
			}
		}
		i = -1;
		name = b.toString ();
		while (declaredNames.get (name) != null)
		{
			name = b.append (++i).toString ();
			b.setLength (n);
		}
		declaredNames.put (name, Boolean.TRUE);
		declaredObjects.put (object, name);
		return name;
	}


	public final PolygonArray polygons = new PolygonArray ();

	public void exportPolygons () throws IOException
	{
		if (polygons.polygons.isEmpty ())
		{
			out.print ("vertex_vectors{1,0} face_indices{1,0}");
			out.println ();
		}
		else
		{
			print (out, "vertex_vectors{", polygons.vertices.elements, null,
				   polygons.vertices.size, polygons.dimension, false);
			print (out, "normal_vectors{", null, polygons,
				   polygons.normals.size / 3, 1, false);
			if (!polygons.uv.isEmpty ())
			{
				print (out, "uv_vectors{", polygons.uv.elements, null,
					   polygons.uv.size, 2, true);
			}
			printFaces (out, "face_indices{", polygons);
		}
	}

	
	void exportLight (Light light) throws IOException
	{
		out.print ("0, color rgb");
		Tuple3fType.setColor (tmpColor, light.getAverageColor ());
		out.print (tmpColor);
		if (light.isShadowless ())
		{
			out.write (" shadowless");
		}
		out.println ();
		if (light instanceof PointLight)
		{
			if (light instanceof SpotLight)
			{
				SpotLight s = (SpotLight) light;
				out.print ("spotlight radius ");
				out.print (s.getInnerAngle () * (float) (180 / Math.PI));
				out.write (" falloff ");
				out.print (s.getOuterAngle () * (float) (180 / Math.PI));
				out.write (" tightness 1 point_at <0,0,1>");
				out.println ();
			}
			float f = ((PointLight) light).getAttenuationExponent ();
			if (f > 0)
			{
				out.print ("fade_power ");
				out.print (f);
				out.write (" fade_distance ");
				out.print (((PointLight) light).getAttenuationDistance ());
				out.println ();
			}
		}
		else if (light instanceof DirectionalLight)
		{
			out.print ("parallel point_at <0,0,1>");
			out.println ();
		}
	}


	private static final NodeExport LIGHT_EXPORT = new POVNode ("light_source")
	{
		@Override
		protected void exportImpl (Object object, Leaf leaf, POVExport export)
			throws IOException
		{
			export.hasLights = true;
			export.exportLight ((Light) leaf.getObject (Attributes.LIGHT));
		}


		@Override
		protected void exportFooter (Object object, Leaf node, InnerNode transform,
									 POVExport export) throws IOException
		{
			exportTransform (node, transform, export);
		}
	};


	private static final NodeExport POLYGONIZABLE_EXPORT = new POVNode ("mesh2", "object")
	{
		@Override
		protected boolean shouldDeclareObject (Object object)
		{
			return !((ContextDependent) object).dependsOnContext ()
				&& super.shouldDeclareObject (object);
		}


		@Override
		public void export (SceneTree.Leaf node, InnerNode transform,
							SceneGraphExport sge) throws IOException
		{
			sge.getGraphState ().setObjectContext (node.object, node.asNode);
			ContextDependent c = ((Polygonizable) node.getObject (Attributes.SHAPE))
				.getPolygonizableSource (sge.getGraphState ());
			if (c != null)
			{
				export (c, (Leaf) node, transform, null, (POVExport) sge);
			}
		}


		@Override
		protected void exportImpl (Object object, Leaf leaf, POVExport export)
			throws IOException
		{
			GraphState gs = export.getGraphState ();
			gs.setObjectContext (leaf.object, leaf.asNode);
			((Polygonizable) leaf.getObject
			 (Attributes.SHAPE)).getPolygonization ().polygonize
				((ContextDependent) object, gs, export.polygons,
				 Polygonization.COMPUTE_NORMALS | Polygonization.COMPUTE_UV,
				 export.getMetaData (FLATNESS, 1f));
			export.exportPolygons ();
		}
	};


	private final float[] ftmp = new float[3];

	void print (POVWriter out, String name, float[] a, PolygonArray normals,
				int arrayLength, int dim,
				boolean uvs) throws IOException
	{
		out.print (name);
		out.print (arrayLength / dim);
		out.write (',');
		out.println ();
		out.indent ();
		int k = 0;
		for (int i = 0; i < arrayLength; i += dim)
		{
			if (k > 0)
			{
				out.print (',');
			}
			if ((++k & 15) == 0)
			{
				out.println ();
			}
			if (normals != null)
			{
				normals.getNormal (ftmp, i);
				out.print (ftmp[0], ftmp[1], ftmp[2]);
			}
			else if (uvs)
			{
				out.print (a[i], a[i + 1]);
			}
			else
			{
				out.print (a[i], (dim > 1) ? a[i + 1] : 0, (dim > 2) ? a[i + 2] : 0);
			}
		}
		out.unindent ();
		out.print ('}');
		out.println ();
	}


	static void printTriangle
		(POVWriter out, int[] indices, int c0, int c1, int c2) throws IOException
	{
		out.print ('<');
		out.print (indices[c0]);
		out.write (',');
		out.print (indices[c1]);
		out.write (',');
		out.print (indices[c2]);
		out.write ('>');
	}


	static void printFaces (POVWriter out, String name, PolygonArray polygons)
		throws IOException
	{
		int ec = polygons.edgeCount;
		int n = polygons.polygons.size;
		int[] polys = polygons.polygons.elements;

		out.print (name);
		out.print ((ec - 2) * n / ec);
		out.write (',');
		out.println ();
		out.indent ();
		int k = 0;
		for (int i = 0; i < n; i += ec)
		{
			if (k > 0)
			{
				out.print (',');
			}
			if ((++k & 15) == 0)
			{
				out.println ();
			}
			printTriangle (out, polys, i, i + 1, i + 2);
			if (ec > 3)
			{
				out.print (',');
				printTriangle (out, polys, i, i + 2, i + 3);
			}
		}
		out.unindent ();
		out.print ('}');
		out.println ();
	}


	public boolean export (Object object, String type) throws IOException
	{
		objectType = type;
		return super.export (object);
	}


	@Override
	public boolean export (Object object) throws IOException
	{
		objectType = null;
		return super.export (object);
	}


	public String getObjectType (String type)
	{
		return (objectType != null) ? objectType : type;
	}


	@Override
	public NodeExport getExportFor (Object object, boolean asNode)
	{
		Object s = getGraphState ().getObjectDefault
			(object, asNode, Attributes.SHAPE, null);
		if (s != null)
		{
			NodeExport ex = super.getExportFor (s, asNode);
			if (ex != null)
			{
				return ex;
			}
			if (s instanceof Polygonizable)
			{
				return POLYGONIZABLE_EXPORT;
			}
			if (s instanceof Sky)
			{
				sky = object;
				skyIsNode = asNode;
			}
		}
		Light light = (Light) getGraphState ().getObjectDefault
			(object, asNode, Attributes.LIGHT, null);
		return ((light != null) && (light.getLightType () != Light.SKY))
			? LIGHT_EXPORT : null;
	}

	
	public static void export (Context ctx, String file)
	{
		View3D v = View3D.getDefaultView (ctx);
		if (v == null)
		{
			return;
		}
		de.grogra.imp.IMP.export (v, MIME_TYPE, new File (file));
	}

}
