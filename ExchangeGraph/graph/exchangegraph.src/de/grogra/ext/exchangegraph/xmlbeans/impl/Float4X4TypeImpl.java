/*
 * XML Type:  float4x4_type
 * Namespace: 
 * Java type: de.grogra.ext.exchangegraph.xmlbeans.Float4X4Type
 *
 * Automatically generated - do not modify.
 */
package de.grogra.ext.exchangegraph.xmlbeans.impl;
/**
 * An XML float4x4_type(@).
 *
 * This is a list type whose items are de.grogra.ext.exchangegraph.xmlbeans.FloatType.
 */
public class Float4X4TypeImpl extends org.apache.xmlbeans.impl.values.XmlListImpl implements de.grogra.ext.exchangegraph.xmlbeans.Float4X4Type
{
    
    public Float4X4TypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected Float4X4TypeImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
}
