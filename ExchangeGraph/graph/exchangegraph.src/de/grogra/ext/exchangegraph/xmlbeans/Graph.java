/*
 * XML Type:  Graph
 * Namespace: 
 * Java type: de.grogra.ext.exchangegraph.xmlbeans.Graph
 *
 * Automatically generated - do not modify.
 */
package de.grogra.ext.exchangegraph.xmlbeans;


/**
 * An XML Graph(@).
 *
 * This is a complex type.
 */
public interface Graph extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(Graph.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s39D0E01DA061D3219FD31D6307765808").resolveHandle("graphe065type");
    
    /**
     * Gets a List of "type" elements
     */
    java.util.List<de.grogra.ext.exchangegraph.xmlbeans.Type> getTypeList();
    
    /**
     * Gets array of all "type" elements
     * @deprecated
     */
    de.grogra.ext.exchangegraph.xmlbeans.Type[] getTypeArray();
    
    /**
     * Gets ith "type" element
     */
    de.grogra.ext.exchangegraph.xmlbeans.Type getTypeArray(int i);
    
    /**
     * Returns number of "type" element
     */
    int sizeOfTypeArray();
    
    /**
     * Sets array of all "type" element
     */
    void setTypeArray(de.grogra.ext.exchangegraph.xmlbeans.Type[] typeArray);
    
    /**
     * Sets ith "type" element
     */
    void setTypeArray(int i, de.grogra.ext.exchangegraph.xmlbeans.Type type);
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "type" element
     */
    de.grogra.ext.exchangegraph.xmlbeans.Type insertNewType(int i);
    
    /**
     * Appends and returns a new empty value (as xml) as the last "type" element
     */
    de.grogra.ext.exchangegraph.xmlbeans.Type addNewType();
    
    /**
     * Removes the ith "type" element
     */
    void removeType(int i);
    
    /**
     * Gets a List of "root" elements
     */
    java.util.List<de.grogra.ext.exchangegraph.xmlbeans.Root> getRootList();
    
    /**
     * Gets array of all "root" elements
     * @deprecated
     */
    de.grogra.ext.exchangegraph.xmlbeans.Root[] getRootArray();
    
    /**
     * Gets ith "root" element
     */
    de.grogra.ext.exchangegraph.xmlbeans.Root getRootArray(int i);
    
    /**
     * Returns number of "root" element
     */
    int sizeOfRootArray();
    
    /**
     * Sets array of all "root" element
     */
    void setRootArray(de.grogra.ext.exchangegraph.xmlbeans.Root[] rootArray);
    
    /**
     * Sets ith "root" element
     */
    void setRootArray(int i, de.grogra.ext.exchangegraph.xmlbeans.Root root);
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "root" element
     */
    de.grogra.ext.exchangegraph.xmlbeans.Root insertNewRoot(int i);
    
    /**
     * Appends and returns a new empty value (as xml) as the last "root" element
     */
    de.grogra.ext.exchangegraph.xmlbeans.Root addNewRoot();
    
    /**
     * Removes the ith "root" element
     */
    void removeRoot(int i);
    
    /**
     * Gets a List of "node" elements
     */
    java.util.List<de.grogra.ext.exchangegraph.xmlbeans.Node> getNodeList();
    
    /**
     * Gets array of all "node" elements
     * @deprecated
     */
    de.grogra.ext.exchangegraph.xmlbeans.Node[] getNodeArray();
    
    /**
     * Gets ith "node" element
     */
    de.grogra.ext.exchangegraph.xmlbeans.Node getNodeArray(int i);
    
    /**
     * Returns number of "node" element
     */
    int sizeOfNodeArray();
    
    /**
     * Sets array of all "node" element
     */
    void setNodeArray(de.grogra.ext.exchangegraph.xmlbeans.Node[] nodeArray);
    
    /**
     * Sets ith "node" element
     */
    void setNodeArray(int i, de.grogra.ext.exchangegraph.xmlbeans.Node node);
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "node" element
     */
    de.grogra.ext.exchangegraph.xmlbeans.Node insertNewNode(int i);
    
    /**
     * Appends and returns a new empty value (as xml) as the last "node" element
     */
    de.grogra.ext.exchangegraph.xmlbeans.Node addNewNode();
    
    /**
     * Removes the ith "node" element
     */
    void removeNode(int i);
    
    /**
     * Gets a List of "edge" elements
     */
    java.util.List<de.grogra.ext.exchangegraph.xmlbeans.Edge> getEdgeList();
    
    /**
     * Gets array of all "edge" elements
     * @deprecated
     */
    de.grogra.ext.exchangegraph.xmlbeans.Edge[] getEdgeArray();
    
    /**
     * Gets ith "edge" element
     */
    de.grogra.ext.exchangegraph.xmlbeans.Edge getEdgeArray(int i);
    
    /**
     * Returns number of "edge" element
     */
    int sizeOfEdgeArray();
    
    /**
     * Sets array of all "edge" element
     */
    void setEdgeArray(de.grogra.ext.exchangegraph.xmlbeans.Edge[] edgeArray);
    
    /**
     * Sets ith "edge" element
     */
    void setEdgeArray(int i, de.grogra.ext.exchangegraph.xmlbeans.Edge edge);
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "edge" element
     */
    de.grogra.ext.exchangegraph.xmlbeans.Edge insertNewEdge(int i);
    
    /**
     * Appends and returns a new empty value (as xml) as the last "edge" element
     */
    de.grogra.ext.exchangegraph.xmlbeans.Edge addNewEdge();
    
    /**
     * Removes the ith "edge" element
     */
    void removeEdge(int i);
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static de.grogra.ext.exchangegraph.xmlbeans.Graph newInstance() {
          return (de.grogra.ext.exchangegraph.xmlbeans.Graph) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static de.grogra.ext.exchangegraph.xmlbeans.Graph newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (de.grogra.ext.exchangegraph.xmlbeans.Graph) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static de.grogra.ext.exchangegraph.xmlbeans.Graph parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (de.grogra.ext.exchangegraph.xmlbeans.Graph) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static de.grogra.ext.exchangegraph.xmlbeans.Graph parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (de.grogra.ext.exchangegraph.xmlbeans.Graph) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static de.grogra.ext.exchangegraph.xmlbeans.Graph parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (de.grogra.ext.exchangegraph.xmlbeans.Graph) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static de.grogra.ext.exchangegraph.xmlbeans.Graph parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (de.grogra.ext.exchangegraph.xmlbeans.Graph) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static de.grogra.ext.exchangegraph.xmlbeans.Graph parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (de.grogra.ext.exchangegraph.xmlbeans.Graph) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static de.grogra.ext.exchangegraph.xmlbeans.Graph parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (de.grogra.ext.exchangegraph.xmlbeans.Graph) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static de.grogra.ext.exchangegraph.xmlbeans.Graph parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (de.grogra.ext.exchangegraph.xmlbeans.Graph) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static de.grogra.ext.exchangegraph.xmlbeans.Graph parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (de.grogra.ext.exchangegraph.xmlbeans.Graph) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static de.grogra.ext.exchangegraph.xmlbeans.Graph parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (de.grogra.ext.exchangegraph.xmlbeans.Graph) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static de.grogra.ext.exchangegraph.xmlbeans.Graph parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (de.grogra.ext.exchangegraph.xmlbeans.Graph) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static de.grogra.ext.exchangegraph.xmlbeans.Graph parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (de.grogra.ext.exchangegraph.xmlbeans.Graph) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static de.grogra.ext.exchangegraph.xmlbeans.Graph parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (de.grogra.ext.exchangegraph.xmlbeans.Graph) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static de.grogra.ext.exchangegraph.xmlbeans.Graph parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (de.grogra.ext.exchangegraph.xmlbeans.Graph) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static de.grogra.ext.exchangegraph.xmlbeans.Graph parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (de.grogra.ext.exchangegraph.xmlbeans.Graph) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static de.grogra.ext.exchangegraph.xmlbeans.Graph parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (de.grogra.ext.exchangegraph.xmlbeans.Graph) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static de.grogra.ext.exchangegraph.xmlbeans.Graph parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (de.grogra.ext.exchangegraph.xmlbeans.Graph) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
