/*
 * XML Type:  list_of_int
 * Namespace: 
 * Java type: de.grogra.ext.exchangegraph.xmlbeans.ListOfInt
 *
 * Automatically generated - do not modify.
 */
package de.grogra.ext.exchangegraph.xmlbeans.impl;
/**
 * An XML list_of_int(@).
 *
 * This is a list type whose items are de.grogra.ext.exchangegraph.xmlbeans.IntType.
 */
public class ListOfIntImpl extends org.apache.xmlbeans.impl.values.XmlListImpl implements de.grogra.ext.exchangegraph.xmlbeans.ListOfInt
{
    
    public ListOfIntImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected ListOfIntImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
}
