/*
 * XML Type:  list_of_double
 * Namespace: 
 * Java type: de.grogra.ext.exchangegraph.xmlbeans.ListOfDouble
 *
 * Automatically generated - do not modify.
 */
package de.grogra.ext.exchangegraph.xmlbeans.impl;
/**
 * An XML list_of_double(@).
 *
 * This is a list type whose items are de.grogra.ext.exchangegraph.xmlbeans.FloatType.
 */
public class ListOfDoubleImpl extends org.apache.xmlbeans.impl.values.XmlListImpl implements de.grogra.ext.exchangegraph.xmlbeans.ListOfDouble
{
    
    public ListOfDoubleImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected ListOfDoubleImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
}
