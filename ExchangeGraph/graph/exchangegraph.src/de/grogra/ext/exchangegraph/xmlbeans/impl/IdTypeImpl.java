/*
 * XML Type:  id_type
 * Namespace: 
 * Java type: de.grogra.ext.exchangegraph.xmlbeans.IdType
 *
 * Automatically generated - do not modify.
 */
package de.grogra.ext.exchangegraph.xmlbeans.impl;
/**
 * An XML id_type(@).
 *
 * This is an atomic type that is a restriction of de.grogra.ext.exchangegraph.xmlbeans.IdType.
 */
public class IdTypeImpl extends org.apache.xmlbeans.impl.values.JavaLongHolderEx implements de.grogra.ext.exchangegraph.xmlbeans.IdType
{
    
    public IdTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected IdTypeImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
}
