/*
 * XML Type:  list_of_float_type
 * Namespace: 
 * Java type: de.grogra.ext.exchangegraph.xmlbeans.ListOfFloatType
 *
 * Automatically generated - do not modify.
 */
package de.grogra.ext.exchangegraph.xmlbeans.impl;
/**
 * An XML list_of_float_type(@).
 *
 * This is a list type whose items are de.grogra.ext.exchangegraph.xmlbeans.FloatType.
 */
public class ListOfFloatTypeImpl extends org.apache.xmlbeans.impl.values.XmlListImpl implements de.grogra.ext.exchangegraph.xmlbeans.ListOfFloatType
{
    
    public ListOfFloatTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected ListOfFloatTypeImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
}
