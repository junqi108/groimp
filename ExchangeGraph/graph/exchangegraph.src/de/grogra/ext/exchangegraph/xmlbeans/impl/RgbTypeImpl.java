/*
 * XML Type:  rgb_type
 * Namespace: 
 * Java type: de.grogra.ext.exchangegraph.xmlbeans.RgbType
 *
 * Automatically generated - do not modify.
 */
package de.grogra.ext.exchangegraph.xmlbeans.impl;
/**
 * An XML rgb_type(@).
 *
 * This is a list type whose items are de.grogra.ext.exchangegraph.xmlbeans.FloatType.
 */
public class RgbTypeImpl extends org.apache.xmlbeans.impl.values.XmlListImpl implements de.grogra.ext.exchangegraph.xmlbeans.RgbType
{
    
    public RgbTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected RgbTypeImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
}
