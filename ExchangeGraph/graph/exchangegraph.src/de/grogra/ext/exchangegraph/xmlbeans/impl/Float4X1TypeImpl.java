/*
 * XML Type:  float4x1_type
 * Namespace: 
 * Java type: de.grogra.ext.exchangegraph.xmlbeans.Float4X1Type
 *
 * Automatically generated - do not modify.
 */
package de.grogra.ext.exchangegraph.xmlbeans.impl;
/**
 * An XML float4x1_type(@).
 *
 * This is a list type whose items are de.grogra.ext.exchangegraph.xmlbeans.FloatType.
 */
public class Float4X1TypeImpl extends org.apache.xmlbeans.impl.values.XmlListImpl implements de.grogra.ext.exchangegraph.xmlbeans.Float4X1Type
{
    
    public Float4X1TypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected Float4X1TypeImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
}
