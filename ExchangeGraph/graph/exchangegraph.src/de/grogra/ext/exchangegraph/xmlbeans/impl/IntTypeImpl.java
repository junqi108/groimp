/*
 * XML Type:  int_type
 * Namespace: 
 * Java type: de.grogra.ext.exchangegraph.xmlbeans.IntType
 *
 * Automatically generated - do not modify.
 */
package de.grogra.ext.exchangegraph.xmlbeans.impl;
/**
 * An XML int_type(@).
 *
 * This is an atomic type that is a restriction of de.grogra.ext.exchangegraph.xmlbeans.IntType.
 */
public class IntTypeImpl extends org.apache.xmlbeans.impl.values.JavaIntHolderEx implements de.grogra.ext.exchangegraph.xmlbeans.IntType
{
    
    public IntTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected IntTypeImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
}
