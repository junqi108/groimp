<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://grogra.de/registry" graph="graph.xml">
<import plugin="de.grogra.pf" version="0.9.3.1"/>
<registry>
<ref name="project">
<ref name="objects">
<ref name="files">
<de.grogra.pf.ui.registry.SourceFile mimeType="text/html" name="pfs:javadoc/overview-summary.html"/>
</ref>
</ref>
</ref>
<ref name="workbench">
<ref name="state">
<de.grogra.pf.ui.registry.Layout name="layout">
<de.grogra.pf.ui.registry.MainWindow>
<de.grogra.pf.ui.registry.Split orientation="0">
<de.grogra.pf.ui.registry.PanelFactory source="/ui/panels/viewer">
<de.grogra.pf.registry.Option name="mimeType" type="java.lang.String" value="text/html"/>
<de.grogra.pf.registry.Option name="panelId" type="java.lang.String" value="/ui/panels/viewer?pfs:javadoc/overview-summary.html"/>
<de.grogra.pf.registry.Option name="panelTitle" type="java.lang.String" value="GroIMP API"/>
<de.grogra.pf.registry.Option name="systemId" type="java.lang.String" value="pfs:javadoc/overview-summary.html"/>
</de.grogra.pf.ui.registry.PanelFactory>
<de.grogra.pf.registry.Link source="/ui/panels/statusbar"/>
</de.grogra.pf.ui.registry.Split>
</de.grogra.pf.ui.registry.MainWindow>
</de.grogra.pf.ui.registry.Layout>
</ref>
</ref>
</registry>
</project>
